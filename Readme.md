# Highlander Watcher Database
![Highlander Watcher Database](./screenshot.jpg) 

## Description
This program was designed to allow players of the Highlander Collectible Card Game to track their collection, build and manage decks, and play the game online.  It comes with a (Microsoft Access) database of every known card that's been produced by various companies over the years.  Players may associate scans of cards with the individual records in the database, which will then result in those scans being viewable throughout the program.  It is capable of connecting directly with another player over the internet in real time, to play a game as well as tracking a game in progress for "off line" game play (usually via email).

More screen shots of the program are available on the [official web page](http://eric2001.brinkster.net/screenshots.asp).

This project was started in August of 2002.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
Please see the documentation page [here](https://gitlab.com/eric2001/watcherdatabase5/-/blob/master/Documentation.md) for additional setup and usage instructions.

## History
**Version 5.7.0:**
> - This version includes support for the newest expansion sets, along with other minor changes. 
> - Released on 22 October 2007.
>
> Download: [Version 1.0.0 Setup](/uploads/3d9ef64b80ddfa039b1ea49fc415250f/WatcherDatabaseSetup570.zip) | [Version 1.0.0 Source](/uploads/8f3fde0589783d11109a369701c47797/WatcherDatabaseSource570.zip)

**Version 5.6.0:**
> - This version includes support for Second Edition sets and the MLE John Garrick Collection.
> - Released on 14 March 2007.

**Version 5.5.1:**
> - This version fixes a bug in the card text search for the deck builder and disables card restrictions when using Xavier's persona or quickening in order to allow additional plot cards.
> - Released on 27 July 2006.

**Version 5.5.0:**
> - Bug fixes and other minor improvements to the Print MLE Cards screen.
> - Bug fix for printing decks that take up more then one page.
> - Improved Print Deck Statistics code.
> - Updates to the deck statistics code for newer card types (such as traps).
> - The "Full Installer" has been updated to the current 5.5.0 version with all current database patches.
> - Released on 25 February 2006.

**Version 5.4.0:**
> - Support for the new MLE 6 Expansion (HHM)
> - Support for Black Raven Volume 2 (Raven2)
> - Support for Black Raven Volume 3 (Raven3)
> - Support for Black Raven Volume 4 (Raven4)
> - Support for SAEC Millennium Preview cards (MILPV)
> - New Requested Feature: A Print Deck option has been added to the File menu in the deck editor. It will print out a deck list containing the card name, quantity, type, and set. Cards are printed in whatever order their currently being displayed in, so apply any sorts before printing. Additionally, the file name of the saved deck (without the .hld extension) will be printed at the top of the first page if there is a file name.
> - Released on 09 February 2006.

**Version 5.3.1:**
> - This version fixes a bug in the Play Game (Email / Chat Room) screen that causes the screen to become empty after quiting the program by using File -> Exit.
> - Released on 08 September 2005.

**Version 5.3.0:**
> - Added: Support for the MLE Chronicles of Connor MacLeod (CCM) Expansion.
> - View Collection -- When associating a card image with the database, you may now drag and drop the .jpg file over the card image area on the view collection screen. The program will then automatically insert the file name and path into the file name field.
> - Play Game (Email / Chat Room) -- You may now left click on the frames (black borders) around various sections of this screen and drag them to other areas. Note: When using this feature, the mouse needs to be over an empty area of the window when you drop the frame in its new location.
> - Play Game (Email / Chat Room) -- Resize frames - Hold down the shift key, then left click in the lower right hand side of a frame to resize it.
> - Play Game (Email / Chat Room) -- Positions and sizes of frames will be automatically saved when the window is closed, and automatically restored when the window is next opened. If you wish to reset the screen back to its default setting, delete the file playemailsettings.txt in the same folder as the Highlander.exe file.
> - Released on 29 August 2005.

**Version 5.2.0:**
> - Added: Black Raven, Volume 1 Support (Raven1).
> - Print Cards Screen:  I'm told the method I used to fit 9 cards to a page caused issues with certain types of printers (either the bottom of the last row was cut off, or the printer would select a different type of paper then what was desired). So, to fix this problem, only 8 cards will be printed per page, instead of 9.
> - Print Cards Screen:  Placed a counter at the top of the screen to indicate how many cards and how many pages will be printed.
> - View Collection Screen: Grouped similar buttons together.
> - View Collection Screen: Made the Card Name field two lines in order to better display really long card titles.
> - Deck Editor: Made the Card Name field two lines in order to better display really long card titles.
> - Released on 15 June 2005.

**Version 5.1.0:**
> - This upgrade includes support for the WWE set.
> - Released on 11 May 2005.

**Version 5.0.0:**
> - Support has been added for the MLE Carl Robinson collection (CRC).
> - Due to a combination of suggestions and some sample code submitted by James McMurray and me remembering that "vb has a printer object" the Print MLE Cards screen has been redesigned. Users may now print up to 9 cards per page, and up to 1000 cards may be printed at once. Also, a checkbox has been added to this screen to allow only cards with images associated with them to be shown.
> - The format of the Trade Lists (.hlt) has been changed. This means that any .hlt file generated with version 5 of the program will not work on an older version and any .hlt file generated with an older version will not work with version 5. More changes will come later to use these changes.
> - The deck editor has been altered to not count the "Gothic Sword Master Swordsman" card toward the master card limit if the immortal is Brian Cullen.
> - Sealed Deck support has been added to the Deck Editor. To generate a sealed deck to build a regular deck from, select Deck Editor -> Options -> Switch to Sealed Deck Generator. To use a sealed deck to build a regular deck, select File -> Load Sealed Deck from within the Deck Editor.
> - The web site listed on the About screen has been changed to watcherdatabase.tk
> - Released on 10 October 2004.

**Version 4.5.0:**
> - Fixed bug that causes program to crash when the file name field just contains '*'. It is now safe to enter in file names for the front and back of cards (put *FileNameOfFront.jpg|FileNameOfBack.jpg to accomplish this)
> - Fixed a few issues related to the Brian Cullen persona in the deck editor
> - Code cleanup - Made sure variables were being declaired before being used, gave global variables more meaningful names then 'i', etc.
> - Released on 15 June 2004.

**Version 4.4.2:**
> - This version fixes a bug that caused the program to crash when pressing cancel for "Show Top X Cards" on the email screen and added support for Brian Cullen's Persona power into the deck editor.
> - Released on 20 January 2004.

**Version 4.4.1:**
> - Fixed a bug that caused BIA and MMQB cards to not register as MLE cards under the deck stats screen.
> - Released on 15 January 2004.

**Version 4.4.0:**
> - View Collection: Added Support for Brothers In Arms (BIA) Set.
> - View Collection: When using "Select File" to find a card's image, only .jpg files will be shown by default.
> - View Collection: When using "Select File" to find a card's image cancel Button should always work.
> - View Collection: When Selecting File->Save Wants list, haves list, or collection list the cancel button should always work.
> - View Collection: When Selecting File->Save Wants list, haves list, or collection list only .csv files will be shown by default.
> - View Collection: When selecting File->Create Trades list cancel button should always work.
> - View Collection: When selecting File->Create Trades list only .hlt files will be shown by default.
> - Print MLE Cards: Added Support for Brothers In Arms (BIA) Set.
> - Print MLE Cards: Browse buttons should now default to .jpg files.
> - Print MLE Cards: Cancel buttons for Browse should now always work.
> - Patch Database: Patch button will now default to only showing .hdb files.
> - Patch Database: Cancel button for selecting a .hdb file should now always work.
> - Deck Editor: Added Support for Brothers In Arms (BIA) Set.
> - Deck Editor: Open and save will now default to only showing .hld files.
> - Deck Editor: Open and save cancel buttons should now always work.
> - Deck Editor: Export Deck will now default to only showing .csv files.
> - Deck Editor: Export Deck cancel button should now always work.
> - Deck Editor: Save Stats to Text file will default to only showing .txt files.
> - Deck Editor: Cancel button for Save Stats to Text File should now always work.
> - Play Game (Email): Added Support for Brothers In Arms (BIA) Set.
> - Play Game (Email): New Game option defaults to only showing .hld files.
> - Play Game (Email): Cancel button for selecting a deck under new game should now always work.
> - Play Game (Email): Save Game and Save Game as should now default to only showing .hlg files.
> - Play Game (Email): Cancel button for entering in a file name under save game/save game as should now always work.
> - Play Game (Email): Open Saved Game should now default to only showing .hlg files.
> - Play Game (Email): Cancel button for selecting a file for open saved game should now always work.
> - Play Game (Email): Window for selecting a file name for a turn transcript will now default to only showing .txt files.
> - Play Game (Email): Cancel button for saving a turn transcript should now always work.
> - Play Game (Online): Enabled ability to select multiple cards on all lists (use shift-click, control-click, click on one card and drag down, etc.).
> - Play Game (Online): When selecting a deck for a new game, only .hld files will be shown by default.
> - Play Game (Online): Cancel button for selecting a deck under new game should now always work.
> - Play Game (Online): When selecting a deck for open saved game, only .hld files will be shown by default.
> - Play Game (Online): Cancel button for selecting a deck under open saved game should now always work.
> - Released on 12 January 2004.

**Version 4.3.0:**
> - Deck Editor -- Added: Progress indicator when opening a deck
> - Deck Editor -- Added: Progress indicator for exporting a deck
> - Deck Editor -- Added: Dialog Box to inform you that the deck was exported successfully
> - Deck Editor -- Added: Text Search Feature
> - Deck Editor -- Improvement: Decks should load a little faster now :)
> - Deck Editor -- Improvement: Deck Stats screen should load faster now
> - Deck Editor -- Improvement: Exporting a deck should run faster now
> - Deck Editor -- Bug Fix: Program no longer appears to stop responding when loading a deck.
> - Deck Editor -- Bug Fix: Program no longer appears to stop responding when exporting a deck.
> - Deck Editor -- Bug Fix: Program no longer stops responding when loading the deck stats screen.
> - Deck Editor -- Bug Fix: Fixed problem where program would not close file after exporting a deck to .csv format.
> - Patch Database Screen -- Bug Fix: Program no longer appears to stop responding when applying a database patch.
> - View Collection -- Added: Processing message when generating haves, wants, collection list or .hlt file
> - View Collection -- Improvement: Generating a wants list should take less time
> - View Collection -- Improvement: Generating a Haves list should take less time
> - View Collection -- Improvement: Generating a collection list should take less time
> - View Collection -- Improvement: Generating a .hlt file should take less time
> - View Collection -- Bug Fix: Program no longer appears to stop responding when generating a Wants list
> - View Collection -- Bug Fix: Program no longer appears to stop responding when generating a haves list
> - View Collection -- Bug Fix: Program no longer appears to stop responding when generating a collection list
> - View Collection -- Bug Fix: Program no longer appears to stop responding when generating a .hlt file
> - Play Game Email -- Added: Progress indicator when loading a deck or game to show that the program has not crashed
> - Play Game Email -- Added: Discard card from hand to bottom of deck option (supports multiple card selection)
> - Play Game Email -- Added: Program now allows for multiple selection of User's Pre-Game cards
> - Play Game Email -- Added: Program now allows for multiple selection of Opponents Pre-Game Cards
> - Play Game Email -- Added: Move to top of deck now supports selecting multiple cards (every list and button on this screen should now work with selecting multiple cards)
> - Play Game Email -- Improvement: Decreased time the program takes to load a deck
> - Play Game Email -- Improvement: Decreased time the program takes to load a game
> - Play Game Email -- Bug Fix: Program no longer appears to stop responding when loading a deck.
> - Play Game Email -- Bug Fix: Program no longer appears to stop responding when loading a saved game.
> - Play Game Online -- Added: Double clicking on a card in the deck moves it to players hand
> - Play Game Online -- Added: double clicking on hidden attack card list reveals the attack
> - Play Game Online -- Added: double click on card in hand plays the card
> - Play Game Online -- Added: double click on in play card to discard it
> - Play Game Online -- Added: double click on card in exertion to play card
> - Play Game Online -- Added: double click on card in dojo to move to hand
> - Play Game Online -- Added: Progress indicator when loading a deck or game to show that the program has not crashed
> - Play Game Online -- Added: Discard card from hand to bottom of deck option
> - Play Game Online -- Improvement: Decreased time the program takes to load a deck
> - Play Game Online -- Improvement: Rearanged several of the items on the screen - Opponents Pre-Game cards are now always visible, Your pre-game cards can be displayed instead of your discard pile, other items in different locations
> - Play Game Online -- Bug Fix: Clicking on card in hidden attack list displays that card
> - Play Game Online -- Bug Fix: Selecting Cancel when selecting a deck for a new game no longer causes connection screen to show up
> - Play Game Online -- Bug Fix: Program no longer appears to stop responding when loading a deck or saved game.
> - Play Game Online -- Bug Fix: Fixed problem where program crashed trying to log a communication error when starting a game
> - Play Game Online -- Bug Fix: Fixed problem where program forgets to close the log file.
> - Play Game Online -- Bug Fix: Communication error's are now displayed in an alert box so the user can actually see them.
> - Play Game Online -- Bug Fix: When one user selects close connection, the connection will be closed on both sides
> - Play Game Online -- Bug Fix: The move card from discard to Top of Endurance now works
> - Play Game Online -- Bug Fix: Program now tells opponent what cards your drawing for an exertion as you draw them
> - Play Game Online -- Bug Fix: When you click on a button without first selecting a card, nothing will happen
> - Other minor improvements and changes throughout the program.
> - Released on 1 January 2004.

**Version 4.2.0:**
> - Main Screen: Changed words "Create/Edit Deck" to "Deck Editor"
> - Deck Editor: Changed words "Create/Edit Deck" to "Deck Editor"
> - Deck Editor: Added support to double click on Persona and other pre-game cards in "All Cards" list to add them to the Pre-Game list
> - Deck Editor: Added support to double click on other cards to add them to the deck list
> - Deck Editor: Added Sort Deck by both type and name option
> - Deck Editor: added keyboard shortcuts for some menu commands
> - Play Game (Email/Chat Room): Added keyboard shortcuts for some of the menu items
> - Play Game (Email/Chat Room): Fixed bug that caused program to crash when pressing buttons to move cards from all cards list when no cards were listed in that list
> - Play Game (Online): Added and Changed some keyboard commands for the menu items
> - Play Game (Online): Fixed a bug that sometimes caused a message saying that the cards.mdb file could not be found to be displayed (even though it did find the file)
> - Released on 10 November 2003.

**Version 4.1.1:**
> - Deck Editor -- Bug Fix: Master cards that end in the word master (for example, Generic Master) are now recognized as being master cards.
> - Deck Editor -- Bug Fix: Master cards that were not placed into the deck due to the restriction limit will no longer count toward your master card limit (as they were not placed into the deck...)
> - Released on 16 October 2003.

**Version 4.1.0:**
> - Deck Editor -- Ability to select multiple cards in deck and pre-game card lists (ctrl-left and ctrl-left click supported)
> - Deck Editor -- Ability to double click on a card in the deck or pre-game list to add another copy of it.
> - Patch Database -- Menu commands with shortcut keys added in addition to buttons
> - Play game by email -- Ability to use shift or ctrl to select multiple cards. (for example, to select every card in your hand, left click on the first card, then press shift-end)
> - Play game by email -- Multi-select not supported with "Move to top of deck" button, pre-game cards
> - Play game by email -- Double Click a card under "All Cards" to add one copy to Opponent's In Play list
> - Play game by email -- Double Click In Play cards (Yours and Opponents) to Discard them
> - Play game by email -- Double Click card in Deck or Top X Cards in Deck list to move to hand
> - Play game by email -- Double Click card in hand to Play Card
> - Play game by email -- Double Click cards under "Removed From Game", to put the card into play
> - Play game by email -- Double Click card in Exertion to Play Card
> - Play game by email -- Double Click card under Dojo to move to Hand
> - Play game by email -- Bug Fix: Moving cards from Dojo to hand is now allowed.
> - Play game by email -- Bug Fix: Moving cards from hand to Dojo now causes the number of cards in dojo to be updated.
> - Play Game Online -- Bug Fix: Moving cards from hand to Dojo now causes the number of cards in dojo to be updated.
> - Released on 03 September 2003.

**Version 4.0.1:**
> - Bug Fix: Pre-Game Cards will no longer count towards your master card limit.
> - Released on 17 June 2003.

**Version 4.0.0:**
> - General -- Added Support for card backs
> - General -- Added Support for Michael Moore / Quentin Barnes cards (MMQB Set, database patch required)
> - General -- Added support for Misprints and oddities section (database patch required)
> - General -- Other minor changes and bug fixes
> - Main Screen -- Changed Button Arrangement
> - Main Screen -- Changed Title
> - Main Screen -- Added View Trade List Button
> - Main Screen -- Added Print MLE Cards Button
> - View Collection -- Changed Design of View Collection area, replaced a lot of buttons with menu commands.
> - View Collection -- Added an option to generate a trade list (.hlt files).
> - Deck Editor -- Search by card type option
> - Deck Editor -- Support for Pre-Game Darius (Increase Restriction Number)
> - Deck Editor -- Added Support for Master's Prize
> - Deck Editor -- Cards from the Misprints and Oddities section will generate a warning message
> - Play Game By Email -- Added Defense/Attack Phase options
> - Play Game By Email -- Changed format of logs
> - Play Game By Email -- Added an option to Look at the top X Cards in the deck
> - Released on 10 June 2003.
