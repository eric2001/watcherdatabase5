VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmViewCollection 
   Caption         =   "View Collection"
   ClientHeight    =   5940
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   12060
   LinkTopic       =   "Form2"
   ScaleHeight     =   5940
   ScaleWidth      =   12060
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSpoilerGuide 
      Caption         =   "Command1"
      Height          =   375
      Left            =   8400
      TabIndex        =   33
      Top             =   5160
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Frame frameFrontBack 
      Height          =   855
      Left            =   9000
      TabIndex        =   32
      Top             =   4920
      Width           =   2415
      Begin VB.CommandButton cmdSwitchImage 
         Caption         =   "Show Back"
         Height          =   495
         Left            =   1320
         TabIndex        =   17
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdShowFront 
         Caption         =   "Show Front"
         Height          =   495
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame frameSelectSet 
      Height          =   855
      Left            =   4680
      TabIndex        =   31
      Top             =   4920
      Width           =   3615
      Begin VB.CommandButton cmdShowSet 
         Caption         =   "Select Set"
         Height          =   495
         Left            =   2160
         TabIndex        =   15
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox cmboSet 
         Height          =   315
         ItemData        =   "frmViewCollection.frx":0000
         Left            =   120
         List            =   "frmViewCollection.frx":0070
         TabIndex        =   14
         Text            =   "Set Names"
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame frameSort 
      Height          =   855
      Left            =   2400
      TabIndex        =   30
      Top             =   4920
      Width           =   2055
      Begin VB.CommandButton cmdSortName 
         Caption         =   "Sort By Name"
         Height          =   495
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdSortSet 
         Caption         =   "Sort By Set"
         Height          =   495
         Left            =   1080
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame frameNav 
      Height          =   855
      Left            =   120
      TabIndex        =   29
      Top             =   4920
      Width           =   2175
      Begin VB.CommandButton cmdMoveBack1Record 
         Caption         =   "Previous Card"
         Height          =   495
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdMoveForward1Record 
         Caption         =   "Next Card"
         Height          =   495
         Left            =   1200
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame frmLoading 
      Caption         =   "Processing..."
      Height          =   735
      Left            =   240
      TabIndex        =   28
      Top             =   2040
      Visible         =   0   'False
      Width           =   3615
      Begin VB.Shape StatusBox 
         DrawMode        =   1  'Blackness
         FillStyle       =   0  'Solid
         Height          =   375
         Left            =   120
         Shape           =   3  'Circle
         Top             =   240
         Width           =   615
      End
   End
   Begin MSDataGridLib.DataGrid dataCards 
      Bindings        =   "frmViewCollection.frx":0146
      Height          =   4695
      Left            =   120
      TabIndex        =   25
      Top             =   120
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   8281
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   0   'False
      DefColWidth     =   273
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Card Details"
      Height          =   4815
      Left            =   4680
      TabIndex        =   0
      Top             =   0
      Width           =   3615
      Begin VB.TextBox txtID 
         DataField       =   "ID"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   120
         TabIndex        =   27
         Text            =   "Text2"
         Top             =   2760
         Visible         =   0   'False
         Width           =   615
      End
      Begin MSComDlg.CommonDialog CommonDialog3 
         Left            =   360
         Top             =   3000
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
         DefaultExt      =   "hlt"
         Filter          =   "Trade Lists (*.hlt)|*.hlt|All Files (*.*)|*.*"
      End
      Begin MSComDlg.CommonDialog CommonDialog2 
         Left            =   240
         Top             =   3360
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
         DefaultExt      =   "csv"
         Filter          =   "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*"
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   240
         Top             =   3840
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
         Filter          =   "Jpeg Files (*.jpg)|*.jpg|All Files (*.*)|*.*"
      End
      Begin VB.CommandButton cmdSelectFile 
         Caption         =   "Select File"
         Height          =   255
         Left            =   2520
         TabIndex        =   9
         Top             =   2040
         Width           =   975
      End
      Begin VB.TextBox txtCardText 
         DataField       =   "Text"
         DataSource      =   "datCardNames"
         Height          =   2325
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   2400
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         DataField       =   "Other"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   5
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox txtType 
         DataField       =   "Type"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   6
         Top             =   1680
         Width           =   2415
      End
      Begin VB.TextBox txtNum 
         DataField       =   "Number"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   3
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox txtCardName 
         DataField       =   "Name"
         DataSource      =   "datCardNames"
         Height          =   525
         Left            =   1080
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   360
         Width           =   2415
      End
      Begin VB.TextBox txtSet 
         DataField       =   "Set"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtFileName 
         DataField       =   "File Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   7
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox txtRarity 
         DataField       =   "Rarity"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   4
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblFileName 
         AutoSize        =   -1  'True
         Caption         =   "File Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   26
         Top             =   2040
         Width           =   750
      End
      Begin VB.Label lblCardText 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   2400
         Width           =   735
      End
      Begin VB.Label lblOther 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   2400
         TabIndex        =   23
         Top             =   1320
         Width           =   435
      End
      Begin VB.Label lblType 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   1680
         Width           =   405
      End
      Begin VB.Label lblNumber 
         AutoSize        =   -1  'True
         Caption         =   "Quantity:"
         Height          =   195
         Left            =   2160
         TabIndex        =   21
         Top             =   960
         Width           =   630
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   840
      End
      Begin VB.Label lblSet 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   285
      End
      Begin VB.Label lblRarity 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   1320
         Width           =   450
      End
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   8400
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "ViewCollectionADODC"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Image HLCard 
      Height          =   4695
      Left            =   8520
      OLEDropMode     =   1  'Manual
      Stretch         =   -1  'True
      Top             =   120
      Width           =   3375
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuSaveWants 
         Caption         =   "Save Wants List"
      End
      Begin VB.Menu menuSaveHaves 
         Caption         =   "Save Haves List"
      End
      Begin VB.Menu menuSaveCollection 
         Caption         =   "Save Collection List"
      End
      Begin VB.Menu menuSaveTradeList 
         Caption         =   "Create Trade List (.hlt)"
         Shortcut        =   ^T
      End
      Begin VB.Menu menuFileBreak 
         Caption         =   "-"
      End
      Begin VB.Menu menuExit 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu menuDisplay 
      Caption         =   "Display"
      Begin VB.Menu menuShowHaves 
         Caption         =   "Show Haves"
         Shortcut        =   ^H
      End
      Begin VB.Menu menuShowCollection 
         Caption         =   "Show Collection"
         Shortcut        =   ^C
      End
      Begin VB.Menu menuShowWants 
         Caption         =   "Show Wants"
         Shortcut        =   ^W
      End
      Begin VB.Menu menuShowHavesAndWants 
         Caption         =   "Show Haves and Wants"
         Shortcut        =   ^B
      End
      Begin VB.Menu menuShowAll 
         Caption         =   "Show All"
         Shortcut        =   ^A
      End
      Begin VB.Menu menuFind 
         Caption         =   "Find..."
         Shortcut        =   ^F
      End
   End
End
Attribute VB_Name = "frmViewCollection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Created By:           Eric Cavaliere
' Program Name:         Watcher Database
' Project File Name:    Highlander.vbp
' Project Started on:   August 05, 2002
' Name of this File:    frmViewCollection.frm
' This file started on: August 05, 2002
' Last Updated on:      22 October 2007

'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Sub cmdMoveBack1Record_Click()
'  If user presses "Previous Card" button
  If datCardNames.Recordset.BOF = False Then
    If datCardNames.Recordset.EOF = False Then
      datCardNames.Recordset.Update  ' Don't update on a blank field
    End If
    datCardNames.Recordset.MovePrevious  ' If not at the beginning of list, move back 1 record
  End If
End Sub

Private Sub cmdMoveForward1Record_Click()
'  If user presses "Next Card" button
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update  ' Don't update on a blank field
    End If
    datCardNames.Recordset.MoveNext ' if not at end of list move forward 1
  End If
End Sub

Private Sub cmdSelectFile_Click()
  On Error Resume Next
  CommonDialog1.ShowOpen
  If Err.number = 32755 Then Exit Sub  ' if cancel pressed then exit
  txtFileName.text = CommonDialog1.FileName
  datCardNames.Recordset.Update
End Sub

Private Sub cmdShowFront_Click()
  Call txtFileName_Change
End Sub

Private Sub cmdShowSet_Click()
  If (cmboSet.List(cmboSet.ListIndex) = "ALL") Then
    datCardNames.Recordset.Filter = ""
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "'"
  End If
End Sub

Private Sub cmdSortName_Click()
'  If user presses "Sort By Name"
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub cmdSortSet_Click()
'  If user presses "Sort By Set"
  datCardNames.Recordset.Sort = "Set, Name"
End Sub

Private Sub cmdSpoilerGuide_Click()
  Open App.Path + "\" + txtSet.text + ".html" For Output As #1
    Print #1, "<html><body><table CELLSPACING=3 CELLPADDING=3>"
      datCardNames.Recordset.MoveFirst
      While datCardNames.Recordset.EOF = False
        Dim tempFileName
        tempFileName = getImageLocation(txtFileName.text, 1)
        If tempFileName = "" Then
          tempFileName = "empty.jpg"
        ElseIf Mid$(tempFileName, 2, 1) <> ":" Then
          tempFileName = txtSet.text + "/" + tempFileName
        End If
        Print #1, "<tr><td valign=top><img src=""" + tempFileName + """ width=100></td><td valign=top>"
        Print #1, "<table CELLPADDING=3><tr><td><b>Title:</b></td><td>"
        Print #1, txtCardName.text
        Print #1, "</td></tr><tr><td><b>Rarity:</b></td><td>"
        Print #1, txtRarity.text
        Print #1, "</td></tr><tr><td><b>Type:</b></td><td>"
        Print #1, txtType.text
        Print #1, "</td></tr><tr><td valign=top><b>Text:</b></td><td>"
        Print #1, txtCardText.text
        Print #1, "</td></tr></table></td></tr>"
        
        datCardNames.Recordset.MoveNext
      Wend
    
    Print #1, "</table></body></html>"
  Close #1
End Sub

Private Sub cmdSwitchImage_Click()
'  If user presses the "Show card back" button
  On Error Resume Next ' in case there's a problem loading an image, skip to the next line
  Dim FileName As String
  HLCard.Stretch = True
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg") ' load the card back, in case of an error
                                                        '   loading a different image, the program
                                                        '   should continue displaying this
  FileName = getImageLocation(txtFileName.text, 2) ' get the image back, if there is one
  If FileName <> "" Then 'If a file name is found...
    If Mid$(FileName, 2, 1) = ":" Then  ' If second character in file name is ':' then assume full path
      HLCard.Picture = LoadPicture(FileName)
    Else ' assume default path...
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub Form_Load()
  On Error Resume Next
  
  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh
  
  HLCard.Stretch = True
  If txtFileName.text = "" Then
    HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  Else
    If Mid$(txtFileName.text, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(txtFileName.text)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "/" + txtFileName.text)
    End If
  End If
  
  dataCards.Columns("ID").Visible = False
  dataCards.Columns("Set").Visible = False
  dataCards.Columns("Number").Visible = False
  dataCards.Columns("File Name").Visible = False
  dataCards.Columns("Other").Visible = False
  dataCards.Columns("Text").Visible = False
  dataCards.Columns("Type").Visible = False
  dataCards.Columns("Rarity").Visible = False
  
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update
    End If
  End If
End Sub

Private Sub HLCard_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    txtFileName.text = Data.Files(1)
    Call txtFileName_Change
End Sub

Private Sub menuExit_Click()
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update
    End If
  End If
  frmViewCollection.Visible = False
  Unload frmViewCollection
End Sub

Private Sub menuFind_Click()
  On Error Resume Next
  Dim searchString As String
  Dim i As Integer
  
  datCardNames.Recordset.Filter = ""
  
  searchString = InputBox("Enter All or part of the title to search for:", "Find Card(s)")
  
  If searchString <> "" Then
    For i = 1 To Len(searchString)
      If (Mid$(searchString, i, 1) = "'") And (Mid$(searchString, i + 1, 1) <> "'") Then
        searchString = Left$(searchString, i) + "'" + Right$(searchString, Len(searchString) - i)
        i = i + 1
      End If
    Next i
    If (cmboSet.ListIndex < 1) Then
      datCardNames.Recordset.Filter = "Name LIKE '%" + searchString + "%'"
    Else
      datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Name LIKE '%" + searchString + "%'"
    End If
  End If
End Sub

Private Sub menuSaveCollection_Click()
  On Error Resume Next
  CommonDialog2.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If CommonDialog2.FileName <> "" Then
    datCardNames.Recordset.MoveLast
    datCardNames.Recordset.MoveNext
    Frame1.Visible = False
    txtFileName.DataField = ""
    frmLoading.Visible = True
    dataCards.Visible = False
    datCardNames.Recordset.MoveFirst
    Open CommonDialog2.FileName For Output As #1
    Write #1, "Quantity", "Name", "Set", "Rarity", "Type"
    While Not datCardNames.Recordset.EOF
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      If txtNum.text > 0 Then
        Write #1, txtNum.text, txtCardName.text, txtSet.text, txtRarity.text, txtType.text
      End If
      datCardNames.Recordset.MoveNext
    Wend
    Close #1
    CommonDialog2.FileName = ""
    txtFileName.DataField = "File Name"
    frmLoading.Visible = False
    dataCards.Visible = True
    Frame1.Visible = True
    MsgBox "Finished", vbOKOnly
  End If
End Sub

Private Sub menuSaveHaves_Click()
  On Error Resume Next
  CommonDialog2.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If CommonDialog2.FileName <> "" Then
    datCardNames.Recordset.MoveLast
    datCardNames.Recordset.MoveNext
    Frame1.Visible = False
    txtFileName.DataField = ""
    frmLoading.Visible = True
    dataCards.Visible = False
    datCardNames.Recordset.MoveFirst
    Open CommonDialog2.FileName For Output As #1
    Write #1, "Quantity", "Name", "Set", "Rarity", "Type"
    While Not datCardNames.Recordset.EOF
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      If txtNum.text > 1 Then
        Write #1, txtNum.text, txtCardName.text, txtSet.text, txtRarity.text, txtType.text
      End If
      datCardNames.Recordset.MoveNext
    Wend
    Close #1
    CommonDialog2.FileName = ""
    txtFileName.DataField = "File Name"
    frmLoading.Visible = False
    dataCards.Visible = True
    Frame1.Visible = True
    MsgBox "Finished", vbOKOnly
  End If
End Sub

Private Sub menuSaveTradeList_Click()
  On Error Resume Next
  CommonDialog3.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If CommonDialog3.FileName <> "" Then
    datCardNames.Recordset.MoveLast
    datCardNames.Recordset.MoveNext
    Frame1.Visible = False
    frmLoading.Visible = True
    dataCards.Visible = False
    txtFileName.DataField = ""
    datCardNames.Recordset.MoveFirst
    Open CommonDialog3.FileName For Output As #1
    While Not datCardNames.Recordset.EOF
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      If txtNum.text > 0 Then
        Write #1, "H", txtNum.text, txtID.text
      Else
        Write #1, "W", 1, txtID.text
      End If
      datCardNames.Recordset.MoveNext
    Wend
    Close #1
    txtFileName.DataField = "File Name"
    CommonDialog2.FileName = ""
    dataCards.Visible = True
    frmLoading.Visible = False
    Frame1.Visible = True
    MsgBox "Finished", vbOKOnly
  End If
End Sub

Private Sub menuSaveWants_Click()
  On Error Resume Next
  CommonDialog2.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If CommonDialog2.FileName <> "" Then
    datCardNames.Recordset.MoveLast
    datCardNames.Recordset.MoveNext
    Frame1.Visible = False
    txtFileName.DataField = ""
    frmLoading.Visible = True
    dataCards.Visible = False
    datCardNames.Recordset.MoveFirst
    Open CommonDialog2.FileName For Output As #1
    Write #1, "Quantity", "Name", "Set", "Rarity", "Type"
    While Not datCardNames.Recordset.EOF
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      If txtNum.text = 0 Then
        Write #1, txtNum.text, txtCardName.text, txtSet.text, txtRarity.text, txtType.text
      End If
      datCardNames.Recordset.MoveNext
    Wend
    Close #1
    txtFileName.DataField = "File Name"
    frmLoading.Visible = False
    dataCards.Visible = True
    Frame1.Visible = True
    CommonDialog2.FileName = ""
    MsgBox "Finished", vbOKOnly
  End If
End Sub

Private Sub menuShowAll_Click()
  datCardNames.Recordset.Filter = adFilterNone
End Sub

Private Sub menuShowCollection_Click()
  datCardNames.Recordset.Filter = ""
  If (cmboSet.ListIndex < 1) Then
    datCardNames.Recordset.Filter = "Number > 0"
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Number > 0"
  End If
End Sub

Private Sub menuShowHaves_Click()
  datCardNames.Recordset.Filter = ""
  If (cmboSet.ListIndex < 1) Then
    datCardNames.Recordset.Filter = "Number > 1"
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Number > 1"
  End If
End Sub

Private Sub menuShowHavesAndWants_Click()
  datCardNames.Recordset.Filter = ""
  If (cmboSet.ListIndex < 1) Then
    datCardNames.Recordset.Filter = "Number <> 1"
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Number <> 1"
  End If
End Sub

Private Sub menuShowWants_Click()
  datCardNames.Recordset.Filter = ""
  If (cmboSet.ListIndex < 1) Then
    datCardNames.Recordset.Filter = "Number = 0"
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Number = 0"
  End If
End Sub

Private Sub txtFileName_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Stretch = True
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileName.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub txtSet_Change()
  Call txtFileName_Change
End Sub

Public Function getImageLocation(txtFileNames As String, nameToRetrive As Integer)
'  This function was created to deal with the whole "two card images in one field" issue
'nameToRetrive = 1 - Front, 2 - Back
  Dim counter As Long
  Dim foundSplit As Boolean
  Dim splitPos As Long
  foundSplit = False
  
  If (Left$(txtFileNames, 1) = "*") Then ' if first character is '*' then check for character '|'
    counter = 0
    While counter <= Len(txtFileNames)
      counter = counter + 1
      If Mid$(txtFileNames, counter, 1) = "|" Then ' '|' represents end of front image and start of back
        foundSplit = True
        splitPos = counter
      End If
    Wend
    
    If foundSplit Then ' if a break was found then...
      If nameToRetrive = 1 Then
        getImageLocation = Mid$(txtFileNames, 2, splitPos - 2)
      Else
        getImageLocation = Mid$(txtFileNames, splitPos + 1, Len(txtFileNames) - splitPos)
      End If
    Else ' if a break was not found, but a '*' was, assume somethings wrong and display empty.jpg
      getImageLocation = ""
    End If
  Else ' if a '*' is not found, assume only the front of the image is given
    If nameToRetrive = 1 Then
      getImageLocation = txtFileNames
    Else
      getImageLocation = ""
    End If
  End If
End Function

