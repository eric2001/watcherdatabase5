VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmPlayGameEmail 
   Caption         =   "Play Game (Email/Chat Room)"
   ClientHeight    =   8190
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   11610
   LinkTopic       =   "Form2"
   ScaleHeight     =   8190
   ScaleWidth      =   11610
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frameTurn 
      Height          =   495
      Left            =   0
      TabIndex        =   121
      Top             =   7680
      Width           =   4695
      Begin VB.TextBox txtYourTurnNum 
         Height          =   285
         Left            =   1200
         TabIndex        =   125
         Top             =   120
         Width           =   375
      End
      Begin VB.TextBox txtYourAbility 
         Height          =   285
         Left            =   2160
         TabIndex        =   124
         Top             =   120
         Width           =   375
      End
      Begin VB.ComboBox cmboPartOfPhase 
         Height          =   315
         ItemData        =   "frmPlayGameEmail.frx":0000
         Left            =   2640
         List            =   "frmPlayGameEmail.frx":001C
         TabIndex        =   123
         Text            =   "Phase"
         Top             =   120
         Width           =   1095
      End
      Begin VB.CommandButton cmdChangePhase 
         Caption         =   "Select"
         Height          =   255
         Left            =   3840
         TabIndex        =   122
         Top             =   120
         Width           =   735
      End
      Begin VB.Label lblTurnNumber 
         AutoSize        =   -1  'True
         Caption         =   "Turn Number:"
         Height          =   195
         Left            =   120
         TabIndex        =   127
         Top             =   120
         Width           =   975
      End
      Begin VB.Label lblYourAbility 
         AutoSize        =   -1  'True
         Caption         =   "Ability:"
         Height          =   195
         Left            =   1680
         TabIndex        =   126
         Top             =   120
         Width           =   450
      End
   End
   Begin VB.Frame frameButtons 
      Height          =   735
      Left            =   4680
      TabIndex        =   117
      Top             =   7440
      Width           =   3255
      Begin VB.CommandButton cmdShuffleEndurance 
         Caption         =   "Shuffle Endurance"
         Height          =   495
         Left            =   2160
         TabIndex        =   120
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton cmdMakeExertion 
         Caption         =   "Make Exertion"
         Height          =   495
         Left            =   1320
         TabIndex        =   119
         Top             =   120
         Width           =   855
      End
      Begin VB.CommandButton cmdMoveDiscardToDeck 
         Caption         =   "Shuffle Discard Pile into Deck"
         Height          =   495
         Left            =   120
         TabIndex        =   118
         Top             =   120
         Width           =   1215
      End
   End
   Begin VB.Frame frmPreGame 
      Caption         =   "Pre-Game Cards"
      Height          =   3975
      Left            =   4440
      TabIndex        =   113
      Top             =   0
      Visible         =   0   'False
      Width           =   3255
      Begin VB.CommandButton cmdUserPreGameToPlay 
         Caption         =   "Move Selected Card To In Play"
         Height          =   495
         Left            =   1560
         TabIndex        =   116
         Top             =   3360
         Width           =   1455
      End
      Begin VB.CommandButton cmdRemovePreGame 
         Caption         =   "Remove Selected Card"
         Height          =   495
         Left            =   240
         TabIndex        =   115
         Top             =   3360
         Width           =   1215
      End
      Begin VB.ListBox lstPreGame 
         Height          =   2985
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   114
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.Frame frameOppPreGame 
      Caption         =   "Pre-Game (Opponent)"
      Height          =   3975
      Left            =   4440
      TabIndex        =   110
      Top             =   0
      Visible         =   0   'False
      Width           =   3255
      Begin VB.ListBox lstOppPreGame 
         Height          =   3180
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   112
         Top             =   240
         Width           =   3015
      End
      Begin VB.CommandButton cmdOppPreGameRemoveCard 
         Caption         =   "Remove Card"
         Height          =   375
         Left            =   840
         TabIndex        =   111
         Top             =   3480
         Width           =   1215
      End
   End
   Begin VB.Frame frameOppStats 
      Height          =   975
      Left            =   5040
      TabIndex        =   103
      Top             =   3120
      Width           =   2775
      Begin VB.TextBox txtOpponetTurnNum 
         Height          =   285
         Left            =   1440
         TabIndex        =   109
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox txtOpponetAbility 
         Height          =   285
         Left            =   1440
         TabIndex        =   108
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox txtOpponetNameEmail 
         Height          =   285
         Left            =   1440
         TabIndex        =   107
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label lblOppName 
         AutoSize        =   -1  'True
         Caption         =   "Opponent Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   106
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label lblOppAbility 
         AutoSize        =   -1  'True
         Caption         =   "Opponent Ability:"
         Height          =   195
         Left            =   120
         TabIndex        =   105
         Top             =   360
         Width           =   1200
      End
      Begin VB.Label lblOppTurnNum 
         AutoSize        =   -1  'True
         Caption         =   "Opponent Turn:"
         Height          =   195
         Left            =   120
         TabIndex        =   104
         Top             =   600
         Width           =   1125
      End
   End
   Begin VB.Frame frmLoading 
      Caption         =   "Loading..."
      Height          =   735
      Left            =   7800
      TabIndex        =   101
      Top             =   3000
      Visible         =   0   'False
      Width           =   3615
      Begin VB.Shape StatusBox 
         DrawMode        =   1  'Blackness
         FillStyle       =   0  'Solid
         Height          =   375
         Left            =   120
         Shape           =   3  'Circle
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame frameTopXCards 
      Caption         =   "Deck"
      Height          =   3255
      Left            =   3240
      TabIndex        =   96
      Top             =   0
      Visible         =   0   'False
      Width           =   3255
      Begin VB.CommandButton cmdTopXCardsFinished 
         Caption         =   "Finished"
         Height          =   495
         Left            =   1800
         TabIndex        =   100
         Top             =   2640
         Width           =   1095
      End
      Begin VB.CommandButton cmdShowXToHand 
         Caption         =   "Move Selected Card To Hand"
         Height          =   495
         Left            =   240
         TabIndex        =   99
         Top             =   2640
         Width           =   1455
      End
      Begin VB.ListBox lstTopXCards 
         Height          =   1815
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   97
         Top             =   360
         Width           =   3015
      End
      Begin VB.Label lblTopCardHelp 
         AutoSize        =   -1  'True
         Caption         =   "(The top card in the deck is listed at the top of this list)"
         Height          =   390
         Left            =   240
         TabIndex        =   98
         Top             =   2160
         Width           =   2505
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame frmRemovedFromGame 
      Caption         =   "Removed From Game"
      Height          =   3255
      Left            =   4920
      TabIndex        =   93
      Top             =   4200
      Visible         =   0   'False
      Width           =   2775
      Begin VB.CommandButton cmdPlayerFromRemovedGameToPlay 
         Caption         =   "Play Selected Card"
         Height          =   255
         Left            =   240
         TabIndex        =   95
         Top             =   2880
         Width           =   2055
      End
      Begin VB.ListBox lstPlayerRemovedFromGame 
         Height          =   2595
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   94
         Top             =   240
         Width           =   2535
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog3 
      Left            =   8040
      Top             =   2040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "txt"
      Filter          =   "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
   End
   Begin VB.ListBox lstTurnTranscript 
      Height          =   1815
      Left            =   8400
      TabIndex        =   88
      Top             =   4080
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Frame frameExertion 
      Caption         =   "Exertion"
      Height          =   3255
      Left            =   4920
      TabIndex        =   75
      Top             =   4200
      Visible         =   0   'False
      Width           =   2775
      Begin VB.ListBox lstExertion 
         Height          =   1815
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   80
         Top             =   240
         Width           =   2535
      End
      Begin VB.CommandButton cmdExert5Cards 
         Caption         =   "Exert 5 Cards"
         Height          =   255
         Left            =   240
         TabIndex        =   79
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton cmdExert1Card 
         Caption         =   "Exert 1 Card"
         Height          =   255
         Left            =   240
         TabIndex        =   78
         Top             =   2160
         Width           =   2175
      End
      Begin VB.CommandButton cmdPlayExertedCard 
         Caption         =   "Play Card"
         Height          =   255
         Left            =   240
         TabIndex        =   77
         Top             =   2640
         Width           =   2175
      End
      Begin VB.CommandButton cmdDiscardExertedCards 
         Caption         =   "Finish Exertion"
         Height          =   315
         Left            =   240
         TabIndex        =   76
         Top             =   2880
         Width           =   2175
      End
   End
   Begin VB.Frame frameRollDice 
      Caption         =   "Roll Dice"
      Height          =   1455
      Left            =   0
      TabIndex        =   69
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
      Begin VB.TextBox txtNumberRolled 
         Height          =   285
         Left            =   1440
         TabIndex        =   74
         Top             =   960
         Width           =   495
      End
      Begin VB.CommandButton cmdRollDice 
         Caption         =   "Roll Dice"
         Height          =   255
         Left            =   120
         TabIndex        =   72
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox txtNumSidesDice 
         Height          =   285
         Left            =   1440
         TabIndex        =   70
         Text            =   "6"
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblNumberRolled 
         AutoSize        =   -1  'True
         Caption         =   "Number Rolled:"
         Height          =   195
         Left            =   120
         TabIndex        =   73
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblNumSidesDice 
         AutoSize        =   -1  'True
         Caption         =   "Number of Sides:"
         Height          =   195
         Left            =   120
         TabIndex        =   71
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame frameDeck 
      Caption         =   "Deck"
      Height          =   3975
      Left            =   120
      TabIndex        =   65
      Top             =   0
      Visible         =   0   'False
      Width           =   3255
      Begin VB.ListBox lstDeck 
         Height          =   2985
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   68
         Top             =   240
         Width           =   3015
      End
      Begin VB.CommandButton cmdMoveCardToTopDeck 
         Caption         =   "Move to Top of Deck"
         Height          =   495
         Left            =   1680
         TabIndex        =   67
         Top             =   3360
         Width           =   1455
      End
      Begin VB.CommandButton cmdPlaceCardInHand 
         Caption         =   "Move Selected Card to Hand"
         Height          =   495
         Left            =   120
         TabIndex        =   66
         Top             =   3360
         Width           =   1215
      End
   End
   Begin VB.Frame frameSortFilter 
      Caption         =   "Sort/Filter"
      Height          =   2055
      Left            =   0
      TabIndex        =   60
      Top             =   4080
      Visible         =   0   'False
      Width           =   3255
      Begin VB.CommandButton cmdShowAll 
         Caption         =   "Show All"
         Height          =   255
         Left            =   1560
         TabIndex        =   86
         Top             =   1680
         Width           =   1095
      End
      Begin VB.CommandButton cmdFindFirst 
         Caption         =   "Find"
         Height          =   255
         Left            =   240
         TabIndex        =   85
         Top             =   1680
         Width           =   975
      End
      Begin VB.TextBox txtNameToSearchFor 
         Height          =   285
         Left            =   240
         TabIndex        =   84
         Top             =   1320
         Width           =   2535
      End
      Begin VB.CommandButton cmdSortSet 
         Caption         =   "Sort By Set"
         Height          =   495
         Left            =   840
         TabIndex        =   64
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdSortName 
         Caption         =   "Sort By Name"
         Height          =   495
         Left            =   1920
         TabIndex        =   63
         Top             =   240
         Width           =   855
      End
      Begin VB.ComboBox cmboSet 
         Height          =   315
         ItemData        =   "frmPlayGameEmail.frx":0089
         Left            =   480
         List            =   "frmPlayGameEmail.frx":00F9
         TabIndex        =   62
         Text            =   "Set Names"
         Top             =   840
         Width           =   1455
      End
      Begin VB.CommandButton cmdShowSet 
         Caption         =   "Select Set"
         Height          =   255
         Left            =   2040
         TabIndex        =   61
         Top             =   840
         Width           =   975
      End
   End
   Begin VB.Frame frameOpponentDiscard 
      Caption         =   "Discard (Opponent)"
      Height          =   3135
      Left            =   5040
      TabIndex        =   55
      Top             =   0
      Width           =   2655
      Begin VB.CommandButton cmdOpponetDiscardRemoveAll 
         Caption         =   "Remove All"
         Height          =   255
         Left            =   1560
         TabIndex        =   59
         Top             =   2760
         Width           =   975
      End
      Begin VB.CommandButton cmdOpponetDiscardPlay 
         Caption         =   "Play"
         Height          =   255
         Left            =   840
         TabIndex        =   58
         Top             =   2760
         Width           =   735
      End
      Begin VB.CommandButton cmdOpponetDiscardRemove 
         Caption         =   "Remove"
         Height          =   255
         Left            =   120
         TabIndex        =   57
         Top             =   2760
         Width           =   735
      End
      Begin VB.ListBox lstOpponetDiscard 
         Height          =   2400
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   56
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame frameOppInPlay 
      Caption         =   "In Play (Opponent)"
      Height          =   4095
      Left            =   2760
      TabIndex        =   51
      Top             =   0
      Width           =   2175
      Begin VB.CommandButton cmdStealSelectedCard 
         Caption         =   "Steal Selected Card"
         Height          =   315
         Left            =   120
         TabIndex        =   87
         Top             =   3480
         Width           =   1695
      End
      Begin VB.ListBox lstOpponetInPlay 
         Height          =   2595
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   54
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton cmdOpponetInPlayRemove 
         Caption         =   "Remove Card"
         Height          =   495
         Left            =   120
         TabIndex        =   53
         Top             =   3000
         Width           =   855
      End
      Begin VB.CommandButton cmdOpponetInPlayDiscard 
         Caption         =   "Discard Card"
         Height          =   495
         Left            =   960
         TabIndex        =   52
         Top             =   3000
         Width           =   855
      End
   End
   Begin VB.Frame frameTempStorage 
      Caption         =   "Dojo"
      Height          =   3255
      Left            =   4920
      TabIndex        =   46
      Top             =   4200
      Visible         =   0   'False
      Width           =   2775
      Begin VB.CommandButton cmdFromDojoToDiscard 
         Caption         =   "Discard"
         Height          =   495
         Left            =   1320
         TabIndex        =   50
         Top             =   2640
         Width           =   975
      End
      Begin VB.CommandButton cmdMoveToHand 
         Caption         =   "To Hand"
         Height          =   495
         Left            =   240
         TabIndex        =   49
         Top             =   2640
         Width           =   1095
      End
      Begin VB.ListBox lstTempStorage 
         Height          =   2400
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   47
         Top             =   240
         Width           =   2535
      End
   End
   Begin VB.Frame framePlayerDiscard 
      Caption         =   "Discard"
      Height          =   3255
      Left            =   4920
      TabIndex        =   37
      Top             =   4200
      Width           =   2655
      Begin VB.CommandButton cmd1CardDiscardToEndurance 
         Caption         =   "Move to Top of Endurance"
         Height          =   615
         Left            =   240
         TabIndex        =   83
         Top             =   2520
         Width           =   975
      End
      Begin VB.CommandButton cmdPlayerDiscardRemoveGame 
         Caption         =   "Remove From Game"
         Height          =   615
         Left            =   1680
         TabIndex        =   44
         Top             =   2520
         Width           =   855
      End
      Begin VB.CommandButton cmdPlayerDiscardToPlay 
         Caption         =   "Play"
         Height          =   615
         Left            =   1200
         TabIndex        =   43
         Top             =   2520
         Width           =   495
      End
      Begin VB.ListBox lstPlayerDiscard 
         Height          =   2205
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   38
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame framePlayerInPlay 
      Caption         =   "In Play"
      Height          =   3255
      Left            =   2520
      TabIndex        =   36
      Top             =   4200
      Width           =   2295
      Begin VB.CommandButton cmdPlayerInPlayDiscard 
         Caption         =   "Discard"
         Height          =   495
         Left            =   1200
         TabIndex        =   42
         Top             =   2640
         Width           =   735
      End
      Begin VB.CommandButton cmdPlayerInPlayToHand 
         Caption         =   "To Hand"
         Height          =   495
         Left            =   360
         TabIndex        =   41
         Top             =   2640
         Width           =   735
      End
      Begin VB.ListBox lstPlayerInPlay 
         Height          =   2400
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   39
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame frameHand 
      Caption         =   "Hand"
      Height          =   3495
      Left            =   0
      TabIndex        =   32
      Top             =   4200
      Width           =   2415
      Begin VB.CommandButton cmdDiscardToBottomOfDeck 
         Caption         =   "Discard (Bottom of Deck)"
         Height          =   255
         Left            =   120
         TabIndex        =   102
         Top             =   3120
         Width           =   2055
      End
      Begin VB.CommandButton cmdDiscardToTopOfDeck 
         Caption         =   "Discard (Top of Deck)"
         Height          =   255
         Left            =   120
         TabIndex        =   81
         Top             =   2880
         Width           =   2055
      End
      Begin VB.CommandButton cmdToTempStorage 
         Caption         =   "Move to Dojo"
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   2640
         Width           =   1215
      End
      Begin VB.CommandButton cmdPlayerPlayCard 
         Caption         =   "Play Card"
         Height          =   255
         Left            =   1320
         TabIndex        =   40
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton cmdPlayerDiscard 
         Caption         =   "Discard"
         Height          =   255
         Left            =   1440
         TabIndex        =   35
         Top             =   2400
         Width           =   735
      End
      Begin VB.CommandButton cmdPlayerDrawCard 
         Caption         =   "Draw Card"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   2400
         Width           =   1335
      End
      Begin VB.ListBox lstPlayerHand 
         Height          =   2010
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   33
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame frameCards 
      Caption         =   "All Cards"
      Height          =   4095
      Left            =   0
      TabIndex        =   28
      Top             =   0
      Width           =   2655
      Begin VB.CommandButton cmdAddToOppPreGame 
         Caption         =   "Add To Opponent Pre-Game"
         Height          =   255
         Left            =   120
         TabIndex        =   82
         Top             =   3480
         Width           =   2415
      End
      Begin VB.CommandButton cmdShowHideSortFilter 
         Caption         =   "Show/Hide Sort/Filter Options"
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   3720
         Width           =   2415
      End
      Begin VB.CommandButton cmdOpponetDiscard 
         Caption         =   "Discard Card"
         Height          =   255
         Left            =   1320
         TabIndex        =   31
         Top             =   3240
         Width           =   1215
      End
      Begin VB.CommandButton cmdOpponetPlay 
         Caption         =   "Play Card"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   3240
         Width           =   1215
      End
      Begin MSDataGridLib.DataGrid dataCards 
         Bindings        =   "frmPlayGameEmail.frx":01CF
         Height          =   2895
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   5106
         _Version        =   393216
         AllowUpdate     =   0   'False
         AllowArrows     =   0   'False
         DefColWidth     =   273
         ColumnHeaders   =   0   'False
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "All Cards"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   7680
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "hld"
      Filter          =   "Highlander Decks (*.hld)|*.hld|All Files (*.*)|*.*"
   End
   Begin VB.Frame frameCardDetailsDeck 
      Caption         =   "Card Details"
      Height          =   8055
      Left            =   7920
      TabIndex        =   13
      Top             =   0
      Visible         =   0   'False
      Width           =   3615
      Begin VB.CommandButton cmdDeckShowBack 
         Caption         =   "Show Back"
         Height          =   255
         Left            =   1920
         TabIndex        =   90
         Top             =   7680
         Width           =   975
      End
      Begin VB.CommandButton cmdDeckShowFront 
         Caption         =   "Show Front"
         Height          =   255
         Left            =   480
         TabIndex        =   89
         Top             =   7680
         Width           =   975
      End
      Begin VB.TextBox txtOtherDeck 
         DataField       =   "Other"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   2760
         TabIndex        =   27
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox txtTypeDeck 
         DataField       =   "Type"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   26
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox txtRarityDeck 
         DataField       =   "Rarity"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   24
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox txtSetDeck 
         DataField       =   "Set"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   2760
         TabIndex        =   23
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox txtCardNameDeck 
         DataField       =   "Name"
         DataSource      =   "datCardDeck"
         Height          =   525
         Left            =   1080
         MultiLine       =   -1  'True
         TabIndex        =   22
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox txtFileNameDeck 
         DataField       =   "File Name"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   240
         TabIndex        =   15
         Top             =   2160
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtCardTextDeck 
         DataField       =   "Text"
         DataSource      =   "datCardDeck"
         Height          =   1365
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   14
         Top             =   1560
         Width           =   2415
      End
      Begin VB.Image HLCardDeck 
         Height          =   4575
         Left            =   120
         Stretch         =   -1  'True
         Top             =   3000
         Width           =   3375
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   1200
         Width           =   450
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   2280
         TabIndex        =   20
         Top             =   840
         Width           =   285
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   840
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   840
         Width           =   405
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   2280
         TabIndex        =   17
         Top             =   1200
         Width           =   435
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   1560
         Width           =   735
      End
   End
   Begin VB.Frame frameCardDetailsDB 
      Caption         =   "Card Details"
      Height          =   8055
      Left            =   7920
      TabIndex        =   0
      Top             =   0
      Width           =   3615
      Begin VB.CommandButton cmdShowBack 
         Caption         =   "Show Back"
         Height          =   255
         Left            =   1920
         TabIndex        =   92
         Top             =   7680
         Width           =   975
      End
      Begin VB.CommandButton cmdShowFront 
         Caption         =   "Show Front"
         Height          =   255
         Left            =   480
         TabIndex        =   91
         Top             =   7680
         Width           =   975
      End
      Begin VB.TextBox txtType 
         DataField       =   "Type"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   25
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox txtCardText 
         DataField       =   "Text"
         DataSource      =   "datCardNames"
         Height          =   1365
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         Top             =   1560
         Width           =   2415
      End
      Begin VB.TextBox txtOther 
         DataField       =   "Other"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2760
         TabIndex        =   9
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox txtCardName 
         DataField       =   "Name"
         DataSource      =   "datCardNames"
         Height          =   525
         Left            =   1080
         TabIndex        =   4
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox txtSet 
         DataField       =   "Set"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2760
         TabIndex        =   3
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox txtFileName 
         DataField       =   "File Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   2160
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtRarity 
         DataField       =   "Rarity"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Image HLCard 
         Height          =   4575
         Left            =   120
         Stretch         =   -1  'True
         Top             =   3000
         Width           =   3375
      End
      Begin VB.Label lblCardText 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1560
         Width           =   735
      End
      Begin VB.Label lblOther 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   2280
         TabIndex        =   10
         Top             =   1200
         Width           =   435
      End
      Begin VB.Label lblType 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   840
         Width           =   405
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   840
      End
      Begin VB.Label lblSet 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   2280
         TabIndex        =   6
         Top             =   840
         Width           =   285
      End
      Begin VB.Label lblRarity 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   450
      End
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   7920
      Top             =   120
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Email Cards Connection"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog CommonDialog2 
      Left            =   7800
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "hlg"
      Filter          =   "Saved Games (*.hlg)|*.hlg|All Files (*.*)|*.*"
   End
   Begin MSAdodcLib.Adodc datCardDeck 
      Height          =   735
      Left            =   7920
      Top             =   960
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Email Deck Connection"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Menu File 
      Caption         =   "File"
      Begin VB.Menu NewGameMenu 
         Caption         =   "New Game"
         Shortcut        =   ^N
      End
      Begin VB.Menu LoadGameMenu 
         Caption         =   "Open Saved Game"
         Shortcut        =   ^O
      End
      Begin VB.Menu SaveGameAsMenu 
         Caption         =   "Save Game As..."
         Shortcut        =   ^A
      End
      Begin VB.Menu SaveGameMenu 
         Caption         =   "Save Game"
         Shortcut        =   ^S
      End
      Begin VB.Menu hr1 
         Caption         =   "-"
      End
      Begin VB.Menu ExitMenu 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu ShowHideMenu 
      Caption         =   "Show/Hide"
      Begin VB.Menu OppPreGameShowHideMenu 
         Caption         =   "Opponent Pre-Game Cards"
         Shortcut        =   ^E
      End
      Begin VB.Menu hr 
         Caption         =   "-"
      End
      Begin VB.Menu RollDiceMenu 
         Caption         =   "Roll Dice"
         Shortcut        =   ^R
      End
      Begin VB.Menu hr3 
         Caption         =   "-"
      End
      Begin VB.Menu RemovedFromGameShowHideMenu 
         Caption         =   "Removed From Game"
         Shortcut        =   ^G
      End
      Begin VB.Menu DojoMenu 
         Caption         =   "Dojo"
         Shortcut        =   ^U
      End
      Begin VB.Menu YourPreGameMenu 
         Caption         =   "Your Pre-Game Cards"
         Shortcut        =   ^P
      End
      Begin VB.Menu menuShowTopXCards 
         Caption         =   "Top X Cards In Deck"
         Shortcut        =   ^X
      End
      Begin VB.Menu YourDeckMenu 
         Caption         =   "Your Deck"
         Shortcut        =   ^D
      End
   End
   Begin VB.Menu menuSortHand 
      Caption         =   "Sort Hand"
      Begin VB.Menu menuSortHandByName 
         Caption         =   "By Name"
         Shortcut        =   ^M
      End
      Begin VB.Menu menuSortHandByType 
         Caption         =   "By Type"
         Shortcut        =   ^T
      End
      Begin VB.Menu sortHandByTypeAndName 
         Caption         =   "By Type and Name"
         Shortcut        =   ^H
      End
   End
End
Attribute VB_Name = "frmPlayGameEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Created By:           Eric Cavaliere
' Program Name:         Watcher Database
' Project File Name:    Highlander.vbp
' Project Started on:   August 05, 2002
' Name of this File:    frmPlayGameEmail.frm
' This file started on: August 07, 2002
' Last Updated on:      22 October 2007

'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'array(*, 1) = id, array(*, 2) = random number, array(*, 3) = Title, array(*, 4) = Type
'* = record number
Dim userDeckCards(500, 4)
Dim userPreGameCards(500, 4)
Dim userHandCards(50, 4)
Dim userInPlayCards(50, 4)
Dim userDiscardCards(500, 4)
Dim userExertion(50, 4)
Dim userTempStorage(50, 4)
Dim oppInPlayCards(50, 4)
Dim oppDiscardCards(500, 4)
Dim oppPreGameCards(50, 4)
Dim userRemovedFromGameCards(50, 4)
Dim userDeckPos As Integer
Dim userPrePos As Integer
Dim userHandPos As Integer
Dim userInPlayPos As Integer
Dim userDiscardPos As Integer
Dim userExertionPos As Integer
Dim userTempStoragePos As Integer
Dim userRemovedFromGamePos As Integer
Dim oppInPlayPos As Integer
Dim oppDiscardPos As Integer
Dim oppPreGamePos As Integer
Dim transcriptPos As Integer, numDrawn As Integer
Dim dx As Single
Dim dy As Single
Dim moveCardDetails As Boolean

Private Sub cmd1CardDiscardToEndurance_Click()
  Dim j As Integer
  TransferCardFromLstToLst lstPlayerDiscard, userDiscardCards(), userDiscardPos, lstDeck, userDeckCards(), userDeckPos
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
  cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3)
    j = j - 1
  Wend
End Sub

Private Sub cmdAddToOppPreGame_Click()
  If Not datCardNames.Recordset.EOF Then
    lstOppPreGame.AddItem (dataCards.Columns("Name").Value)
    oppPreGameCards(oppPreGamePos, 1) = dataCards.Columns("ID").Value
    oppPreGameCards(oppPreGamePos, 2) = 1
    oppPreGameCards(oppPreGamePos, 3) = dataCards.Columns("Name").Value
    oppPreGamePos = oppPreGamePos + 1
  End If
End Sub

Private Sub cmdChangePhase_Click()
  Dim counterdisp As Integer
  Dim transcriptcounter As Integer
  
  If cmboPartOfPhase.ListIndex = 0 Then ' Begin Turn
    lstTurnTranscript.Clear
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "** Begin Turn " + txtYourTurnNum.text + " **"
    numDrawn = 0
    transcriptPos = 2
  End If
  If cmboPartOfPhase.ListIndex = 1 Then ' Begin Sweep Phase
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Sweep:"
    transcriptPos = 1
  End If
  If cmboPartOfPhase.ListIndex = 2 Then ' Defense Phase
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Defense Phase:"
    transcriptPos = 1
  End If
  
  If cmboPartOfPhase.ListIndex = 3 Then ' Attack Phase
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Attack Phase:"
    transcriptPos = 1
  End If
  If cmboPartOfPhase.ListIndex = 4 Then ' Ability Adjustment Phase
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Ability Adjustment Phase:"
    transcriptPos = 1
  End If
  If cmboPartOfPhase.ListIndex = 5 Then ' Discard
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Discard:"
    transcriptPos = 1
  End If
  If cmboPartOfPhase.ListIndex = 6 Then ' End Turn
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Draw:      " + Str$(numDrawn)
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Hand:               " + Str$(userHandPos)
    lstTurnTranscript.AddItem "Ability:             " + txtYourAbility.text
    lstTurnTranscript.AddItem "Endurance:          " + Str$(userDeckPos)
    lstTurnTranscript.AddItem "Discard:            " + Str$(userDiscardPos)
    lstTurnTranscript.AddItem "Removed From Game:  " + Str$(userRemovedFromGamePos)
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Display:    " + lstPlayerInPlay.List(0)
    For counterdisp = 1 To userInPlayPos
      lstTurnTranscript.AddItem "            " + lstPlayerInPlay.List(counterdisp)
    Next counterdisp
    lstTurnTranscript.AddItem "Pre-Game Cards:     " + lstPreGame.List(0)
    For counterdisp = 1 To userPrePos
      lstTurnTranscript.AddItem "                    " + lstPreGame.List(counterdisp)
    Next counterdisp
    lstTurnTranscript.AddItem "** End Turn " + txtYourTurnNum.text + " **"
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem ""
    lstTurnTranscript.AddItem "Card Text:"
    For counterdisp = 0 To userInPlayPos - 1
      lstTurnTranscript.AddItem lstPlayerInPlay.List(counterdisp)
      DisplayCardById (userInPlayCards(counterdisp, 1))
      lstTurnTranscript.AddItem "Type:  " + txtTypeDeck.text
      lstTurnTranscript.AddItem "Text:  " + txtCardTextDeck.text
      lstTurnTranscript.AddItem ""
    Next counterdisp
    For counterdisp = 0 To userPrePos - 1
      lstTurnTranscript.AddItem lstPreGame.List(counterdisp) + ":"
      DisplayCardById (userPreGameCards(counterdisp, 1))
      lstTurnTranscript.AddItem "Type:  " + txtTypeDeck.text
      lstTurnTranscript.AddItem "Text:  " + txtCardTextDeck.text
      lstTurnTranscript.AddItem ""
    Next counterdisp
    
    lstTurnTranscript.AddItem "_____________________________________________________"
    lstTurnTranscript.AddItem "Log generated using Watcher Database Version " + Str(App.Major) + "." + Str(App.Minor) + "." + Str(App.Revision)
    lstTurnTranscript.AddItem "Download your free copy at http://www.watcherdatabase.tk"
    lstTurnTranscript.AddItem ""

    transcriptPos = 2

  End If
  If cmboPartOfPhase.ListIndex = 7 Then 'save
    On Error Resume Next
    CommonDialog3.FileName = txtOpponetNameEmail.text + " " + txtYourTurnNum.text + ".txt"
    CommonDialog3.ShowSave
    If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
    If CommonDialog3.FileName <> "" Then
      Open CommonDialog3.FileName For Output As #3
        For transcriptcounter = 1 To lstTurnTranscript.ListCount
          Print #3, lstTurnTranscript.List(transcriptcounter)
        Next transcriptcounter
      Close #3
      MsgBox "Log has been Saved.", vbOKOnly, "Save Complete"
    End If
  End If
End Sub

Private Sub cmdDeckShowBack_Click()
  On Error Resume Next
  Dim FileName As Variant
  HLCardDeck.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileNameDeck.text, 2)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCardDeck.Picture = LoadPicture(FileName)
    Else
      HLCardDeck.Picture = LoadPicture(App.Path + "\" + txtSetDeck.text + "\" + FileName)
    End If
  End If

End Sub

Private Sub cmdDeckShowFront_Click()
  Call txtFileNameDeck_Change
End Sub

Private Sub cmdDiscardExertedCards_Click()
  While (userExertionPos > 0)
    lstPlayerDiscard.AddItem (userExertion(userExertionPos - 1, 3))
    userDiscardCards(userDiscardPos, 1) = userExertion(userExertionPos - 1, 1)
    userDiscardCards(userDiscardPos, 2) = userExertion(userExertionPos - 1, 2)
    userDiscardCards(userDiscardPos, 3) = userExertion(userExertionPos - 1, 3)
    userDiscardPos = userDiscardPos + 1
    userExertionPos = userExertionPos - 1
  Wend
  lstExertion.Clear
  frameExertion.Visible = False
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
End Sub

Private Sub cmdDiscardToBottomOfDeck_Click()
  Dim j As Integer
  Dim counter As Integer
  Dim k As Integer
  
  j = 0
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
    
  If userHandPos > 0 Then
    While (j < lstPlayerHand.ListCount)
      If lstPlayerHand.Selected(j) = True Then
        k = j
        lstTurnTranscript.AddItem "            " + lstPlayerHand.List(j) + " moved from hand to bottom of deck"
        DisplayCardById (userHandCards(j, 1))
        'lstDeck.AddItem (txtCardNameDeck.text)
        userDeckPos = userDeckPos + 1
        counter = userDeckPos
        While counter > -1
          userDeckCards(counter + 1, 1) = userDeckCards(counter, 1)
          userDeckCards(counter + 1, 2) = userDeckCards(counter, 2)
          userDeckCards(counter + 1, 3) = userDeckCards(counter, 3)
          userDeckCards(counter + 1, 4) = userDeckCards(counter, 4)
          counter = counter - 1
        Wend
        userDeckCards(0, 1) = userHandCards(j, 1)
        userDeckCards(0, 2) = userHandCards(j, 2)
        userDeckCards(0, 3) = userHandCards(j, 3)
        userDeckCards(0, 4) = userHandCards(j, 4)
        lstPlayerHand.RemoveItem (j)
        
        While (k < userHandPos)
          userHandCards(k, 1) = userHandCards(k + 1, 1)
          userHandCards(k, 2) = userHandCards(k + 1, 2)
          userHandCards(k, 3) = userHandCards(k + 1, 3)
          userHandCards(k, 4) = userHandCards(k + 1, 4)
          k = k + 1
        Wend
        
        userHandPos = userHandPos - 1
        j = j - 1
      End If
      j = j + 1
    Wend
  End If
  
  
  frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
  cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3)
    j = j - 1
  Wend


End Sub

Private Sub cmdDiscardToTopOfDeck_Click()
  Dim j As Integer
  
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstDeck, userDeckCards, userDeckPos
  
  frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
  cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3)
    j = j - 1
  Wend
End Sub

Private Sub cmdExert1Card_Click()
  If userDeckPos > 0 Then
    userExertion(userExertionPos, 1) = userDeckCards(userDeckPos - 1, 1)
    userExertion(userExertionPos, 2) = userDeckCards(userDeckPos - 1, 2)
    userExertion(userExertionPos, 3) = userDeckCards(userDeckPos - 1, 3)
    DisplayCardById (userExertion(userExertionPos, 1))
    lstTurnTranscript.AddItem "Exert Card:   " + txtCardNameDeck.text
    lstExertion.AddItem (txtCardNameDeck.text)
    lstDeck.RemoveItem (0)
    userExertionPos = userExertionPos + 1
    userDeckPos = userDeckPos - 1
    frameExertion.Caption = "Exertion (" + Str$(userExertionPos) + ")"
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  Else
    MsgBox "There are no more cards in the deck", vbOKOnly, "Error"
  End If
End Sub

Private Sub cmdExert5Cards_Click()
  Dim tempExertCount As Integer
  
  tempExertCount = 0
  While tempExertCount < 5
    If userDeckPos > 0 Then
      userExertion(userExertionPos, 1) = userDeckCards(userDeckPos - 1, 1)
      userExertion(userExertionPos, 2) = userDeckCards(userDeckPos - 1, 2)
      userExertion(userExertionPos, 3) = userDeckCards(userDeckPos - 1, 3)
      DisplayCardById (userExertion(userExertionPos, 1))
      lstTurnTranscript.AddItem "Exert Card:   " + txtCardNameDeck.text
      lstExertion.AddItem (txtCardNameDeck.text)
      lstDeck.RemoveItem (0)
      userExertionPos = userExertionPos + 1
      userDeckPos = userDeckPos - 1
      frameExertion.Caption = "Exertion (" + Str$(userExertionPos) + ")"
      frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
      cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
      tempExertCount = tempExertCount + 1
    Else
      MsgBox "There are no more cards in the deck", vbOKOnly, "Error"
      tempExertCount = 5
    End If
  Wend
End Sub

Private Sub cmdFindFirst_Click()
  On Error Resume Next
  Dim tempSearchString As String
  Dim i As Integer
  
  datCardNames.Recordset.Filter = ""
  If txtNameToSearchFor.text <> "" Then
    tempSearchString = txtNameToSearchFor.text
    For i = 1 To Len(txtNameToSearchFor.text)
      If (Mid$(txtNameToSearchFor.text, i, 1) = "'") And (Mid$(txtNameToSearchFor.text, i + 1, 1) <> "'") Then
        txtNameToSearchFor.text = Left$(txtNameToSearchFor.text, i) + "'" + Right$(txtNameToSearchFor.text, Len(txtNameToSearchFor.text) - i)
        i = i + 1
      End If
    Next i
    If (cmboSet.ListIndex < 1) Then
      datCardNames.Recordset.Filter = "Name LIKE '%" + txtNameToSearchFor.text + "%'"
    Else
      datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Name LIKE '%" + txtNameToSearchFor.text + "%'"
    End If
    txtNameToSearchFor.text = tempSearchString
  End If
End Sub

Private Sub cmdFromDojoToDiscard_Click()
  TransferCardFromLstToLst lstTempStorage, userTempStorage, userTempStoragePos, lstPlayerDiscard, userDiscardCards, userDiscardPos
  frameTempStorage.Caption = "Dojo (" + Str$(userTempStoragePos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
End Sub


Private Sub cmdMakeExertion_Click()
  frameExertion.Visible = True
End Sub

Private Sub cmdMoveCardToTopDeck_Click()
  Dim j, k
  j = 0
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If userDeckPos > 0 Then
    While (j < lstDeck.ListCount)
      If (lstDeck.Selected(j) = True) Then
        k = j
        lstDeck.Selected(j) = False
        While k > 0
          lstDeck.List(k) = lstDeck.List(k - 1)
          k = k - 1
        Wend
        k = userDeckPos - j - 1
        userDeckCards(userDeckPos, 1) = userDeckCards(k, 1)
        userDeckCards(userDeckPos, 2) = userDeckCards(k, 2)
        userDeckCards(userDeckPos, 3) = userDeckCards(k, 3)
        lstDeck.List(0) = userDeckCards(userDeckPos, 3)
        While (k < userDeckPos + 1)
          userDeckCards(k, 1) = userDeckCards(k + 1, 1)
          userDeckCards(k, 2) = userDeckCards(k + 1, 2)
          userDeckCards(k, 3) = userDeckCards(k + 1, 3)
          k = k + 1
        Wend
    
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub cmdMoveDiscardToDeck_Click()
  Dim k As Integer
  Dim tempCounter As Integer
  Dim a As Integer
  Dim b As Integer
  Dim j As Integer
  
  k = 0
  While (k < userDiscardPos)
    userDeckCards(userDeckPos, 1) = userDiscardCards(k, 1)
    userDeckCards(userDeckPos, 2) = userDiscardCards(k, 2)
    userDeckCards(userDeckPos, 3) = userDiscardCards(k, 3)
    userDeckPos = userDeckPos + 1
    k = k + 1
  Wend
  userDiscardPos = 0
  
  lstPlayerDiscard.Clear
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
  cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  
  Randomize Timer
  For tempCounter = 0 To userDeckPos - 1
    userDeckCards(tempCounter, 2) = Rnd()
  Next tempCounter
  
  For a = 0 To userDeckPos
    For b = 0 To userDeckPos - 2
      If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
        Dim tempCard(3)
        tempCard(1) = userDeckCards(b, 1)
        tempCard(2) = userDeckCards(b, 2)
        tempCard(3) = userDeckCards(b, 3)
        
        userDeckCards(b, 1) = userDeckCards(b + 1, 1)
        userDeckCards(b, 2) = userDeckCards(b + 1, 2)
        userDeckCards(b, 3) = userDeckCards(b + 1, 3)
        
        userDeckCards(b + 1, 1) = tempCard(1)
        userDeckCards(b + 1, 2) = tempCard(2)
        userDeckCards(b + 1, 3) = tempCard(3)
      End If
    Next b
  Next a
          
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3)
    j = j - 1
  Wend
End Sub

Private Sub cmdMoveToHand_Click()
  If userTempStoragePos > 0 Then
    TransferCardFromLstToLst lstTempStorage, userTempStorage, userTempStoragePos, lstPlayerHand, userHandCards, userHandPos
    userHandCards(userHandPos - 1, 4) = txtTypeDeck.text
    frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
    frameTempStorage.Caption = "Dojo (" + Str$(userTempStoragePos) + ")"
  End If
End Sub

Private Sub cmdOppPreGameRemoveCard_Click()
  RemoveCardFromList lstOppPreGame, oppPreGameCards, oppPreGamePos
End Sub

Public Sub RemoveCardFromList(ByRef tempLst As ListBox, ByRef tempArray() As Variant, ByRef tempPos As Integer)
  Dim j As Integer
  Dim k As Integer
  j = 0
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If (tempPos <> 0) Then
    While (j < tempLst.ListCount)
      If (tempLst.Selected(j) = True) Then
        k = j
        lstTurnTranscript.AddItem "            " + tempLst.List(j)
        While (k < tempPos)
          tempArray(k, 1) = tempArray(k + 1, 1)
          tempArray(k, 2) = tempArray(k + 1, 2)
          tempArray(k, 3) = tempArray(k + 1, 3)
          k = k + 1
        Wend
        tempPos = tempPos - 1
        tempLst.RemoveItem (j)
        j = j - 1
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub cmdPlaceCardInHand_Click()
  Dim j As Integer
  Dim k As Integer
  
  j = 0
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  If userDeckPos > 0 Then
    While (j < lstDeck.ListCount)
      If (lstDeck.Selected(j) = True) Then
        lstTurnTranscript.AddItem "            " + lstDeck.List(j)
        k = userDeckPos - j - 1
        DisplayCardById (userDeckCards(k, 1))
   
        lstPlayerHand.AddItem (txtCardNameDeck.text)
        userHandCards(userHandPos, 1) = userDeckCards(k, 1)
        userHandCards(userHandPos, 2) = userDeckCards(k, 2)
        userHandCards(userHandPos, 3) = userDeckCards(k, 3)
        userHandCards(userHandPos, 4) = txtTypeDeck.text
        userHandPos = userHandPos + 1
  
        While (k < userDeckPos)
          userDeckCards(k, 1) = userDeckCards(k + 1, 1)
          userDeckCards(k, 2) = userDeckCards(k + 1, 2)
          userDeckCards(k, 3) = userDeckCards(k + 1, 3)
          k = k + 1
        Wend
        userDeckPos = userDeckPos - 1
        lstDeck.RemoveItem (j)
        j = j - 1
        frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
        frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
        cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub cmdPlayerDiscard_Click()
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstPlayerDiscard, userDiscardCards, userDiscardPos
  frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
End Sub

Private Sub cmdPlayerDiscardRemoveGame_Click()
  TransferCardFromLstToLst lstPlayerDiscard, userDiscardCards, userDiscardPos, lstPlayerRemovedFromGame, userRemovedFromGameCards, userRemovedFromGamePos
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  frmRemovedFromGame.Caption = "Removed From Game (" + Str$(userRemovedFromGamePos) + ")"
End Sub

Private Sub cmdPlayerDiscardToPlay_Click()
  TransferCardFromLstToLst lstPlayerDiscard, userDiscardCards, userDiscardPos, lstPlayerInPlay, userInPlayCards, userInPlayPos
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
End Sub

Private Sub cmdPlayerDrawCard_Click()
  If userDeckPos > 0 Then
    userHandCards(userHandPos, 1) = userDeckCards(userDeckPos - 1, 1)
    userHandCards(userHandPos, 2) = userDeckCards(userDeckPos - 1, 2)
    userHandCards(userHandPos, 3) = userDeckCards(userDeckPos - 1, 3)
    userHandCards(userHandPos, 4) = userDeckCards(userDeckPos - 1, 4)
    
    DisplayCardById (userHandCards(userHandPos, 1))
    userHandCards(userHandPos, 4) = txtTypeDeck.text
    lstPlayerHand.AddItem (txtCardNameDeck.text)
    lstDeck.RemoveItem (0)
    userHandPos = userHandPos + 1
    userDeckPos = userDeckPos - 1
    frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
    numDrawn = numDrawn + 1
  Else
    MsgBox "There are no more cards in the deck", vbOKOnly, "Error"
  End If
End Sub

Private Sub cmdPlayerFromRemovedGameToPlay_Click()
  TransferCardFromLstToLst lstPlayerRemovedFromGame, userRemovedFromGameCards, userRemovedFromGamePos, lstPlayerInPlay, userInPlayCards, userInPlayPos
  frmRemovedFromGame.Caption = "Removed From Game (" + Str$(userRemovedFromGamePos) + ")"
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
End Sub

Private Sub cmdPlayerInPlayDiscard_Click()
  TransferCardFromLstToLst lstPlayerInPlay, userInPlayCards, userInPlayPos, lstPlayerDiscard, userDiscardCards, userDiscardPos
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
End Sub

Private Sub cmdPlayerInPlayToHand_Click()
    TransferCardFromLstToLst lstPlayerInPlay, userInPlayCards, userInPlayPos, lstPlayerHand, userHandCards, userHandPos
    userHandCards(userHandPos - 1, 4) = txtTypeDeck.text
    frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
    framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
End Sub

Private Sub cmdPlayerPlayCard_Click()
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstPlayerInPlay, userInPlayCards, userInPlayPos
  frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
End Sub

Private Sub cmdPlayExertedCard_Click()
  TransferCardFromLstToLst lstExertion, userExertion, userExertionPos, lstPlayerInPlay, userInPlayCards, userInPlayPos
  frameExertion.Caption = "Exertion (" + Str$(userHandPos) + ")"
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
End Sub

Private Sub cmdRemovePreGame_Click()
  RemoveCardFromList lstPreGame, userPreGameCards, userPrePos
End Sub

Private Sub cmdRollDice_Click()
  Randomize Timer
  txtNumberRolled.text = Int(Rnd() * txtNumSidesDice.text) + 1
End Sub

Private Sub cmdOpponetDiscardPlay_Click()
  TransferCardFromLstToLst lstOpponetDiscard, oppDiscardCards, oppDiscardPos, lstOpponetInPlay, oppInPlayCards, oppInPlayPos
End Sub

Private Sub cmdOpponetDiscardRemoveAll_Click()
  lstOpponetDiscard.Clear
  oppDiscardPos = 0
End Sub

Private Sub cmdOpponetInPlayDiscard_Click()
  TransferCardFromLstToLst lstOpponetInPlay, oppInPlayCards, oppInPlayPos, lstOpponetDiscard, oppDiscardCards, oppDiscardPos
End Sub

Private Sub cmdOpponetDiscardRemove_Click()
  RemoveCardFromList lstOpponetDiscard, oppDiscardCards, oppDiscardPos
End Sub

Private Sub cmdOpponetInPlayRemove_Click()
  RemoveCardFromList lstOpponetInPlay, oppInPlayCards, oppInPlayPos
End Sub

Private Sub cmdShowAll_Click()
  datCardNames.Recordset.Filter = ""
End Sub

Private Sub cmdShowBack_Click()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileName.text, 2)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub cmdShowFront_Click()
  Call txtFileName_Change
End Sub

Private Sub cmdShowHideSortFilter_Click()
  If (frameSortFilter.Visible = True) Then
    frameSortFilter.Visible = False
  Else
    frameSortFilter.Visible = True
  End If
End Sub

Private Sub cmdShowSet_Click()
  If (cmboSet.List(cmboSet.ListIndex) = "ALL") Then
    datCardNames.Recordset.Filter = ""
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "'"
  End If
End Sub


Private Sub cmdShowXToHand_Click()
  Dim j As Integer
  Dim k As Integer
  
  j = 0
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If userDeckPos > 0 Then
    While (j < lstTopXCards.ListCount)
      If (lstTopXCards.Selected(j) = True) Then
        lstTurnTranscript.AddItem "            " + lstTopXCards.List(j)
        k = userDeckPos - j - 1
        DisplayCardById (userDeckCards(k, 1))

        lstPlayerHand.AddItem (txtCardNameDeck.text)
        userHandCards(userHandPos, 1) = userDeckCards(k, 1)
        userHandCards(userHandPos, 2) = userDeckCards(k, 2)
        userHandCards(userHandPos, 3) = userDeckCards(k, 3)
        userHandCards(userHandPos, 4) = txtTypeDeck.text
        userHandPos = userHandPos + 1
  
        While (k < userDeckPos)
          userDeckCards(k, 1) = userDeckCards(k + 1, 1)
          userDeckCards(k, 2) = userDeckCards(k + 1, 2)
          userDeckCards(k, 3) = userDeckCards(k + 1, 3)
          k = k + 1
        Wend
        userDeckPos = userDeckPos - 1
        lstTopXCards.RemoveItem (j)
        lstDeck.RemoveItem (j)
        j = j - 1
        frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
        frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
        cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub cmdShuffleEndurance_Click()
  Dim tempCounter As Integer
  Dim a As Integer
  Dim b As Integer
  Dim j As Integer
  
  Randomize Timer
  For tempCounter = 0 To userDeckPos - 1
    userDeckCards(tempCounter, 2) = Rnd()
  Next tempCounter
  
  For a = 0 To userDeckPos
    For b = 0 To userDeckPos - 2
      If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
        Dim tempCard(3)
        tempCard(1) = userDeckCards(b, 1)
        tempCard(2) = userDeckCards(b, 2)
        tempCard(3) = userDeckCards(b, 3)
          
        userDeckCards(b, 1) = userDeckCards(b + 1, 1)
        userDeckCards(b, 2) = userDeckCards(b + 1, 2)
        userDeckCards(b, 3) = userDeckCards(b + 1, 3)
          
        userDeckCards(b + 1, 1) = tempCard(1)
        userDeckCards(b + 1, 2) = tempCard(2)
        userDeckCards(b + 1, 3) = tempCard(3)
      End If
    Next b
  Next a
          
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3) 'txtCardName.text
    j = j - 1
  Wend
End Sub

Private Sub cmdSortName_Click()
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub cmdSortSet_Click()
  datCardNames.Recordset.Sort = "Set, Name"
End Sub

Private Sub cmdStealSelectedCard_Click()
  TransferCardFromLstToLst lstOpponetInPlay, oppInPlayCards, oppInPlayPos, lstPlayerInPlay, userInPlayCards, userInPlayPos
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
End Sub

Private Sub cmdTopXCardsFinished_Click()
  frameTopXCards.Visible = False
End Sub

Private Sub cmdToTempStorage_Click()
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstTempStorage, userTempStorage, userTempStoragePos
  frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
  frameTempStorage.Caption = "Dojo (" + Str$(userTempStoragePos) + ")"
End Sub

Private Sub cmdUserPreGameToPlay_Click()
  TransferCardFromLstToLst lstPreGame, userPreGameCards, userPrePos, lstPlayerInPlay, userInPlayCards, userInPlayPos
  frmPreGame.Caption = "Pre-Game (" + Str$(userPrePos) + ")"
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
End Sub

Private Sub dataCards_DblClick()
  Call cmdOpponetPlay_Click
End Sub

Private Sub DojoMenu_Click()
  If frameTempStorage.Visible = True Then
    frameTempStorage.Visible = False
  Else
    frameTempStorage.Visible = True
  End If
End Sub

Private Sub ExitMenu_Click()
  Unload frmPlayGameEmail
End Sub


Private Sub Form_DragDrop(Source As Control, X As Single, Y As Single)
  If moveCardDetails = True Then
    moveCardDetails = False
    frameCardDetailsDeck.Move X - dx, Y - dy
    frameCardDetailsDB.Move X - dx, Y - dy
  Else
    Source.Move X - dx, Y - dy
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Open App.Path + "\playemailsettings.txt" For Output As #1
  Write #1, frmPlayGameEmail.Top, frmPlayGameEmail.Left, frmPlayGameEmail.Height, frmPlayGameEmail.Width, frmPlayGameEmail.WindowState
  Write #1, frameButtons.Visible, frameButtons.Left, frameButtons.Top
  Write #1, frameCardDetailsDB.Visible, frameCardDetailsDB.Left, frameCardDetailsDB.Top
  Write #1, frameCardDetailsDeck.Visible, frameCardDetailsDeck.Left, frameCardDetailsDeck.Top
  Write #1, frameCards.Visible, frameCards.Left, frameCards.Top, frameCards.Height, frameCards.Width
  Write #1, frameDeck.Visible, frameDeck.Left, frameDeck.Top, frameDeck.Height, frameDeck.Width
  Write #1, frameExertion.Visible, frameExertion.Left, frameExertion.Top, frameExertion.Height, frameExertion.Width
  Write #1, frameHand.Visible, frameHand.Left, frameHand.Top, frameHand.Height, frameHand.Width
  Write #1, frameOppInPlay.Visible, frameOppInPlay.Left, frameOppInPlay.Top, frameOppInPlay.Height, frameOppInPlay.Width
  Write #1, frameOpponentDiscard.Visible, frameOpponentDiscard.Left, frameOpponentDiscard.Top, frameOpponentDiscard.Height, frameOpponentDiscard.Width
  Write #1, frameOppPreGame.Visible, frameOppPreGame.Left, frameOppPreGame.Top, frameOppPreGame.Height, frameOppPreGame.Width
  Write #1, frameOppStats.Visible, frameOppStats.Left, frameOppStats.Top
  Write #1, framePlayerDiscard.Visible, framePlayerDiscard.Left, framePlayerDiscard.Top, framePlayerDiscard.Height, framePlayerDiscard.Width
  Write #1, framePlayerInPlay.Visible, framePlayerInPlay.Left, framePlayerInPlay.Top, framePlayerInPlay.Height, framePlayerInPlay.Width
  Write #1, frameRollDice.Visible, frameRollDice.Left, frameRollDice.Top
  Write #1, frameSortFilter.Visible, frameSortFilter.Left, frameSortFilter.Top
  Write #1, frameTempStorage.Visible, frameTempStorage.Left, frameTempStorage.Top, frameTempStorage.Height, frameTempStorage.Width
  Write #1, frameTopXCards.Visible, frameTopXCards.Left, frameTopXCards.Top, frameTopXCards.Height, frameTopXCards.Width
  Write #1, frameTurn.Visible, frameTurn.Left, frameTurn.Top
  Write #1, frmPreGame.Visible, frmPreGame.Left, frmPreGame.Top, frmPreGame.Height, frmPreGame.Width
  Write #1, frmRemovedFromGame.Visible, frmRemovedFromGame.Left, frmRemovedFromGame.Top, frmRemovedFromGame.Height, frmRemovedFromGame.Width
  
  Write #1, lstPreGame.Width, lstPreGame.Height, cmdRemovePreGame.Top, cmdUserPreGameToPlay.Top
  Write #1, lstOppPreGame.Width, lstOppPreGame.Height, cmdOppPreGameRemoveCard.Top
  Write #1, lstOpponetDiscard.Width, lstOpponetDiscard.Height, cmdOpponetDiscardRemove.Top, cmdOpponetDiscardPlay.Top, cmdOpponetDiscardRemoveAll.Top
  Write #1, lstOpponetInPlay.Width, lstOpponetInPlay.Height, cmdOpponetInPlayRemove.Top, cmdOpponetInPlayDiscard.Top, cmdStealSelectedCard.Top
  Write #1, lstTopXCards.Width, lstTopXCards.Height, cmdShowXToHand.Top, cmdTopXCardsFinished.Top, lblTopCardHelp.Top
  Write #1, lstPlayerRemovedFromGame.Width, lstPlayerRemovedFromGame.Height, cmdPlayerFromRemovedGameToPlay.Top
  Write #1, lstTempStorage.Width, lstTempStorage.Height, cmdMoveToHand.Top, cmdFromDojoToDiscard.Top
  Write #1, lstPlayerDiscard.Width, lstPlayerDiscard.Height, cmd1CardDiscardToEndurance.Top, cmdPlayerDiscardToPlay.Top, cmdPlayerDiscardRemoveGame.Top
  Write #1, lstPlayerInPlay.Width, lstPlayerInPlay.Height, cmdPlayerInPlayToHand.Top, cmdPlayerInPlayDiscard.Top
  Write #1, lstPlayerHand.Width, lstPlayerHand.Height, cmdPlayerDrawCard.Top, cmdPlayerDiscard.Top, cmdToTempStorage.Top; cmdPlayerPlayCard.Top, cmdDiscardToTopOfDeck.Top, cmdDiscardToBottomOfDeck.Top
  Write #1, dataCards.Width, dataCards.Height, cmdOpponetPlay.Top, cmdOpponetDiscard.Top, cmdAddToOppPreGame.Top, cmdShowHideSortFilter.Top
  Write #1, lstDeck.Width, lstDeck.Height, cmdPlaceCardInHand.Top, cmdMoveCardToTopDeck.Top
  Write #1, lstExertion.Width, lstExertion.Height, cmdExert1Card.Top, cmdExert5Cards.Top, cmdPlayExertedCard.Top, cmdDiscardExertedCards.Top
  Close #1
End Sub

Private Sub frameButtons_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  frameButtons.Drag
End Sub

Private Sub frameCardDetailsDB_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  moveCardDetails = True
  dx = X
  dy = Y
  frameCardDetailsDB.Drag
End Sub

Private Sub frameCardDetailsDeck_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  moveCardDetails = True
  dx = X
  dy = Y
  frameCardDetailsDeck.Drag
End Sub

Private Sub frameCards_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameCards.Drag
  End If
End Sub

Private Sub frameCards_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameCards.Move frameCards.Left, frameCards.Top, frameCards.Width + (X - frameCards.Width), frameCards.Height + (Y - frameCards.Height)
    dataCards.Width = frameCards.Width - 240
    dataCards.Height = frameCards.Height - 1200
    cmdOpponetPlay.Top = frameCards.Height - 855
    cmdOpponetDiscard.Top = frameCards.Height - 855
    cmdAddToOppPreGame.Top = frameCards.Height - 615
    cmdShowHideSortFilter.Top = frameCards.Height - 375
End Sub

Private Sub frameDeck_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameDeck.Drag
  End If
End Sub

Private Sub frameDeck_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameDeck.Move frameDeck.Left, frameDeck.Top, frameDeck.Width + (X - frameDeck.Width), frameDeck.Height + (Y - frameDeck.Height)
    lstDeck.Width = frameDeck.Width - 240
    lstDeck.Height = frameDeck.Height - 990
    cmdPlaceCardInHand.Top = frameDeck.Height - 615
    cmdMoveCardToTopDeck.Top = frameDeck.Height - 615
End Sub

Private Sub frameExertion_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameExertion.Drag
  End If
End Sub

Private Sub frameExertion_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameExertion.Move frameExertion.Left, frameExertion.Top, frameExertion.Width + (X - frameExertion.Width), frameExertion.Height + (Y - frameExertion.Height)
    lstExertion.Width = frameExertion.Width - 240
    lstExertion.Height = frameExertion.Height - 1440
    cmdExert1Card.Top = frameExertion.Height - 1095
    cmdExert5Cards.Top = frameExertion.Height - 855
    cmdPlayExertedCard.Top = frameExertion.Height - 615
    cmdDiscardExertedCards.Top = frameExertion.Height - 375
End Sub

Private Sub frameHand_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameHand.Drag
  End If
End Sub

Private Sub frameHand_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameHand.Move frameHand.Left, frameHand.Top, frameHand.Width + (X - frameHand.Width), frameHand.Height + (Y - frameHand.Height)
    lstPlayerHand.Width = frameHand.Width - 240
    lstPlayerHand.Height = frameHand.Height - 1485
    cmdPlayerDrawCard.Top = frameHand.Height - 1095
    cmdPlayerDiscard.Top = frameHand.Height - 1095
    cmdToTempStorage.Top = frameHand.Height - 855
    cmdPlayerPlayCard.Top = frameHand.Height - 855
    cmdDiscardToTopOfDeck.Top = frameHand.Height - 615
    cmdDiscardToBottomOfDeck.Top = frameHand.Height - 375
End Sub

Private Sub frameOppInPlay_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameOppInPlay.Drag
  End If
End Sub

Private Sub frameOppInPlay_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameOppInPlay.Move frameOppInPlay.Left, frameOppInPlay.Top, frameOppInPlay.Width + (X - frameOppInPlay.Width), frameOppInPlay.Height + (Y - frameOppInPlay.Height)
    lstOpponetInPlay.Width = frameOppInPlay.Width - 240
    lstOpponetInPlay.Height = frameOppInPlay.Height - 1500
    cmdOpponetInPlayRemove.Top = frameOppInPlay.Height - 1095
    cmdOpponetInPlayDiscard.Top = frameOppInPlay.Height - 1095
    cmdStealSelectedCard.Top = frameOppInPlay.Height - 615
End Sub

Private Sub frameOpponentDiscard_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameOpponentDiscard.Drag
  End If
End Sub

Private Sub frameOpponentDiscard_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameOpponentDiscard.Move frameOpponentDiscard.Left, frameOpponentDiscard.Top, frameOpponentDiscard.Width + (X - frameOpponentDiscard.Width), frameOpponentDiscard.Height + (Y - frameOpponentDiscard.Height)
    lstOpponetDiscard.Width = frameOpponentDiscard.Width - 240
    lstOpponetDiscard.Height = frameOpponentDiscard.Height - 735
    cmdOpponetDiscardRemove.Top = frameOpponentDiscard.Height - 375
    cmdOpponetDiscardPlay.Top = frameOpponentDiscard.Height - 375
    cmdOpponetDiscardRemoveAll.Top = frameOpponentDiscard.Height - 375
End Sub

Private Sub frameOppPreGame_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameOppPreGame.Drag
  End If
End Sub

Private Sub frameOppPreGame_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameOppPreGame.Move frameOppPreGame.Left, frameOppPreGame.Top, frameOppPreGame.Width + (X - frameOppPreGame.Width), frameOppPreGame.Height + (Y - frameOppPreGame.Height)
    lstOppPreGame.Width = frameOppPreGame.Width - 240
    lstOppPreGame.Height = frameOppPreGame.Height - 795
    cmdOppPreGameRemoveCard.Top = frameOppPreGame.Height - 495
End Sub

Private Sub frameOppStats_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  frameOppStats.Drag
End Sub

Private Sub framePlayerDiscard_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  framePlayerDiscard.Drag
  End If
End Sub

Private Sub framePlayerDiscard_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    framePlayerDiscard.Move framePlayerDiscard.Left, framePlayerDiscard.Top, framePlayerDiscard.Width + (X - framePlayerDiscard.Width), framePlayerDiscard.Height + (Y - framePlayerDiscard.Height)
    lstPlayerDiscard.Width = framePlayerDiscard.Width - 240
    lstPlayerDiscard.Height = framePlayerDiscard.Height - 1050
    cmd1CardDiscardToEndurance.Top = framePlayerDiscard.Height - 735
    cmdPlayerDiscardToPlay.Top = framePlayerDiscard.Height - 735
    cmdPlayerDiscardRemoveGame.Top = framePlayerDiscard.Height - 735
End Sub

Private Sub framePlayerInPlay_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  framePlayerInPlay.Drag
  End If
End Sub

Private Sub framePlayerInPlay_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    framePlayerInPlay.Move framePlayerInPlay.Left, framePlayerInPlay.Top, framePlayerInPlay.Width + (X - framePlayerInPlay.Width), framePlayerInPlay.Height + (Y - framePlayerInPlay.Height)
    lstPlayerInPlay.Width = framePlayerInPlay.Width - 240
    lstPlayerInPlay.Height = framePlayerInPlay.Height - 855
    cmdPlayerInPlayToHand.Top = framePlayerInPlay.Height - 615
    cmdPlayerInPlayDiscard.Top = framePlayerInPlay.Height - 615
End Sub

Private Sub frameRollDice_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  frameRollDice.Drag
End Sub

Private Sub frameSortFilter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  frameSortFilter.Drag
End Sub

Private Sub frameTempStorage_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameTempStorage.Drag
  End If
End Sub

Private Sub frameTempStorage_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameTempStorage.Move frameTempStorage.Left, frameTempStorage.Top, frameTempStorage.Width + (X - frameTempStorage.Width), frameTempStorage.Height + (Y - frameTempStorage.Height)
    lstTempStorage.Width = frameTempStorage.Width - 240
    lstTempStorage.Height = frameTempStorage.Height - 855
    cmdMoveToHand.Top = frameTempStorage.Height - 615
    cmdFromDojoToDiscard.Top = frameTempStorage.Height - 615
End Sub

Private Sub frameTopXCards_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frameTopXCards.Drag
  End If
End Sub

Private Sub frameTopXCards_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frameTopXCards.Move frameTopXCards.Left, frameTopXCards.Top, frameTopXCards.Width + (X - frameTopXCards.Width), frameTopXCards.Height + (Y - frameTopXCards.Height)
    lstTopXCards.Width = frameTopXCards.Width - 240
    lstTopXCards.Height = frameTopXCards.Height - 1440
    cmdShowXToHand.Top = frameTopXCards.Height - 615
    cmdTopXCardsFinished.Top = frameTopXCards.Height - 615
    lblTopCardHelp.Top = frameTopXCards.Height - 1095
End Sub

Private Sub frameTurn_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  frameTurn.Drag
End Sub

Private Sub frmPreGame_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frmPreGame.Drag
  End If
End Sub

Private Sub frmPreGame_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frmPreGame.Move frmPreGame.Left, frmPreGame.Top, frmPreGame.Width + (X - frmPreGame.Width), frmPreGame.Height + (Y - frmPreGame.Height)
    lstPreGame.Width = frmPreGame.Width - 240
    lstPreGame.Height = frmPreGame.Height - 990
    cmdRemovePreGame.Top = frmPreGame.Height - 615
    cmdUserPreGameToPlay.Top = frmPreGame.Height - 615
End Sub

Private Sub frmRemovedFromGame_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  dx = X
  dy = Y
  If Shift = 0 Then
  frmRemovedFromGame.Drag
  End If
End Sub

Private Sub frmRemovedFromGame_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    frmRemovedFromGame.Move frmRemovedFromGame.Left, frmRemovedFromGame.Top, frmRemovedFromGame.Width + (X - frmRemovedFromGame.Width), frmRemovedFromGame.Height + (Y - frmRemovedFromGame.Height)
    lstPlayerRemovedFromGame.Width = frmRemovedFromGame.Width - 240
    lstPlayerRemovedFromGame.Height = frmRemovedFromGame.Height - 660
    cmdPlayerFromRemovedGameToPlay.Top = frmRemovedFromGame.Height - 375
End Sub

Private Sub LoadGameMenu_Click()
  Call LoadGame
End Sub

Public Sub DisplayClickedCard(ByRef tempLst As ListBox, ByRef tempArray() As Variant)
  Dim j As Integer
  j = 0
  While (j < Int(tempLst.ListCount))
    If (tempLst.Selected(j) = True) Then
      frameCardDetailsDB.Visible = False
      frameCardDetailsDeck.Visible = True
      DisplayCardById (tempArray(j, 1))
    End If
    j = j + 1
  Wend
End Sub

Private Sub lstDeck_DblClick()
  Call cmdPlaceCardInHand_Click
End Sub

Private Sub lstExertion_Click()
  DisplayClickedCard lstExertion, userExertion
End Sub

Private Sub lstExertion_DblClick()
  Call cmdPlayExertedCard_Click
End Sub

Private Sub lstOpponetDiscard_Click()
  DisplayClickedCard lstOpponetDiscard, oppDiscardCards
End Sub

Private Sub lstOpponetInPlay_Click()
  DisplayClickedCard lstOpponetInPlay, oppInPlayCards
End Sub

Private Sub cmdOpponetDiscard_Click()
  If Not datCardNames.Recordset.EOF Then
    lstOpponetDiscard.AddItem (dataCards.Columns("Name").Value)
    oppDiscardCards(oppDiscardPos, 1) = dataCards.Columns("ID").Value
    oppDiscardCards(oppDiscardPos, 2) = 1
    oppDiscardPos = oppDiscardPos + 1
  End If
End Sub

Private Sub cmdOpponetPlay_Click()
  If Not datCardNames.Recordset.EOF Then
    lstOpponetInPlay.AddItem (dataCards.Columns("Name").Value)
    oppInPlayCards(oppInPlayPos, 1) = dataCards.Columns("ID").Value
    oppInPlayCards(oppInPlayPos, 2) = 1
    oppInPlayPos = oppInPlayPos + 1
  End If
End Sub

Private Sub Form_Load()
  On Error Resume Next
  datCardDeck.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardDeck.RecordSource = "cards"
  datCardDeck.Refresh

  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh

    lstDeck.Clear
    lstPreGame.Clear
    lstPlayerHand.Clear
    lstPlayerDiscard.Clear
    lstExertion.Clear
    lstPlayerInPlay.Clear
    lstTempStorage.Clear
    lstOpponetInPlay.Clear
    lstOpponetDiscard.Clear
    lstOppPreGame.Clear
    oppPreGamePos = 0
    userDeckPos = 0
    userPrePos = 0
    userHandPos = 0
    userInPlayPos = 0
    userDiscardPos = 0
    userExertionPos = 0
    userTempStoragePos = 0
    oppInPlayPos = 0
    oppDiscardPos = 0
    lstPlayerRemovedFromGame.Clear
    userRemovedFromGamePos = 0
    moveCardDetails = False
  
    HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
 If txtFileName.text <> "" Then
    If Mid$(txtFileName.text, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(txtFileName.text)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + txtFileName.text)
    End If
  End If
  dataCards.Columns("ID").Visible = False
  dataCards.Columns("Set").Visible = False
  dataCards.Columns("Number").Visible = False
  dataCards.Columns("File Name").Visible = False
  dataCards.Columns("Other").Visible = False
  dataCards.Columns("Text").Visible = False
  dataCards.Columns("Type").Visible = False
  dataCards.Columns("Rarity").Visible = False
  datCardNames.Recordset.Sort = "Name"
  If (Dir(App.Path + "\playemailsettings.txt") <> "") Then
    Dim tempVisible, tempLeft, tempTop, tempWidth, tempWindowState, tempHeight, tempTop2, tempTop3
    Open App.Path + "\playemailsettings.txt" For Input As #1
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState
    frmPlayGameEmail.Top = tempVisible: frmPlayGameEmail.Left = tempLeft
    frmPlayGameEmail.Height = tempTop: frmPlayGameEmail.Width = tempWidth
    frmPlayGameEmail.WindowState = tempWindowState
    Input #1, tempVisible, tempLeft, tempTop
    frameButtons.Visible = tempVisible: frameButtons.Left = tempLeft: frameButtons.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop
    frameCardDetailsDB.Visible = tempVisible: frameCardDetailsDB.Left = tempLeft: frameCardDetailsDB.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop
    frameCardDetailsDeck.Visible = tempVisible: frameCardDetailsDeck.Left = tempLeft: frameCardDetailsDeck.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameCards.Height = tempHeight: frameCards.Width = tempWidth
    frameCards.Visible = tempVisible: frameCards.Left = tempLeft: frameCards.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameDeck.Height = tempHeight: frameDeck.Width = tempWidth
    frameDeck.Visible = tempVisible: frameDeck.Left = tempLeft: frameDeck.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameExertion.Height = tempHeight: frameExertion.Width = tempWidth
    frameExertion.Visible = tempVisible: frameExertion.Left = tempLeft: frameExertion.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameHand.Height = tempHeight: frameHand.Width = tempWidth
    frameHand.Visible = tempVisible: frameHand.Left = tempLeft: frameHand.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameOppInPlay.Height = tempHeight: frameOppInPlay.Width = tempWidth
    frameOppInPlay.Visible = tempVisible: frameOppInPlay.Left = tempLeft: frameOppInPlay.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameOpponentDiscard.Height = tempHeight: frameOpponentDiscard.Width = tempWidth
    frameOpponentDiscard.Visible = tempVisible: frameOpponentDiscard.Left = tempLeft: frameOpponentDiscard.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameOppPreGame.Height = tempHeight: frameOppPreGame.Width = tempWidth
    frameOppPreGame.Visible = tempVisible: frameOppPreGame.Left = tempLeft: frameOppPreGame.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop
    frameOppStats.Visible = tempVisible: frameOppStats.Left = tempLeft: frameOppStats.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    framePlayerDiscard.Height = tempHeight: framePlayerDiscard.Width = tempWidth
    framePlayerDiscard.Visible = tempVisible: framePlayerDiscard.Left = tempLeft: framePlayerDiscard.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    framePlayerInPlay.Height = tempHeight: framePlayerInPlay.Width = tempWidth
    framePlayerInPlay.Visible = tempVisible: framePlayerInPlay.Left = tempLeft: framePlayerInPlay.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop
    frameRollDice.Visible = tempVisible: frameRollDice.Left = tempLeft: frameRollDice.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop
    frameSortFilter.Visible = tempVisible: frameSortFilter.Left = tempLeft: frameSortFilter.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameTempStorage.Height = tempHeight: frameTempStorage.Width = tempWidth
    frameTempStorage.Visible = tempVisible: frameTempStorage.Left = tempLeft: frameTempStorage.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frameTopXCards.Height = tempHeight: frameTopXCards.Width = tempWidth
    frameTopXCards.Visible = tempVisible: frameTopXCards.Left = tempLeft: frameTopXCards.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop
    frameTurn.Visible = tempVisible: frameTurn.Left = tempLeft: frameTurn.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frmPreGame.Height = tempHeight: frmPreGame.Width = tempWidth
    frmPreGame.Visible = tempVisible: frmPreGame.Left = tempLeft: frmPreGame.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempHeight, tempWidth
    frmRemovedFromGame.Height = tempHeight: frmRemovedFromGame.Width = tempWidth
    frmRemovedFromGame.Visible = tempVisible: frmRemovedFromGame.Left = tempLeft: frmRemovedFromGame.Top = tempTop
    
    Input #1, tempVisible, tempLeft, tempTop, tempWidth
    lstPreGame.Width = tempVisible: lstPreGame.Height = tempLeft: cmdRemovePreGame.Top = tempTop: cmdUserPreGameToPlay.Top = tempWidth
    Input #1, tempVisible, tempLeft, tempTop
    lstOppPreGame.Width = tempVisible: lstOppPreGame.Height = tempLeft: cmdOppPreGameRemoveCard.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState
    lstOpponetDiscard.Width = tempVisible: lstOpponetDiscard.Height = tempLeft: cmdOpponetDiscardRemove.Top = tempTop
    cmdOpponetDiscardPlay.Top = tempWidth: cmdOpponetDiscardRemoveAll.Top = tempWindowState
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState
    lstOpponetInPlay.Width = tempVisible: lstOpponetInPlay.Height = tempLeft: cmdOpponetInPlayRemove.Top = tempTop
    cmdOpponetInPlayDiscard.Top = tempWidth: cmdStealSelectedCard.Top = tempWindowState
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState
    lstTopXCards.Width = tempVisible: lstTopXCards.Height = tempLeft: cmdShowXToHand.Top = tempTop
    cmdTopXCardsFinished.Top = tempWidth: lblTopCardHelp.Top = tempWindowState
    Input #1, tempVisible, tempLeft, tempTop
    lstPlayerRemovedFromGame.Width = tempVisible: lstPlayerRemovedFromGame.Height = tempLeft
    cmdPlayerFromRemovedGameToPlay.Top = tempTop
    Input #1, tempVisible, tempLeft, tempTop, tempWidth
    lstTempStorage.Width = tempVisible: lstTempStorage.Height = tempLeft
    cmdMoveToHand.Top = tempTop: cmdFromDojoToDiscard.Top = tempWidth
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState
    lstPlayerDiscard.Width = tempVisible: lstPlayerDiscard.Height = tempLeft: cmd1CardDiscardToEndurance.Top = tempTop
    cmdPlayerDiscardToPlay.Top = tempWidth: cmdPlayerDiscardRemoveGame.Top = tempWindowState
    Input #1, tempVisible, tempLeft, tempTop, tempWidth
    lstPlayerInPlay.Width = tempVisible: lstPlayerInPlay.Height = tempLeft
    cmdPlayerInPlayToHand.Top = tempTop: cmdPlayerInPlayDiscard.Top = tempWidth
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState, tempHeight, tempTop2, tempTop3
    lstPlayerHand.Width = tempVisible: lstPlayerHand.Height = tempLeft: cmdPlayerDrawCard.Top = tempTop
    cmdPlayerDiscard.Top = tempWidth: cmdToTempStorage.Top = tempWindowState: cmdPlayerPlayCard.Top = tempHeight
    cmdDiscardToTopOfDeck.Top = tempTop2: cmdDiscardToBottomOfDeck.Top = tempTop3
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState, tempHeight
    dataCards.Width = tempVisible: dataCards.Height = tempLeft: cmdOpponetPlay.Top = tempTop
    cmdOpponetDiscard.Top = tempWidth: cmdAddToOppPreGame.Top = tempWindowState: cmdShowHideSortFilter.Top = tempHeight
    Input #1, tempVisible, tempLeft, tempTop, tempWidth
    lstDeck.Width = tempVisible: lstDeck.Height = tempLeft
    cmdPlaceCardInHand.Top = tempTop: cmdMoveCardToTopDeck.Top = tempWidth
    Input #1, tempVisible, tempLeft, tempTop, tempWidth, tempWindowState, tempHeight
    lstExertion.Width = tempVisible: lstExertion.Height = tempLeft: cmdExert1Card.Top = tempTop
    cmdExert5Cards.Top = tempWidth: cmdPlayExertedCard.Top = tempWindowState: cmdDiscardExertedCards.Top = tempHeight
    Close #1
  End If

End Sub

Private Sub dataCards_Click()
  frameCardDetailsDB.Visible = True
  frameCardDetailsDeck.Visible = False
End Sub

Private Sub lstOpponetInPlay_DblClick()
  Call cmdOpponetInPlayDiscard_Click
End Sub

Private Sub lstOppPreGame_Click()
  DisplayClickedCard lstOppPreGame, oppPreGameCards
End Sub

Private Sub lstPlayerDiscard_Click()
  DisplayClickedCard lstPlayerDiscard, userDiscardCards
End Sub

Private Sub lstPlayerHand_Click()
  DisplayClickedCard lstPlayerHand, userHandCards
End Sub

Private Sub lstPlayerHand_DblClick()
  Call cmdPlayerPlayCard_Click
End Sub

Private Sub lstPlayerInPlay_Click()
  DisplayClickedCard lstPlayerInPlay, userInPlayCards
End Sub

Private Sub lstPlayerInPlay_DblClick()
  Call cmdPlayerInPlayDiscard_Click
End Sub

Private Sub lstPlayerRemovedFromGame_Click()
  DisplayClickedCard lstPlayerRemovedFromGame, userRemovedFromGameCards
End Sub

Private Sub lstPlayerRemovedFromGame_DblClick()
  Call cmdPlayerFromRemovedGameToPlay_Click
End Sub

Private Sub lstTempStorage_Click()
  DisplayClickedCard lstTempStorage, userTempStorage
End Sub

Private Sub lstTempStorage_DblClick()
  Call cmdMoveToHand_Click
End Sub

Private Sub lstTopXCards_Click()
  Dim j As Integer
  Dim k As Integer
  
  j = 0
  While (lstTopXCards.Selected(j) = False)
    j = j + 1
  Wend

  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  k = userDeckPos - j - 1
  DisplayCardById (userDeckCards(k, 1))

End Sub

Private Sub lstTopXCards_DblClick()
  Call cmdShowXToHand_Click
End Sub

Private Sub menuShowTopXCards_Click()
  Dim number As Variant
  Dim counter As Integer
  number = InputBox("Enter the number of cards to show (0 to cancel)", "Show X Cards From Deck")
  If Val(number) > 0 And userDeckPos > 0 Then
    lstTopXCards.Clear
    frameTopXCards.Caption = "Deck (Top " + Str$(number) + " cards)"
    frameTopXCards.Visible = True
    counter = userDeckPos - 1
    While counter >= userDeckPos - number
      lstTopXCards.AddItem userDeckCards(counter, 3) 'txtCardName.text
      counter = counter - 1
    Wend
  End If
End Sub

Private Sub menuSortHandByName_Click()
  Dim counter1 As Integer
  Dim counter2 As Integer

  For counter1 = 0 To userHandPos - 1
    For counter2 = 0 To userHandPos - 2
      If (userHandCards(counter2, 3) > userHandCards(counter2 + 1, 3)) Then
        Dim tempCard(4)
        tempCard(1) = userHandCards(counter2, 1)
        tempCard(2) = userHandCards(counter2, 2)
        tempCard(3) = userHandCards(counter2, 3)
        tempCard(4) = userHandCards(counter2, 4)
        
        userHandCards(counter2, 1) = userHandCards(counter2 + 1, 1)
        userHandCards(counter2, 2) = userHandCards(counter2 + 1, 2)
        userHandCards(counter2, 3) = userHandCards(counter2 + 1, 3)
        userHandCards(counter2, 4) = userHandCards(counter2 + 1, 4)
        
        userHandCards(counter2 + 1, 1) = tempCard(1)
        userHandCards(counter2 + 1, 2) = tempCard(2)
        userHandCards(counter2 + 1, 3) = tempCard(3)
        userHandCards(counter2 + 1, 4) = tempCard(4)
      End If
    Next counter2
  Next counter1
  
  lstPlayerHand.Clear
  For counter1 = 0 To userHandPos - 1
    lstPlayerHand.AddItem userHandCards(counter1, 3)
  Next counter1
End Sub

Private Sub menuSortHandByType_Click()
  Dim counter1 As Integer
  Dim counter2 As Integer
  
  For counter1 = 0 To userHandPos - 1
    For counter2 = 0 To userHandPos - 2
      If (userHandCards(counter2, 4) > userHandCards(counter2 + 1, 4)) Then
        Dim tempCard(4)
        tempCard(1) = userHandCards(counter2, 1)
        tempCard(2) = userHandCards(counter2, 2)
        tempCard(3) = userHandCards(counter2, 3)
        tempCard(4) = userHandCards(counter2, 4)
        
        userHandCards(counter2, 1) = userHandCards(counter2 + 1, 1)
        userHandCards(counter2, 2) = userHandCards(counter2 + 1, 2)
        userHandCards(counter2, 3) = userHandCards(counter2 + 1, 3)
        userHandCards(counter2, 4) = userHandCards(counter2 + 1, 4)
        
        userHandCards(counter2 + 1, 1) = tempCard(1)
        userHandCards(counter2 + 1, 2) = tempCard(2)
        userHandCards(counter2 + 1, 3) = tempCard(3)
        userHandCards(counter2 + 1, 4) = tempCard(4)
      End If
    Next counter2
  Next counter1
  
  lstPlayerHand.Clear
  For counter1 = 0 To userHandPos - 1
    lstPlayerHand.AddItem userHandCards(counter1, 3)
  Next counter1
End Sub

Private Sub NewGameMenu_Click()
  Call NewGame
End Sub

Private Sub OppPreGameShowHideMenu_Click()
  If frameOppPreGame.Visible = True Then
    frameOppPreGame.Visible = False
  Else
    frameOppPreGame.Visible = True
  End If
End Sub

Private Sub RemovedFromGameShowHideMenu_Click()
  If frmRemovedFromGame.Visible = True Then
    frmRemovedFromGame.Visible = False
  Else
    frmRemovedFromGame.Visible = True
  End If
End Sub

Private Sub RollDiceMenu_Click()
  If frameRollDice.Visible = True Then
    frameRollDice.Visible = False
  Else
    Randomize Timer
    frameRollDice.Visible = True
  End If
End Sub

Private Sub SaveGameAsMenu_Click()
  Call SaveGame(2)
End Sub

Private Sub SaveGameMenu_Click()
  Call SaveGame(1)
End Sub

Private Sub sortHandByTypeAndName_Click()
  Call menuSortHandByName_Click
  Call menuSortHandByType_Click
End Sub

Private Sub txtFileName_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileName.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub txtFileNameDeck_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCardDeck.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileNameDeck.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCardDeck.Picture = LoadPicture(FileName)
    Else
      HLCardDeck.Picture = LoadPicture(App.Path + "\" + txtSetDeck.text + "\" + FileName)
    End If
  End If
End Sub

Public Function getImageLocation(txtFileNames As String, nameToRetrive As Integer)
'  This function was created to deal with the whole "two card images in one field" issue
'nameToRetrive = 1 - Front, 2 - Back
  Dim counter As Long
  Dim foundSplit As Boolean
  Dim splitPos As Long
  foundSplit = False
  
  If (Left$(txtFileNames, 1) = "*") Then ' if first character is '*' then check for character '|'
    counter = 0
    While counter <= Len(txtFileNames)
      counter = counter + 1
      If Mid$(txtFileNames, counter, 1) = "|" Then ' '|' represents end of front image and start of back
        foundSplit = True
        splitPos = counter
      End If
    Wend
    
    If foundSplit Then ' if a break was found then...
      If nameToRetrive = 1 Then
        getImageLocation = Mid$(txtFileNames, 2, splitPos - 2)
      Else
        getImageLocation = Mid$(txtFileNames, splitPos + 1, Len(txtFileNames) - splitPos)
      End If
    Else ' if a break was not found, but a '*' was, assume somethings wrong and display empty.jpg
      getImageLocation = ""
    End If
  Else ' if a '*' is not found, assume only the front of the image is given
    If nameToRetrive = 1 Then
      getImageLocation = txtFileNames
    Else
      getImageLocation = ""
    End If
  End If
End Function

Private Sub lstDeck_Click()
  Dim j As Integer
  Dim k As Integer
  
  j = 0
  Do While j < lstDeck.ListCount
    If lstDeck.Selected(j) = True Then
      Exit Do
    End If
    j = j + 1
  Loop

  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If j < lstDeck.ListCount Then
    If lstDeck.Selected(j) = True Then
      k = userDeckPos - j - 1
      DisplayCardById (userDeckCards(k, 1))
    End If
  End If
End Sub


Private Sub lstPreGame_Click()
  DisplayClickedCard lstPreGame, userPreGameCards
End Sub

Public Sub DisplayCardById(idToDisplay)
  datCardDeck.Recordset.MoveFirst
  datCardDeck.Recordset.Find ("Id = '" + idToDisplay + "'")
End Sub

Public Sub NewGame()
  On Error Resume Next
  Dim cardlocation, temp1, temp2
  Dim tempCounter As Integer
  Dim a As Integer
  Dim b As Integer
  Dim j As Integer
  
  Randomize Timer
  CommonDialog1.FileName = ""
  CommonDialog1.ShowOpen
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog1.FileName <> "") Then
    txtSetDeck.DataField = ""
    txtFileNameDeck.DataField = ""
    frmLoading.Visible = True
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = False
    lstDeck.Clear
    lstPreGame.Clear
    lstPlayerHand.Clear
    lstPlayerDiscard.Clear
    lstExertion.Clear
    lstPlayerInPlay.Clear
    lstTempStorage.Clear
    lstOpponetInPlay.Clear
    lstOpponetDiscard.Clear
    lstOppPreGame.Clear
    oppPreGamePos = 0
    userDeckPos = 0
    userPrePos = 0
    userHandPos = 0
    userInPlayPos = 0
    userDiscardPos = 0
    userExertionPos = 0
    userTempStoragePos = 0
    oppInPlayPos = 0
    oppDiscardPos = 0
    lstPlayerRemovedFromGame.Clear
    userRemovedFromGamePos = 0
    
    Open CommonDialog1.FileName For Input As #1
    While Not EOF(1)
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      Input #1, cardlocation, temp1, temp2
      If (cardlocation = "DECK") Then
        tempCounter = 0
        DisplayCardById temp1
        While (tempCounter < temp2)
          userDeckCards(userDeckPos, 1) = temp1
          userDeckCards(userDeckPos, 2) = Rnd()
          userDeckCards(userDeckPos, 3) = txtCardNameDeck.text
          userDeckPos = userDeckPos + 1
          tempCounter = tempCounter + 1
        Wend
      End If
      If (cardlocation = "PREGAME") Then
        tempCounter = 0
        DisplayCardById (temp1)
        While (tempCounter < temp2)
          userPreGameCards(userPrePos, 1) = temp1
          userPreGameCards(userPrePos, 2) = 1
          userPreGameCards(userPrePos, 3) = txtCardNameDeck.text
          lstPreGame.AddItem (txtCardNameDeck.text)
          userPrePos = userPrePos + 1
          tempCounter = tempCounter + 1
        Wend
      End If
    Wend
    Close #1
    
    For a = 0 To userDeckPos
      For b = 0 To userDeckPos - 2
        If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
          DoEvents
          StatusBox.Left = StatusBox.Left + 1
          If StatusBox.Left >= frmLoading.Width Then
            StatusBox.Left = 0
          End If
          Dim tempCard(10)
          tempCard(1) = userDeckCards(b, 1)
          tempCard(2) = userDeckCards(b, 2)
          tempCard(3) = userDeckCards(b, 3)
          
          userDeckCards(b, 1) = userDeckCards(b + 1, 1)
          userDeckCards(b, 2) = userDeckCards(b + 1, 2)
          userDeckCards(b, 3) = userDeckCards(b + 1, 3)
          
          userDeckCards(b + 1, 1) = tempCard(1)
          userDeckCards(b + 1, 2) = tempCard(2)
          userDeckCards(b + 1, 3) = tempCard(3)
        End If
      Next b
    Next a
          
    j = userDeckPos - 1
    While j >= 0
      DoEvents
      lstDeck.AddItem (userDeckCards(j, 3))
      j = j - 1
    Wend
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
    frmLoading.Visible = False
    txtSetDeck.DataField = "Set"
    txtFileNameDeck.DataField = "File Name"
    frameCardDetailsDeck.Visible = True
  End If
End Sub

Public Sub SaveGame(choice As Integer)
' choice  = 1 - Save, choice = 2 - Save As
  On Error Resume Next
  Dim j As Integer
  
  If (CommonDialog2.FileName = "") Or choice = 2 Then
    CommonDialog2.ShowSave
    If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  End If
  If (CommonDialog2.FileName <> "") Then
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = True
    Open CommonDialog2.FileName For Output As #1
    
    Write #1, "USERTURNNUM", txtYourTurnNum.text, ""
    Write #1, "USERABILITY", txtYourAbility.text, ""
    Write #1, "OPPNAME", txtOpponetNameEmail.text, ""
    Write #1, "OPPABILITY", txtOpponetAbility.text, ""
    Write #1, "OPPTURNNUM", txtOpponetTurnNum.text, ""
    
    j = 0
    While j < userDeckPos
      Write #1, "USERDECK", userDeckCards(j, 1), userDeckCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userPrePos
      Write #1, "USERPREGAME", userPreGameCards(j, 1), userPreGameCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userHandPos
      Write #1, "USERHAND", userHandCards(j, 1), userHandCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userDiscardPos
      Write #1, "USERDISCARD", userDiscardCards(j, 1), userDiscardCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userExertionPos
      Write #1, "USEREXERTION", userExertion(j, 1), userExertion(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userInPlayPos
      Write #1, "USERINPLAY", userInPlayCards(j, 1), userInPlayCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userRemovedFromGamePos
      Write #1, "USERREMOVEDFROMGAME", userRemovedFromGameCards(j, 1), userRemovedFromGameCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userTempStoragePos
      Write #1, "USERTEMPSTORAGE", userTempStorage(j, 1), userTempStorage(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < oppInPlayPos
      Write #1, "OPPINPLAY", oppInPlayCards(j, 1), oppInPlayCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < oppDiscardPos
      Write #1, "OPPDISCARD", oppDiscardCards(j, 1), oppDiscardCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < oppPreGamePos
      Write #1, "OPPPREGAME", oppPreGameCards(j, 1), oppPreGameCards(j, 2)
      j = j + 1
    Wend
       
    Close #1
    MsgBox "Your game has been saved.", vbOKOnly, "Save Game"
  End If
End Sub

Public Sub LoadGame()
  On Error Resume Next
  Dim cardlocation, temp1, temp2
  Dim a As Integer
  Dim b As Integer
  Dim j As Integer
  
  CommonDialog2.FileName = ""
  CommonDialog2.ShowOpen
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog2.FileName <> "") Then
    frmLoading.Visible = True
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = False
    txtSetDeck.DataField = ""
    txtFileNameDeck.DataField = ""
    lstDeck.Clear
    lstPreGame.Clear
    lstPlayerHand.Clear
    lstPlayerDiscard.Clear
    lstExertion.Clear
    lstPlayerInPlay.Clear
    lstTempStorage.Clear
    lstOpponetInPlay.Clear
    lstOpponetDiscard.Clear
    lstOppPreGame.Clear
    oppPreGamePos = 0
    userDeckPos = 0
    userPrePos = 0
    userHandPos = 0
    userInPlayPos = 0
    userDiscardPos = 0
    userExertionPos = 0
    userTempStoragePos = 0
    oppInPlayPos = 0
    oppDiscardPos = 0
    lstPlayerRemovedFromGame.Clear
    userRemovedFromGamePos = 0

    Open CommonDialog2.FileName For Input As #1
    While Not EOF(1)
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      Input #1, cardlocation, temp1, temp2
      
      If (cardlocation = "USERTURNNUM") Then
        txtYourTurnNum.text = temp1
      End If
      If (cardlocation = "USERABILITY") Then
        txtYourAbility.text = temp1
      End If
      If (cardlocation = "OPPNAME") Then
        txtOpponetNameEmail.text = temp1
      End If
      If (cardlocation = "OPPABILITY") Then
        txtOpponetAbility.text = temp1
      End If
      If (cardlocation = "OPPTURNNUM") Then
        txtOpponetTurnNum.text = temp1
      End If
      
      If (cardlocation = "USERDECK") Then
        DisplayCardById temp1
        userDeckCards(userDeckPos, 1) = temp1
        userDeckCards(userDeckPos, 2) = temp2
        userDeckCards(userDeckPos, 3) = txtCardNameDeck.text
        userDeckPos = userDeckPos + 1
      End If
      If (cardlocation = "USERPREGAME") Then
        DisplayCardById (temp1)
        userPreGameCards(userPrePos, 1) = temp1
        userPreGameCards(userPrePos, 2) = temp2
        userPreGameCards(userPrePos, 3) = txtCardNameDeck.text
        lstPreGame.AddItem (txtCardNameDeck.text)
        userPrePos = userPrePos + 1
      End If
      If (cardlocation = "USERHAND") Then
        DisplayCardById (temp1)
        userHandCards(userHandPos, 1) = temp1
        userHandCards(userHandPos, 2) = temp2
        userHandCards(userHandPos, 3) = txtCardNameDeck.text
        userHandCards(userHandPos, 4) = txtTypeDeck.text
        lstPlayerHand.AddItem (txtCardNameDeck.text)
        userHandPos = userHandPos + 1
      End If
      If (cardlocation = "USERDISCARD") Then
        DisplayCardById (temp1)
        userDiscardCards(userDiscardPos, 1) = temp1
        userDiscardCards(userDiscardPos, 2) = temp2
        userDiscardCards(userDiscardPos, 3) = txtCardNameDeck.text
        lstPlayerDiscard.AddItem (txtCardNameDeck.text)
        userDiscardPos = userDiscardPos + 1
      End If
      
      If (cardlocation = "USERREMOVEDFROMGAME") Then
        DisplayCardById (temp1)
        userRemovedFromGameCards(userRemovedFromGamePos, 1) = temp1
        userRemovedFromGameCards(userRemovedFromGamePos, 2) = temp2
        userRemovedFromGameCards(userRemovedFromGamePos, 3) = txtCardNameDeck.text
        lstPlayerRemovedFromGame.AddItem (txtCardNameDeck.text)
        userRemovedFromGamePos = userRemovedFromGamePos + 1
      End If
      
      If (cardlocation = "USEREXERTION") Then
        DisplayCardById (temp1)
        userExertion(userExertionPos, 1) = temp1
        userExertion(userExertionPos, 2) = temp2
        userExertion(userExertionPos, 3) = txtCardNameDeck.text
        lstExertion.AddItem (txtCardNameDeck.text)
        userExertionPos = userExertionPos + 1
      End If
      If (cardlocation = "USERINPLAY") Then
        DisplayCardById (temp1)
        userInPlayCards(userInPlayPos, 1) = temp1
        userInPlayCards(userInPlayPos, 2) = temp2
        userInPlayCards(userInPlayPos, 3) = txtCardNameDeck.text
        lstPlayerInPlay.AddItem (txtCardNameDeck.text)
        userInPlayPos = userInPlayPos + 1
      End If
      If (cardlocation = "USERTEMPSTORAGE") Then
        DisplayCardById (temp1)
        userTempStorage(userTempStoragePos, 1) = temp1
        userTempStorage(userTempStoragePos, 2) = temp2
        userTempStorage(userTempStoragePos, 3) = txtCardNameDeck.text
        lstTempStorage.AddItem (txtCardNameDeck.text)
        userTempStoragePos = userTempStoragePos + 1
      End If
      If (cardlocation = "OPPINPLAY") Then
        DisplayCardById (temp1)
        oppInPlayCards(oppInPlayPos, 1) = temp1
        oppInPlayCards(oppInPlayPos, 2) = temp2
        oppInPlayCards(oppInPlayPos, 3) = txtCardNameDeck.text
        lstOpponetInPlay.AddItem (txtCardNameDeck.text)
        oppInPlayPos = oppInPlayPos + 1
      End If
      If (cardlocation = "OPPDISCARD") Then
        DisplayCardById (temp1)
        oppDiscardCards(oppDiscardPos, 1) = temp1
        oppDiscardCards(oppDiscardPos, 2) = temp2
        oppDiscardCards(oppDiscardPos, 3) = txtCardNameDeck.text
        lstOpponetDiscard.AddItem (txtCardNameDeck.text)
        oppDiscardPos = oppDiscardPos + 1
      End If
      If (cardlocation = "OPPPREGAME") Then
        DisplayCardById (temp1)
        oppPreGameCards(oppPreGamePos, 1) = temp1
        oppPreGameCards(oppPreGamePos, 2) = temp2
        oppPreGameCards(oppPreGamePos, 3) = txtCardNameDeck.text
        lstOppPreGame.AddItem (txtCardNameDeck.text)
        oppPreGamePos = oppPreGamePos + 1
      End If
    Wend
    Close #1

    For a = 0 To userDeckPos
      For b = 0 To userDeckPos - 2
        If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
          DoEvents
          StatusBox.Left = StatusBox.Left + 1
          If StatusBox.Left >= frmLoading.Width Then
            StatusBox.Left = 0
          End If
          Dim tempCard(3)
          tempCard(1) = userDeckCards(b, 1)
          tempCard(2) = userDeckCards(b, 2)
          tempCard(3) = userDeckCards(b, 3)
          
          userDeckCards(b, 1) = userDeckCards(b + 1, 1)
          userDeckCards(b, 2) = userDeckCards(b + 1, 2)
          userDeckCards(b, 3) = userDeckCards(b + 1, 3)
          
          userDeckCards(b + 1, 1) = tempCard(1)
          userDeckCards(b + 1, 2) = tempCard(2)
          userDeckCards(b + 1, 3) = tempCard(3)
        End If
      Next b
    Next a
          
    j = userDeckPos - 1
    While j >= 0
      DoEvents
      lstDeck.AddItem userDeckCards(j, 3)
      j = j - 1
    Wend
    frmLoading.Visible = False
    frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
    frameTempStorage.Caption = "Dojo (" + Str$(userInPlayPos) + ")"
    framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
    framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
    frmRemovedFromGame.Caption = "Removed From Game (" + Str$(userRemovedFromGamePos) + ")"
    txtSetDeck.DataField = "Set"
    txtFileNameDeck.DataField = "File Name"
    frameCardDetailsDeck.Visible = True
  End If
End Sub

Private Sub txtSet_Change()
  Call txtFileName_Change
End Sub

Private Sub txtSetDeck_Change()
  Call txtFileNameDeck_Change
End Sub

Private Sub YourDeckMenu_Click()
  If frameDeck.Visible = True Then
    frameDeck.Visible = False
  Else
    frameDeck.Visible = True
  End If
End Sub

Private Sub YourPreGameMenu_Click()
  If frmPreGame.Visible = True Then
    frmPreGame.Visible = False
  Else
    frmPreGame.Visible = True
  End If
End Sub

Private Sub TransferCardFromLstToLst(ByRef sendingLst As ListBox, ByRef sendingArray() As Variant, ByRef sendingPos As Integer, ByRef receivingLst As ListBox, ByRef receivingArray(), ByRef receivingPos As Integer)
  Dim j As Integer
  Dim k As Integer
  
  j = 0
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If sendingPos > 0 Then
    While (j < sendingLst.ListCount)
      If sendingLst.Selected(j) = True Then
        k = j
        lstTurnTranscript.AddItem "            " + sendingLst.List(j)
        DisplayCardById (sendingArray(k, 1))
        receivingLst.AddItem (txtCardNameDeck.text)
        receivingArray(receivingPos, 1) = sendingArray(k, 1)
        receivingArray(receivingPos, 2) = sendingArray(k, 2)
        receivingArray(receivingPos, 3) = sendingArray(k, 3)
        receivingArray(receivingPos, 4) = sendingArray(k, 4)
        receivingPos = receivingPos + 1
  
        While (k < sendingPos)
          sendingArray(k, 1) = sendingArray(k + 1, 1)
          sendingArray(k, 2) = sendingArray(k + 1, 2)
          sendingArray(k, 3) = sendingArray(k + 1, 3)
          sendingArray(k, 4) = sendingArray(k + 1, 4)
          k = k + 1
        Wend
        sendingPos = sendingPos - 1
        sendingLst.RemoveItem (j)
        j = j - 1
      End If
      j = j + 1
    Wend
  End If
End Sub
