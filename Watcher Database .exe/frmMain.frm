VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Watcher Database"
   ClientHeight    =   5070
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   5070
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   Palette         =   "frmMain.frx":08CA
   Picture         =   "frmMain.frx":0BFD
   ScaleHeight     =   5070
   ScaleWidth      =   5070
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdTradeLists 
      Caption         =   "View &Trade List"
      DownPicture     =   "frmMain.frx":0F30
      Height          =   1455
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   240
      Width           =   1455
   End
   Begin VB.CommandButton cmdAbout 
      Caption         =   "&About"
      DownPicture     =   "frmMain.frx":26AF
      Height          =   1455
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton cmdMLEPrintingWizard 
      Caption         =   "&Print MLE Cards"
      DownPicture     =   "frmMain.frx":3E2E
      Height          =   1455
      Left            =   3360
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   240
      Width           =   1455
   End
   Begin VB.CommandButton cmdPatchDB 
      Caption         =   "Apply D&B Patch (.hdb file)"
      DownPicture     =   "frmMain.frx":55AD
      Height          =   1455
      Left            =   240
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton cmdShowPlayGameOnline 
      Caption         =   "Play Game (&Online)"
      DownPicture     =   "frmMain.frx":6D2C
      Height          =   1455
      Left            =   3360
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1800
      Width           =   1455
   End
   Begin VB.CommandButton Command4 
      Caption         =   "E&xit"
      DownPicture     =   "frmMain.frx":84AB
      Height          =   1455
      Left            =   3360
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Play Game (&Email/Chat Room)"
      DownPicture     =   "frmMain.frx":9C2A
      Height          =   1455
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1800
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Deck Editor"
      DownPicture     =   "frmMain.frx":B3A9
      Height          =   1455
      Left            =   240
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1800
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&View Collection"
      DownPicture     =   "frmMain.frx":CB28
      Height          =   1455
      Left            =   240
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuViewCollection 
         Caption         =   "View Collection"
         Shortcut        =   ^V
      End
      Begin VB.Menu menuViewTradeList 
         Caption         =   "View Trade List"
         Shortcut        =   ^T
      End
      Begin VB.Menu menuPrintCards 
         Caption         =   "Print MLE Cards"
         Shortcut        =   ^P
      End
      Begin VB.Menu menuDeckEditor 
         Caption         =   "Deck Editor"
         Shortcut        =   ^D
      End
      Begin VB.Menu menuPlayGameOffline 
         Caption         =   "Play Game (Email/Chat Room)"
         Shortcut        =   ^E
      End
      Begin VB.Menu menuPlayGameOnline 
         Caption         =   "Play Game (Online)"
         Shortcut        =   ^O
      End
      Begin VB.Menu menuDBPatch 
         Caption         =   "Apply Database Patch"
         Shortcut        =   ^B
      End
      Begin VB.Menu menuAbout 
         Caption         =   "About"
         Shortcut        =   ^A
      End
      Begin VB.Menu menuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Created By:           Eric Cavaliere
' Program Name:         Watcher Database
' Project File Name:    Highlander.vbp
' Project Started on:   August 05, 2002
' Name of this File:    main.frm
' This file started on: August 05, 2002
' Last Updated on:      22 October 2007

'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Sub cmdAbout_Click()
  Load frmAbout
  frmAbout.Show
End Sub

Private Sub cmdMLEPrintingWizard_Click()
  Load frmMLEPrintingWizard
  frmMLEPrintingWizard.Show
End Sub

Private Sub cmdPatchDB_Click()
  Load frmPatchDB
  frmPatchDB.Show
End Sub

Private Sub cmdShowPlayGameOnline_Click()
  Load frmPlayGameOnline
  frmPlayGameOnline.Show
End Sub

Private Sub cmdTradeLists_Click()
  Load frmTradeCards
  frmTradeCards.Show
End Sub

Private Sub Command1_Click()
  Load frmViewCollection
  frmViewCollection.Show
End Sub

Private Sub Command2_Click()
  Load frmCreateEditDeck
  frmCreateEditDeck.Show
End Sub

Private Sub Command3_Click()
  Load frmPlayGameEmail
  frmPlayGameEmail.Show
End Sub

Private Sub Command4_Click()
  End
End Sub

Private Sub Form_Unload(Cancel As Integer)
  End
End Sub

Private Sub menuAbout_Click()
  Call cmdAbout_Click
End Sub

Private Sub menuDBPatch_Click()
  Call cmdPatchDB_Click
End Sub

Private Sub menuDeckEditor_Click()
  Call Command2_Click
End Sub

Private Sub menuExit_Click()
  Call Command4_Click
End Sub

Private Sub menuPlayGameOffline_Click()
  Call Command3_Click
End Sub

Private Sub menuPlayGameOnline_Click()
  Call cmdShowPlayGameOnline_Click
End Sub

Private Sub menuPrintCards_Click()
  Call cmdMLEPrintingWizard_Click
End Sub

Private Sub menuViewCollection_Click()
  Call Command1_Click
End Sub

Private Sub menuViewTradeList_Click()
  Call cmdTradeLists_Click
End Sub
