VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTradeCards 
   Caption         =   "Display Trade List"
   ClientHeight    =   6615
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   9600
   LinkTopic       =   "Form1"
   ScaleHeight     =   6615
   ScaleWidth      =   9600
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstCardsYouWantID 
      Height          =   255
      Left            =   6840
      TabIndex        =   27
      Top             =   4680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ListBox lstCardsYouHaveID 
      Height          =   255
      Left            =   6840
      TabIndex        =   26
      Top             =   4320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ListBox lstTradeListID 
      Height          =   450
      Left            =   6840
      TabIndex        =   25
      Top             =   3720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Card Details"
      Height          =   3255
      Left            =   3240
      TabIndex        =   6
      Top             =   3120
      Width           =   3615
      Begin VB.TextBox txtRarity 
         DataField       =   "Rarity"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   16
         Top             =   1080
         Width           =   2415
      End
      Begin VB.TextBox txtFileName 
         DataField       =   "File Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   15
         Top             =   1800
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtSet 
         DataField       =   "Set"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   14
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox txtCardName 
         DataField       =   "Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   13
         Top             =   360
         Width           =   2415
      End
      Begin VB.TextBox txtNum 
         DataField       =   "Number"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1920
         TabIndex        =   12
         Top             =   960
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox txtType 
         DataField       =   "Type"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   11
         Top             =   1440
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         DataField       =   "Other"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2640
         TabIndex        =   10
         Top             =   720
         Width           =   855
      End
      Begin VB.TextBox txtCardText 
         DataField       =   "Text"
         DataSource      =   "datCardNames"
         Height          =   1245
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         Top             =   1800
         Width           =   2415
      End
      Begin VB.CommandButton cmdSelectFile 
         Caption         =   "Select File"
         Height          =   255
         Left            =   2520
         TabIndex        =   8
         Top             =   1800
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox txtID 
         DataField       =   "ID"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Text            =   "Text2"
         Top             =   2520
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label lblRarity 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   1080
         Width           =   450
      End
      Begin VB.Label lblSet 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   720
         Width           =   285
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   360
         Width           =   840
      End
      Begin VB.Label lblNumber 
         AutoSize        =   -1  'True
         Caption         =   "Quantity:"
         Height          =   195
         Left            =   2160
         TabIndex        =   21
         Top             =   720
         Visible         =   0   'False
         Width           =   630
      End
      Begin VB.Label lblType 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   1440
         Width           =   405
      End
      Begin VB.Label lblOther 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   2160
         TabIndex        =   19
         Top             =   720
         Width           =   435
      End
      Begin VB.Label lblCardText 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label lblFileName 
         AutoSize        =   -1  'True
         Caption         =   "File Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   1800
         Visible         =   0   'False
         Width           =   750
      End
   End
   Begin VB.ListBox lstTradeList 
      Height          =   5715
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   2775
   End
   Begin VB.ListBox lstCardsYouHave 
      Height          =   2400
      Left            =   3240
      TabIndex        =   3
      Top             =   600
      Width           =   2895
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1560
      Top             =   1920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DefaultExt      =   "hlt"
      Filter          =   "Trade Lists (*.hlt)|*.hlt|All Files (*.*)|*.*"
   End
   Begin VB.ListBox lstCardsYouWant 
      Height          =   2400
      Left            =   6360
      TabIndex        =   0
      Top             =   600
      Width           =   3015
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   840
      Top             =   1200
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "ViewCollectionADODC"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Image HLCard 
      Height          =   3135
      Left            =   6960
      Stretch         =   -1  'True
      Top             =   3240
      Width           =   2175
   End
   Begin VB.Label lblTradeList 
      AutoSize        =   -1  'True
      Caption         =   "Entire Trade List:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   2115
   End
   Begin VB.Label lblCardsyouhave 
      AutoSize        =   -1  'True
      Caption         =   "Cards You Have:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3240
      TabIndex        =   2
      Top             =   120
      Width           =   3075
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblCardsYouWant 
      AutoSize        =   -1  'True
      Caption         =   "Cards You're Missing:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6480
      TabIndex        =   1
      Top             =   120
      Width           =   2745
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuOpen 
         Caption         =   "Open Trade List (.hlt)"
         Shortcut        =   ^O
      End
      Begin VB.Menu menuExit 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
End
Attribute VB_Name = "frmTradeCards"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Created By:           Eric Cavaliere
' Program Name:         Watcher Database
' Project File Name:    Highlander.vbp
' Project Started on:   August 05, 2002
' Name of this File:    frmTradeCards.frm
' This file started on: March 04, 2003
' Last Updated on:      22 October 2007

'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Private Sub Form_Load()
  On Error Resume Next
  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh
End Sub

Public Sub DisplayCardById(idToDisplay)
  datCardNames.Recordset.MoveFirst
  datCardNames.Recordset.Find ("Id = '" + Str$(idToDisplay) + "'")
End Sub

Public Function getImageLocation(txtFileNames As String, nameToRetrive As Integer)
'  This function was created to deal with the whole "two card images in one field" issue
'nameToRetrive = 1 - Front, 2 - Back
  Dim counter As Long
  Dim foundSplit As Boolean
  Dim splitPos As Long
  foundSplit = False
  
  If (Left$(txtFileNames, 1) = "*") Then ' if first character is '*' then check for character '|'
    counter = 0
    While counter <= Len(txtFileNames)
      counter = counter + 1
      If Mid$(txtFileNames, counter, 1) = "|" Then ' '|' represents end of front image and start of back
        foundSplit = True
        splitPos = counter
      End If
    Wend
    
    If foundSplit Then ' if a break was found then...
      If nameToRetrive = 1 Then
        getImageLocation = Mid$(txtFileNames, 2, splitPos - 2)
      Else
        getImageLocation = Mid$(txtFileNames, splitPos + 1, Len(txtFileNames) - splitPos)
      End If
    Else ' if a break was not found, but a '*' was, assume somethings wrong and display empty.jpg
      getImageLocation = ""
    End If
  Else ' if a '*' is not found, assume only the front of the image is given
    If nameToRetrive = 1 Then
      getImageLocation = txtFileNames
    Else
      getImageLocation = ""
    End If
  End If
End Function

Private Sub lstCardsYouHave_Click()
    DisplayCardById lstCardsYouHaveID.List(lstCardsYouHave.ListIndex)
End Sub

Private Sub lstCardsYouWant_Click()
  DisplayCardById lstCardsYouWantID.List(lstCardsYouWant.ListIndex)
End Sub

Private Sub lstTradeList_Click()
  DisplayCardById lstTradeListID.List(lstTradeList.ListIndex)
End Sub

Private Sub menuExit_Click()
  frmTradeCards.Hide
  Unload frmTradeCards
End Sub

Private Sub menuOpen_Click()
  CommonDialog1.ShowOpen
  Dim tempNum, tempId, tempHW
  
  If CommonDialog1.FileName <> "" Then
    datCardNames.Recordset.MoveFirst
    Open CommonDialog1.FileName For Input As #1
    While Not EOF(1)
      Input #1, tempHW, tempNum, tempId
      DisplayCardById tempId
      lstTradeList.AddItem "(" + tempHW + Str$(tempNum) + ") " + txtCardName.text
      lstTradeListID.AddItem tempId
      If txtNum.text > 0 Then
        If tempHW = "W" Then
          lstCardsYouHave.AddItem "(" + Str$(tempNum) + ") " + txtCardName.text
          lstCardsYouHaveID.AddItem tempId
        End If
      Else
        If tempHW = "H" Then
          lstCardsYouWant.AddItem "(" + Str$(tempNum) + ") " + txtCardName.text
          lstCardsYouWantID.AddItem tempId
        End If
      End If
        
    Wend
    Close #1
    CommonDialog1.FileName = ""
  End If

End Sub

Private Sub txtFileName_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Stretch = True
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileName.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub txtSet_Change()
  Call txtFileName_Change
End Sub
