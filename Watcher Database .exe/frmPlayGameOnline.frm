VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Begin VB.Form frmPlayGameOnline 
   Caption         =   "Play Game"
   ClientHeight    =   9210
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   11505
   LinkTopic       =   "Form1"
   ScaleHeight     =   9210
   ScaleWidth      =   11505
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frameConnect 
      Caption         =   "Connection Information"
      Height          =   1695
      Left            =   3960
      TabIndex        =   88
      Top             =   3720
      Visible         =   0   'False
      Width           =   2655
      Begin VB.TextBox txtName 
         Height          =   285
         Left            =   1080
         TabIndex        =   92
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox txtIP 
         Height          =   285
         Left            =   1080
         TabIndex        =   91
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdListen 
         Caption         =   "Listen"
         Height          =   495
         Left            =   1320
         TabIndex        =   90
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CommandButton cmdConnect 
         Caption         =   "Connect"
         Height          =   495
         Left            =   120
         TabIndex        =   89
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   94
         Top             =   600
         Width           =   465
      End
      Begin VB.Label lblIp 
         AutoSize        =   -1  'True
         Caption         =   "IP Address:"
         Height          =   195
         Left            =   120
         TabIndex        =   93
         Top             =   240
         Width           =   810
      End
   End
   Begin VB.Frame frameCover 
      Height          =   9135
      Left            =   120
      TabIndex        =   86
      Top             =   0
      Width           =   11295
      Begin VB.Timer timerCheckConnection 
         Interval        =   100
         Left            =   5160
         Top             =   6000
      End
      Begin VB.Frame frmLoading 
         Caption         =   "Loading..."
         Height          =   735
         Left            =   3360
         TabIndex        =   87
         Top             =   2880
         Visible         =   0   'False
         Width           =   3615
         Begin VB.Shape StatusBox 
            DrawMode        =   1  'Blackness
            FillStyle       =   0  'Solid
            Height          =   375
            Left            =   120
            Shape           =   3  'Circle
            Top             =   240
            Width           =   615
         End
      End
   End
   Begin VB.Frame frameRollDice 
      Caption         =   "Roll Dice"
      Height          =   1455
      Left            =   120
      TabIndex        =   80
      Top             =   360
      Visible         =   0   'False
      Width           =   2055
      Begin VB.TextBox txtNumSidesDice 
         Height          =   285
         Left            =   1440
         TabIndex        =   83
         Text            =   "6"
         Top             =   240
         Width           =   495
      End
      Begin VB.CommandButton cmdRollDice 
         Caption         =   "Roll Dice"
         Height          =   255
         Left            =   120
         TabIndex        =   82
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox txtNumberRolled 
         Height          =   285
         Left            =   1440
         TabIndex        =   81
         Top             =   960
         Width           =   495
      End
      Begin VB.Label lblNumSidesDice 
         AutoSize        =   -1  'True
         Caption         =   "Number of Sides:"
         Height          =   195
         Left            =   120
         TabIndex        =   85
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label lblNumberRolled 
         AutoSize        =   -1  'True
         Caption         =   "Number Rolled:"
         Height          =   195
         Left            =   120
         TabIndex        =   84
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.Frame frmHiddenAttacks 
      Caption         =   "Hidden Attacks"
      Height          =   2655
      Left            =   5280
      TabIndex        =   75
      Top             =   360
      Visible         =   0   'False
      Width           =   2415
      Begin VB.ListBox lstHiddenAttacks 
         Height          =   1815
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   77
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton cmdRevealHiddenAttack 
         Caption         =   "Reveal Attack"
         Height          =   375
         Left            =   480
         TabIndex        =   76
         Top             =   2160
         Width           =   1455
      End
   End
   Begin VB.Frame frameTempArea 
      Caption         =   "Frame1"
      Height          =   4095
      Left            =   5040
      TabIndex        =   71
      Top             =   3240
      Visible         =   0   'False
      Width           =   2655
      Begin VB.CommandButton cmdHideTempArea 
         Caption         =   "Finished"
         Height          =   375
         Left            =   480
         TabIndex        =   73
         Top             =   3600
         Width           =   1575
      End
      Begin VB.ListBox lstTempArea 
         Height          =   3180
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   72
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.CommandButton cmdMoveDiscardToDeck 
      Caption         =   "Shuffle Discard Pile into Deck"
      Height          =   495
      Left            =   7800
      TabIndex        =   69
      Top             =   8160
      Width           =   1215
   End
   Begin VB.Frame frameOppPreGame 
      Caption         =   "Pre-Game (Opponent)"
      Height          =   2655
      Left            =   5280
      TabIndex        =   66
      Top             =   360
      Width           =   2415
      Begin VB.ListBox lstOppPreGame 
         Height          =   2205
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   67
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame frameExertion 
      Caption         =   "Exertion"
      Height          =   4095
      Left            =   5040
      TabIndex        =   60
      Top             =   3240
      Visible         =   0   'False
      Width           =   2655
      Begin VB.ListBox lstExertion 
         Height          =   2595
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   65
         Top             =   240
         Width           =   2415
      End
      Begin VB.CommandButton cmdExert5Cards 
         Caption         =   "Exert 5 Cards"
         Height          =   255
         Left            =   240
         TabIndex        =   64
         Top             =   3240
         Width           =   2175
      End
      Begin VB.CommandButton cmdExert1Card 
         Caption         =   "Exert 1 Card"
         Height          =   255
         Left            =   240
         TabIndex        =   63
         Top             =   3000
         Width           =   2175
      End
      Begin VB.CommandButton cmdPlayExertedCard 
         Caption         =   "Play Card"
         Height          =   255
         Left            =   240
         TabIndex        =   62
         Top             =   3480
         Width           =   2175
      End
      Begin VB.CommandButton cmdDiscardExertedCards 
         Caption         =   "Finish Exertion"
         Height          =   315
         Left            =   240
         TabIndex        =   61
         Top             =   3720
         Width           =   2175
      End
   End
   Begin VB.Frame frameTempStorage 
      Caption         =   "Dojo"
      Height          =   4095
      Left            =   5040
      TabIndex        =   56
      Top             =   3240
      Visible         =   0   'False
      Width           =   2655
      Begin VB.CommandButton cmdFromDojoToDiscard 
         Caption         =   "Discard"
         Height          =   495
         Left            =   1320
         TabIndex        =   59
         Top             =   3480
         Width           =   975
      End
      Begin VB.CommandButton cmdMoveToHand 
         Caption         =   "To Hand"
         Height          =   495
         Left            =   240
         TabIndex        =   58
         Top             =   3480
         Width           =   1095
      End
      Begin VB.ListBox lstTempStorage 
         Height          =   2985
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   57
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame frmPreGame 
      Caption         =   "Pre-Game Cards"
      Height          =   4095
      Left            =   5040
      TabIndex        =   52
      Top             =   3240
      Visible         =   0   'False
      Width           =   2655
      Begin VB.ListBox lstPreGame 
         Height          =   2985
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   55
         Top             =   240
         Width           =   2415
      End
      Begin VB.CommandButton cmdRemovePreGame 
         Caption         =   "Remove Selected Card"
         Height          =   375
         Left            =   120
         TabIndex        =   54
         Top             =   3240
         Width           =   2415
      End
      Begin VB.CommandButton cmdUserPreGameToPlay 
         Caption         =   "Move Selected Card To In Play"
         Height          =   375
         Left            =   120
         TabIndex        =   53
         Top             =   3600
         Width           =   2415
      End
   End
   Begin VB.Frame frameDeck 
      Caption         =   "Deck"
      Height          =   3015
      Left            =   120
      TabIndex        =   48
      Top             =   360
      Visible         =   0   'False
      Width           =   3255
      Begin VB.ListBox lstDeck 
         Height          =   2010
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   51
         Top             =   240
         Width           =   3015
      End
      Begin VB.CommandButton cmdMoveCardToTopDeck 
         Caption         =   "Move to Top of Deck"
         Height          =   495
         Left            =   1680
         TabIndex        =   50
         Top             =   2400
         Width           =   1455
      End
      Begin VB.CommandButton cmdPlaceCardInHand 
         Caption         =   "Move Selected Card to Hand"
         Height          =   495
         Left            =   240
         TabIndex        =   49
         Top             =   2400
         Width           =   1215
      End
   End
   Begin VB.Frame frameCardDetailsDeck 
      Caption         =   "Card Details"
      Height          =   8055
      Left            =   7800
      TabIndex        =   30
      Top             =   0
      Width           =   3615
      Begin VB.CommandButton cmdShowBack 
         Caption         =   "Show Back"
         Height          =   255
         Left            =   1920
         TabIndex        =   79
         Top             =   7680
         Width           =   975
      End
      Begin VB.CommandButton cmdShowFront 
         Caption         =   "Show Front"
         Height          =   255
         Left            =   360
         TabIndex        =   78
         Top             =   7680
         Width           =   975
      End
      Begin VB.TextBox txtFileNameDeck 
         DataField       =   "File Name"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   2400
         TabIndex        =   68
         Top             =   1080
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox txtCardTextDeck 
         DataField       =   "Text"
         DataSource      =   "datCardDeck"
         Height          =   1245
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   36
         Top             =   1680
         Width           =   2415
      End
      Begin VB.TextBox txtCardNameDeck 
         DataField       =   "Name"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   35
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox txtSetDeck 
         DataField       =   "Set"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   3000
         TabIndex        =   34
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox txtRarityDeck 
         DataField       =   "Rarity"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   33
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox txtTypeDeck 
         DataField       =   "Type"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   32
         Top             =   960
         Width           =   1215
      End
      Begin VB.TextBox txtOtherDeck 
         DataField       =   "Other"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   31
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Image HLCard 
         Height          =   4575
         Left            =   120
         Stretch         =   -1  'True
         Top             =   3000
         Width           =   3375
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   42
         Top             =   1680
         Width           =   735
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   120
         TabIndex        =   41
         Top             =   1320
         Width           =   435
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   40
         Top             =   960
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   39
         Top             =   240
         Width           =   840
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   2400
         TabIndex        =   38
         Top             =   600
         Width           =   285
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   37
         Top             =   600
         Width           =   450
      End
   End
   Begin VB.Frame frameHand 
      Caption         =   "Hand"
      Height          =   4455
      Left            =   120
      TabIndex        =   23
      Top             =   3240
      Width           =   2415
      Begin VB.CommandButton cmdDiscardToBottomOfDeck 
         Caption         =   "Discard (Bottom of Deck)"
         Height          =   255
         Left            =   120
         TabIndex        =   95
         Top             =   3840
         Width           =   2175
      End
      Begin VB.CommandButton cmdHiddenAttack 
         Caption         =   "Play Hidden Attack"
         Height          =   255
         Left            =   720
         TabIndex        =   74
         Top             =   3120
         Width           =   1575
      End
      Begin VB.ListBox lstPlayerHand 
         Height          =   2790
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   29
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton cmdPlayerDrawCard 
         Caption         =   "Draw Card"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   4080
         Width           =   1455
      End
      Begin VB.CommandButton cmdPlayerDiscard 
         Caption         =   "Discard"
         Height          =   255
         Left            =   1560
         TabIndex        =   27
         Top             =   4080
         Width           =   735
      End
      Begin VB.CommandButton cmdPlayerPlayCard 
         Caption         =   "Play"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   3120
         Width           =   615
      End
      Begin VB.CommandButton cmdToTempStorage 
         Caption         =   "Move Card to Dojo"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   3360
         Width           =   2175
      End
      Begin VB.CommandButton cmdDiscardToTopOfDeck 
         Caption         =   "Discard (Top of Deck)"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   3600
         Width           =   2175
      End
   End
   Begin VB.Frame framePlayerInPlay 
      Caption         =   "In Play"
      Height          =   4095
      Left            =   2640
      TabIndex        =   19
      Top             =   3240
      Width           =   2295
      Begin VB.ListBox lstPlayerInPlay 
         Height          =   3180
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   22
         Top             =   240
         Width           =   2055
      End
      Begin VB.CommandButton cmdPlayerInPlayToHand 
         Caption         =   "To Hand"
         Height          =   495
         Left            =   240
         TabIndex        =   21
         Top             =   3480
         Width           =   735
      End
      Begin VB.CommandButton cmdPlayerInPlayDiscard 
         Caption         =   "Discard"
         Height          =   495
         Left            =   1200
         TabIndex        =   20
         Top             =   3480
         Width           =   735
      End
   End
   Begin VB.Frame framePlayerDiscard 
      Caption         =   "Discard"
      Height          =   4095
      Left            =   5040
      TabIndex        =   15
      Top             =   3240
      Width           =   2655
      Begin VB.CommandButton cmd1CardDiscardToEndurance 
         Caption         =   "Move To Top of Endurance"
         Height          =   615
         Left            =   240
         TabIndex        =   70
         Top             =   3360
         Width           =   975
      End
      Begin VB.ListBox lstPlayerDiscard 
         Height          =   2985
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   18
         Top             =   240
         Width           =   2415
      End
      Begin VB.CommandButton cmdPlayerDiscardToPlay 
         Caption         =   "Play"
         Height          =   615
         Left            =   1200
         TabIndex        =   17
         Top             =   3360
         Width           =   495
      End
      Begin VB.CommandButton cmdPlayerDiscardRemoveGame 
         Caption         =   "Remove From Game"
         Height          =   615
         Left            =   1680
         TabIndex        =   16
         Top             =   3360
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdShuffleEndurance 
      Caption         =   "Shuffle Endurance"
      Height          =   495
      Left            =   10200
      TabIndex        =   14
      Top             =   8160
      Width           =   975
   End
   Begin VB.CommandButton cmdMakeExertion 
      Caption         =   "Make Exertion"
      Height          =   495
      Left            =   9240
      TabIndex        =   13
      Top             =   8160
      Width           =   855
   End
   Begin VB.TextBox txtOpponetTurnNum 
      Height          =   285
      Left            =   6840
      TabIndex        =   12
      Top             =   0
      Width           =   735
   End
   Begin VB.TextBox txtYourTurnNum 
      Height          =   285
      Left            =   8880
      TabIndex        =   11
      Top             =   8760
      Width           =   495
   End
   Begin VB.TextBox txtOpponetAbility 
      Height          =   285
      Left            =   4800
      TabIndex        =   10
      Top             =   0
      Width           =   735
   End
   Begin VB.TextBox txtYourAbility 
      Height          =   285
      Left            =   10440
      TabIndex        =   9
      Top             =   8760
      Width           =   495
   End
   Begin VB.TextBox txtOpponetNameEmail 
      Height          =   285
      Left            =   1440
      TabIndex        =   8
      Top             =   0
      Width           =   1935
   End
   Begin VB.Frame frameOppInPlay 
      Caption         =   "In Play (Opponent)"
      Height          =   2655
      Left            =   120
      TabIndex        =   6
      Top             =   360
      Width           =   2415
      Begin VB.ListBox lstOpponetInPlay 
         Height          =   2205
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   7
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame frameOpponentDiscard 
      Caption         =   "Discard (Opponent)"
      Height          =   2655
      Left            =   2640
      TabIndex        =   4
      Top             =   360
      Width           =   2535
      Begin VB.ListBox lstOpponetDiscard 
         Height          =   2205
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   5
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.TextBox txtOut 
      Height          =   285
      Left            =   4080
      TabIndex        =   2
      Top             =   7440
      Width           =   2775
   End
   Begin VB.TextBox txtIn 
      Height          =   1215
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   7800
      Width           =   7455
   End
   Begin VB.CommandButton cmdSend 
      Caption         =   "Send"
      Height          =   255
      Left            =   6960
      TabIndex        =   0
      Top             =   7440
      Width           =   615
   End
   Begin MSWinsockLib.Winsock wsChat 
      Left            =   2760
      Top             =   7560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1200
      Top             =   7560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "hld"
      Filter          =   "Highlander Decks (*.hld)|*.hld|All Files (*.*)|*.*"
   End
   Begin MSComDlg.CommonDialog CommonDialog2 
      Left            =   1800
      Top             =   7680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "hlg"
      Filter          =   "Saved Games (*.hlg)|*.hlg|All Files (*.*)|*.*"
   End
   Begin MSAdodcLib.Adodc datCardDeck 
      Height          =   735
      Left            =   8280
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Online Deck Connection"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid dataCards 
      Bindings        =   "frmPlayGameOnline.frx":0000
      Height          =   2895
      Left            =   8520
      TabIndex        =   96
      Top             =   600
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   5106
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   0   'False
      DefColWidth     =   273
      ColumnHeaders   =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "All Cards"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   7680
      Y1              =   3120
      Y2              =   3120
   End
   Begin VB.Label lblTurnNumber 
      AutoSize        =   -1  'True
      Caption         =   "Turn Number:"
      Height          =   195
      Left            =   7800
      TabIndex        =   47
      Top             =   8760
      Width           =   975
   End
   Begin VB.Label lblYourAbility 
      AutoSize        =   -1  'True
      Caption         =   "Ability:"
      Height          =   195
      Left            =   9840
      TabIndex        =   46
      Top             =   8760
      Width           =   450
   End
   Begin VB.Label lblOppName 
      AutoSize        =   -1  'True
      Caption         =   "Opponent Name:"
      Height          =   195
      Left            =   120
      TabIndex        =   45
      Top             =   0
      Width           =   1215
   End
   Begin VB.Label lblOppAbility 
      AutoSize        =   -1  'True
      Caption         =   "Opponent Ability:"
      Height          =   195
      Left            =   3480
      TabIndex        =   44
      Top             =   0
      Width           =   1200
   End
   Begin VB.Label lblOppTurnNum 
      AutoSize        =   -1  'True
      Caption         =   "Opponent Turn:"
      Height          =   195
      Left            =   5640
      TabIndex        =   43
      Top             =   0
      Width           =   1125
   End
   Begin VB.Label lblMsgToSend 
      AutoSize        =   -1  'True
      Caption         =   "Message To Send:"
      Height          =   195
      Left            =   2640
      TabIndex        =   3
      Top             =   7440
      Width           =   1350
   End
   Begin VB.Menu FileMenu 
      Caption         =   "File"
      Begin VB.Menu NewGameMenu 
         Caption         =   "New Game"
         Shortcut        =   ^N
      End
      Begin VB.Menu menuLoadSavedGame 
         Caption         =   "Open Saved Game"
         Shortcut        =   ^O
      End
      Begin VB.Menu CloseConnectionMenu 
         Caption         =   "Close Connection"
         Shortcut        =   ^C
      End
      Begin VB.Menu ExitMenu 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu ShowHideMenu 
      Caption         =   "Show/Hide"
      Begin VB.Menu OppPreGameShowHideMenu 
         Caption         =   "Opponent's Pre-Game Cards"
         Shortcut        =   ^E
      End
      Begin VB.Menu hr1 
         Caption         =   "-"
      End
      Begin VB.Menu RollDiceMenu 
         Caption         =   "Roll Dice"
         Shortcut        =   ^R
      End
      Begin VB.Menu hr2 
         Caption         =   "-"
      End
      Begin VB.Menu DojoMenu 
         Caption         =   "Dojo"
         Shortcut        =   ^U
      End
      Begin VB.Menu YourPreGameMenu 
         Caption         =   "Your Pre-Game Cards"
         Shortcut        =   ^P
      End
      Begin VB.Menu YourDeckMenu 
         Caption         =   "Your Deck"
         Shortcut        =   ^D
      End
   End
   Begin VB.Menu menushowcardstoOpponent 
      Caption         =   "Show Cards To Opponent"
      Enabled         =   0   'False
      Begin VB.Menu menuSendSelectedCardHand 
         Caption         =   "Show Selected Card From Hand"
         Shortcut        =   ^B
      End
      Begin VB.Menu menuSendRandomCardsHand 
         Caption         =   "Show Random Card From Hand"
         Shortcut        =   ^G
      End
      Begin VB.Menu menuShowEntireHandOpponent 
         Caption         =   "Show Entire Hand"
         Shortcut        =   ^J
      End
      Begin VB.Menu menuSendCardsFromDeck 
         Caption         =   "Show X Cards From Deck"
         Shortcut        =   ^X
      End
      Begin VB.Menu menuShowEntireDeckOpponent 
         Caption         =   "Show Entire Deck"
         Shortcut        =   ^K
      End
   End
   Begin VB.Menu menuSortHand 
      Caption         =   "Sort Hand"
      Begin VB.Menu menuSortHandByName 
         Caption         =   "By Name"
         Shortcut        =   ^M
      End
      Begin VB.Menu menuSortHandByType 
         Caption         =   "By Type"
         Shortcut        =   ^T
      End
      Begin VB.Menu menuSortHandByBoth 
         Caption         =   "By Both"
         Shortcut        =   ^H
      End
   End
End
Attribute VB_Name = "frmPlayGameOnline"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Created By:           Eric Cavaliere
' Program Name:         Watcher Database
' Project File Name:    Highlander.vbp
' Project Started on:   August 05, 2002
' Name of this File:    chat.frm
' This file started on: August 23, 2002
' Last Updated on:      22 October 2007

'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

'array(*, 1) = id, array(*, 2) = random number, array(*, 3) = Title
'* = record number
Dim userDeckCards(500, 4)
Dim userPreGameCards(500, 4)
Dim userHandCards(50, 4)
Dim userInPlayCards(50, 4)
Dim userDiscardCards(500, 4)
Dim userExertion(50, 4)
Dim userTempStorage(50, 4)
Dim tempArea(500, 4)
Dim oppInPlayCards(50, 4)
Dim oppDiscardCards(500, 4)
Dim oppPreGameCards(50, 4)
Dim userHiddenAttacks(50, 4)
Dim userHiddenAttacksPos As Integer
Dim userDeckPos As Integer
Dim userPrePos As Integer
Dim userHandPos As Integer
Dim userInPlayPos As Integer
Dim userDiscardPos As Integer
Dim userExertionPos As Integer
Dim userTempStoragePos As Integer
Dim oppTempAreaPos As Integer
Dim oppInPlayPos As Integer
Dim oppDiscardPos As Integer
Dim oppPreGamePos As Integer
Dim loadingGame As Boolean

Private Sub CloseConnectionMenu_Click()
'If Player Clicks on the Close Connection Menu Item...
  On Error Resume Next
  wsChat.SendData "addChat"
  wsChat.SendData "Game Over"
  wsChat.SendData "*"
  wsChat.SendData "addChat"
  wsChat.SendData "----- Connection Closed -----"
  wsChat.SendData "*"
  Write #2, "[" & txtName.text & "] " & txtOut.text
  txtOut.text = ""
  txtOut.SetFocus
  wsChat.SendData "endGame"
  wsChat.SendData "*"
  wsChat.Close
  txtIn.text = "----- Connection Closed -----" & vbCrLf
  Write #2, "----- Connection Closed -----"
  frameCover.Visible = True
  cmdListen.Enabled = True
  cmdConnect.Enabled = True
  txtName.Enabled = True
End Sub

Private Sub cmd1CardDiscardToEndurance_Click()
'  Move selected cards from Players discard list to Players Endurance
  Dim j As Integer
  TransferCardFromLstToLst lstPlayerDiscard, userDiscardCards(), userDiscardPos, lstDeck, userDeckCards(), userDeckPos, "Discard1TopEndurance"
  
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3)
    j = j - 1
  Wend
End Sub

Private Sub cmdDiscardExertedCards_Click()
  Write #2, "Remaining cards from exertion discarded:"
  While (userExertionPos > 0)
    lstPlayerDiscard.AddItem (userExertion(userExertionPos - 1, 3))
    Write #2, "          " + userExertion(userExertionPos - 1, 3)
    userDiscardCards(userDiscardPos, 1) = userExertion(userExertionPos - 1, 1)
    userDiscardCards(userDiscardPos, 2) = userExertion(userExertionPos - 1, 2)
    userDiscardCards(userDiscardPos, 3) = userExertion(userExertionPos - 1, 3)
    userDiscardCards(userDiscardPos, 4) = userExertion(userExertionPos - 1, 4)
    userDiscardPos = userDiscardPos + 1
    userExertionPos = userExertionPos - 1
    wsChat.SendData "oppDiscardCardAdd"
    wsChat.SendData userDiscardCards(userDiscardPos - 1, 1)
    wsChat.SendData "*"
  Wend
  lstExertion.Clear
  frameExertion.Visible = False
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
End Sub

Private Sub cmdDiscardToBottomOfDeck_Click()
  Dim counter As Integer
  Dim j As Integer
  Dim k As Integer
  j = 0
    
  If userHandPos > 0 Then
    While (j < lstPlayerHand.ListCount)
      If lstPlayerHand.Selected(j) = True Then
        k = j
        wsChat.SendData "addChat"
        wsChat.SendData "Opponent discards a card to the bottom of the deck."
        wsChat.SendData "*"

        DisplayCardById (userHandCards(j, 1))
        Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from hand to bottom of deck"
        userDeckPos = userDeckPos + 1
        counter = userDeckPos
        While counter > -1
          userDeckCards(counter + 1, 1) = userDeckCards(counter, 1)
          userDeckCards(counter + 1, 2) = userDeckCards(counter, 2)
          userDeckCards(counter + 1, 3) = userDeckCards(counter, 3)
          userDeckCards(counter + 1, 4) = userDeckCards(counter, 4)
          counter = counter - 1
        Wend
        userDeckCards(0, 1) = userHandCards(j, 1)
        userDeckCards(0, 2) = userHandCards(j, 2)
        userDeckCards(0, 3) = userHandCards(j, 3)
        userDeckCards(0, 4) = userHandCards(j, 4)
        lstPlayerHand.RemoveItem (j)
        
        While (k < userHandPos)
          userHandCards(k, 1) = userHandCards(k + 1, 1)
          userHandCards(k, 2) = userHandCards(k + 1, 2)
          userHandCards(k, 3) = userHandCards(k + 1, 3)
          userHandCards(k, 4) = userHandCards(k + 1, 4)
          k = k + 1
        Wend
        
        userHandPos = userHandPos - 1
        j = j - 1
      End If
      j = j + 1
    Wend
  End If
  
  frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
  cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3)
    j = j - 1
  Wend
End Sub

Private Sub cmdDiscardToTopOfDeck_Click()
  Dim j As Integer
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstDeck, userDeckCards, userDeckPos, "fromHandToTopDeck"
  
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3)
    j = j - 1
  Wend
End Sub

Private Sub cmdExert1Card_Click()
  If userDeckPos > 0 Then
    userExertion(userExertionPos, 1) = userDeckCards(userDeckPos - 1, 1)
    userExertion(userExertionPos, 2) = userDeckCards(userDeckPos - 1, 2)
    userExertion(userExertionPos, 3) = userDeckCards(userDeckPos - 1, 3)
    userExertion(userExertionPos, 4) = userDeckCards(userDeckPos - 1, 4)
    DisplayCardById (userExertion(userExertionPos, 1))
    lstExertion.AddItem (txtCardNameDeck.text)
    wsChat.SendData "addChat"
    wsChat.SendData "Opponent draws " & txtCardNameDeck.text & " for exertion"
    wsChat.SendData "*"
    Write #2, txtCardNameDeck.text + " is in exertion"
    lstDeck.RemoveItem (0)
    userExertionPos = userExertionPos + 1
    userDeckPos = userDeckPos - 1
    frameExertion.Caption = "Exertion (" + Str$(userExertionPos) + ")"
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  Else
    MsgBox "There are no more cards in the deck", vbOKOnly, "Error"
  End If
End Sub

Private Sub cmdExert5Cards_Click()
  tempExertCount = 0
  While tempExertCount < 5
    If userDeckPos > 0 Then
      userExertion(userExertionPos, 1) = userDeckCards(userDeckPos - 1, 1)
      userExertion(userExertionPos, 2) = userDeckCards(userDeckPos - 1, 2)
      userExertion(userExertionPos, 3) = userDeckCards(userDeckPos - 1, 3)
      userExertion(userExertionPos, 4) = userDeckCards(userDeckPos - 1, 4)
      
      DisplayCardById (userExertion(userExertionPos, 1))
      Write #2, txtCardNameDeck.text + " is in exertion"
      lstExertion.AddItem (txtCardNameDeck.text)
      wsChat.SendData "addChat"
      wsChat.SendData "Opponent draws " & txtCardNameDeck.text & " for exertion"
      wsChat.SendData "*"
      lstDeck.RemoveItem (0)
      userExertionPos = userExertionPos + 1
      userDeckPos = userDeckPos - 1
      frameExertion.Caption = "Exertion (" + Str$(userExertionPos) + ")"
      frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
      cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
      tempExertCount = tempExertCount + 1
    Else
      j = MsgBox("There are no more cards in the deck", vbOKOnly, "Error")
      tempExertCount = 5
    End If
  Wend
End Sub

Private Sub cmdFromDojoToDiscard_Click()
  TransferCardFromLstToLst lstTempStorage, userTempStorage, userTempStoragePos, lstPlayerDiscard, userDiscardCards, userDiscardPos, "userDojoToDiscard"
  frameTempStorage.Caption = "Dojo (" + Str$(userTempStoragePos) + ")"
End Sub


Private Sub cmdHiddenAttack_Click()
  frmHiddenAttacks.Visible = True
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstHiddenAttacks, userHiddenAttacks, userHiddenAttacksPos, "playHiddenAttack"
End Sub

Private Sub cmdHideTempArea_Click()
  frameTempArea.Visible = False
End Sub

Private Sub cmdMakeExertion_Click()
  frameExertion.Visible = True
  wsChat.SendData "addChat"
  wsChat.SendData "Opponent Makes an Exertion."
  wsChat.SendData "*"
  Write #2, txtName.text + " makes an exertion"
End Sub

Private Sub cmdMoveCardToTopDeck_Click()
  Dim j, k
  j = 0
  
  If userDeckPos > 0 Then
    While (j < lstDeck.ListCount)
      If (lstDeck.Selected(j) = True) Then
        k = j
        lstDeck.Selected(j) = False
        While k > 0
          lstDeck.List(k) = lstDeck.List(k - 1)
          k = k - 1
        Wend
        k = userDeckPos - j - 1
        userDeckCards(userDeckPos, 1) = userDeckCards(k, 1)
        userDeckCards(userDeckPos, 2) = userDeckCards(k, 2)
        userDeckCards(userDeckPos, 3) = userDeckCards(k, 3)
        lstDeck.List(0) = userDeckCards(userDeckPos, 3)
        While (k < userDeckPos + 1)
          userDeckCards(k, 1) = userDeckCards(k + 1, 1)
          userDeckCards(k, 2) = userDeckCards(k + 1, 2)
          userDeckCards(k, 3) = userDeckCards(k + 1, 3)
          k = k + 1
        Wend
    
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub cmdMoveDiscardToDeck_Click()
    k = 0
    Write #2, txtName.text + " shuffled discard pile into endurance"
    While (k < userDiscardPos)
      userDeckCards(userDeckPos, 1) = userDiscardCards(k, 1)
      userDeckCards(userDeckPos, 2) = userDiscardCards(k, 2)
      userDeckCards(userDeckPos, 3) = userDiscardCards(k, 3)
      userDeckCards(userDeckPos, 4) = userDiscardCards(k, 4)
      userDeckPos = userDeckPos + 1
      k = k + 1
    Wend
    userDiscardPos = 0
    
    lstPlayerDiscard.Clear
    framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
    
    Randomize Timer
    For tempCounter = 0 To userDeckPos - 1
      userDeckCards(tempCounter, 2) = Rnd()
    Next tempCounter
  
    For a = 0 To userDeckPos
      For b = 0 To userDeckPos - 2
        If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
          Dim tempCard(4)
          tempCard(1) = userDeckCards(b, 1)
          tempCard(2) = userDeckCards(b, 2)
          tempCard(3) = userDeckCards(b, 3)
          tempCard(4) = userDeckCards(b, 4)
          
          userDeckCards(b, 1) = userDeckCards(b + 1, 1)
          userDeckCards(b, 2) = userDeckCards(b + 1, 2)
          userDeckCards(b, 3) = userDeckCards(b + 1, 3)
          userDeckCards(b, 4) = userDeckCards(b + 1, 4)
          
          userDeckCards(b + 1, 1) = tempCard(1)
          userDeckCards(b + 1, 2) = tempCard(2)
          userDeckCards(b + 1, 3) = tempCard(3)
          userDeckCards(b + 1, 4) = tempCard(4)
        End If
      Next b
    Next a
          
    j = userDeckPos - 1
    lstDeck.Clear
    While j >= 0
      lstDeck.AddItem userDeckCards(j, 3)
      j = j - 1
    Wend
  wsChat.SendData "addChat"
  wsChat.SendData "Opponent Shuffles discard Pile into deck."
  wsChat.SendData "*"
  wsChat.SendData "oppClearDiscard"
  wsChat.SendData "*"
End Sub

Private Sub cmdMoveToHand_Click()
  TransferCardFromLstToLst lstTempStorage, userTempStorage, userTempStoragePos, lstPlayerHand, userHandCards, userHandPos, "fromDojoToHand"
  frameTempStorage.Caption = "Dojo (" + Str$(userTempStoragePos) + ")"
End Sub

Private Sub cmdPlaceCardInHand_Click()
  Dim j As Integer
  Dim k As Integer
  j = 0
  If userDeckPos > 0 Then
  While j < lstDeck.ListCount

    If (lstDeck.Selected(j) = True) Then
      k = userDeckPos - j - 1
      DisplayCardById (userDeckCards(k, 1))
      Write #2, txtName.text + " moved " + txtCardNameDeck.text + " to his hand from endurance"
      lstPlayerHand.AddItem (txtCardNameDeck.text)
      userHandCards(userHandPos, 1) = userDeckCards(k, 1)
      userHandCards(userHandPos, 2) = userDeckCards(k, 2)
      userHandCards(userHandPos, 3) = userDeckCards(k, 3)
      userHandCards(userHandPos, 4) = txtTypeDeck.text
      userHandPos = userHandPos + 1
  
      While (k < userDeckPos)
        userDeckCards(k, 1) = userDeckCards(k + 1, 1)
        userDeckCards(k, 2) = userDeckCards(k + 1, 2)
        userDeckCards(k, 3) = userDeckCards(k + 1, 3)
        userDeckCards(k, 4) = userDeckCards(k + 1, 4)
        k = k + 1
      Wend
      userDeckPos = userDeckPos - 1
      lstDeck.RemoveItem (j)
      frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
      frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
      cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
      wsChat.SendData "addChat"
      wsChat.SendData "Opponent moves a card from deck to hand."
      wsChat.SendData "*"
      j = j - 1
    End If
    j = j + 1
  Wend
  End If
End Sub

Private Sub cmdPlayerDiscard_Click()
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstPlayerDiscard, userDiscardCards, userDiscardPos, "fromHandToDiscard"
End Sub

Private Sub cmdPlayerDiscardRemoveGame_Click()
  If userDiscardPos > 0 Then
  j = 0
  While j < lstPlayerDiscard.ListCount

    If (lstPlayerDiscard.Selected(j) = True) Then
      k = j
      wsChat.SendData "oppDiscardCardRemove"
      wsChat.SendData userDiscardCards(k, 1)
      wsChat.SendData "*"
      Write #2, txtName.text + " removes " + userDiscardCards(k, 3) + " from the game"
        
      While (k < userDiscardPos)
        userDiscardCards(k, 1) = userDiscardCards(k + 1, 1)
        userDiscardCards(k, 2) = userDiscardCards(k + 1, 2)
        userDiscardCards(k, 3) = userDiscardCards(k + 1, 3)
        userDiscardCards(k, 4) = userDiscardCards(k + 1, 4)
        k = k + 1
      Wend
      userDiscardPos = userDiscardPos - 1
      lstPlayerDiscard.RemoveItem (j)
      framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
      framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
      j = j - 1
    End If
    j = j + 1
    Wend
  End If
End Sub

Private Sub cmdPlayerDiscardToPlay_Click()
  TransferCardFromLstToLst lstPlayerDiscard, userDiscardCards, userDiscardPos, lstPlayerInPlay, userInPlayCards, userInPlayPos, "userFromDiscardToPlay"
End Sub

Private Sub cmdPlayerDrawCard_Click()
  If userDeckPos > 0 Then
    userHandCards(userHandPos, 1) = userDeckCards(userDeckPos - 1, 1)
    userHandCards(userHandPos, 2) = userDeckCards(userDeckPos - 1, 2)
    userHandCards(userHandPos, 3) = userDeckCards(userDeckPos - 1, 3)
    userHandCards(userHandPos, 4) = userDeckCards(userDeckPos - 1, 4)
    DisplayCardById (userHandCards(userHandPos, 1))
    userHandCards(userHandPos, 4) = txtTypeDeck.text
    
    lstPlayerHand.AddItem (txtCardNameDeck.text)
    lstDeck.RemoveItem (0)
    userHandPos = userHandPos + 1
    userDeckPos = userDeckPos - 1
    Write #2, txtName.text + " drew " + txtCardNameDeck.text
    frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
    wsChat.SendData "addChat"
    wsChat.SendData "Opponent Draws a card (Hand Size = " + Str$(userHandPos) + ")"
    wsChat.SendData "*"
  Else
    MsgBox "There are no more cards in the deck", vbOKOnly, "Error"
  End If
End Sub

Private Sub cmdPlayerInPlayDiscard_Click()
  TransferCardFromLstToLst lstPlayerInPlay, userInPlayCards, userInPlayPos, lstPlayerDiscard, userDiscardCards, userDiscardPos, "userPlayToDiscard"
End Sub

Private Sub cmdPlayerInPlayToHand_Click()
  TransferCardFromLstToLst lstPlayerInPlay, userInPlayCards, userInPlayPos, lstPlayerHand, userHandCards, userHandPos, "userPlayToHand"
End Sub

Private Sub cmdPlayerPlayCard_Click()
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstPlayerInPlay, userInPlayCards, userInPlayPos, "userHandToPlay"
End Sub

Private Sub cmdPlayExertedCard_Click()
  TransferCardFromLstToLst lstExertion, userExertion, userExertionPos, lstPlayerInPlay, userInPlayCards, userInPlayPos, "toPlayFromExertion"
  frameExertion.Caption = "Exertion (" + Str$(userHandPos) + ")"
End Sub

Private Sub cmdRemovePreGame_Click()
  Dim j As Integer
  Dim k As Integer
  j = 0
  While j < lstPreGame.ListCount

    If (lstPreGame.Selected(j) = True) Then
      k = j
      wsChat.SendData "oppPreGameRemove"
      wsChat.SendData userPreGameCards(k, 1)
      wsChat.SendData "*"
    
      While (k < userPrePos)
        userPreGameCards(k, 1) = userPreGameCards(k + 1, 1)
        userPreGameCards(k, 2) = userPreGameCards(k + 1, 2)
        userPreGameCards(k, 3) = userPreGameCards(k + 1, 3)
        k = k + 1
      Wend
      userPrePos = userPrePos - 1
      lstPreGame.RemoveItem (j)
      j = j - 1
    End If
    j = j + 1
  Wend
End Sub

Private Sub cmdRevealHiddenAttack_Click()
  TransferCardFromLstToLst lstHiddenAttacks, userHiddenAttacks, userHiddenAttacksPos, lstPlayerInPlay, userInPlayCards, userInPlayPos, "userRevealHidden"
  If userHiddenAttacksPos = 0 Then
    frmHiddenAttacks.Visible = False
  End If
End Sub

Private Sub cmdRollDice_Click()
  Randomize Timer
  txtNumberRolled.text = Int(Rnd() * txtNumSidesDice.text) + 1
  wsChat.SendData "addChat"
  wsChat.SendData "Opponent Rolled a " + txtNumberRolled.text
  wsChat.SendData "*"
End Sub

Private Sub cmdShowBack_Click()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileNameDeck.text, 2)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSetDeck.text + "\" + FileName)
    End If
  End If
End Sub

Public Function getImageLocation(txtFileNames As String, nameToRetrive As Integer)
'  This function was created to deal with the whole "two card images in one field" issue
'nameToRetrive = 1 - Front, 2 - Back
  Dim counter As Long
  Dim foundSplit As Boolean
  Dim splitPos As Long
  foundSplit = False
  
  If (Left$(txtFileNames, 1) = "*") Then ' if first character is '*' then check for character '|'
    counter = 0
    While counter <= Len(txtFileNames)
      counter = counter + 1
      If Mid$(txtFileNames, counter, 1) = "|" Then ' '|' represents end of front image and start of back
        foundSplit = True
        splitPos = counter
      End If
    Wend
    
    If foundSplit Then ' if a break was found then...
      If nameToRetrive = 1 Then
        getImageLocation = Mid$(txtFileNames, 2, splitPos - 2)
      Else
        getImageLocation = Mid$(txtFileNames, splitPos + 1, Len(txtFileNames) - splitPos)
      End If
    Else ' if a break was not found, but a '*' was, assume somethings wrong and display empty.jpg
      getImageLocation = ""
    End If
  Else ' if a '*' is not found, assume only the front of the image is given
    If nameToRetrive = 1 Then
      getImageLocation = txtFileNames
    Else
      getImageLocation = ""
    End If
  End If
End Function

Private Sub cmdShowFront_Click()
  Call txtFileNameDeck_Change
End Sub

Private Sub cmdShuffleEndurance_Click()
  Randomize Timer
  Write #2, txtName.text + " shuffled the endurance"
  For tempCounter = 0 To userDeckPos - 1
    userDeckCards(tempCounter, 2) = Rnd()
  Next tempCounter
  
  For a = 0 To userDeckPos
    For b = 0 To userDeckPos - 2
      If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
        Dim tempCard(4)
        tempCard(1) = userDeckCards(b, 1)
        tempCard(2) = userDeckCards(b, 2)
        tempCard(3) = userDeckCards(b, 3)
        tempCard(4) = userDeckCards(b, 4)
          
        userDeckCards(b, 1) = userDeckCards(b + 1, 1)
        userDeckCards(b, 2) = userDeckCards(b + 1, 2)
        userDeckCards(b, 3) = userDeckCards(b + 1, 3)
        userDeckCards(b, 4) = userDeckCards(b + 1, 4)
          
        userDeckCards(b + 1, 1) = tempCard(1)
        userDeckCards(b + 1, 2) = tempCard(2)
        userDeckCards(b + 1, 3) = tempCard(3)
        userDeckCards(b + 1, 4) = tempCard(4)
      End If
    Next b
  Next a
          
  j = userDeckPos - 1
  lstDeck.Clear
  While j >= 0
    lstDeck.AddItem userDeckCards(j, 3) 'txtCardName.text
    j = j - 1
  Wend
  wsChat.SendData "addChat"
  wsChat.SendData "Opponent Shuffles Endurance."
  wsChat.SendData "*"
End Sub

Private Sub cmdToTempStorage_Click()
  TransferCardFromLstToLst lstPlayerHand, userHandCards, userHandPos, lstTempStorage, userTempStorage, userTempStoragePos, "userHandToDojo"
  frameTempStorage.Caption = "Dojo (" + Str$(userTempStoragePos) + ")"
End Sub

Private Sub cmdUserPreGameToPlay_Click()
  TransferCardFromLstToLst lstPreGame, userPreGameCards, userPrePos, lstPlayerInPlay, userInPlayCards, userInPlayPos, "userPreGameToPlay"
End Sub

Private Sub DojoMenu_Click()
  If frameTempStorage.Visible = True Then
    frameTempStorage.Visible = False
  Else
    frameTempStorage.Visible = True
  End If
End Sub

Private Sub ExitMenu_Click()
  frmPlayGameOnline.Hide
  Unload frmPlayGameOnline
End Sub

Private Sub LoadGameMenu_Click()
  Call LoadGame
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call CloseConnectionMenu_Click
  Write #2, "----- End Transcript -----"
  Close #2
End Sub

Private Sub lstDeck_DblClick()
  Call cmdPlaceCardInHand_Click
End Sub

Public Sub showClickedCard(ByRef lstClickedBox As ListBox, ByRef clickedArray() As Variant)
  Dim j As Integer
  j = 0
  While lstClickedBox.Selected(j) = False
    j = j + 1
  Wend
  If lstClickedBox.Selected(j) = True Then
    DisplayCardById clickedArray(j, 1)
  End If
End Sub

Private Sub lstExertion_Click()
  showClickedCard lstExertion, userExertion
End Sub

Private Sub lstExertion_DblClick()
  Call cmdPlayExertedCard_Click
End Sub

Private Sub lstHiddenAttacks_Click()
  showClickedCard lstHiddenAttacks, userHiddenAttacks
End Sub

Private Sub lstHiddenAttacks_DblClick()
  Call cmdRevealHiddenAttack_Click
End Sub

Private Sub lstOpponetDiscard_Click()
  showClickedCard lstOpponetDiscard, oppDiscardCards
End Sub

Private Sub lstOpponetInPlay_Click()
  showClickedCard lstOpponetInPlay, oppInPlayCards
End Sub

Private Sub Form_Load()
  datCardDeck.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardDeck.RecordSource = "cards"
  datCardDeck.Refresh
  loadingGame = False
  Path = App.Path
    lstDeck.Clear
    lstPreGame.Clear
    lstPlayerHand.Clear
    lstPlayerDiscard.Clear
    lstExertion.Clear
    lstPlayerInPlay.Clear
    lstTempStorage.Clear
    lstOpponetInPlay.Clear
    lstOpponetDiscard.Clear
    lstOppPreGame.Clear
    oppPreGamePos = 0
    userDeckPos = 0
    userPrePos = 0
    userHandPos = 0
    userInPlayPos = 0
    userDiscardPos = 0
    userExertionPos = 0
    userTempStoragePos = 0
    oppInPlayPos = 0
    oppDiscardPos = 0
    userHiddenAttacksPos = 0
    Open "gamelog.txt" For Append As #2
  Write #2, "----- Begin Transcript -----"

End Sub

Private Sub lstOppPreGame_Click()
  showClickedCard lstOppPreGame, oppPreGameCards
End Sub

Private Sub lstPlayerDiscard_Click()
  showClickedCard lstPlayerDiscard, userDiscardCards
End Sub

Private Sub lstPlayerHand_Click()
  showClickedCard lstPlayerHand, userHandCards
End Sub

Private Sub lstPlayerHand_DblClick()
  Call cmdPlayerPlayCard_Click
End Sub

Private Sub lstPlayerInPlay_Click()
  showClickedCard lstPlayerInPlay, userInPlayCards
End Sub

Private Sub lstPlayerInPlay_DblClick()
  Call cmdPlayerInPlayDiscard_Click
End Sub

Private Sub lstTempArea_Click()
  showClickedCard lstTempArea, tempArea
End Sub

Private Sub lstTempStorage_Click()
  showClickedCard lstTempStorage, userTempStorage
End Sub

Private Sub lstTempStorage_DblClick()
  Call cmdMoveToHand_Click
End Sub

Private Sub menuLoadSavedGame_Click()
  loadingGame = True
  Call LoadGame
End Sub

Private Sub menuSendCardsFromDeck_Click()
  Dim number As Variant
  Dim counter As Integer
  number = InputBox("This will Show the Top X Cards to your opponent.  Enter the number of cards to show (0 to cancel)", "Show X Cards From Deck")
  If Val(number) > 0 Then
    Write #2, txtName.text + " showed the top " + Str$(number) + " cards in deck to " + txtOpponetNameEmail.text
    wsChat.SendData "DisplayTempOpponent's Deck"
    wsChat.SendData "*"
    For counter = userDeckPos - number To userDeckPos - 1
      wsChat.SendData "AddTemp"
      wsChat.SendData userDeckCards(counter, 1)
      wsChat.SendData "*"
      Write #2, "          " + userDeckCards(counter, 3)
    Next counter
  End If
  AddText "You showed " + number + " cards from you deck to your opponent", txtIn
End Sub

Private Sub menuSendRandomCardsHand_Click()
  Dim j As Integer
  If userHandPos > 0 Then
    Randomize Timer
    j = Rnd(userHandPos)
    DisplayCardById (userHandCards(j, 1))
    wsChat.SendData "DisplayTempOpponent's Hand"
    wsChat.SendData "*"
    wsChat.SendData "AddTemp"
    wsChat.SendData userHandCards(j, 1)
    wsChat.SendData "*"
    Write #2, txtName.text + " showed " + txtCardNameDeck.text + " to " + txtOpponetNameEmail.text
    AddText "You showed " + txtCardNameDeck.text + " to your opponent", txtIn
  End If
End Sub

Private Sub menuSendSelectedCardHand_Click()
  Dim j As Integer
  j = 0
  If userHandPos > 0 Then
    While j < lstPlayerHand.ListCount
    
      If (lstPlayerHand.Selected(j) = True) Then
        DisplayCardById (userHandCards(j, 1))
        Write #2, txtName.text + " showed " + txtCardNameDeck.text + " to " + txtOpponetNameEmail.text
        wsChat.SendData "DisplayTempOpponent's Hand"
        wsChat.SendData "*"
        wsChat.SendData "AddTemp"
        wsChat.SendData userHandCards(j, 1)
        wsChat.SendData "*"
        AddText "You showed " + txtCardNameDeck.text + " to your opponent", txtIn
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub menuShowEntireDeckOpponent_Click()
  Dim counter As Integer
    wsChat.SendData "DisplayTempOpponent's Deck"
    wsChat.SendData "*"
    Write #2, txtName.text + " showed entire deck to " + txtOpponetNameEmail.text
    For counter = 0 To userDeckPos - 1
      wsChat.SendData "AddTemp"
      wsChat.SendData userDeckCards(counter, 1)
      wsChat.SendData "*"
    Next counter
  AddText "You showed your entire deck to your opponent", txtIn
End Sub

Private Sub menuShowEntireHandOpponent_Click()
  If userHandPos > 0 Then
  wsChat.SendData "DisplayTempOpponent's Hand"
  wsChat.SendData "*"
  Write #2, txtName.text + " showed your entire hand to " + txtOpponetNameEmail.text
  For counter = 0 To userHandPos - 1
    wsChat.SendData "AddTemp"
    wsChat.SendData userHandCards(counter, 1)
    wsChat.SendData "*"
    Write #2, "          " + userHandCards(counter, 3)
  Next counter
  AddText "You showed your opponent your entire hand.", txtIn
  End If
End Sub

Private Sub menuSortHandByBoth_Click()
  Call menuSortHandByName_Click
  Call menuSortHandByType_Click
End Sub

Private Sub menuSortHandByName_Click()
  For counter1 = 0 To userHandPos - 1
    For counter2 = 0 To userHandPos - 2
      If (userHandCards(counter2, 3) > userHandCards(counter2 + 1, 3)) Then
        Dim tempCard(4)
        tempCard(1) = userHandCards(counter2, 1)
        tempCard(2) = userHandCards(counter2, 2)
        tempCard(3) = userHandCards(counter2, 3)
        tempCard(4) = userHandCards(counter2, 4)
        
        userHandCards(counter2, 1) = userHandCards(counter2 + 1, 1)
        userHandCards(counter2, 2) = userHandCards(counter2 + 1, 2)
        userHandCards(counter2, 3) = userHandCards(counter2 + 1, 3)
        userHandCards(counter2, 4) = userHandCards(counter2 + 1, 4)
        
        userHandCards(counter2 + 1, 1) = tempCard(1)
        userHandCards(counter2 + 1, 2) = tempCard(2)
        userHandCards(counter2 + 1, 3) = tempCard(3)
        userHandCards(counter2 + 1, 4) = tempCard(4)
      End If
    Next counter2
  Next counter1
  
  lstPlayerHand.Clear
  For counter1 = 0 To userHandPos - 1
    lstPlayerHand.AddItem userHandCards(counter1, 3)
  Next counter1

End Sub

Private Sub menuSortHandByType_Click()
  For counter1 = 0 To userHandPos - 1
    For counter2 = 0 To userHandPos - 2
      If (userHandCards(counter2, 4) > userHandCards(counter2 + 1, 4)) Then
        Dim tempCard(4)
        tempCard(1) = userHandCards(counter2, 1)
        tempCard(2) = userHandCards(counter2, 2)
        tempCard(3) = userHandCards(counter2, 3)
        tempCard(4) = userHandCards(counter2, 4)
        
        userHandCards(counter2, 1) = userHandCards(counter2 + 1, 1)
        userHandCards(counter2, 2) = userHandCards(counter2 + 1, 2)
        userHandCards(counter2, 3) = userHandCards(counter2 + 1, 3)
        userHandCards(counter2, 4) = userHandCards(counter2 + 1, 4)
        
        userHandCards(counter2 + 1, 1) = tempCard(1)
        userHandCards(counter2 + 1, 2) = tempCard(2)
        userHandCards(counter2 + 1, 3) = tempCard(3)
        userHandCards(counter2 + 1, 4) = tempCard(4)
      End If
    Next counter2
  Next counter1
  
  lstPlayerHand.Clear
  For counter1 = 0 To userHandPos - 1
    lstPlayerHand.AddItem userHandCards(counter1, 3)
  Next counter1
End Sub

Private Sub NewGameMenu_Click()
  Call NewGame
End Sub

Private Sub OppPreGameShowHideMenu_Click()
  If frameOppPreGame.Visible = True Then
    frameOppPreGame.Visible = False
  Else
    frameOppPreGame.Visible = True
  End If
End Sub

Private Sub RollDiceMenu_Click()
  If frameRollDice.Visible = True Then
    frameRollDice.Visible = False
  Else
    Randomize Timer
    frameRollDice.Visible = True
  End If
End Sub

Private Sub SaveGameMenu_Click()
  Call SaveGame
End Sub

Private Sub timerCheckConnection_Timer()
  If wsChat.State = 8 Then
    Call CloseConnectionMenu_Click
    MsgBox "Connection Closed", vbOKOnly, "Game Over"
  End If
End Sub

Private Sub txtFileNameDeck_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileNameDeck.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSetDeck.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub lstDeck_Click()
  j = 0
  While j < lstDeck.ListCount
    If lstDeck.Selected(j) = True Then
  k = userDeckPos - j - 1
  DisplayCardById (userDeckCards(k, 1))
  End If
    j = j + 1
  Wend
End Sub

Private Sub lstPreGame_Click()
  Dim j As Integer
  j = 0
  While (lstPreGame.Selected(j) = False)
    j = j + 1
  Wend
  DisplayCardById (userPreGameCards(j, 1))
End Sub

Public Sub DisplayCardById(idToDisplay)
  datCardDeck.Recordset.MoveFirst
  datCardDeck.Recordset.Find ("Id = '" + idToDisplay + "'")
End Sub

Public Sub NewGame()
  On Error Resume Next
  Randomize Timer
  CommonDialog1.FileName = ""
  CommonDialog1.ShowOpen
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog1.FileName <> "") Then
    frmLoading.Visible = True
    txtSetDeck.DataField = ""
    txtFileNameDeck.DataField = ""
    frameCardDetailsDeck.Visible = False
    lstDeck.Clear
    lstPreGame.Clear
    lstPlayerHand.Clear
    lstPlayerDiscard.Clear
    lstExertion.Clear
    lstPlayerInPlay.Clear
    lstTempStorage.Clear
    lstOpponetInPlay.Clear
    lstOpponetDiscard.Clear
    lstOppPreGame.Clear
    oppPreGamePos = 0
    userDeckPos = 0
    userPrePos = 0
    userHandPos = 0
    userInPlayPos = 0
    userDiscardPos = 0
    userExertionPos = 0
    userTempStoragePos = 0
    oppInPlayPos = 0
    oppDiscardPos = 0
    userHiddenAttacksPos = 0
    
    Open CommonDialog1.FileName For Input As #1
    While Not EOF(1)
  
      'Code to update Status box during loop
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      Input #1, cardlocation, temp1, temp2
      If (cardlocation = "DECK") Then
        tempCounter = 0
        DisplayCardById temp1
        While (tempCounter < temp2)
          userDeckCards(userDeckPos, 1) = temp1
          userDeckCards(userDeckPos, 2) = Rnd()
          userDeckCards(userDeckPos, 3) = txtCardNameDeck.text
          userDeckCards(userDeckPos, 4) = txtTypeDeck.text
          userDeckPos = userDeckPos + 1
          tempCounter = tempCounter + 1
        Wend
      End If
      If (cardlocation = "PREGAME") Then
        tempCounter = 0
        DisplayCardById (temp1)
        While (tempCounter < temp2)
          userPreGameCards(userPrePos, 1) = temp1
          userPreGameCards(userPrePos, 2) = 1
          userPreGameCards(userPrePos, 3) = txtCardNameDeck.text
          userPreGameCards(userPrePos, 4) = txtTypeDeck.text
          lstPreGame.AddItem (txtCardNameDeck.text)
          userPrePos = userPrePos + 1
          tempCounter = tempCounter + 1
        Wend
      End If
    Wend
    Close #1
    
    For a = 0 To userDeckPos
      For b = 0 To userDeckPos - 2
        If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
          Dim tempCard(4)
      
          'Code to update Status box during loop
          DoEvents
          StatusBox.Left = StatusBox.Left + 1
          If StatusBox.Left >= frmLoading.Width Then
            StatusBox.Left = 0
          End If
          
          tempCard(1) = userDeckCards(b, 1)
          tempCard(2) = userDeckCards(b, 2)
          tempCard(3) = userDeckCards(b, 3)
          tempCard(4) = userDeckCards(b, 4)
          
          userDeckCards(b, 1) = userDeckCards(b + 1, 1)
          userDeckCards(b, 2) = userDeckCards(b + 1, 2)
          userDeckCards(b, 3) = userDeckCards(b + 1, 3)
          userDeckCards(b, 4) = userDeckCards(b + 1, 4)
          
          userDeckCards(b + 1, 1) = tempCard(1)
          userDeckCards(b + 1, 2) = tempCard(2)
          userDeckCards(b + 1, 3) = tempCard(3)
          userDeckCards(b + 1, 4) = tempCard(4)
        End If
      Next b
    Next a
          
    j = userDeckPos - 1
    While j >= 0
      
      'Code to update Status box during loop
      DoEvents
      StatusBox.Left = StatusBox.Left + 1
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      lstDeck.AddItem (userDeckCards(j, 3))
      j = j - 1
    Wend
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
    frameConnect.Visible = True
    frmLoading.Visible = False
    txtSetDeck.DataField = "Set"
    txtFileNameDeck.DataField = "File Name"
    frameCardDetailsDeck.Visible = True
  End If
End Sub

Public Sub SaveGame()
  On Error Resume Next
  CommonDialog2.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  
  If (CommonDialog2.FileName <> "") Then
    Open CommonDialog2.FileName For Output As #1
    
    Write #1, "USERTURNNUM", txtYourTurnNum.text, ""
    Write #1, "USERABILITY", txtYourAbility.text, ""
    Write #1, "OPPNAME", txtOpponetNameEmail.text, ""
    Write #1, "OPPABILITY", txtOpponetAbility.text, ""
    Write #1, "OPPTURNNUM", txtOpponetTurnNum.text, ""
    
    j = 0
    While j < userDeckPos
      Write #1, "USERDECK", userDeckCards(j, 1), userDeckCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userPrePos
      Write #1, "USERPREGAME", userPreGameCards(j, 1), userPreGameCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userHandPos
      Write #1, "USERHAND", userHandCards(j, 1), userHandCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userDiscardPos
      Write #1, "USERDISCARD", userDiscardCards(j, 1), userDiscardCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userExertionPos
      Write #1, "USEREXERTION", userExertion(j, 1), userExertion(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userInPlayPos
      Write #1, "USERINPLAY", userInPlayCards(j, 1), userInPlayCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < userTempStoragePos
      Write #1, "USERTEMPSTORAGE", userTempStorage(j, 1), userTempStorage(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < oppInPlayPos
      Write #1, "OPPINPLAY", oppInPlayCards(j, 1), oppInPlayCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < oppDiscardPos
      Write #1, "OPPDISCARD", oppDiscardCards(j, 1), oppDiscardCards(j, 2)
      j = j + 1
    Wend
    
    j = 0
    While j < oppPreGamePos
      Write #1, "OPPPREGAME", oppPreGameCards(j, 1), oppPreGameCards(j, 2)
      j = j + 1
    Wend
       
    Close #1
  End If
End Sub

Public Sub LoadGame()
  On Error Resume Next
  CommonDialog2.FileName = ""
  CommonDialog2.ShowOpen
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog2.FileName <> "") Then
    frmLoading.Visible = True
    lstDeck.Clear
    lstPreGame.Clear
    lstPlayerHand.Clear
    lstPlayerDiscard.Clear
    lstExertion.Clear
    lstPlayerInPlay.Clear
    lstTempStorage.Clear
    lstOpponetInPlay.Clear
    lstOpponetDiscard.Clear
    lstOppPreGame.Clear
    oppPreGamePos = 0
    userDeckPos = 0
    userPrePos = 0
    userHandPos = 0
    userInPlayPos = 0
    userDiscardPos = 0
    userExertionPos = 0
    userTempStoragePos = 0
    oppInPlayPos = 0
    oppDiscardPos = 0
    userHiddenAttacksPos = 0
    
    Open CommonDialog2.FileName For Input As #1
    While Not EOF(1)
      'Code to update Status box during loop
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      Input #1, cardlocation, temp1, temp2
      
      If (cardlocation = "USERTURNNUM") Then
        txtYourTurnNum.text = temp1
      End If
      If (cardlocation = "USERABILITY") Then
        txtYourAbility.text = temp1
      End If
      
      If (cardlocation = "USERDECK") Then
        DisplayCardById temp1
        userDeckCards(userDeckPos, 1) = temp1
        userDeckCards(userDeckPos, 2) = temp2
        userDeckCards(userDeckPos, 3) = txtCardNameDeck.text
        userDeckCards(userDeckPos, 4) = txtTypeDeck.text
        userDeckPos = userDeckPos + 1
      End If
      If (cardlocation = "USERPREGAME") Then
        DisplayCardById (temp1)
        userPreGameCards(userPrePos, 1) = temp1
        userPreGameCards(userPrePos, 2) = temp2
        userPreGameCards(userPrePos, 3) = txtCardNameDeck.text
        userPreGameCards(userPrePos, 4) = txtTypeDeck.text
        lstPreGame.AddItem (txtCardNameDeck.text)
        userPrePos = userPrePos + 1
      End If
      If (cardlocation = "USERHAND") Then
        DisplayCardById (temp1)
        userHandCards(userHandPos, 1) = temp1
        userHandCards(userHandPos, 2) = temp2
        userHandCards(userHandPos, 3) = txtCardNameDeck.text
        userHandCards(userHandPos, 4) = txtTypeDeck.text
        lstPlayerHand.AddItem (txtCardNameDeck.text)
        userHandPos = userHandPos + 1
      End If
      If (cardlocation = "USERDISCARD") Then
        DisplayCardById (temp1)
        userDiscardCards(userDiscardPos, 1) = temp1
        userDiscardCards(userDiscardPos, 2) = temp2
        userDiscardCards(userDiscardPos, 3) = txtCardNameDeck.text
        userDiscardCards(userDiscardPos, 4) = txtTypeDeck.text
        lstPlayerDiscard.AddItem (txtCardNameDeck.text)
        userDiscardPos = userDiscardPos + 1
      End If
      If (cardlocation = "USEREXERTION") Then
        DisplayCardById (temp1)
        userExertion(userExertionPos, 1) = temp1
        userExertion(userExertionPos, 2) = temp2
        userExertion(userExertionPos, 3) = txtCardNameDeck.text
        userExertion(userExertionPos, 4) = txtTypeDeck.text
        lstExertion.AddItem (txtCardNameDeck.text)
        userExertionPos = userExertionPos + 1
      End If
      If (cardlocation = "USERINPLAY") Then
        DisplayCardById (temp1)
        userInPlayCards(userInPlayPos, 1) = temp1
        userInPlayCards(userInPlayPos, 2) = temp2
        userInPlayCards(userInPlayPos, 3) = txtCardNameDeck.text
        userInPlayCards(userInPlayPos, 4) = txtTypeDeck.text
        lstPlayerInPlay.AddItem (txtCardNameDeck.text)
        userInPlayPos = userInPlayPos + 1
      End If
      If (cardlocation = "USERTEMPSTORAGE") Then
        DisplayCardById (temp1)
        userTempStorage(userTempStoragePos, 1) = temp1
        userTempStorage(userTempStoragePos, 2) = temp2
        userTempStorage(userTempStoragePos, 3) = txtCardNameDeck.text
        userTempStorage(userTempStoragePos, 4) = txtTypeDeck.text
        lstTempStorage.AddItem (txtCardNameDeck.text)
        userTempStoragePos = userTempStoragePos + 1
      End If
    Wend
    Close #1

    For a = 0 To userDeckPos
      For b = 0 To userDeckPos - 2
        If userDeckCards(b, 2) > userDeckCards(b + 1, 2) Then
          'Code to update Status box during loop
          DoEvents
          StatusBox.Left = StatusBox.Left + 1
          If StatusBox.Left >= frmLoading.Width Then
            StatusBox.Left = 0
          End If
          
          Dim tempCard(4)
          tempCard(1) = userDeckCards(b, 1)
          tempCard(2) = userDeckCards(b, 2)
          tempCard(3) = userDeckCards(b, 3)
          tempCard(4) = userDeckCards(b, 4)
          
          userDeckCards(b, 1) = userDeckCards(b + 1, 1)
          userDeckCards(b, 2) = userDeckCards(b + 1, 2)
          userDeckCards(b, 3) = userDeckCards(b + 1, 3)
          userDeckCards(b, 4) = userDeckCards(b + 1, 4)
          
          userDeckCards(b + 1, 1) = tempCard(1)
          userDeckCards(b + 1, 2) = tempCard(2)
          userDeckCards(b + 1, 3) = tempCard(3)
          userDeckCards(b + 1, 4) = tempCard(4)
        End If
      Next b
    Next a
          
    j = userDeckPos - 1
    While j >= 0
      lstDeck.AddItem userDeckCards(j, 3) '(txtCardNameDeck.text)
      j = j - 1
    Wend
    frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
    cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
    frameConnect.Visible = True
    frmLoading.Visible = False
  End If
End Sub

Private Sub txtSetDeck_Change()
  Call txtFileNameDeck_Change
End Sub

Private Sub txtYourAbility_Change()
  If loadingGame = False Then
    wsChat.SendData "ability"
    wsChat.SendData txtYourAbility.text
    wsChat.SendData "*"
  End If
End Sub

Private Sub txtYourTurnNum_Change()
  If loadingGame = False Then
    wsChat.SendData "turnNum"
    wsChat.SendData txtYourTurnNum.text
    wsChat.SendData "*"
    Write #2, txtName.text + ":  Turn #" + txtYourTurnNum.text
  End If
End Sub

Private Sub YourDeckMenu_Click()
  If frameDeck.Visible = True Then
    frameDeck.Visible = False
  Else
    frameDeck.Visible = True
    Write #2, txtName.text + " looked at his deck"
    wsChat.SendData "addChat"
    wsChat.SendData "Opponent is looking at his deck"
    wsChat.SendData "*"
  End If
End Sub

Private Sub YourPreGameMenu_Click()
  If frmPreGame.Visible = True Then
    frmPreGame.Visible = False
  Else
    frmPreGame.Visible = True
  End If
End Sub

Private Sub TransferCardFromLstToLst(ByRef sendingLst As ListBox, ByRef sendingArray() As Variant, ByRef sendingPos As Integer, ByRef receivingLst As ListBox, ByRef receivingArray(), ByRef receivingPos As Integer, logCode As String)
  Dim j As Integer
  j = 0
  
  If sendingPos > 0 Then
    While (j < sendingLst.ListCount)
      If (sendingLst.Selected(j) = True) Then
        k = j
    
        DisplayCardById (sendingArray(k, 1))
        If logCode = "Discard1TopEndurance" Then   'cmd1CardDiscardToEndurance_Click()
          wsChat.SendData "oppDiscardCardRemove"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          wsChat.SendData "addChat"
          wsChat.SendData "Opponent Moves " & txtCardNameDeck.text & " from discard to top of deck"
          wsChat.SendData "*"
          Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from discard pile to top of endurance"
        ElseIf logCode = "fromHandToTopDeck" Then  'cmdDiscardToTopOfDeck_Click()
          wsChat.SendData "addChat"
          wsChat.SendData "Opponent discards a card from hand to the top of the deck."
          wsChat.SendData "*"
          Write #2, txtName.text + " discards " + txtCardNameDeck.text + " from hand to top of deck"
        ElseIf logCode = "userDojoToDiscard" Then
          wsChat.SendData "oppDiscardCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          wsChat.SendData "addChat"
          wsChat.SendData "Opponent discards " & txtCardNameDeck.text & " from the dojo."
          wsChat.SendData "*"
          Write #2, txtName.text + " discards " + txtCardNameDeck.text; " from dojo to discard"
        ElseIf logCode = "playHiddenAttack" Then   'cmdHiddenAttack_Click()
          wsChat.SendData "oppPlayCardAdd"
          wsChat.SendData "2425" 'Database ID for "Hidden Attack" card
          wsChat.SendData "*"
          Write #2, txtName.text + " plays " + txtCardNameDeck.text + " as a hidden attack"
        ElseIf logCode = "fromDojoToHand" Then 'cmdMoveToHand_Click()
          wsChat.SendData "addChat"
          wsChat.SendData "Opponent moves card from dojo to hand"
          wsChat.SendData "*"
          Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from dojo to hand"
        ElseIf logCode = "fromHandToDiscard" Then   'cmdPlayerDiscard_Click
          wsChat.SendData "oppDiscardCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          Write #2, txtName.text + " discards " + txtCardNameDeck.text + " from hand"
        ElseIf logCode = "userFromDiscardToPlay" Then 'cmdPlayerDiscardToPlay_Click
          wsChat.SendData "oppPlayCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          wsChat.SendData "oppDiscardCardRemove"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from discard to play"
        ElseIf logCode = "userPlayToDiscard" Then  'cmdPlayerInPlayDiscard_Click
          wsChat.SendData "oppPlayCardRemove"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          wsChat.SendData "oppDiscardCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          Write #2, txtName.text + " sweeps " + txtCardNameDeck.text + " from play"
        ElseIf logCode = "userPlayToHand" Then 'cmdPlayerInPlayToHand_Click
          wsChat.SendData "oppPlayCardRemove"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          wsChat.SendData "addChat"
          wsChat.SendData "Opponent moves " & txtCardNameDeck.text & " from play to hand"
          wsChat.SendData "*"
          Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from play to hand"
        ElseIf logCode = "userHandToPlay" Then   'cmdPlayerPlayCard_Click
          wsChat.SendData "oppPlayCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          Write #2, txtName.text + " plays " + txtCardNameDeck.text + " from hand"
        ElseIf logCode = "toPlayFromExertion" Then 'cmdPlayExertedCard_Click
          wsChat.SendData "oppPlayCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          wsChat.SendData "addChat"
          wsChat.SendData "Opponent plays " & txtCardNameDeck.text & " from exertion"
          wsChat.SendData "*"
          Write #2, txtName.text + " plays " + txtCardNameDeck.text + " from Exertion"
        ElseIf logCode = "userRevealHidden" Then 'cmdRevealHiddenAttack_Click
          wsChat.SendData "oppPlayCardRemove"
          wsChat.SendData "2425"
          wsChat.SendData "*"
          wsChat.SendData "oppPlayCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          Write #2, txtName.text + " reveals hidden attack: " + txtCardNameDeck.text
        ElseIf logCode = "userHandToDojo" Then 'cmdToTempStorage_Click
          wsChat.SendData "addChat"
          wsChat.SendData "Opponent moves a card to dojo."
          wsChat.SendData "*"
          Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from hand to dojo"
        ElseIf logCode = "userPreGameToPlay" Then 'cmdUserPreGameToPlay_Click
          wsChat.SendData "oppPreGameRemove"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          wsChat.SendData "oppPlayCardAdd"
          wsChat.SendData sendingArray(k, 1)
          wsChat.SendData "*"
          Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from Pre-Game to Play"
        Else
          MsgBox "Error [TransferCards:  transfer code unknown:" + logCode + "]", vbOKOnly, "Programming Error"
          Write #2, "Error [TransferCards:  transfer code unknown:" + logCode + "]"
        End If
        'Write #2, txtName.text + " moves " + txtCardNameDeck.text + " from " + sendingLst.name + " to " + receivingLst.name
        receivingLst.AddItem (txtCardNameDeck.text)
        receivingArray(receivingPos, 1) = sendingArray(k, 1)
        receivingArray(receivingPos, 2) = sendingArray(k, 2)
        receivingArray(receivingPos, 3) = sendingArray(k, 3)
        receivingArray(receivingPos, 4) = sendingArray(k, 4)
        receivingPos = receivingPos + 1
  
        While (k < sendingPos)
          sendingArray(k, 1) = sendingArray(k + 1, 1)
          sendingArray(k, 2) = sendingArray(k + 1, 2)
          sendingArray(k, 3) = sendingArray(k + 1, 3)
          sendingArray(k, 4) = sendingArray(k + 1, 4)
          k = k + 1
        Wend
        sendingPos = sendingPos - 1
        sendingLst.RemoveItem (j)
        j = j - 1
      End If
      j = j + 1
    Wend
  End If
  
  ' Don't update frameTempStorage.Caption here
  framePlayerDiscard.Caption = "Discard (" + Str$(userDiscardPos) + ")"
  frameDeck.Caption = "Deck (" + Str$(userDeckPos) + ")"
  cmdPlayerDrawCard.Caption = "Draw Card (" + Str$(userDeckPos) + ")"
  frameHand.Caption = "Hand (" + Str$(userHandPos) + ")"
  framePlayerInPlay.Caption = "In Play (" + Str$(userInPlayPos) + ")"
  frmPreGame.Caption = "Pre-Game (" + Str$(userPrePos) + ")"
End Sub

Private Sub cmdConnect_Click()
  If txtIP.text = "" Or txtName.text = "" Then
    MsgBox "You must enter both an IP and alias first!", vbCritical, "Error!"
    txtName.SetFocus
    Exit Sub
  End If
  On Error Resume Next
  wsChat.Close
  wsChat.Connect txtIP.text, 1234
  cmdClose.Enabled = True
  cmdListen.Enabled = False
  cmdConnect.Enabled = False
  txtName.Enabled = False
End Sub

Private Sub cmdListen_Click()
  If txtName.text = "" Then
    MsgBox "You must enter an alias first!", vbCritical, "Error!"
    txtName.SetFocus
    Exit Sub
  End If
  txtIP.text = wsChat.LocalIP
  wsChat.Close
  wsChat.LocalPort = 1234
  wsChat.Listen
  cmdListen.Enabled = False
  cmdConnect.Enabled = False
  txtName.Enabled = False
  AddText "----- Waiting for Connection -----", txtIn
End Sub

Private Sub cmdSend_Click()
  wsChat.SendData "addChat"
  wsChat.SendData "[" & txtName.text & "] " & txtOut.text
  wsChat.SendData "*"
  AddText "[" & txtName.text & "] " & txtOut.text, txtIn
  Write #2, "[" & txtName.text & "] " & txtOut.text
  txtOut.text = ""
  txtOut.SetFocus
End Sub

Private Sub wsChat_Connect()
  On Error Resume Next
  Do
    DoEvents
  Loop Until wsChat.State = sckConnected Or wsChat.State = sckError

  If wsChat.State = sckConnected Then
    AddText "----- Connection Established -----" & vbCrLf, txtIn
    menushowcardstoOpponent.Enabled = True
    frameCover.Visible = False
    frameConnect.Visible = False
    wsChat.SendData "oppName"
    wsChat.SendData txtName.text
    wsChat.SendData "*"
    sendPreGame
    cmdSend.Enabled = True
    txtName.Enabled = False
    txtOut.SetFocus
  Else
    AddText "----- Connection Failed -----" & vbCrLf, txtIn
    MsgBox "Connection Failed", vbOKOnly, "Error"
  End If
End Sub

Private Sub AddText(ByVal text As String, ByRef Box As TextBox)
  Box.text = Box.text & text & vbCrLf
  Box.SelStart = Len(Box.text)
End Sub

Private Sub wschat_ConnectionRequest(ByVal requestID As Long)
  On Error Resume Next
  wsChat.Close
  wsChat.Accept requestID
  AddText "----- Connection Established -----" & vbCrLf, txtIn
  menushowcardstoOpponent.Enabled = True
  frameCover.Visible = False
  frameConnect.Visible = False
  wsChat.SendData "oppName"
  wsChat.SendData txtName.text
  wsChat.SendData "*"
  sendPreGame
  cmdSend.Enabled = True
  txtName.Enabled = False
  txtOut.SetFocus
End Sub

Private Sub wschat_DataArrival(ByVal bytesTotal As Long)
  Dim incoming As String
  wsChat.GetData incoming
  While (Len(incoming) > 0)
    i = 1
    While (Mid$(incoming, i, 1) <> "*")
      i = i + 1
    Wend
    tempStr = Left(incoming, i - 1)
    incoming = Right(incoming, Len(incoming) - i)
    If Left(tempStr, 7) = "addChat" Then
      AddText Right(tempStr, Len(tempStr) - 7), txtIn
      Write #2, txtOpponetNameEmail.text + ":  " + Right(tempStr, Len(tempStr) - 7)
    End If
    If (Left(tempStr, 7) = "endGame") Then
      Call CloseConnectionMenu_Click
      MsgBox "Connection Closed by Opponent", vbOKOnly, "Game Over"
    End If
    If (Left(tempStr, 7) = "turnNum") Then
      txtOpponetTurnNum.text = Right(tempStr, Len(tempStr) - 7)
    End If
    If (Left(tempStr, 7) = "ability") Then
      txtOpponetAbility.text = Right(tempStr, Len(tempStr) - 7)
    End If
    If (Left(tempStr, 7) = "oppName") Then
      txtOpponetNameEmail.text = Right(tempStr, Len(tempStr) - 7)
    End If
    If (Left(tempStr, 13) = "oppPreGameAdd") Then
      AddCardToList Right(tempStr, Len(tempStr) - 13), lstOppPreGame, oppPreGameCards, oppPreGamePos
      Write #2, "Opponent's Pre Game Cards: " + txtCardNameDeck.text
    End If
    If (Left(tempStr, 14) = "oppPlayCardAdd") Then
      AddCardToList Right(tempStr, Len(tempStr) - 14), lstOpponetInPlay, oppInPlayCards, oppInPlayPos
      Write #2, txtOpponetNameEmail.text + " Puts " + txtCardNameDeck.text + " into play"
    End If
    If (Left(tempStr, 17) = "oppPlayCardRemove") Then
      RemoveCardFromList Right(tempStr, Len(tempStr) - 17), lstOpponetInPlay, oppInPlayCards, oppInPlayPos
      Write #2, txtOpponetNameEmail.text + " Removes " + txtCardNameDeck.text + " from play"
    End If
    If (Left(tempStr, 17) = "oppDiscardCardAdd") Then
      AddCardToList Right(tempStr, Len(tempStr) - 17), lstOpponetDiscard, oppDiscardCards, oppDiscardPos
      Write #2, txtOpponetNameEmail.text + " Puts " + txtCardNameDeck.text + " into discard."
    End If
    If (Left(tempStr, 20) = "oppDiscardCardRemove") Then
      RemoveCardFromList Right(tempStr, Len(tempStr) - 20), lstOpponetDiscard, oppDiscardCards, oppDiscardPos
      Write #2, txtOpponetNameEmail.text + " Removes " + txtCardNameDeck.text + " From Discard"
    End If
    If (Left(tempStr, 15) = "oppClearDiscard") Then
      oppDiscardPos = 0
      lstOpponetDiscard.Clear
      Write #2, txtOpponetNameEmail.text + " Clears all discarded cards (probably reshuffled into deck)"
    End If
    If (Left(tempStr, 16) = "oppPreGameRemove") Then
      RemoveCardFromList Right(tempStr, Len(tempStr) - 16), lstOppPreGame, oppPreGameCards, oppPreGamePos
      Write #2, txtOpponetNameEmail.text + " Removes " + txtCardNameDeck.text + " from pre-game"
    End If
    If (Left(tempStr, 11) = "DisplayTemp") Then
      frameTempArea.Visible = True
      frameTempArea.Caption = Right(tempStr, Len(tempStr) - 11)
      oppTempAreaPos = 0
      lstTempArea.Clear
      Write #2, Right(tempStr, Len(tempStr) - 11) + " has been displayed"
    End If
    If (Left(tempStr, 7) = "AddTemp") Then
      AddCardToList Right(tempStr, Len(tempStr) - 7), lstTempArea, tempArea, oppTempAreaPos
      Write #2, txtOpponetNameEmail.text + " shows " + txtCardNameDeck.text
    End If
  Wend
End Sub

Private Sub wschat_Error(ByVal number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
  If number <> 0 Then
    AddText "----- Error [" & Description & "] -----" & vbCrLf, txtIn
    Write #2, "----- Error [" & Description & "] -----"
    Call CloseConnectionMenu_Click
    MsgBox "Communication Error [" & Description & "]", vbOKOnly, "Game Error"
  End If
End Sub

Public Sub AddCardToList(ByVal cardID, ByRef tempLst As ListBox, ByRef tempArray() As Variant, ByRef tempPos As Integer)
  DisplayCardById (cardID)
  tempLst.AddItem (txtCardNameDeck.text)
  tempArray(tempPos, 1) = cardID
  tempArray(tempPos, 2) = 1
  tempArray(tempPos, 3) = txtCardNameDeck.text
  tempArray(tempPos, 4) = txtTypeDeck.text
  tempPos = tempPos + 1
End Sub

Public Sub RemoveCardFromList(ByVal cardID, ByRef tempLst As ListBox, ByRef tempArray() As Variant, ByRef tempPos As Integer)
  For i = 0 To tempPos - 1
    If tempArray(i, 1) = cardID Then
      j = i
      Exit For
    End If
  Next i
  k = j
  While (k < tempPos)
      tempArray(k, 1) = tempArray(k + 1, 1)
      tempArray(k, 2) = tempArray(k + 1, 2)
      tempArray(k, 3) = tempArray(k + 1, 3)
      tempArray(k, 4) = tempArray(k + 1, 4)
      k = k + 1
  Wend
  tempPos = tempPos - 1
  tempLst.RemoveItem (j)
End Sub

Public Sub sendPreGame()
  Dim counter
  counter = 0
  While counter < userPrePos
    wsChat.SendData "oppPreGameAdd"
    wsChat.SendData userPreGameCards(counter, 1)
    wsChat.SendData "*"
    counter = counter + 1
  Wend
  If loadingGame = True Then
    wsChat.SendData "ability"
    wsChat.SendData txtYourAbility.text
    wsChat.SendData "*"
    wsChat.SendData "turnNum"
    wsChat.SendData txtYourTurnNum.text
    wsChat.SendData "*"
    counter = 0
    While counter < userInPlayPos
      wsChat.SendData "oppPlayCardAdd"
      wsChat.SendData userInPlayCards(counter, 1)
      wsChat.SendData "*"
      counter = counter + 1
    Wend
    counter = 0
    While counter < userDiscardPos
      wsChat.SendData "oppDiscardCardAdd"
      wsChat.SendData userDiscardCards(counter, 1)
      wsChat.SendData "*"
      counter = counter + 1
    Wend
    loadingGame = False
  End If
End Sub
