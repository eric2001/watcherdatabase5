VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMLEPrintingWizard 
   Caption         =   "Print MLE Cards"
   ClientHeight    =   6150
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   12615
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   410
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   841
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frameSelectSet 
      Height          =   855
      Left            =   4920
      TabIndex        =   38
      Top             =   5160
      Width           =   4095
      Begin VB.CommandButton cmdShowSet 
         Caption         =   "Select Set"
         Height          =   495
         Left            =   2520
         TabIndex        =   40
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox cmboSet 
         Height          =   315
         ItemData        =   "frmMLEPrintingWizard.frx":0000
         Left            =   120
         List            =   "frmMLEPrintingWizard.frx":0070
         TabIndex        =   39
         Text            =   "Set Names"
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.Frame frameFrontBack 
      Height          =   855
      Left            =   9720
      TabIndex        =   35
      Top             =   5160
      Width           =   2415
      Begin VB.CommandButton cmdShowFront 
         Caption         =   "Show Front"
         Height          =   495
         Left            =   120
         TabIndex        =   37
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdShowBack 
         Caption         =   "Show Back"
         Height          =   495
         Left            =   1320
         TabIndex        =   36
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame frameOtherButtons 
      Height          =   1695
      Left            =   4560
      TabIndex        =   32
      Top             =   2760
      Width           =   1455
      Begin VB.CommandButton cmdPrintCards 
         Caption         =   "Print"
         Height          =   495
         Left            =   120
         TabIndex        =   34
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "Clear List"
         Height          =   495
         Left            =   120
         TabIndex        =   33
         Top             =   1080
         Width           =   1215
      End
   End
   Begin VB.Frame frameAddDel 
      Height          =   2175
      Left            =   4560
      TabIndex        =   28
      Top             =   240
      Width           =   1455
      Begin VB.CommandButton cmdSelectedImage1 
         Caption         =   "Add Front >>"
         Height          =   495
         Left            =   120
         TabIndex        =   31
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton cmdRemoveCardFromList 
         Caption         =   "<< Delete"
         Height          =   495
         Left            =   120
         TabIndex        =   30
         Top             =   1560
         Width           =   1215
      End
      Begin VB.CommandButton cmdAddBackToList 
         Caption         =   "Add Back >>"
         Height          =   495
         Left            =   120
         TabIndex        =   29
         Top             =   840
         Width           =   1215
      End
   End
   Begin VB.Frame frameSort 
      Height          =   855
      Left            =   2400
      TabIndex        =   25
      Top             =   5160
      Width           =   2055
      Begin VB.CommandButton cmdSortName 
         Caption         =   "Sort By Name"
         Height          =   495
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdSortSet 
         Caption         =   "Sort By Set"
         Height          =   495
         Left            =   1080
         TabIndex        =   26
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame frameNav 
      Height          =   855
      Left            =   120
      TabIndex        =   22
      Top             =   5160
      Width           =   2055
      Begin VB.CommandButton cmdMoveBack1Record 
         Caption         =   "Previous Card"
         Height          =   495
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdMoveForward1Record 
         Caption         =   "Next Card"
         Height          =   495
         Left            =   1080
         TabIndex        =   23
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.ListBox lstMLECardList 
      Height          =   4350
      Left            =   6120
      TabIndex        =   19
      Top             =   360
      Width           =   2895
   End
   Begin VB.CheckBox checkValidFileNames 
      Caption         =   "Only show cards with images"
      Height          =   255
      Left            =   840
      TabIndex        =   18
      Top             =   4920
      Width           =   2535
   End
   Begin MSComDlg.CommonDialog CommonDialog3 
      Left            =   2160
      Top             =   7200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Filter          =   "JPEG Files (*.jpg)|*.jpg|All Files (*.*)|*.*"
   End
   Begin MSDataGridLib.DataGrid dataCards 
      Bindings        =   "frmMLEPrintingWizard.frx":0146
      Height          =   4815
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   8493
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   0   'False
      DefColWidth     =   273
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Card Details"
      Height          =   5775
      Left            =   4200
      TabIndex        =   0
      Top             =   6720
      Visible         =   0   'False
      Width           =   5415
      Begin VB.TextBox txtCardID 
         DataField       =   "ID"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2760
         TabIndex        =   20
         Top             =   1680
         Width           =   615
      End
      Begin MSComDlg.CommonDialog CommonDialog2 
         Left            =   360
         Top             =   3720
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "csv"
         Filter          =   "csv"
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   360
         Top             =   3000
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.TextBox txtCardText 
         DataField       =   "Text"
         DataSource      =   "datCardNames"
         Height          =   2565
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   14
         Top             =   2160
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         DataField       =   "Other"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   12
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox txtType 
         DataField       =   "Type"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   10
         Top             =   1440
         Width           =   2415
      End
      Begin VB.TextBox txtNum 
         DataField       =   "Number"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   8
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox txtCardName 
         DataField       =   "Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   4
         Top             =   360
         Width           =   2415
      End
      Begin VB.TextBox txtSet 
         DataField       =   "Set"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox txtFileName 
         DataField       =   "File Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox txtRarity 
         DataField       =   "Rarity"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label lblFileName 
         AutoSize        =   -1  'True
         Caption         =   "File Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   1800
         Width           =   750
      End
      Begin VB.Label lblCardText 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   2160
         Width           =   735
      End
      Begin VB.Label lblOther 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   2400
         TabIndex        =   13
         Top             =   1080
         Width           =   435
      End
      Begin VB.Label lblType 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   405
      End
      Begin VB.Label lblNumber 
         AutoSize        =   -1  'True
         Caption         =   "Quantity:"
         Height          =   195
         Left            =   2160
         TabIndex        =   9
         Top             =   720
         Width           =   630
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   840
      End
      Begin VB.Label lblSet 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   285
      End
      Begin VB.Label lblRarity 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   1080
         Width           =   450
      End
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   720
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "PrintMLECardsDatabase"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblTotalToPrint 
      Caption         =   "0 Cards (0 Pages)"
      Height          =   255
      Left            =   6120
      TabIndex        =   21
      Top             =   0
      Width           =   2775
   End
   Begin VB.Image HLCardDeck 
      Height          =   4680
      Left            =   9120
      Stretch         =   -1  'True
      Top             =   120
      Width           =   3420
   End
   Begin VB.Image HLCard 
      Height          =   75600
      Left            =   -840
      Stretch         =   -1  'True
      Top             =   4800
      Visible         =   0   'False
      Width           =   54000
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuFind 
         Caption         =   "Find..."
         Shortcut        =   ^F
      End
      Begin VB.Menu menuExit 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
End
Attribute VB_Name = "frmMLEPrintingWizard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Created By:           Eric Cavaliere
' Program Name:         Watcher Database
' Project File Name:    Highlander.vbp
' Project Started on:   August 05, 2002
' Name of this File:    frmMLEPrintingWizard.frm
' This file started on: November 28, 2002
' Last Updated on:      22 October 2007

'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

' ID, File Name Title
Dim cardsToPrint(500, 3)
Dim printCardPos As Integer
Dim printCardTotalPos As Integer ' not currently in use

Private Sub checkValidFileNames_Click()
  databaseFilter
End Sub

Private Sub cmdAddBackToList_Click()
  addCardToPrintList False
  updatePrintTotals
End Sub

Private Sub cmdMoveBack1Record_Click()
  If datCardNames.Recordset.BOF = False Then
    If datCardNames.Recordset.EOF = False Then
      datCardNames.Recordset.Update
    End If
    datCardNames.Recordset.MovePrevious
  End If
End Sub

Private Sub cmdMoveForward1Record_Click()
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update
    End If
    datCardNames.Recordset.MoveNext
  End If
End Sub

Private Sub cmdPrintCards_Click()
' Conversion:
' (cm * .393700787) * 1440 = number to use for height / width to twips conversion
  On Error Resume Next
  Dim rowPos As Integer
  Dim rowPage As Integer
  Dim counter As Integer
  Dim imageleft As Integer
  Dim imageTop As Integer
  
  CommonDialog1.ShowPrinter
  
  imageleft = 150
  imageTop = 300
  HLCard.Width = 2.5 * 1440
  HLCard.Height = 3.5 * 1440
  rowPos = 0
  rowPage = 0
  counter = 0

  Printer.Orientation = vbPRORLandscape
  While counter < printCardPos
    DoEvents
    HLCard.Picture = LoadPicture(cardsToPrint(counter, 2))
    Printer.PaintPicture HLCard, imageleft, imageTop, HLCard.Width, HLCard.Height
    rowPos = rowPos + 1
    counter = counter + 1
    imageleft = imageleft + 150 + HLCard.Width
    If rowPos = 4 Then ' if forth card printed in a row
        rowPos = 0
        imageleft = 150
        rowPage = rowPage + 1
        imageTop = imageTop + HLCard.Height + 300
    End If
    If rowPage = 2 Then ' new page
        Printer.EndDoc
        imageleft = 150
        imageTop = 300
        rowPos = 0
        rowPage = 0
    End If

  Wend
  If rowPage > 0 Or rowPos > 0 Then
    Printer.EndDoc
  End If
End Sub

Private Sub cmdRemoveCardFromList_Click()
  Delete1CardFromList lstMLECardList, cardsToPrint, printCardPos, printCardTotalPos
  updatePrintTotals
End Sub

Private Sub cmdReset_Click()
  lstMLECardList.Clear
  printCardPos = 0
End Sub

Private Sub cmdSelectedImage1_Click()
  addCardToPrintList True
  updatePrintTotals
End Sub

Private Sub cmdShowBack_Click()
'  If user presses the "Show card back" button
  On Error Resume Next ' in case there's a problem loading an image, skip to the next line
  Dim FileName As String
  HLCardDeck.Picture = LoadPicture(App.Path + "\empty.jpg") ' load the card back, in case of an error
                                                        '   loading a different image, the program
                                                        '   should continue displaying this
  FileName = getImageLocation(txtFileName.text, 2) ' get the image back, if there is one
  If FileName <> "" Then 'If a file name is found...
    If Mid$(FileName, 2, 1) = ":" Then  ' If second character in file name is ':' then assume full path
      HLCardDeck.Picture = LoadPicture(FileName)
    Else ' assume default path...
      HLCardDeck.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If

End Sub

Private Sub cmdShowFront_Click()
  Call txtFileName_Change
End Sub

Private Sub cmdShowSet_Click()
  databaseFilter
End Sub

Private Sub cmdSortName_Click()
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub cmdSortSet_Click()
  datCardNames.Recordset.Sort = "Set, Name"
End Sub

Private Sub Form_Load()
  On Error Resume Next
  
  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh
printCardPos = 0
  HLCard.Stretch = True
    HLCardDeck.Picture = LoadPicture(App.Path + "\empty.jpg")
  If txtFileName.text <> "" Then
    If Mid$(txtFileName.text, 2, 1) = ":" Then
      HLCardDeck.Picture = LoadPicture(txtFileName.text)
    Else
      HLCardDeck.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + txtFileName.text)
    End If
  End If
  dataCards.Columns("ID").Visible = False
  dataCards.Columns("Set").Visible = False
  dataCards.Columns("Number").Visible = False
  dataCards.Columns("File Name").Visible = False
  dataCards.Columns("Other").Visible = False
  dataCards.Columns("Text").Visible = False
  dataCards.Columns("Type").Visible = False
  dataCards.Columns("Rarity").Visible = False
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub lstMLECardList_Click()
  DisplayClickedCard lstMLECardList, cardsToPrint
End Sub

Private Sub menuExit_Click()
  frmMLEPrintingWizard.Visible = False
  Unload frmMLEPrintingWizard
End Sub

Private Sub menuFind_Click()
  On Error Resume Next
  Dim searchString As String
  Dim i As Integer
  
  datCardNames.Recordset.Filter = ""
  
  searchString = InputBox("Enter All or part of the title to search for:", "Find Card(s)")
  
  If searchString <> "" Then
    For i = 1 To Len(searchString)
      If (Mid$(searchString, i, 1) = "'") And (Mid$(searchString, i + 1, 1) <> "'") Then
        searchString = Left$(searchString, i) + "'" + Right$(searchString, Len(searchString) - i)
        i = i + 1
      End If
    Next i
    If (cmboSet.ListIndex < 1) Then
      datCardNames.Recordset.Filter = "Name LIKE '%" + searchString + "%'"
    Else
      datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Name LIKE '%" + searchString + "%'"
    End If
  End If

End Sub

Private Sub txtFileName_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCardDeck.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileName.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCardDeck.Picture = LoadPicture(FileName)
    Else
      HLCardDeck.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub txtSet_Change()
  Call txtFileName_Change
End Sub

Public Sub databaseFilter()
  On Error Resume Next
  datCardNames.Recordset.Update
  Dim tempSearchString As String
  Dim databaseQ As String
  tempSearchString = ""
  databaseQ = ""
  datCardNames.Recordset.Filter = ""
  
  If (cmboSet.List(cmboSet.ListIndex) <> "ALL") And (cmboSet.List(cmboSet.ListIndex) <> "") Then
    If databaseQ <> "" Then
      databaseQ = databaseQ + " AND " + "Set = '" + cmboSet.List(cmboSet.ListIndex) + "'"
    Else
      databaseQ = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "'"
    End If
  End If
  
  If checkValidFileNames.Value = 1 Then
    If databaseQ <> "" Then
      databaseQ = databaseQ + " AND " + "[File Name] <> ''"
    Else
      databaseQ = "[File Name] <> ''"
    End If
  End If
  
  datCardNames.Recordset.Filter = databaseQ
End Sub

Public Function getImageLocation(txtFileNames As String, nameToRetrive As Integer)
'  This function was created to deal with the whole "two card images in one field" issue
'nameToRetrive = 1 - Front, 2 - Back
  Dim counter As Long
  Dim foundSplit As Boolean
  Dim splitPos As Long
  foundSplit = False
  
  If (Left$(txtFileNames, 1) = "*") Then ' if first character is '*' then check for character '|'
    counter = 0
    While counter <= Len(txtFileNames)
      counter = counter + 1
      If Mid$(txtFileNames, counter, 1) = "|" Then ' '|' represents end of front image and start of back
        foundSplit = True
        splitPos = counter
      End If
    Wend
    
    If foundSplit Then ' if a break was found then...
      If nameToRetrive = 1 Then
        getImageLocation = Mid$(txtFileNames, 2, splitPos - 2)
      Else
        getImageLocation = Mid$(txtFileNames, splitPos + 1, Len(txtFileNames) - splitPos)
      End If
    Else ' if a break was not found, but a '*' was, assume somethings wrong and display empty.jpg
      getImageLocation = ""
    End If
  Else ' if a '*' is not found, assume only the front of the image is given
    If nameToRetrive = 1 Then
      getImageLocation = txtFileNames
    Else
      getImageLocation = ""
    End If
  End If
End Function

Public Sub addCardToPrintList(front As Boolean)
'  Adds currently selected card to list
' ID, File Name Title
'Dim cardsToPrint(500, 3)
  On Error Resume Next
  Dim FileName As Variant
    Dim fs
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  If front = True Then
    FileName = getImageLocation(txtFileName.text, 1)
  Else
    FileName = getImageLocation(txtFileName.text, 2)
  End If
  If FileName = "" Then
    MsgBox "This card does not have an image associated with it", vbOKOnly, "Error"
  Else
    If Mid$(FileName, 2, 1) = ":" Then
      Set fs = CreateObject("Scripting.FileSystemObject")
      If fs.FileExists(FileName) Then
        HLCard.Picture = LoadPicture(FileName)
        lstMLECardList.AddItem txtCardName.text
        cardsToPrint(printCardPos, 1) = txtCardID.text
        cardsToPrint(printCardPos, 2) = FileName
        cardsToPrint(printCardPos, 3) = txtCardName.text
        printCardPos = printCardPos + 1
      Else
        MsgBox "The file associated with the card does not exist"
      End If
    Else
      Set fs = CreateObject("Scripting.FileSystemObject")
      If fs.FileExists(App.Path + "\" + txtSet.text + "\" + FileName) Then
        HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
        lstMLECardList.AddItem txtCardName.text
        cardsToPrint(printCardPos, 1) = txtCardID.text
        cardsToPrint(printCardPos, 2) = App.Path + "\" + txtSet.text + "\" + FileName
        cardsToPrint(printCardPos, 3) = txtCardName.text
        printCardPos = printCardPos + 1
      Else
        MsgBox "The file associated with the card does not exist"
      End If
    End If
  End If

End Sub

Public Sub DisplayClickedCard(ByRef tempLst As ListBox, ByRef tempArray() As Variant)
' Make sure a card is selected before attempting to display it
  Dim j As Integer
  j = 0
  While (j < Int(tempLst.ListCount))
    If (tempLst.Selected(j) = True) Then
      HLCardDeck.Picture = LoadPicture(tempArray(j, 2))
    End If
    j = j + 1
  Wend
End Sub

Public Sub DisplayCardById(idToDisplay)
  datCardNames.Recordset.MoveFirst
  datCardNames.Recordset.Find ("Id = '" + idToDisplay + "'")
End Sub

Public Sub Delete1CardFromList(ByRef tempLst As ListBox, ByRef tempArray() As Variant, ByRef pos As Variant, ByRef totalCards)
  Dim j As Integer
  Dim k As Integer
  Dim counter As Integer
  Dim msgBoxAnswer
    
  
  If pos > 0 Then
    j = 0
    While (j < tempLst.ListCount) ' loop through every card in current list
      If (tempLst.Selected(j) = True) Then ' if current card in list is selected
        k = j
                    
          ' Remove card from list and array, update display
          While (k < pos)
            tempArray(k, 1) = tempArray(k + 1, 1)
            tempArray(k, 2) = tempArray(k + 1, 2)
            tempArray(k, 3) = tempArray(k + 1, 3)
            k = k + 1
          Wend
          totalCards = totalCards - 1
          pos = pos - 1
          tempLst.RemoveItem (j)
          j = j - 1
      End If
      j = j + 1
    Wend
  End If
End Sub

Public Sub updatePrintTotals()
  Dim tempString
  Dim pageCount
  If printCardPos = 1 Then
    tempString = "1 Card"
  Else
    tempString = printCardPos & " Cards"
  End If
  
  pageCount = Int(printCardPos / 8)
  If (printCardPos Mod 8) <> 0 Then
    pageCount = pageCount + 1
  End If
  
  If pageCount = 1 Then
    tempString = tempString & " (1 Page)"
  Else
    tempString = tempString & " (" & pageCount & " Pages)"
  End If
  
  lblTotalToPrint.Caption = tempString
End Sub
