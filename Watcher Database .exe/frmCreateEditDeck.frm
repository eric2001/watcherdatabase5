VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmCreateEditDeck 
   Caption         =   "Deck Editor"
   ClientHeight    =   8160
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   11505
   LinkTopic       =   "Form2"
   ScaleHeight     =   8160
   ScaleWidth      =   11505
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frameDeckStats 
      Caption         =   "Deck Stats"
      Height          =   8055
      Left            =   120
      TabIndex        =   103
      Top             =   0
      Visible         =   0   'False
      Width           =   4215
      Begin VB.CommandButton cmdFinishedDeckStats 
         Caption         =   "Hide Deck Stats"
         Height          =   615
         Left            =   120
         TabIndex        =   106
         Top             =   7320
         Width           =   1575
      End
      Begin VB.CommandButton cmdPrintStats 
         Caption         =   "Print Deck Stats"
         Height          =   615
         Left            =   3000
         TabIndex        =   105
         Top             =   7320
         Width           =   975
      End
      Begin VB.CommandButton cmdSaveDeckStats 
         Caption         =   "Save Stats to Text File"
         Height          =   615
         Left            =   1800
         TabIndex        =   104
         Top             =   7320
         Width           =   1095
      End
      Begin MSComDlg.CommonDialog CommonDialog3 
         Left            =   2640
         Top             =   3960
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
         DefaultExt      =   "txt"
         Filter          =   "Text File (*.txt)|*.txt|All Files (*.*)|*.*"
      End
      Begin VB.Label lblAttacks 
         AutoSize        =   -1  'True
         Caption         =   "Attacks:"
         Height          =   195
         Left            =   120
         TabIndex        =   142
         Top             =   240
         Width           =   585
      End
      Begin VB.Label lblAttackNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   141
         Top             =   240
         Width           =   90
      End
      Begin VB.Label lblBlocks 
         AutoSize        =   -1  'True
         Caption         =   "Blocks:"
         Height          =   195
         Left            =   120
         TabIndex        =   140
         Top             =   480
         Width           =   525
      End
      Begin VB.Label lblBlockNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   139
         Top             =   480
         Width           =   90
      End
      Begin VB.Label lblAttackBlock 
         AutoSize        =   -1  'True
         Caption         =   "Attack/Block:"
         Height          =   195
         Left            =   120
         TabIndex        =   138
         Top             =   720
         Width           =   990
      End
      Begin VB.Label lblAttackBlockNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   137
         Top             =   720
         Width           =   90
      End
      Begin VB.Label lblDodge 
         Caption         =   "Dodge:"
         Height          =   255
         Left            =   120
         TabIndex        =   136
         Top             =   960
         Width           =   615
      End
      Begin VB.Label lblDodgeNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   135
         Top             =   960
         Width           =   90
      End
      Begin VB.Label lblEvent 
         AutoSize        =   -1  'True
         Caption         =   "Event:"
         Height          =   195
         Left            =   120
         TabIndex        =   134
         Top             =   1320
         Width           =   465
      End
      Begin VB.Label lblEventNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   133
         Top             =   1320
         Width           =   90
      End
      Begin VB.Label lblSituation 
         AutoSize        =   -1  'True
         Caption         =   "Situation:"
         Height          =   195
         Left            =   120
         TabIndex        =   132
         Top             =   1560
         Width           =   660
      End
      Begin VB.Label lblSituationNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   131
         Top             =   1560
         Width           =   90
      End
      Begin VB.Label lblObject 
         AutoSize        =   -1  'True
         Caption         =   "Object:"
         Height          =   195
         Left            =   120
         TabIndex        =   130
         Top             =   1800
         Width           =   510
      End
      Begin VB.Label lblObjectNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   129
         Top             =   1800
         Width           =   90
      End
      Begin VB.Label lblLocation 
         AutoSize        =   -1  'True
         Caption         =   "Location:"
         Height          =   195
         Left            =   120
         TabIndex        =   128
         Top             =   2040
         Width           =   660
      End
      Begin VB.Label lblLocationNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   127
         Top             =   2040
         Width           =   90
      End
      Begin VB.Label lblEdge 
         AutoSize        =   -1  'True
         Caption         =   "Edge:"
         Height          =   195
         Left            =   120
         TabIndex        =   126
         Top             =   2280
         Width           =   420
      End
      Begin VB.Label lblEdgeNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   125
         Top             =   2280
         Width           =   90
      End
      Begin VB.Label lblCombatTrick 
         AutoSize        =   -1  'True
         Caption         =   "Combat Trick:"
         Height          =   195
         Left            =   120
         TabIndex        =   124
         Top             =   2640
         Width           =   990
      End
      Begin VB.Label lblCombatTrickNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   123
         Top             =   2640
         Width           =   90
      End
      Begin VB.Label lblMartialArts 
         AutoSize        =   -1  'True
         Caption         =   "Martial Arts Cards:"
         Height          =   195
         Left            =   120
         TabIndex        =   122
         Top             =   2880
         Width           =   1275
      End
      Begin VB.Label lblMartialArtsNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   121
         Top             =   2880
         Width           =   90
      End
      Begin VB.Label lblUnexpectedEvent 
         AutoSize        =   -1  'True
         Caption         =   "Unexpected Event:"
         Height          =   195
         Left            =   120
         TabIndex        =   120
         Top             =   3120
         Width           =   1380
      End
      Begin VB.Label lblUnexpectedEventNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   119
         Top             =   3120
         Width           =   90
      End
      Begin VB.Label lblUnexpectedLocation 
         AutoSize        =   -1  'True
         Caption         =   "Unexpected Location:"
         Height          =   195
         Left            =   120
         TabIndex        =   118
         Top             =   3360
         Width           =   1575
      End
      Begin VB.Label lblUnexpectedLocationNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   117
         Top             =   3360
         Width           =   90
      End
      Begin VB.Label lblUnexpectedSituaton 
         AutoSize        =   -1  'True
         Caption         =   "Unexpected Situation:"
         Height          =   195
         Left            =   120
         TabIndex        =   116
         Top             =   3600
         Width           =   1575
      End
      Begin VB.Label lblUnexpectedSituationNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   115
         Top             =   3600
         Width           =   90
      End
      Begin VB.Label lblPreGame 
         AutoSize        =   -1  'True
         Caption         =   "Pre-Game Cards:"
         Height          =   195
         Left            =   120
         TabIndex        =   114
         Top             =   4560
         Width           =   1200
      End
      Begin VB.Label lblDeck 
         AutoSize        =   -1  'True
         Caption         =   "Deck Size:"
         Height          =   195
         Left            =   120
         TabIndex        =   113
         Top             =   4320
         Width           =   780
      End
      Begin VB.Label lblDeckNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   112
         Top             =   4320
         Width           =   90
      End
      Begin VB.Label lblPreGameNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   111
         Top             =   4560
         Width           =   90
      End
      Begin VB.Label lblMLE 
         AutoSize        =   -1  'True
         Caption         =   "MLE Cards:"
         Height          =   195
         Left            =   120
         TabIndex        =   110
         Top             =   4920
         Width           =   825
      End
      Begin VB.Label lblMLENum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   109
         Top             =   4920
         Width           =   90
      End
      Begin VB.Label lblMasterCardNum 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   1800
         TabIndex        =   108
         Top             =   3960
         Width           =   90
      End
      Begin VB.Label lblMasterCards 
         AutoSize        =   -1  'True
         Caption         =   "Master Cards:"
         Height          =   195
         Left            =   120
         TabIndex        =   107
         Top             =   3960
         Width           =   975
      End
   End
   Begin VB.Frame frameSealedDeckOptions 
      Caption         =   "Create Sealed Deck"
      Height          =   8055
      Left            =   120
      TabIndex        =   73
      Top             =   0
      Visible         =   0   'False
      Width           =   4215
      Begin VB.CommandButton cmdSetStarterDeck 
         Caption         =   "Create Starter Deck"
         Height          =   495
         Left            =   2280
         TabIndex        =   144
         Top             =   5640
         Width           =   1095
      End
      Begin VB.CommandButton cmdSetBoosterPack 
         Caption         =   "Create Booster Pack"
         Height          =   495
         Left            =   720
         TabIndex        =   143
         Top             =   5640
         Width           =   1095
      End
      Begin VB.TextBox txtBasic 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   89
         Text            =   "0"
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtUncommon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   88
         Text            =   "0"
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtRare 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   87
         Text            =   "0"
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox txtCommon 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   86
         Text            =   "0"
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox txtVeryRare 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   85
         Text            =   "0"
         Top             =   1680
         Width           =   975
      End
      Begin VB.TextBox txtUltraRare 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   84
         Text            =   "0"
         Top             =   2040
         Width           =   975
      End
      Begin VB.TextBox txtPremium 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   83
         Text            =   "0"
         Top             =   2400
         Width           =   975
      End
      Begin VB.TextBox txtBasicPremium 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   82
         Text            =   "0"
         Top             =   2760
         Width           =   975
      End
      Begin VB.TextBox txtUltraRarePremium 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   81
         Text            =   "0"
         Top             =   3120
         Width           =   975
      End
      Begin VB.TextBox txtCollection 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   80
         Text            =   "0"
         Top             =   3480
         Width           =   975
      End
      Begin VB.TextBox txtNemesis 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   79
         Text            =   "0"
         Top             =   3840
         Width           =   975
      End
      Begin VB.TextBox txtPromo 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   78
         Text            =   "0"
         Top             =   4200
         Width           =   975
      End
      Begin VB.TextBox txtQuickening 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1800
         TabIndex        =   77
         Text            =   "0"
         Top             =   4560
         Width           =   975
      End
      Begin VB.CommandButton cmdGenerateSealed 
         Caption         =   "Add Pack / Deck to List"
         Height          =   495
         Left            =   1320
         TabIndex        =   76
         Top             =   6480
         Width           =   1335
      End
      Begin VB.CommandButton cmdExitSealedDeck 
         Caption         =   "Exit Sealed Deck Mode"
         Height          =   495
         Left            =   2880
         TabIndex        =   75
         Top             =   7320
         Width           =   1095
      End
      Begin VB.ComboBox cmboSealedSet 
         Height          =   315
         ItemData        =   "frmCreateEditDeck.frx":0000
         Left            =   1200
         List            =   "frmCreateEditDeck.frx":006D
         TabIndex        =   74
         Text            =   "Set Names"
         Top             =   5040
         Width           =   1455
      End
      Begin VB.Label lblCommonQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Common:"
         Height          =   195
         Left            =   240
         TabIndex        =   102
         Top             =   600
         Width           =   660
      End
      Begin VB.Label lblUncommonQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Uncommon:"
         Height          =   195
         Left            =   240
         TabIndex        =   101
         Top             =   960
         Width           =   855
      End
      Begin VB.Label lblRaresQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Rare:"
         Height          =   195
         Left            =   240
         TabIndex        =   100
         Top             =   1320
         Width           =   390
      End
      Begin VB.Label lblBasicQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Basic:"
         Height          =   195
         Left            =   240
         TabIndex        =   99
         Top             =   240
         Width           =   435
      End
      Begin VB.Label lblVeryRareQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Very Rare:"
         Height          =   195
         Left            =   240
         TabIndex        =   98
         Top             =   1680
         Width           =   750
      End
      Begin VB.Label lblUltraRareQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Ultra-Rare"
         Height          =   195
         Left            =   240
         TabIndex        =   97
         Top             =   2040
         Width           =   720
      End
      Begin VB.Label lblPremium 
         AutoSize        =   -1  'True
         Caption         =   "Premium:"
         Height          =   195
         Left            =   240
         TabIndex        =   96
         Top             =   2400
         Width           =   645
      End
      Begin VB.Label lblBasicPremiumQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Basic Premium:"
         Height          =   195
         Left            =   240
         TabIndex        =   95
         Top             =   2760
         Width           =   1080
      End
      Begin VB.Label lblUltraRarePremium 
         AutoSize        =   -1  'True
         Caption         =   "Ultra-Rare Premium:"
         Height          =   195
         Left            =   240
         TabIndex        =   94
         Top             =   3120
         Width           =   1410
      End
      Begin VB.Label lblCollectionQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Collection:"
         Height          =   195
         Left            =   240
         TabIndex        =   93
         Top             =   3480
         Width           =   735
      End
      Begin VB.Label lblNemesisQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Nemesis:"
         Height          =   195
         Left            =   240
         TabIndex        =   92
         Top             =   3840
         Width           =   645
      End
      Begin VB.Label lblPromoQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Promo:"
         Height          =   195
         Left            =   240
         TabIndex        =   91
         Top             =   4200
         Width           =   495
      End
      Begin VB.Label lblQuickeningQuantity 
         AutoSize        =   -1  'True
         Caption         =   "Quickening:"
         Height          =   195
         Left            =   240
         TabIndex        =   90
         Top             =   4560
         Width           =   855
      End
   End
   Begin VB.Frame frameSealedDeckCards 
      Caption         =   "All Cards (Sealed Deck)"
      Height          =   8055
      Left            =   120
      TabIndex        =   69
      Top             =   0
      Visible         =   0   'False
      Width           =   4215
      Begin VB.CommandButton cmdAddSealedPreGame 
         Caption         =   "Add To Pre-Game"
         Height          =   435
         Left            =   2040
         TabIndex        =   72
         Top             =   7440
         Width           =   1515
      End
      Begin VB.CommandButton cmdAddSealedInGame 
         Caption         =   "Add To Deck"
         Height          =   495
         Left            =   120
         TabIndex        =   71
         Top             =   7440
         Width           =   1215
      End
      Begin VB.ListBox lstAllSealedDeckCards 
         Height          =   6885
         Left            =   120
         TabIndex        =   70
         Top             =   240
         Width           =   3975
      End
   End
   Begin VB.Frame frmSealedDeck 
      Caption         =   "Sealed Deck (0)"
      Height          =   8055
      Left            =   8160
      TabIndex        =   67
      Top             =   0
      Visible         =   0   'False
      Width           =   3255
      Begin VB.CommandButton cmdSaveSealedDeck 
         Caption         =   "Save Sealed Deck"
         Height          =   495
         Left            =   720
         TabIndex        =   66
         Top             =   6840
         Width           =   1695
      End
      Begin VB.ListBox lstSealedDeck 
         Height          =   6105
         Left            =   120
         TabIndex        =   68
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.Frame frmLoading 
      Caption         =   "Loading..."
      Height          =   735
      Left            =   4440
      TabIndex        =   62
      Top             =   3360
      Visible         =   0   'False
      Width           =   3615
      Begin VB.Shape StatusBox 
         DrawMode        =   1  'Blackness
         FillStyle       =   0  'Solid
         Height          =   375
         Left            =   120
         Shape           =   3  'Circle
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame frameCards 
      Caption         =   "All Cards"
      Height          =   8055
      Left            =   120
      TabIndex        =   40
      Top             =   0
      Width           =   4215
      Begin VB.CommandButton cmdTextSearch 
         Caption         =   "Find"
         Height          =   255
         Left            =   3240
         TabIndex        =   65
         Top             =   7320
         Width           =   735
      End
      Begin VB.TextBox txtTextSearch 
         Height          =   285
         Left            =   1200
         TabIndex        =   64
         Top             =   7320
         Width           =   1935
      End
      Begin VB.CommandButton cmdSelectCardType 
         Caption         =   "Select Type"
         Height          =   255
         Left            =   2760
         TabIndex        =   61
         Top             =   6480
         Width           =   1215
      End
      Begin VB.ComboBox cmboCardType 
         Height          =   315
         ItemData        =   "frmCreateEditDeck.frx":013E
         Left            =   1320
         List            =   "frmCreateEditDeck.frx":0196
         TabIndex        =   60
         Text            =   "Card Type"
         Top             =   6480
         Width           =   1335
      End
      Begin VB.CommandButton cmdClassification 
         Caption         =   "Display"
         Height          =   255
         Left            =   3240
         TabIndex        =   58
         Top             =   7680
         Width           =   735
      End
      Begin VB.ComboBox lstClassificationOptions 
         Height          =   315
         ItemData        =   "frmCreateEditDeck.frx":032F
         Left            =   1200
         List            =   "frmCreateEditDeck.frx":0339
         TabIndex        =   57
         Text            =   "Classifications"
         Top             =   7680
         Width           =   1935
      End
      Begin VB.CommandButton cmdShowAll 
         Caption         =   "Show All"
         Height          =   375
         Left            =   2880
         TabIndex        =   50
         Top             =   5640
         Width           =   1095
      End
      Begin VB.CommandButton cmdFind 
         Caption         =   "Find"
         Height          =   255
         Left            =   3240
         TabIndex        =   49
         Top             =   6960
         Width           =   735
      End
      Begin VB.TextBox txtNameToFind 
         Height          =   285
         Left            =   1200
         TabIndex        =   48
         Top             =   6960
         Width           =   1935
      End
      Begin VB.CommandButton cmdShowSet 
         Caption         =   "Select Set"
         Height          =   255
         Left            =   2760
         TabIndex        =   47
         Top             =   6120
         Width           =   1215
      End
      Begin VB.ComboBox cmboSet 
         Height          =   315
         ItemData        =   "frmCreateEditDeck.frx":0359
         Left            =   1320
         List            =   "frmCreateEditDeck.frx":03C9
         TabIndex        =   46
         Text            =   "Set Names"
         Top             =   6120
         Width           =   1335
      End
      Begin VB.CommandButton cmdSortName 
         Caption         =   "Sort By Name"
         Height          =   375
         Left            =   120
         TabIndex        =   45
         Top             =   6480
         Width           =   1095
      End
      Begin VB.CommandButton cmdSortSet 
         Caption         =   "Sort By Set"
         Height          =   375
         Left            =   120
         TabIndex        =   44
         Top             =   6120
         Width           =   1095
      End
      Begin VB.CommandButton cmdAddPreGame 
         Caption         =   "Add To Pre-Game"
         Height          =   375
         Left            =   1320
         TabIndex        =   43
         Top             =   5640
         Width           =   1455
      End
      Begin VB.CommandButton cmdAddDeck 
         Caption         =   "Add To Deck"
         Height          =   375
         Left            =   120
         TabIndex        =   42
         Top             =   5640
         Width           =   1095
      End
      Begin MSDataGridLib.DataGrid dataCards 
         Bindings        =   "frmCreateEditDeck.frx":049F
         Height          =   5295
         Left            =   120
         TabIndex        =   41
         Top             =   240
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   9340
         _Version        =   393216
         AllowUpdate     =   0   'False
         AllowArrows     =   0   'False
         DefColWidth     =   273
         ColumnHeaders   =   0   'False
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "All Cards"
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin VB.Label lblTextSearch 
         AutoSize        =   -1  'True
         Caption         =   "Text Search:"
         Height          =   195
         Left            =   120
         TabIndex        =   63
         Top             =   7320
         Width           =   915
      End
      Begin VB.Label lblClassificationSearch 
         AutoSize        =   -1  'True
         Caption         =   "Classification:"
         Height          =   195
         Left            =   120
         TabIndex        =   56
         Top             =   7680
         Width           =   960
      End
      Begin VB.Label lblCardTitleSearch 
         Caption         =   "Card Title:"
         Height          =   255
         Left            =   120
         TabIndex        =   55
         Top             =   6960
         Width           =   855
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "hld"
      Filter          =   "Highlander Decks (*.hld)|*.hld|All Files (*.*)|*.*"
   End
   Begin VB.Frame frameCardDetailsDeck 
      Caption         =   "Card Details"
      Height          =   8055
      Left            =   4440
      TabIndex        =   23
      Top             =   0
      Visible         =   0   'False
      Width           =   3615
      Begin VB.TextBox txtIDDeck 
         DataField       =   "ID"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   2640
         TabIndex        =   59
         Top             =   1320
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CommandButton cmdDeckShowBack 
         Caption         =   "Show Back"
         Height          =   255
         Left            =   2040
         TabIndex        =   52
         Top             =   7680
         Width           =   975
      End
      Begin VB.CommandButton cmdDeckShowFront 
         Caption         =   "Show Front"
         Height          =   255
         Left            =   600
         TabIndex        =   51
         Top             =   7680
         Width           =   975
      End
      Begin VB.TextBox txtOtherDeck 
         DataField       =   "Other"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   39
         Top             =   1920
         Width           =   1215
      End
      Begin VB.TextBox txtTypeDeck 
         DataField       =   "Type"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   38
         Top             =   1560
         Width           =   1215
      End
      Begin VB.TextBox txtNumberDeck 
         Height          =   285
         Left            =   2760
         TabIndex        =   36
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox txtRarityDeck 
         DataField       =   "Rarity"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   35
         Top             =   1200
         Width           =   1215
      End
      Begin VB.TextBox txtSetDeck 
         DataField       =   "Set"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   1080
         TabIndex        =   34
         Top             =   840
         Width           =   495
      End
      Begin VB.TextBox txtCardNameDeck 
         DataField       =   "Name"
         DataSource      =   "datCardDeck"
         Height          =   525
         Left            =   1080
         MultiLine       =   -1  'True
         TabIndex        =   33
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox txtFileNameDeck 
         DataField       =   "File Name"
         DataSource      =   "datCardDeck"
         Height          =   285
         Left            =   2640
         TabIndex        =   25
         Top             =   1800
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtCardTextDeck 
         DataField       =   "Text"
         DataSource      =   "datCardDeck"
         Height          =   885
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   24
         Top             =   2280
         Width           =   2415
      End
      Begin VB.Image HLCardDeck 
         Height          =   4335
         Left            =   240
         Stretch         =   -1  'True
         Top             =   3240
         Width           =   3135
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   32
         Top             =   1200
         Width           =   450
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   120
         TabIndex        =   31
         Top             =   840
         Width           =   285
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   30
         Top             =   240
         Width           =   840
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Quantity:"
         Height          =   195
         Left            =   2040
         TabIndex        =   29
         Top             =   840
         Width           =   630
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   28
         Top             =   1560
         Width           =   405
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   120
         TabIndex        =   27
         Top             =   1920
         Width           =   435
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   26
         Top             =   2280
         Width           =   735
      End
   End
   Begin VB.Frame frameDeck 
      Caption         =   "Deck"
      Height          =   4215
      Left            =   8160
      TabIndex        =   19
      Top             =   0
      Width           =   3255
      Begin VB.CommandButton cmdAddAnother 
         Caption         =   "Add Another Card"
         Height          =   375
         Left            =   1680
         TabIndex        =   22
         Top             =   3720
         Width           =   1455
      End
      Begin VB.CommandButton cmdDelDeck 
         Caption         =   "Delete 1 Card"
         Height          =   375
         Left            =   120
         TabIndex        =   21
         Top             =   3720
         Width           =   1095
      End
      Begin VB.ListBox lstDeck 
         Height          =   3375
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   20
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.Frame frmPreGame 
      Caption         =   "Pre-Game Cards"
      Height          =   3735
      Left            =   8160
      TabIndex        =   15
      Top             =   4320
      Width           =   3255
      Begin VB.CommandButton cmdPreGameAdd 
         Caption         =   "Add Another Card"
         Height          =   375
         Left            =   1680
         TabIndex        =   18
         Top             =   3240
         Width           =   1455
      End
      Begin VB.CommandButton cmdPreGameDel 
         Caption         =   "Delete 1 Card"
         Height          =   375
         Left            =   120
         TabIndex        =   17
         Top             =   3240
         Width           =   1215
      End
      Begin VB.ListBox lstPreGame 
         Height          =   2790
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   16
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.Frame frameCardDetailsDB 
      Caption         =   "Card Details"
      Height          =   8055
      Left            =   4440
      TabIndex        =   0
      Top             =   0
      Width           =   3615
      Begin VB.CommandButton cmdShowBack 
         Caption         =   "Show Back"
         Height          =   255
         Left            =   2040
         TabIndex        =   54
         Top             =   7680
         Width           =   975
      End
      Begin VB.CommandButton cmdShowFront 
         Caption         =   "Show Front"
         Height          =   255
         Left            =   600
         TabIndex        =   53
         Top             =   7680
         Width           =   975
      End
      Begin VB.TextBox txtType 
         DataField       =   "Type"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   37
         Top             =   1560
         Width           =   1215
      End
      Begin VB.TextBox txtCardText 
         DataField       =   "Text"
         DataSource      =   "datCardNames"
         Height          =   885
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   13
         Top             =   2280
         Width           =   2415
      End
      Begin VB.TextBox txtOther 
         DataField       =   "Other"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   11
         Top             =   1920
         Width           =   1215
      End
      Begin VB.TextBox txtNum 
         DataField       =   "Number"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2760
         TabIndex        =   8
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox txtCardName 
         DataField       =   "Name"
         DataSource      =   "datCardNames"
         Height          =   525
         Left            =   1080
         MultiLine       =   -1  'True
         TabIndex        =   4
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox txtSet 
         DataField       =   "Set"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   840
         Width           =   495
      End
      Begin VB.TextBox txtFileName 
         DataField       =   "File Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2640
         TabIndex        =   2
         Top             =   1800
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtRarity 
         DataField       =   "Rarity"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Image HLCard 
         Height          =   4335
         Left            =   240
         Stretch         =   -1  'True
         Top             =   3240
         Width           =   3135
      End
      Begin VB.Label lblCardText 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   2280
         Width           =   735
      End
      Begin VB.Label lblOther 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1920
         Width           =   435
      End
      Begin VB.Label lblType 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   1560
         Width           =   405
      End
      Begin VB.Label lblNumber 
         AutoSize        =   -1  'True
         Caption         =   "Quantity:"
         Height          =   195
         Left            =   2040
         TabIndex        =   9
         Top             =   840
         Width           =   630
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   840
      End
      Begin VB.Label lblSet 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   285
      End
      Begin VB.Label lblRarity 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   450
      End
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   8880
      Top             =   1440
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Database"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc datCardDeck 
      Height          =   735
      Left            =   9240
      Top             =   2640
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Deck"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog CommonDialog2 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "csv"
      Filter          =   "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*"
   End
   Begin MSComDlg.CommonDialog comDlgSealedDeck 
      Left            =   0
      Top             =   1440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "hld"
      Filter          =   "Highlander Decks (*.hsd)|*.hsd|All Files (*.*)|*.*"
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuOpen 
         Caption         =   "Open Deck"
         Shortcut        =   ^O
      End
      Begin VB.Menu menuSaveAs 
         Caption         =   "Save Deck As..."
         Shortcut        =   ^A
      End
      Begin VB.Menu menuSave 
         Caption         =   "Save Deck"
         Shortcut        =   ^S
      End
      Begin VB.Menu menuNewDeck 
         Caption         =   "New Deck"
         Shortcut        =   ^N
      End
      Begin VB.Menu menuExport 
         Caption         =   "Export Deck"
         Shortcut        =   ^X
      End
      Begin VB.Menu menuPrintDeck 
         Caption         =   "Print Deck"
         Shortcut        =   ^P
      End
      Begin VB.Menu menuLoadSealedDeck 
         Caption         =   "Load Sealed Deck"
         Shortcut        =   ^D
      End
      Begin VB.Menu menuExit 
         Caption         =   "Exit Deck Manager"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu menuOptions 
      Caption         =   "Options"
      Begin VB.Menu menuShowStats 
         Caption         =   "Show Deck Stats"
         Shortcut        =   ^I
      End
      Begin VB.Menu menuSortName 
         Caption         =   "Sort Deck By Name"
         Shortcut        =   ^M
      End
      Begin VB.Menu menuSortType 
         Caption         =   "Sort Deck By Type"
         Shortcut        =   ^T
      End
      Begin VB.Menu menuSortBoth 
         Caption         =   "Sort By Type and Name"
         Shortcut        =   ^H
      End
      Begin VB.Menu menuSwitchToSealedDeckG 
         Caption         =   "Switch to Sealed Deck Generator"
         Shortcut        =   ^C
      End
   End
End
Attribute VB_Name = "frmCreateEditDeck"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Created By:           Eric Cavaliere
' Program Name:         Watcher Database
' Project File Name:    Highlander.vbp
' Project Started on:   August 05, 2002
' Name of this File:    frmCreateEditDeck.frm
' This file started on: August 05, 2002
' Last Updated on:      22 October 2007

'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

' Note to self - as the code currently stands, removing a persona from a list will NOT
'    reset mastercardcount, personacullen, etc.  This is done this way in case of multiple
'    persona cards in pre-game list.  Saving the deck and re-loading will reset variables.

'Notes with sealed deck generator - change buttons on deck and pregame lists to check and see
'  if in sealed deck mode and if so limit user to available # of cards.
'  speed up new sealed deck creation
'  currently cards being added from sealed deck are done so without the usual restrictions.
'     fix this.
'  add preset options for booster and starter decks...
'  random number equation for choosing premiums.
'  Enable multi-select when created a new deck from a sealed deck.

Option Explicit

' Declare Global Variables
'deckcards(*, 1) = ID Number, 2 = number, 3 = card name, 4 = type
Dim deckCards(500, 4)
Dim sealedDeckCards(500, 4)
Dim allSealedCards(500, 4)
Dim preGameCards(100, 4)
Dim deckCount As Integer
Dim sealedDeckCount As Integer
Dim allSealedCount As Integer
Dim preGameCount As Integer
Dim masterCardLimit As Integer
Dim masterCardCount As Integer
Dim totalDeckCount As Integer
Dim totalPreGameCount As Integer
Dim totalSealedDeckCount As Integer
Dim totalAllSealedCount As Integer

Dim dariusRestrictionCardNum
Dim mastersPrizeCount
Dim personaCullen As Boolean
Dim xavierPower As Boolean

Public Sub DisplayCardById(idToDisplay)
  datCardDeck.Recordset.MoveFirst
  datCardDeck.Recordset.Find ("Id = '" + Str$(idToDisplay) + "'")
End Sub

Public Sub AddAnotherCardToList(ByRef tempLst As ListBox, ByRef tempArray() As Variant, ByRef pos As Variant, ByRef totalCards)
'  Called when add another button is clicked for either the deck or pre-game cards
  Dim j As Integer
  Dim k As Integer
    
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If pos > 0 Then ' check to make sure there are cards in the list before looking for selected cards.
    j = 0
    While (j < tempLst.ListCount) ' loop through every card in current list
      If (tempLst.Selected(j) = True) Then ' if current card in list is selected
        DisplayCardById tempArray(j, 1)
        k = 1
        
        'Enforce Global 6 Card Restriction
        If xavierPower = False Then
        If tempArray(j, 2) >= 6 + mastersPrizeCount Then
          MsgBox "Card Limit of 6 has been Reached", vbOKOnly, "Deck Construction Error"
          Exit Sub
        End If
        End If
        
        'Enforce Restriction Number of individual Cards
        If xavierPower = False Then
        
        If LCase(txtTypeDeck.text) <> "persona" Then
          If Val(Right$(txtOtherDeck.text, 1)) > 0 Then
            If Int(Right$(txtOtherDeck.text, 1)) + dariusRestrictionCardNum = tempArray(j, 2) Then
              MsgBox "Restriction Limit Reached", vbOKOnly, "Deck Construction Error"
              Exit Sub
            End If
          End If
        End If
        End If
        
        'Enforce Master Card Restriction
        If (tempLst.name <> lstPreGame.name) Then ' Don't count pre-Game cards as master cards
          If isMaster(Val(tempArray(j, 1))) Then
            masterCardCount = masterCardCount + 1
            If (masterCardCount > masterCardLimit) Then
              MsgBox "Your master card limit (" + Str(masterCardLimit) + ") has already been reached.  To add more master cards, you must first remove some of the older ones.", vbOKOnly, "Deck Construction Error"
              masterCardCount = masterCardCount - 1
              Exit Sub
            End If
          End If
        End If
    
        ' Add card to list
        totalCards = totalCards + 1
        tempArray(j, 2) = tempArray(j, 2) + 1
        txtNumberDeck.text = tempArray(j, 2)
        tempLst.List(j) = ("(" + Str$(tempArray(j, 2)) + ") " + txtCardNameDeck.text)
        
        adjustCardVariables
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub cmdAddAnother_Click()
' Add another copy of the selected card to the deck.
  AddAnotherCardToList lstDeck, deckCards, deckCount, totalDeckCount
  frameDeck.Caption = "Deck (" + Str$(totalDeckCount) + ")"
End Sub

Public Sub Move1CardFromDBtoList(ByRef tempLst As ListBox, ByRef tempArray() As Variant, ByRef pos As Variant, ByRef totalCards)
'  This function is called when adding a card from the database list to the deck or pre-game lists
  Dim foundcard
  Dim msgBoxAnswer
  
  foundcard = False
  Dim j As Integer
  If pos > 0 Then
    For j = 0 To pos - 1
      ' If the card to be added is already in the deck...
      If (dataCards.Columns("ID") = tempArray(j, 1)) Then
      
        DisplayCardById tempArray(j, 1)
        If tempLst.name <> "lstSealedDeck" Then
        
        'Enforce Global card restriction limit of 6
        If xavierPower = False Then
        
        If tempArray(j, 2) >= 6 + mastersPrizeCount Then
          MsgBox "Card Limit of 6 has been Reached", vbOKOnly, "Deck Construction Error"
          Exit Sub
        End If
      End If
        
      ' Enforce individual card restriction limits
      If xavierPower = False Then
        If LCase(txtTypeDeck.text) <> "persona" Then
          If Val(Right$(txtOtherDeck.text, 1)) > 0 Then
            If Int(Right$(txtOtherDeck.text, 1)) + dariusRestrictionCardNum = tempArray(j, 2) Then
              MsgBox "Restriction Limit Reached", vbOKOnly, "Deck Construction Error"
              Exit Sub
            End If
          End If
        End If
      End If
        
      'Enforce Master Card Restriction
      If (tempLst.name <> lstPreGame.name) Then ' Don't count pre-Game cards as master cards
        If isMaster(tempArray(j, 1)) Then
          masterCardCount = masterCardCount + 1
          If (masterCardCount > masterCardLimit) Then
            MsgBox "Your master card limit (" + Str(masterCardLimit) + ") has already been reached.  To add more master cards, you must first remove some of the older ones.", vbOKOnly, "Deck Construction Error"
            masterCardCount = masterCardCount - 1
            Exit Sub
          End If
        End If
      End If
    End If
    
    ' Add card to list
    totalCards = totalCards + 1
    tempArray(j, 2) = tempArray(j, 2) + 1
    txtNumberDeck.text = tempArray(j, 2)
    tempLst.List(j) = "(" + Str$(tempArray(j, 2)) + ") " + txtCardNameDeck.text
    foundcard = True
    If tempLst.name <> "lstSealedDeck" Then
      adjustCardVariables
    End If
  Exit For
End If
    Next j
  End If
  
  'If the card is not already in the deck
  If foundcard = False Then
    
    DisplayCardById (Val(dataCards.Columns("ID").Value))
    
        If tempLst.name <> "lstSealedDeck" Then
    '  Alert user before adding misprints into the deck / pre-game lists
    If txtSetDeck.text = "MAO" Then
      msgBoxAnswer = MsgBox("This card is not legal for tournament play, continue anyway?", vbYesNo, "Warning")
      If msgBoxAnswer = 7 Then
        Exit Sub
      End If
    End If
    
    'Enforce Master Card Restriction
    If (tempLst.name <> lstPreGame.name) Then ' Don't count pre-Game cards as master cards
      If isMaster(Val(dataCards.Columns("ID").Value)) Then
        masterCardCount = masterCardCount + 1
        If (masterCardCount > masterCardLimit) Then
          MsgBox "Your master card limit (" + Str(masterCardLimit) + ") has already been reached.  To add more master cards, you must first remove some of the older ones.", vbOKOnly, "Deck Construction Error"
          masterCardCount = masterCardCount - 1
          Exit Sub
        End If
      End If
    End If
End If
    ' Add the card and update the list
    totalCards = totalCards + 1
    tempLst.AddItem ("(1) " + dataCards.Columns("Name").Value)
    tempArray(pos, 1) = dataCards.Columns("ID").Value
    tempArray(pos, 2) = 1
    tempArray(pos, 3) = txtCardNameDeck.text
    tempArray(pos, 4) = txtTypeDeck.text
    DisplayCardById tempArray(pos, 1)
    pos = pos + 1
        If tempLst.name <> "lstSealedDeck" Then
    
    adjustCardVariables
    End If
  End If
End Sub

Private Sub cmdAddDeck_Click()
  Move1CardFromDBtoList lstDeck, deckCards, deckCount, totalDeckCount
  frameDeck.Caption = "Deck (" + Str$(totalDeckCount) + ")"
End Sub

Private Sub cmdAddPreGame_Click()
  Move1CardFromDBtoList lstPreGame, preGameCards, preGameCount, totalPreGameCount
  frmPreGame.Caption = "Pre-Game Cards (" + Str$(totalPreGameCount) + ")"
End Sub

Public Sub Delete1CardFromList(ByRef tempLst As ListBox, ByRef tempArray() As Variant, ByRef pos As Variant, ByRef totalCards)
  Dim j As Integer
  Dim k As Integer
  Dim counter As Integer
  Dim msgBoxAnswer
    
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If pos > 0 Then
    j = 0
    While (j < tempLst.ListCount) ' loop through every card in current list
      If (tempLst.Selected(j) = True) Then ' if current card in list is selected
        k = j
        If (tempArray(k, 2) > 1) Then  'If more then one copy of card
        
          ' Decrease deck count and # in deck by one, decrease # in list by 1
          totalCards = totalCards - 1
          tempArray(k, 2) = tempArray(k, 2) - 1
          DisplayCardById (tempArray(k, 1))
          txtNumberDeck.text = tempArray(k, 2)
          tempLst.List(k) = ("(" + Str$(tempArray(k, 2)) + ") " + txtCardNameDeck.text)
          
          'Adjust Master card count if necessary
          If (tempLst.name <> lstPreGame.name) Then ' Don't count pre-Game cards as master cards
            If isMaster(tempArray(k, 1)) Then
              masterCardCount = masterCardCount - 1 ' if card being removed is master card, decrease master card count by 1
              If (masterCardCount > masterCardLimit) Then
                MsgBox "Your master card limit is " + Str(masterCardLimit) + ".  This card brings the master cards in your deck to " + Str(masterCardCount), vbOKOnly, "Deck Construction Warning"
              End If
            End If
          End If
          
          ' Check if card being removed is master's prize, adjust as necessary
          If (dataCards.Columns("ID") = "2428") Then
            mastersPrizeCount = mastersPrizeCount - 1
          End If
          
          ' Check if card being removed is Pre-Game darius, adjust as necessary
          If (dataCards.Columns("ID") = 1871) Then
            dariusRestrictionCardNum = dariusRestrictionCardNum - 1
          End If
          
          If (dataCards.Columns("ID") = 2309) Then
            msgBoxAnswer = MsgBox("Decrease Master Card Limit By 1?", vbYesNo, "Decrease Master Limit")
            If msgBoxAnswer = 6 Then
              masterCardLimit = masterCardLimit - 1
              MsgBox "New Master Card Limit Set.", vbOKOnly, "Limit Set"
            End If
          End If
          
        Else ' if only one copy of card remove entry from list
          DisplayCardById tempArray(k, 1)
                    
          ' Remove card from list and array, update display
          While (k < pos)
            tempArray(k, 1) = tempArray(k + 1, 1)
            tempArray(k, 2) = tempArray(k + 1, 2)
            tempArray(k, 3) = tempArray(k + 1, 3)
            tempArray(k, 4) = tempArray(k + 1, 4)
            k = k + 1
          Wend
          totalCards = totalCards - 1
          pos = pos - 1
          tempLst.RemoveItem (j)
          j = j - 1
          
          ' Adjust system variables if card was "special"
          
          ' check and see if the card is the cullen persona
          If dataCards.Columns("ID") = 2554 Or dataCards.Columns("ID") = 2555 Then
            personaCullen = True
          End If
          
  If (txtIDDeck.text = 968) Or (txtIDDeck.text = 1414) Or (txtIDDeck.text = 2080) Then
    xavierPower = True
  End If
          'Adjust Master card count if necessary
          If (tempLst.name <> lstPreGame.name) Then ' Don't count pre-Game cards as master cards
            If isMaster(tempArray(k, 1)) Then
              masterCardCount = masterCardCount - 1 ' if card being removed is master card, decrease master card count by 1
              If (masterCardCount > masterCardLimit) Then
                MsgBox "Your master card limit is " + Str(masterCardLimit) + ".  This card brings the master cards in your deck to " + Str(masterCardCount), vbOKOnly, "Deck Construction Warning"
              End If
            End If
          End If
          
          'if darius master card limit increase
          If (dataCards.Columns("ID") = 2309) Then
            msgBoxAnswer = MsgBox("Decrease Master Card Limit By 1?", vbYesNo, "Decrease Master Limit")
            If msgBoxAnswer = 6 Then
              masterCardLimit = masterCardLimit - 1
              MsgBox "New Master Card Limit Set.", vbOKOnly, "Limit Set"
            End If
          End If
        
        End If
      End If
      j = j + 1
    Wend
  End If
End Sub

Private Sub cmdAddSealedInGame_Click()
  TransferCardFromLstToLst lstAllSealedDeckCards, allSealedCards, allSealedCount, lstDeck, deckCards, deckCount, totalDeckCount
  frameDeck.Caption = "Deck (" + Str$(totalDeckCount) + ")"
End Sub

Private Sub cmdAddSealedPreGame_Click()
  TransferCardFromLstToLst lstAllSealedDeckCards, allSealedCards, allSealedCount, lstPreGame, preGameCards, preGameCount, totalPreGameCount
  frmPreGame.Caption = "Pre-Game Cards (" + Str$(totalPreGameCount) + ")"
End Sub

Private Sub cmdClassification_Click()
  datCardNames.Recordset.Filter = "Text LIKE '%Classification:  " + lstClassificationOptions.List(lstClassificationOptions.ListIndex) + "%'"
End Sub

Private Sub cmdDeckShowBack_Click()
  On Error Resume Next
  Dim FileName As Variant
  HLCardDeck.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileNameDeck.text, 2)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCardDeck.Picture = LoadPicture(FileName)
    Else
      HLCardDeck.Picture = LoadPicture(App.Path + "\" + txtSetDeck.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub cmdDeckShowFront_Click()
  Call txtFileNameDeck_Change
End Sub

Private Sub cmdDelDeck_Click()
  Delete1CardFromList lstDeck, deckCards, deckCount, totalDeckCount
  frameDeck.Caption = "Deck (" + Str$(totalDeckCount) + ")"
End Sub

Private Sub cmdExitSealedDeck_Click()
  menuFile.Enabled = True
  menuOptions.Enabled = True
  frameSealedDeckOptions.Visible = False
  frmSealedDeck.Visible = False
  lstSealedDeck.Clear
  txtBasic.text = 0
  txtCommon.text = 0
  txtUncommon.text = 0
  txtRare.text = 0
  txtVeryRare.text = 0
  txtUltraRare.text = 0
  txtPremium.text = 0
  txtBasicPremium.text = 0
  txtUltraRarePremium.text = 0
  txtCollection.text = 0
  txtNemesis.text = 0
  txtPromo.text = 0
  txtQuickening.text = 0
  sealedDeckCount = 0
  totalSealedDeckCount = 0
  frmSealedDeck.Caption = "Sealed Deck (0)"
  cmboSealedSet.ListIndex = -1
  
End Sub

Private Sub cmdFind_Click()
  On Error Resume Next
  Dim tempSearchString As String
  Dim counter As Integer
  
  datCardNames.Recordset.Filter = ""
  If txtNameToFind.text <> "" Then
    tempSearchString = txtNameToFind.text
    For counter = 1 To Len(txtNameToFind.text)
      If (Mid$(txtNameToFind.text, counter, 1) = "'") And (Mid$(txtNameToFind.text, counter + 1, 1) <> "'") Then
        txtNameToFind.text = Left$(txtNameToFind.text, counter) + "'" + Right$(txtNameToFind.text, Len(txtNameToFind.text) - counter)
        counter = counter + 1
      End If
    Next counter
    If (cmboSet.ListIndex < 1) Then
      datCardNames.Recordset.Filter = "Name LIKE '%" + txtNameToFind.text + "%'"
    Else
      datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Name LIKE '%" + txtNameToFind.text + "%'"
    End If
    txtNameToFind.text = tempSearchString
  End If
End Sub

Private Sub cmdFinishedDeckStats_Click()
  frameDeckStats.Visible = False
End Sub

Private Sub cmdGenerateSealed_Click()
  
  If cmboSealedSet.List(cmboSealedSet.ListIndex) = "" Then
    MsgBox "Please Select a set first", vbOKOnly, "Error"
    Exit Sub
  End If
  
  If txtBasic.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Basic'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Basic cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtCommon.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Common'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Common cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
    
  If txtUncommon.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Uncommon'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Uncommon cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
    
  If txtRare.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Rare'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Rare cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
    
  If txtVeryRare.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Very Rare'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Very Rare cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtUltraRare.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Ultra Rare'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Ultra Rare cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtPremium.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Premium'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Premium cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtBasicPremium.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Basic Premium'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Basic Premium cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtUltraRarePremium.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Ultra-Rare Premium'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Ultra-Rare Premium cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtCollection.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Collection'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Collection cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtNemesis.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Nemesis'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Nemesis cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtPromo.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Promo'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Promo cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  If txtQuickening.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Quickening'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    If datCardNames.Recordset.EOF = True Then
      MsgBox "There are no Quickening cards in the selected set", vbOKOnly, "Error"
      Exit Sub
    End If
  End If
  DoEvents
  
  Randomize Timer
  Dim sealedCounter
  Dim randomPos
  Dim randomCounter
    ' Disconnect Two fields from the database to get the deck to load faster
    txtSetDeck.DataField = ""
    txtFileNameDeck.DataField = ""
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = False
  
  If txtBasic.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Basic'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtBasic.text)
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtCommon.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Common'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtCommon.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
 
  If txtUncommon.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Uncommon'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtUncommon.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
    
  If txtRare.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Rare'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtRare.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
    
  If txtVeryRare.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Very Rare'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtVeryRare.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtUltraRare.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Ultra Rare'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtUltraRare.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtPremium.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Premium'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtPremium.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtBasicPremium.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Basic Premium'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtBasicPremium.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtUltraRarePremium.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Ultra-Rare Premium'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtUltraRarePremium.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtCollection.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Collection'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtCollection.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtNemesis.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Nemesis'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtNemesis.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtPromo.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Promo'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtPromo.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  
  If txtQuickening.text > 0 Then
    datCardNames.Recordset.Filter = "Rarity = 'Quickening'" + " AND " + "Set = '" + cmboSealedSet.List(cmboSealedSet.ListIndex) + "'"
    sealedCounter = 0
    While sealedCounter < Int(txtQuickening.text) ' change this to match set
      datCardNames.Recordset.MoveFirst
      randomPos = Int(Rnd() * datCardNames.Recordset.RecordCount)
      randomCounter = 0
      While randomCounter < randomPos
        datCardNames.Recordset.MoveNext
        randomCounter = randomCounter + 1
        DoEvents
      Wend
      ' add card to list code here
      Move1CardFromDBtoList lstSealedDeck, sealedDeckCards, sealedDeckCount, totalSealedDeckCount
      frmSealedDeck.Caption = "Sealed Deck (" + Str$(totalSealedDeckCount) + ")"
      sealedCounter = sealedCounter + 1
      DoEvents
    Wend
  End If
  DoEvents
    ' Reconnect Fields to Database, Set up display correctly
    txtSetDeck.DataField = "Set"
    txtFileNameDeck.DataField = "File Name"
    frameCardDetailsDeck.Visible = True

End Sub

Private Sub cmdPreGameAdd_Click()
  AddAnotherCardToList lstPreGame, preGameCards, preGameCount, totalPreGameCount
  frmPreGame.Caption = "Pre-Game Cards (" + Str$(totalPreGameCount) + ")"
End Sub

Private Sub cmdPreGameDel_Click()
  Delete1CardFromList lstPreGame, preGameCards, preGameCount, totalPreGameCount
  frmPreGame.Caption = "Pre-Game Cards (" + Str$(totalPreGameCount) + ")"
End Sub

Private Sub cmdPrintStats_Click()
    
    CommonDialog1.ShowPrinter
    Printer.CurrentY = 1440
    Printer.Font.name = "Courier New"
      
    Printer.Font.Underline = True
    Printer.Font.Bold = True
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Deck Statistics:") / 2)
    Printer.Print "Deck Statistics:"
    Printer.Font.Underline = False
    Printer.Font.Bold = False
      Printer.Print "Attacks: ", , lblAttackNum.Caption
      Printer.Print "Blocks: ", , lblBlockNum.Caption
      Printer.Print "Attack/Block: ", lblAttackBlockNum.Caption
      Printer.Print "Dodge: ", , lblDodgeNum.Caption
      Printer.Print ""
      Printer.Print "Event: ", , lblEventNum.Caption
      Printer.Print "Situation: ", , lblSituationNum.Caption
      Printer.Print "Object: ", , lblObjectNum.Caption
      Printer.Print "Location: ", , lblLocationNum.Caption
      Printer.Print "Edge: ", , lblEdgeNum.Caption
      Printer.Print ""
      Printer.Print "Combat Tricks: ", lblCombatTrickNum.Caption
      Printer.Print "Martial Arts Cards: ", lblMartialArtsNum.Caption
      Printer.Print "Unexpected Event: ", lblUnexpectedEventNum.Caption
      Printer.Print "Unexpected Location: ", lblUnexpectedLocationNum.Caption
      Printer.Print "Unexpected Situation: ", lblUnexpectedSituationNum.Caption
      Printer.Print ""
      Printer.Print "Master Cards: ", lblMasterCardNum.Caption
      Printer.Print ""
      Printer.Print "Deck Size: ", , lblDeckNum.Caption
      Printer.Print "Pre-Game Size: ", lblPreGameNum.Caption
      Printer.Print ""
      Printer.Print "MLE Cards: ", , lblMLENum.Caption
    
      Printer.EndDoc
    MsgBox "Deck Stats have been printed successfully", vbOKOnly, "Complete"

End Sub

Private Sub cmdSaveDeckStats_Click()
  On Error Resume Next
  CommonDialog3.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog3.FileName <> "") Then
    Open CommonDialog3.FileName For Output As #2
      Print #2, "Deck Stats:"
      Print #2, "Attacks: " + lblAttackNum.Caption
      Print #2, "Blocks: " + lblBlockNum.Caption
      Print #2, "Attack/Block: " + lblAttackBlockNum.Caption
      Print #2, "Dodge: " + lblDodgeNum.Caption
      Print #2, "Event: " + lblEventNum.Caption
      Print #2, "Situation: " + lblSituationNum.Caption
      Print #2, "Object: " + lblObjectNum.Caption
      Print #2, "Location: " + lblLocationNum.Caption
      Print #2, "Edge: " + lblEdgeNum.Caption
      Print #2, "Combat Tricks: " + lblCombatTrickNum.Caption
      Print #2, "Martial Arts Cards: " + lblMartialArtsNum.Caption
      Print #2, "Unexpected Event: " + lblUnexpectedEventNum.Caption
      Print #2, "Unexpected Location: " + lblUnexpectedLocationNum.Caption
      Print #2, "Unexpected Situation: " + lblUnexpectedSituationNum.Caption
      Print #2, "Master Cards: " + lblMasterCardNum.Caption
      Print #2, "Deck Size: " + lblDeckNum.Caption
      Print #2, "Pre-Game Size: " + lblPreGameNum.Caption
      Print #2, "MLE Cards: " + lblMLENum.Caption
    Close #2
  End If
End Sub

Private Sub cmdSaveSealedDeck_Click()
  Dim j
  comDlgSealedDeck.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (comDlgSealedDeck.FileName <> "") Then
    Open comDlgSealedDeck.FileName For Output As #1
    j = 0
    While j < sealedDeckCount
      Write #1, "DECK", sealedDeckCards(j, 1), sealedDeckCards(j, 2)
      j = j + 1
    Wend
    Close #1
  End If
  MsgBox "Your Sealed Deck List Has Been Saved.", vbOKOnly, "Save Complete"
End Sub

Private Sub cmdSelectCardType_Click()
    datCardNames.Recordset.Filter = "Type = '" + cmboCardType.List(cmboCardType.ListIndex) + "'"
End Sub

Private Sub cmdSetBoosterPack_Click()
  txtCommon.text = 10
  txtUncommon.text = 4
  txtRare.text = 1
  txtBasic.text = 0
  txtVeryRare.text = 0
  txtUltraRare.text = 0
  txtPremium.text = 0
  txtBasicPremium.text = 0
  txtUltraRarePremium.text = 0
  txtCollection.text = 0
  txtNemesis.text = 0
  txtPromo.text = 0
  txtQuickening.text = 0
End Sub

Private Sub cmdSetStarterDeck_Click()
  txtCommon.text = 40
  txtUncommon.text = 12
  txtRare.text = 3
  txtBasic.text = 0
  txtVeryRare.text = 0
  txtUltraRare.text = 0
  txtPremium.text = 0
  txtBasicPremium.text = 0
  txtUltraRarePremium.text = 0
  txtCollection.text = 0
  txtNemesis.text = 0
  txtPromo.text = 0
  txtQuickening.text = 0
End Sub

Private Sub cmdShowAll_Click()
  datCardNames.Recordset.Filter = adFilterNone
End Sub

Private Sub cmdShowBack_Click()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileName.text, 2)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub cmdShowFront_Click()
  Call txtFileName_Change
End Sub

Private Sub cmdShowSet_Click()
  If (cmboSet.List(cmboSet.ListIndex) = "ALL") Then
    datCardNames.Recordset.Filter = ""
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "'"
  End If
End Sub

Private Sub cmdSortName_Click()
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub cmdSortSet_Click()
  datCardNames.Recordset.Sort = "Set, Name"
End Sub

Private Sub cmdTextSearch_Click()
  On Error Resume Next
  Dim tempSearchString As String
  Dim counter As Integer
  
  datCardNames.Recordset.Filter = ""
  If txtTextSearch.text <> "" Then
    tempSearchString = txtTextSearch.text
    For counter = 1 To Len(txtTextSearch.text)
      If (Mid$(txtTextSearch.text, counter, 1) = "'") And (Mid$(txtTextSearch.text, counter + 1, 1) <> "'") Then
        txtTextSearch.text = Left$(txtTextSearch.text, counter) + "'" + Right$(txtTextSearch.text, Len(txtTextSearch.text) - counter)
        counter = counter + 1
      End If
    Next counter
    If (cmboSet.ListIndex < 1) Then
      datCardNames.Recordset.Filter = "Text LIKE '%" + txtTextSearch.text + "%'"
    Else
      datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Text LIKE '%" + txtTextSearch.text + "%'"
    End If
    txtTextSearch.text = tempSearchString
  End If

End Sub

Private Sub dataCards_Click()
  frameCardDetailsDB.Visible = True
  frameCardDetailsDeck.Visible = False
End Sub

Private Sub dataCards_DblClick()
  If txtType.text = "Persona" Then
    Call cmdAddPreGame_Click
  ElseIf txtType.text = "Pre-Game" Then
    Call cmdAddPreGame_Click
  Else
    Call cmdAddDeck_Click
  End If
End Sub

Private Sub Form_Load()
  On Error Resume Next
  
  ' Establish Database Connections
  datCardDeck.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardDeck.RecordSource = "cards"
  datCardDeck.Refresh
  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh

  ' Set variables to default values
  dariusRestrictionCardNum = 0
  preGameCount = 0
  totalDeckCount = 0
  totalPreGameCount = 0
  personaCullen = False
  xavierPower = False
  deckCount = 0
    
  ' Hide Database Fields other then card name
  dataCards.Columns("ID").Visible = False
  dataCards.Columns("Set").Visible = False
  dataCards.Columns("Number").Visible = False
  dataCards.Columns("File Name").Visible = False
  dataCards.Columns("Other").Visible = False
  dataCards.Columns("Text").Visible = False
  dataCards.Columns("Type").Visible = False
  dataCards.Columns("Rarity").Visible = False
  
  ' Sort Database by card name field
  datCardNames.Recordset.Sort = "Name"
End Sub

Public Sub DisplayClickedCard(ByRef tempLst As ListBox, ByRef tempArray() As Variant)
' Make sure a card is selected before attempting to display it
  Dim j As Integer
  j = 0
  While (j < Int(tempLst.ListCount))
    If (tempLst.Selected(j) = True) Then
      frameCardDetailsDB.Visible = False
      frameCardDetailsDeck.Visible = True
      DisplayCardById (tempArray(j, 1))
      txtNumberDeck.text = tempArray(j, 2)
    End If
    j = j + 1
  Wend
End Sub

Private Sub lstAllSealedDeckCards_Click()
  DisplayClickedCard lstAllSealedDeckCards, allSealedCards

End Sub

Private Sub lstAllSealedDeckCards_DblClick()
 If txtTypeDeck.text = "Persona" Then
   Call cmdAddSealedPreGame_Click
 ElseIf txtTypeDeck.text = "Pre-Game" Then
   Call cmdAddSealedPreGame_Click
 Else
   Call cmdAddSealedInGame_Click
 End If
End Sub

Private Sub lstDeck_Click()
  DisplayClickedCard lstDeck, deckCards
End Sub

Private Sub lstDeck_DblClick()
  Call cmdAddAnother_Click
End Sub

Private Sub lstPreGame_Click()
  DisplayClickedCard lstPreGame, preGameCards
End Sub

Private Sub lstPreGame_DblClick()
  Call cmdPreGameAdd_Click
End Sub

Private Sub lstSealedDeck_Click()
  DisplayClickedCard lstSealedDeck, sealedDeckCards

End Sub

Private Sub menuExit_Click()
  frmCreateEditDeck.Hide
  Unload frmCreateEditDeck
End Sub

Private Sub menuExport_Click()
  On Error Resume Next
  Dim j As Integer
  
  CommonDialog2.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog2.FileName <> "") Then
    frmLoading.Visible = True
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = False
    txtFileNameDeck.DataField = ""
    Open CommonDialog2.FileName For Output As #1
    j = 0
    Write #1, "Deck (" + Str$(totalDeckCount) + " cards)"
    Write #1, "Quantity", "Card Name", "Type", "Set", "Rarity"
    While j < deckCount
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      DisplayCardById (deckCards(j, 1))
      Write #1, deckCards(j, 2), txtCardNameDeck.text, txtTypeDeck.text, txtSetDeck.text, txtRarityDeck.text
      j = j + 1
    Wend
    Write #1, "", "", "", "", ""
    Write #1, "Pre-Game (" + Str$(totalPreGameCount) + " cards)"
    Write #1, "Quantity", "Card Name", "Type", "Set", "Rarity"
    j = 0
    While j < preGameCount
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      DisplayCardById (preGameCards(j, 1))
      Write #1, preGameCards(j, 2), txtCardNameDeck.text, txtTypeDeck.text, txtSetDeck.text, txtRarityDeck.text
      j = j + 1
    Wend
    frmLoading.Visible = False
    Close #1
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = True
    txtFileNameDeck.DataField = "File Name"
    MsgBox "Your deck has been exported successfully", vbOKOnly, "Complete"
  End If
End Sub

Private Sub menuLoadSealedDeck_Click()
  On Error Resume Next
  Dim cardlocation, temp1, temp2
  Dim msgBoxAnswer
  Dim counter As Integer
  
  comDlgSealedDeck.FileName = ""
  comDlgSealedDeck.ShowOpen
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (comDlgSealedDeck.FileName <> "") Then
    Call menuNewDeck_Click
    allSealedCount = 0
    totalAllSealedCount = 0
    
    ' Disconnect Two fields from the database to get the deck to load faster
    txtSetDeck.DataField = ""
    txtFileNameDeck.DataField = ""
    
    ' Reset Variables and setup the display
    frmLoading.Visible = True
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = False
    frameSealedDeckCards.Visible = True
    
    Open comDlgSealedDeck.FileName For Input As #1

    While Not EOF(1)
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      Input #1, cardlocation, temp1, temp2
      
        allSealedCards(allSealedCount, 1) = temp1
        allSealedCards(allSealedCount, 2) = temp2
        DisplayCardById allSealedCards(allSealedCount, 1)
        
        allSealedCards(allSealedCount, 3) = txtCardNameDeck.text
        allSealedCards(allSealedCount, 4) = txtTypeDeck.text
        totalAllSealedCount = totalAllSealedCount + allSealedCards(allSealedCount, 2)
        allSealedCount = allSealedCount + 1
    Wend
    Close #1
    
    ' Update Deck List
    counter = 0
    While counter < allSealedCount
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      DisplayCardById (allSealedCards(counter, 1))
      txtNumberDeck.text = allSealedCards(counter, 2)
      lstAllSealedDeckCards.AddItem ("(" + Str$(allSealedCards(counter, 2)) + ") " + txtCardNameDeck.text)
      counter = counter + 1
    Wend
    
    
    ' Reconnect Fields to Database, Set up display correctly
    txtSetDeck.DataField = "Set"
    txtFileNameDeck.DataField = "File Name"
    frmLoading.Visible = False
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = True
    frameDeck.Caption = "Deck (" + Str$(totalDeckCount) + ")"
    frmPreGame.Caption = "Pre-Game Cards (" + Str$(totalPreGameCount) + ")"
  End If

End Sub

Private Sub menuNewDeck_Click()
  dariusRestrictionCardNum = 0
  lstDeck.Clear
  lstPreGame.Clear
  deckCount = 0
  preGameCount = 0
  totalDeckCount = 0
  totalPreGameCount = 0
  masterCardLimit = 0
  masterCardCount = 0
  personaCullen = False
  xavierPower = False
  CommonDialog1.FileName = ""
  lstAllSealedDeckCards.Clear
  allSealedCount = 0
  totalAllSealedCount = 0
  frameSealedDeckCards.Visible = False
  frameDeck.Caption = "Deck (0)"
  frmPreGame.Caption = "Pre-Game Cards (0)"
End Sub

Private Sub menuOpen_Click()
  On Error Resume Next
  Dim cardlocation, temp1, temp2
  Dim msgBoxAnswer
  Dim counter As Integer
  Dim masterSwordsmanCount As Integer
  
  CommonDialog1.FileName = ""
  CommonDialog1.ShowOpen
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog1.FileName <> "") Then
    frameSealedDeckCards.Visible = False
    ' Disconnect Two fields from the database to get the deck to load faster
    txtSetDeck.DataField = ""
    txtFileNameDeck.DataField = ""
    
    ' Reset Variables and setup the display
    frmLoading.Visible = True
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = False
    lstDeck.Clear
    lstPreGame.Clear
    deckCount = 0
    preGameCount = 0
    personaCullen = False
    xavierPower = False
    totalDeckCount = 0
    totalPreGameCount = 0
    masterCardLimit = 0
    masterCardCount = 0
    dariusRestrictionCardNum = 0
    masterSwordsmanCount = 0
    
    Open CommonDialog1.FileName For Input As #1

    While Not EOF(1)
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      Input #1, cardlocation, temp1, temp2
      
      If (cardlocation = "DECK") Then
        deckCards(deckCount, 1) = temp1
        deckCards(deckCount, 2) = temp2
        DisplayCardById deckCards(deckCount, 1)
        
        adjustCardVariables
        
        ' Check if card being loaded has 'master' in the title
        counter = 1
        While counter < (Len(txtCardNameDeck.text) - Len("master"))
          StatusBox.Left = StatusBox.Left + 1
          If StatusBox.Left >= frmLoading.Width Then
            StatusBox.Left = 0
          End If
          
          If (LCase$(Mid$(txtCardNameDeck.text, counter, 6)) = "master") Then
            masterCardCount = masterCardCount + deckCards(deckCount, 2)
            If Right$(txtCardNameDeck.text, Len("Master Swordsman")) = "Master Swordsman" Then ' For Cullen's Persona Power
              If Left$(txtCardNameDeck.text, Len("Brian Cullen")) = "Brian Cullen" Then
                masterSwordsmanCount = masterSwordsmanCount + deckCards(deckCount, 2)
              ElseIf Left$(txtCardNameDeck.text, Len("Generic")) = "Generic" Then
                masterSwordsmanCount = masterSwordsmanCount + deckCards(deckCount, 2)
              End If
            End If
          End If
          counter = counter + 1
        Wend
        
        ' Add Card to Deck List
        deckCards(deckCount, 3) = txtCardNameDeck.text
        deckCards(deckCount, 4) = txtTypeDeck.text
        totalDeckCount = totalDeckCount + deckCards(deckCount, 2)
        deckCount = deckCount + 1
      End If
      
      If (cardlocation = "PREGAME") Then
        preGameCards(preGameCount, 1) = temp1
        preGameCards(preGameCount, 2) = temp2
        DisplayCardById preGameCards(preGameCount, 1)
        
        adjustCardVariables
        
        ' Add Card to Pre-Game List
        totalPreGameCount = totalPreGameCount + preGameCards(preGameCount, 2)
        preGameCount = preGameCount + 1
      End If
    Wend
    Close #1
    
    ' Adjust Master Card Count if Persona is Brian Cullen
    If personaCullen Then
      masterCardCount = masterCardCount - masterSwordsmanCount
    End If
    
    ' Update Deck List
    counter = 0
    While counter < deckCount
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      DisplayCardById (deckCards(counter, 1))
      txtNumberDeck.text = deckCards(counter, 2)
      lstDeck.AddItem ("(" + Str$(deckCards(counter, 2)) + ") " + txtCardNameDeck.text)
      counter = counter + 1
    Wend
    
    ' Update Pre-Game List
    counter = 0
    While counter < preGameCount
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      DisplayCardById (preGameCards(counter, 1))
      txtNumberDeck.text = preGameCards(counter, 2)
      lstPreGame.AddItem ("(" + Str$(preGameCards(counter, 2)) + ") " + txtCardNameDeck.text)
      counter = counter + 1
    Wend
    
    ' Reconnect Fields to Database, Set up display correctly
    txtSetDeck.DataField = "Set"
    txtFileNameDeck.DataField = "File Name"
    frmLoading.Visible = False
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = True
    frameDeck.Caption = "Deck (" + Str$(totalDeckCount) + ")"
    frmPreGame.Caption = "Pre-Game Cards (" + Str$(totalPreGameCount) + ")"
  End If
End Sub

Private Sub menuPrintDeck_Click()
  Dim j As Integer
  Dim spaceCounter As Integer, TotalSpaceCount As Integer
  Dim tempPrintLine, printedLines
  printedLines = 0
    frmLoading.Visible = True
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = False
    txtFileNameDeck.DataField = ""
    j = 0
    
    CommonDialog1.ShowPrinter
    Printer.CurrentY = 1440
    Printer.Font.name = "Courier New"
      
      ' Print the Deck List
    Printer.Font.Bold = True
    If (CommonDialog1.FileName <> "") Then
      Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(Left$(CommonDialog1.FileTitle, Len(CommonDialog1.FileTitle) - 4)) / 2)
      Printer.Print Left$(CommonDialog1.FileTitle, Len(CommonDialog1.FileTitle) - 4)
      printedLines = printedLines + 1
    End If
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Deck (" + LTrim$(Str$(totalDeckCount)) + " cards)") / 2)
    Printer.Print "Deck (" + LTrim$(Str$(totalDeckCount)) + " cards)"
      printedLines = printedLines + 1
    Printer.Font.Underline = True
    Printer.CurrentX = 1440
    Printer.Print "Quantity", "Card Name", , , "Type", , "Set"
      printedLines = printedLines + 1
    Printer.Font.Underline = False
    Printer.Font.Bold = False
    While j < deckCount
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      
      DisplayCardById (deckCards(j, 1))
      Printer.CurrentX = 1440
      tempPrintLine = ""
    
    ' Quantity
    spaceCounter = 0
    TotalSpaceCount = 7 - Len(deckCards(j, 2))
    While spaceCounter < TotalSpaceCount
      spaceCounter = spaceCounter + 1
      tempPrintLine = tempPrintLine + " "
    Wend
    tempPrintLine = tempPrintLine + Str$(deckCards(j, 2)) + "      "
    
    ' Card Title
    If Len(txtCardNameDeck.text) < 40 Then
      spaceCounter = 0
      TotalSpaceCount = 40 - Len(txtCardNameDeck.text)
      tempPrintLine = tempPrintLine + txtCardNameDeck.text
      While spaceCounter < TotalSpaceCount
        spaceCounter = spaceCounter + 1
        tempPrintLine = tempPrintLine + " "
      Wend
    Else
      tempPrintLine = tempPrintLine + Left$(txtCardNameDeck.text, 40)
    End If
    tempPrintLine = tempPrintLine + "  "
    ' Card Type
    If Len(txtTypeDeck.text) < 20 Then
      spaceCounter = 0
      TotalSpaceCount = 20 - Len(txtTypeDeck.text)
      tempPrintLine = tempPrintLine + txtTypeDeck.text
      While spaceCounter < TotalSpaceCount
        spaceCounter = spaceCounter + 1
        tempPrintLine = tempPrintLine + " "
      Wend
    Else
      tempPrintLine = tempPrintLine + Left$(txtTypeDeck.text, 20)
    End If
    
    
    Printer.Print tempPrintLine, txtSetDeck.text
      printedLines = printedLines + 1
    
    If (printedLines > 70) Then
        Printer.NewPage
        printedLines = 0
        Printer.CurrentY = 1440
    End If
    
      j = j + 1
    Wend
    
    Printer.Print ""
    printedLines = printedLines + 1
 
    If (printedLines > 67) And (preGameCount > 0) Then
        Printer.NewPage
        printedLines = 0
        Printer.CurrentY = 1440
    End If
    ' Print the Pre-Game List
    Printer.Font.Bold = True
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Pre-Game (" + LTrim$(Str$(totalPreGameCount)) + " cards)") / 2)
    Printer.Print "Pre-Game (" + LTrim$(Str$(totalPreGameCount)) + " cards)"
    printedLines = printedLines + 1
    Printer.Font.Underline = True
    Printer.CurrentX = 1440
    Printer.Print "Quantity", "Card Name", , , "Type", , "Set"
    printedLines = printedLines + 1
    Printer.Font.Underline = False
    Printer.Font.Bold = False
    
    j = 0
    While j < preGameCount
      DoEvents
      StatusBox.Left = StatusBox.Left + 10
      If StatusBox.Left >= frmLoading.Width Then
        StatusBox.Left = 0
      End If
      DisplayCardById (preGameCards(j, 1))
      Printer.CurrentX = 1440
      tempPrintLine = ""
    
    ' Quantity
    spaceCounter = 0
    TotalSpaceCount = 7 - Len(preGameCards(j, 2))
    While spaceCounter < TotalSpaceCount
      spaceCounter = spaceCounter + 1
      tempPrintLine = tempPrintLine + " "
    Wend
    tempPrintLine = tempPrintLine + Str$(preGameCards(j, 2)) + "      "
    
    ' Card Title
    If Len(txtCardNameDeck.text) < 40 Then
      spaceCounter = 0
      TotalSpaceCount = 40 - Len(txtCardNameDeck.text)
      tempPrintLine = tempPrintLine + txtCardNameDeck.text
      While spaceCounter < TotalSpaceCount
        spaceCounter = spaceCounter + 1
        tempPrintLine = tempPrintLine + " "
      Wend
    Else
      tempPrintLine = tempPrintLine + Left$(txtCardNameDeck.text, 40)
    End If
    tempPrintLine = tempPrintLine + "  "
    ' Card Type
    If Len(txtTypeDeck.text) < 20 Then
      spaceCounter = 0
      TotalSpaceCount = 20 - Len(txtTypeDeck.text)
      tempPrintLine = tempPrintLine + txtTypeDeck.text
      While spaceCounter < TotalSpaceCount
        spaceCounter = spaceCounter + 1
        tempPrintLine = tempPrintLine + " "
      Wend
    Else
      tempPrintLine = tempPrintLine + Left$(txtTypeDeck.text, 20)
    End If
    
    
    Printer.Print tempPrintLine, txtSetDeck.text
      printedLines = printedLines + 1
      
    If (printedLines > 70) Then
        Printer.NewPage
        printedLines = 0
        Printer.CurrentY = 1440
    End If
'      Printer.Print preGameCards(j, 2), txtCardNameDeck.text, txtTypeDeck.text, txtSetDeck.text, txtRarityDeck.text
      j = j + 1
    Wend
    If (printedLines > 0) Then
      Printer.EndDoc
    End If
    
    frmLoading.Visible = False
    frameCardDetailsDB.Visible = False
    frameCardDetailsDeck.Visible = True
    txtFileNameDeck.DataField = "File Name"
    MsgBox "Your deck has been printed successfully", vbOKOnly, "Complete"
  

End Sub

Private Sub menuSave_Click()
  On Error Resume Next
  Dim j As Integer
  
  If CommonDialog1.FileName = "" Then
    CommonDialog1.ShowSave
    If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  End If
  If (CommonDialog1.FileName <> "") Then
    Open CommonDialog1.FileName For Output As #1
    
    ' Save Deck
    j = 0
    While j < deckCount
      Write #1, "DECK", deckCards(j, 1), deckCards(j, 2)
      j = j + 1
    Wend
    
    'Save Pre-Game
    j = 0
    While j < preGameCount
      Write #1, "PREGAME", preGameCards(j, 1), preGameCards(j, 2)
      j = j + 1
    Wend
    Close #1
  End If
  MsgBox "Your Deck Has Been Saved.", vbOKOnly, "Save Complete"
End Sub

Private Sub menuSaveAs_Click()
  On Error Resume Next
  Dim j As Integer
  
  CommonDialog1.ShowSave
  If Err.number = 32755 Then Exit Sub ' exit if cancel is pressed
  If (CommonDialog1.FileName <> "") Then
    Open CommonDialog1.FileName For Output As #1
    
    ' Save Deck
    j = 0
    While j < deckCount
      Write #1, "DECK", deckCards(j, 1), deckCards(j, 2)
      j = j + 1
    Wend
    
    'Save Pre-Game
    j = 0
    While j < preGameCount
      Write #1, "PREGAME", preGameCards(j, 1), preGameCards(j, 2)
      j = j + 1
    Wend
    Close #1
  End If
  MsgBox "Your Deck Has Been Saved.", vbOKOnly, "Save Complete"
End Sub

Private Sub menuShowStats_Click()
  Dim mleCount
  Dim j As Integer
  
  txtFileNameDeck.DataField = ""
  mleCount = 0
  lblAttackNum.Caption = 0
  lblAttackBlockNum.Caption = 0
  lblAttackBlockNum.Caption = 0
  lblBlockNum.Caption = 0
  lblDodgeNum.Caption = 0
  lblEventNum.Caption = 0
  lblSituationNum.Caption = 0
  lblObjectNum.Caption = 0
  lblLocationNum.Caption = 0
  lblEdgeNum.Caption = 0
  lblCombatTrickNum.Caption = 0
  lblMartialArtsNum.Caption = 0
  lblUnexpectedEventNum.Caption = 0
  lblUnexpectedLocationNum.Caption = 0
  lblUnexpectedSituationNum.Caption = 0
  lblPreGameNum.Caption = 0
  lblDeckNum.Caption = 0
  lblMasterCardNum.Caption = 0
  
  frameDeckStats.Visible = True
    j = 0
    While j < deckCount
      DoEvents
      DisplayCardById deckCards(j, 1)
      Dim k
      k = 1
      While k < (Len(txtCardNameDeck.text) - Len("master"))
        If (LCase$(Mid$(txtCardNameDeck.text, k, 6)) = "master") Then
          lblMasterCardNum.Caption = lblMasterCardNum.Caption + deckCards(j, 2)
        End If
        k = k + 1
      Wend
            
      ' Count MLE Cards
      If txtSetDeck.text = "DDC" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "MLC" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "MLE" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "MLEMD" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "MLEPR" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "RC" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "HHM" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "SOI" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "MMQB" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "BIA" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "CRC" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "WWE" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "CCM" Then
        mleCount = mleCount + deckCards(j, 2)
      ElseIf txtSetDeck.text = "JGC" Then
        mleCount = mleCount + deckCards(j, 2)
      End If
            
      ' Figure out how many of each card type
      If txtTypeDeck.text = "Attack" Then
        lblAttackNum.Caption = lblAttackNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Attack: Trap" Then
        lblAttackNum.Caption = lblAttackNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Hidden Attack: Trap" Then
        lblAttackNum.Caption = lblAttackNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Special Attack" Then
        lblAttackNum.Caption = lblAttackNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Attack/Defense" Then
        lblAttackBlockNum.Caption = lblAttackBlockNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Dodge-N-Strike" Then
        lblAttackBlockNum.Caption = lblAttackBlockNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Block/Attack" Then
        lblAttackBlockNum.Caption = lblAttackBlockNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Block" Then
        lblBlockNum.Caption = lblBlockNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Dodge" Then
        lblDodgeNum.Caption = lblDodgeNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Event" Then
        lblEventNum.Caption = lblEventNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Situation" Then
        lblSituationNum.Caption = lblSituationNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Situation: Time" Then
        lblSituationNum.Caption = lblSituationNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Situation: Trap" Then
        lblSituationNum.Caption = lblSituationNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Object" Then
        lblObjectNum.Caption = lblObjectNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Object: Trap" Then
        lblObjectNum.Caption = lblObjectNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Location" Then
        lblLocationNum.Caption = lblLocationNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Edge" Then
        lblEdgeNum.Caption = lblEdgeNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Edge: Trap" Then
        lblEdgeNum.Caption = lblEdgeNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Combat Trick" Then
        lblCombatTrickNum.Caption = lblCombatTrickNum.Caption + deckCards(j, 2)
      ElseIf Left$(txtTypeDeck.text, 12) = "Martial Arts" Then
        lblMartialArtsNum.Caption = lblMartialArtsNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Unexpected Event" Then
        lblUnexpectedEventNum.Caption = lblUnexpectedEventNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Unexpected Location" Then
        lblUnexpectedLocationNum.Caption = lblUnexpectedLocationNum.Caption + deckCards(j, 2)
      ElseIf txtTypeDeck.text = "Unexpected Situation" Then
        lblUnexpectedSituationNum.Caption = lblUnexpectedSituationNum.Caption + deckCards(j, 2)
      End If
      j = j + 1
    Wend
    
    ' Deck and Pre-Game Counts
    lblPreGameNum.Caption = totalPreGameCount
    lblDeckNum.Caption = totalDeckCount
    
    ' MLE Card count (don't divide by 0 if no cards in deck)
    If totalDeckCount > 0 Then
      lblMLENum.Caption = Str(mleCount) + " Cards (" + Str(mleCount / totalDeckCount * 100) + "%)"
    End If
      
    txtFileNameDeck.DataField = "File Name"
End Sub

Private Sub menuSortBoth_Click()
  Call menuSortName_Click
  Call menuSortType_Click
End Sub

Private Sub menuSortName_Click()
  Dim counter1 As Integer
  Dim counter2 As Integer
  
  For counter1 = 0 To deckCount - 1
    For counter2 = 0 To deckCount - 2
      If (deckCards(counter2, 3) > deckCards(counter2 + 1, 3)) Then
        Dim tempCard(4)
        tempCard(1) = deckCards(counter2, 1)
        tempCard(2) = deckCards(counter2, 2)
        tempCard(3) = deckCards(counter2, 3)
        tempCard(4) = deckCards(counter2, 4)
        
        deckCards(counter2, 1) = deckCards(counter2 + 1, 1)
        deckCards(counter2, 2) = deckCards(counter2 + 1, 2)
        deckCards(counter2, 3) = deckCards(counter2 + 1, 3)
        deckCards(counter2, 4) = deckCards(counter2 + 1, 4)
        
        deckCards(counter2 + 1, 1) = tempCard(1)
        deckCards(counter2 + 1, 2) = tempCard(2)
        deckCards(counter2 + 1, 3) = tempCard(3)
        deckCards(counter2 + 1, 4) = tempCard(4)
      End If
    Next counter2
  Next counter1
  
  lstDeck.Clear
  For counter1 = 0 To deckCount - 1
    lstDeck.AddItem ("(" + Str$(deckCards(counter1, 2)) + ") " + deckCards(counter1, 3))
  Next counter1
End Sub

Private Sub menuSortType_Click()
  Dim counter1 As Integer
  Dim counter2 As Integer
  
  For counter1 = 0 To deckCount - 1
    For counter2 = 0 To deckCount - 2
      If (deckCards(counter2, 4) > deckCards(counter2 + 1, 4)) Then
        Dim tempCard(4)
        tempCard(1) = deckCards(counter2, 1)
        tempCard(2) = deckCards(counter2, 2)
        tempCard(3) = deckCards(counter2, 3)
        tempCard(4) = deckCards(counter2, 4)
        
        deckCards(counter2, 1) = deckCards(counter2 + 1, 1)
        deckCards(counter2, 2) = deckCards(counter2 + 1, 2)
        deckCards(counter2, 3) = deckCards(counter2 + 1, 3)
        deckCards(counter2, 4) = deckCards(counter2 + 1, 4)
        
        deckCards(counter2 + 1, 1) = tempCard(1)
        deckCards(counter2 + 1, 2) = tempCard(2)
        deckCards(counter2 + 1, 3) = tempCard(3)
        deckCards(counter2 + 1, 4) = tempCard(4)
      End If
    Next counter2
  Next counter1
  
  lstDeck.Clear
  For counter1 = 0 To deckCount - 1
    lstDeck.AddItem ("(" + Str$(deckCards(counter1, 2)) + ") " + deckCards(counter1, 3))
  Next counter1
End Sub

Private Sub menuSwitchToSealedDeckG_Click()
  menuFile.Enabled = False
  menuOptions.Enabled = False
  frameSealedDeckOptions.Visible = True
  frmSealedDeck.Visible = True
End Sub

Private Sub txtFileName_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileName.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(FileName)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.text + "\" + FileName)
    End If
  End If
End Sub

Public Function getImageLocation(txtFileNames As String, nameToRetrive As Integer)
'  This function was created to deal with the whole "two card images in one field" issue
'nameToRetrive = 1 - Front, 2 - Back
  Dim counter As Long
  Dim foundSplit As Boolean
  Dim splitPos As Long
  foundSplit = False
  
  If (Left$(txtFileNames, 1) = "*") Then ' if first character is '*' then check for character '|'
    counter = 0
    While counter <= Len(txtFileNames)
      counter = counter + 1
      If Mid$(txtFileNames, counter, 1) = "|" Then ' '|' represents end of front image and start of back
        foundSplit = True
        splitPos = counter
      End If
    Wend
    
    If foundSplit Then ' if a break was found then...
      If nameToRetrive = 1 Then
        getImageLocation = Mid$(txtFileNames, 2, splitPos - 2)
      Else
        getImageLocation = Mid$(txtFileNames, splitPos + 1, Len(txtFileNames) - splitPos)
      End If
    Else ' if a break was not found, but a '*' was, assume somethings wrong and display empty.jpg
      getImageLocation = ""
    End If
  Else ' if a '*' is not found, assume only the front of the image is given
    If nameToRetrive = 1 Then
      getImageLocation = txtFileNames
    Else
      getImageLocation = ""
    End If
  End If
End Function

Private Sub txtFileNameDeck_Change()
  On Error Resume Next
  Dim FileName As Variant
  HLCardDeck.Picture = LoadPicture(App.Path + "\empty.jpg")
  FileName = getImageLocation(txtFileNameDeck.text, 1)
  If FileName <> "" Then
    If Mid$(FileName, 2, 1) = ":" Then
      HLCardDeck.Picture = LoadPicture(FileName)
    Else
      HLCardDeck.Picture = LoadPicture(App.Path + "\" + txtSetDeck.text + "\" + FileName)
    End If
  End If
End Sub

Private Sub txtSet_Change()
  Call txtFileName_Change
End Sub

Private Sub txtSetDeck_Change()
  Call txtFileNameDeck_Change
End Sub

Public Function isMaster(ByRef cardID As Variant)
  Dim result As Boolean
  Dim k As Integer
  k = 1
  result = False
  While k < (Len(txtCardNameDeck.text))
    If (LCase$(Mid$(txtCardNameDeck.text, k, 6)) = "master") Then
      If (personaCullen = True) Then
        If Right$(txtCardNameDeck.text, Len("Master Swordsman")) = "Master Swordsman" Then
          If Left$(txtCardNameDeck.text, Len("Brian Cullen")) = "Brian Cullen" Then
            result = False
          ElseIf Left$(txtCardNameDeck.text, Len("Generic")) = "Generic" Then
            result = False
          ElseIf Left$(txtCardNameDeck.text, Len("Gothic Sword")) = "Gothic Sword" Then
            result = False
          Else
            result = True
          End If
        Else
          result = True
        End If
      Else
        result = True
      End If
    End If
    k = k + 1
  Wend
  isMaster = result
End Function

Public Sub adjustCardVariables()
'  Tell system that a card with specific rules was added to deck or pre-game
  Dim msgBoxAnswer
        
  ' check and see if the card is the cullen persona
  If txtIDDeck.text = 2554 Or txtIDDeck.text = 2555 Then
    personaCullen = True
  End If
  
  ' if xavier persona or quickening set to true for plot cards
  If (txtIDDeck.text = 968) Or (txtIDDeck.text = 1414) Or (txtIDDeck.text = 2080) Then
    xavierPower = True
  End If
        
  'If card is master's prize
  If (txtIDDeck.text = 2428) Then
    mastersPrizeCount = mastersPrizeCount + 1
  End If
  
  ' if card is darius (increase restriction number)
  If (txtIDDeck.text = 1871) Then
    dariusRestrictionCardNum = dariusRestrictionCardNum + 1
  End If
  
  ' if card is darius (incrase master card limit)
  If (txtIDDeck.text = 2309) Then
    msgBoxAnswer = MsgBox("Increase Master Card Limit By 1?", vbYesNo, "Increase Master Limit")
    If msgBoxAnswer = 6 Then
      masterCardLimit = masterCardLimit + 1
      MsgBox "New Master Card Limit Set.", vbOKOnly, "Limit Set"
    End If
  End If

  ' If card is a persona, set master card limit
  If LCase(txtTypeDeck.text) = "persona" Then
    msgBoxAnswer = MsgBox("Set Master Card Limit Set to " + txtOtherDeck.text + "?", vbOKCancel, "Master Card Limit Set")
    If msgBoxAnswer = 1 Then
      masterCardLimit = txtOtherDeck.text
      MsgBox "New Master Card Limit Set.", vbOKOnly, "Limit Set"
    End If
  End If

End Sub

Private Sub TransferCardFromLstToLst(ByRef sendingLst As ListBox, ByRef sendingArray() As Variant, ByRef sendingPos As Integer, ByRef receivingLst As ListBox, ByRef receivingArray(), ByRef receivingPos As Integer, ByRef receivingTotalCount As Integer)
  Dim j As Integer
  Dim k As Integer
  Dim counter
  Dim foundcard As Boolean
  j = 0
  frameCardDetailsDB.Visible = False
  frameCardDetailsDeck.Visible = True
  
  If sendingPos > 0 Then
    While (j < sendingLst.ListCount)
      If sendingLst.Selected(j) = True Then
        foundcard = False
        k = j
        DisplayCardById (sendingArray(k, 1))
        counter = 0
        For counter = 0 To receivingPos - 1
          ' If the card to be added is already in the deck...
          If (sendingArray(k, 1) = receivingArray(counter, 1)) Then
            foundcard = True
            If sendingArray(k, 2) > receivingArray(counter, 2) Then
              receivingArray(counter, 2) = receivingArray(counter, 2) + 1
              receivingLst.List(counter) = "(" + Str(receivingArray(counter, 2)) + ") " + txtCardNameDeck.text
              receivingTotalCount = receivingTotalCount + 1
            Else
              MsgBox "Every available copy of this card is already in your deck.", vbOKOnly, "Deck Construction Error"
            End If
          End If
        Next counter
        If foundcard = False Then
          receivingArray(receivingPos, 1) = sendingArray(k, 1)
          receivingArray(receivingPos, 2) = 1
          receivingArray(receivingPos, 3) = sendingArray(k, 3)
          receivingArray(receivingPos, 4) = sendingArray(k, 4)
          receivingPos = receivingPos + 1
          receivingTotalCount = receivingTotalCount + 1
          receivingLst.AddItem "(" + Str(receivingArray(counter, 2)) + ") " + txtCardNameDeck.text
      End If
      End If
      j = j + 1
    Wend
  End If
End Sub

