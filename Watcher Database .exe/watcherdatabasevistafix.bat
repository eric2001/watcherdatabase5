@echo off
echo Un-Registering components...
regsvr32 /u /s msflxgrd.ocx
regsvr32 /u /s comdlg32.ocx
regsvr32 /u /s msbind.dll
regsvr32 /u /s mswinsck.ocx
regsvr32 /u /s msderun.dll
regsvr32 /u /s msinet.ocx
regsvr32 /u /s mswinsck.ocx
regsvr32 /u /s mscomctl.ocx
regsvr32 /u /s msdatlst.ocx
regsvr32 /u /s msflxgrd.ocx
regsvr32 /u /s msmask32.ocx
regsvr32 /u /s tabctl32.ocx
regsvr32 /u /s mscomm32.ocx
regsvr32 /u /s msdbrptr.dll
regsvr32 /u /s msdatgrd.ocx
regsvr32 /u /s mscomct2.ocx
regsvr32 /u /s mshflxgd.ocx
regsvr32 /u /s msadodc.ocx
RegSvr32 /u /s msvbvm60.dll
RegSvr32 /u /s olepro32.dll
RegSvr32 /u /s comcat.dll
regSvr32 /u /s msstdfmt.dll
echo Re-Registering Components...
regsvr32 /s msflxgrd.ocx
regsvr32 /s comdlg32.ocx
regsvr32 /s msbind.dll
regsvr32 /s mswinsck.ocx
regsvr32 /s msderun.dll
regsvr32 /s msinet.ocx
regsvr32 /s mswinsck.ocx
regsvr32 /s mscomctl.ocx
regsvr32 /s msdatlst.ocx
regsvr32 /s msflxgrd.ocx
regsvr32 /s msmask32.ocx
regsvr32 /s tabctl32.ocx
regsvr32 /s mscomm32.ocx
regsvr32 /s msdbrptr.dll
regsvr32 /s msdatgrd.ocx
regsvr32 /s mscomct2.ocx
regsvr32 /s mshflxgd.ocx
regsvr32 /s msadodc.ocx
regSvr32 /s msvbvm60.dll
regSvr32 /s olepro32.dll
regSvr32 /s comcat.dll
regSvr32 /s msstdfmt.dll
echo All Done!
pause