VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmPatchDB 
   Caption         =   "Patch Database"
   ClientHeight    =   5175
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   9525
   LinkTopic       =   "Form2"
   ScaleHeight     =   5175
   ScaleWidth      =   9525
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      Height          =   495
      Left            =   7680
      TabIndex        =   23
      Top             =   4560
      Width           =   1095
   End
   Begin VB.ListBox lstPatchLog 
      Height          =   1620
      Left            =   4560
      TabIndex        =   22
      Top             =   2760
      Width           =   4455
   End
   Begin VB.CommandButton cmdPatchDB 
      Caption         =   "&Import CSV"
      Height          =   495
      Left            =   4920
      TabIndex        =   19
      Top             =   4560
      Width           =   1335
   End
   Begin MSDataGridLib.DataGrid dataCards 
      Bindings        =   "frmPatchDB.frx":0000
      Height          =   4815
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   8493
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   0   'False
      DefColWidth     =   273
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Card Details"
      Height          =   4815
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin MSComDlg.CommonDialog CommonDialog2 
         Left            =   360
         Top             =   3720
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "csv"
         Filter          =   "csv"
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   360
         Top             =   3000
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
         DefaultExt      =   "hdb"
         DialogTitle     =   "Select the DB Patch to Use"
         Filter          =   "Database Patch Files (*.hdb)|*.hdb|All Files (*.*)|*.*"
      End
      Begin VB.CommandButton cmdSelectFile 
         Caption         =   "Select File"
         Height          =   255
         Left            =   2520
         TabIndex        =   18
         Top             =   1800
         Width           =   975
      End
      Begin VB.TextBox txtCardText 
         DataField       =   "Text"
         DataSource      =   "datCardNames"
         Height          =   2565
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   14
         Top             =   2160
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         DataField       =   "Other"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   12
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox txtType 
         DataField       =   "Type"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   10
         Top             =   1440
         Width           =   2415
      End
      Begin VB.TextBox txtNum 
         DataField       =   "Number"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   8
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox txtCardName 
         DataField       =   "Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   4
         Top             =   360
         Width           =   2415
      End
      Begin VB.TextBox txtSet 
         DataField       =   "Set"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox txtFileName 
         DataField       =   "File Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox txtRarity 
         DataField       =   "Rarity"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label lblFileName 
         AutoSize        =   -1  'True
         Caption         =   "File Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   1800
         Width           =   750
      End
      Begin VB.Label lblCardText 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   2160
         Width           =   735
      End
      Begin VB.Label lblOther 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   2400
         TabIndex        =   13
         Top             =   1080
         Width           =   435
      End
      Begin VB.Label lblType 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   405
      End
      Begin VB.Label lblNumber 
         AutoSize        =   -1  'True
         Caption         =   "Quantity:"
         Height          =   195
         Left            =   2160
         TabIndex        =   9
         Top             =   720
         Width           =   630
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   840
      End
      Begin VB.Label lblSet 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   285
      End
      Begin VB.Label lblRarity 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   1080
         Width           =   450
      End
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   2640
      Top             =   4200
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label3 
      Caption         =   "It is recommended that you make a copy/backup of your cards.mdb file before using this program."
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   4560
      TabIndex        =   21
      Top             =   480
      Width           =   4455
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Instructions:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6000
      TabIndex        =   20
      Top             =   0
      Width           =   1485
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuExit 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
End
Attribute VB_Name = "frmPatchDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'    Project:  Watcher Database CVS Import Utility
'    Created:  26 January 2007
'    Updated:  22 October 2007
'    Copyright 2007 Eric Cavaliere
'    Description:  The purpose of this program is to import card
'       lists from external sources, such as the lists found on
'       mleccg.com.  The "cmdPatchDB_Click" function may need to
'       be altered based on the format of a specific data source.
'       Additionally "startingIDNum" needs to be set to the first
'       unused ID number in the cards.mdb file.

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Sub DisplayCardById(idToDisplay)
' Jump to a specific record in the database.
'   This record is determined by the variable idToDisplay.
  datCardNames.Recordset.MoveFirst
  datCardNames.Recordset.Find ("Id = '" + Str(idToDisplay) + "'")
End Sub

Private Sub cmdExit_Click()
' Quit from the program.
  frmPatchDB.Hide
  Unload frmPatchDB
End Sub

Private Sub cmdPatchDB_Click()
' This sub will import data from a .csv file
'   into the cards.mdb database.

  On Error Resume Next
  Dim id, name, setSym, other, cardType, rarity, text, FileName
  Dim startingIDNum As Integer
  startingIDNum = 4330
  
  CommonDialog1.ShowOpen
  If Err.Number = 32755 Then Exit Sub ' exit if cancel is pressed
  
  If (CommonDialog1.FileName <> "") Then
  Open CommonDialog1.FileName For Input As #1
  lstPatchLog.AddItem "Importing Data from " + CommonDialog1.FileName
  While Not EOF(1)
    DoEvents
    Input #1, badSet, cardTitle, cardImmortal, CardRarityShort, CardReserRestrict, cardRestrictNum, Card2EOnly, cardType, cardGems, CardHand, CardGrid, CardText
    
    id = startingIDNum
    startingIDNum = startingIDNum + 1
    DisplayCardById (id)
    
    If txtCardName.text = "" Then
      lstPatchLog.AddItem cardImmortal + " " + cardTitle + " not found, creating record"
      
      other = CardReserRestrict
      If cardRestrictNum > 0 Then
      other = other + LTrim$(RTrim$(Str$(cardRestrictNum)))
      End If
      If LTrim$(RTrim$(CardHand)) <> "" Then
        If other = "" Then
          other = LTrim$(RTrim$(CardHand))
        Else
          other = other + ", " + LTrim$(RTrim$(CardHand))
        End If
      End If
      
      If LTrim$(RTrim$(cardGems)) <> "" Then
        If other = "" Then
          other = "GEMS: " + LTrim$(RTrim$(cardGems))
        Else
          other = other + ", GEMS: " + LTrim$(RTrim$(cardGems))
        End If
      End If
      
      If CardRarityShort = "U" Then
        CardRarityShort = "Uncommon"
      End If
      If CardRarityShort = "P" Then
        CardRarityShort = "Premium"
      End If
      If CardRarityShort = "R" Then
        CardRarityShort = "Rare"
      End If
      If CardRarityShort = "C" Then
        CardRarityShort = "Common"
      End If
      If CardRarityShort = "F" Then
        CardRarityShort = "Basic"
      End If
      

      ' If the card grid field contains a value,
      '     format and add it to the card text.
      If LTrim$(RTrim$(CardGrid)) <> "" Then
        Dim i As Integer
        i = 1
        While i < 10
          If (Mid$(CardGrid, i, 1)) = "o" Then
            Mid$(CardGrid, i, 1) = "-"
          End If
          i = i + 1
        Wend
        CardGrid = (Mid$(CardGrid, 1, 3)) + "/" + (Mid$(CardGrid, 4, 3)) + "/" + (Mid$(CardGrid, 7, 3))
        CardGrid = "GRID:  " + CardGrid
        CardText = CardGrid + vbNewLine + CardText
      End If
      
      ' if the Second Edition Only field has a value,
      '     add it to the card text.
      If LTrim$(RTrim$(Card2EOnly)) <> "" Then
        CardText = CardText + vbNewLine + "2E"
      End If
      
      datCardNames.Recordset.AddNew
      dataCards.Columns("ID").Value = id
      If cardImmortal <> "" Then
        dataCards.Columns("Name").Value = cardImmortal + " " + cardTitle
      Else
          dataCards.Columns("Name").Value = cardTitle
      End If
      
      dataCards.Columns("Set").Value = "2EBetaS2"
      dataCards.Columns("Number").Value = 0
      dataCards.Columns("File Name").Value = ""
      dataCards.Columns("Other").Value = other
      dataCards.Columns("Text").Value = CardText
      dataCards.Columns("Type").Value = cardType
      dataCards.Columns("Rarity").Value = CardRarityShort
      lstPatchLog.AddItem "Record #" + Str$(id) + " created"
    Else
      lstPatchLog.AddItem txtCardName.text + " (" + cardImmortal + " " + cardTitle + ") found, WTF?  Skipping..."
    End If
    datCardNames.Recordset.Update
  Wend
  
  Close #1
  lstPatchLog.AddItem "Database Update Completed."
  tempVar = MsgBox("Finished", vbOKOnly, "Database Patch Complete")
  End If
End Sub

Private Sub Form_Load()
  On Error Resume Next
  
  ' Connect to the database.
  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh
  
  ' Hide all fields except for the card title.
  dataCards.Columns("ID").Visible = False
  dataCards.Columns("Set").Visible = False
  dataCards.Columns("Number").Visible = False
  dataCards.Columns("File Name").Visible = False
  dataCards.Columns("Other").Visible = False
  dataCards.Columns("Text").Visible = False
  dataCards.Columns("Type").Visible = False
  dataCards.Columns("Rarity").Visible = False
End Sub

Private Sub menuExit_Click()
' Quit this program.
  Call cmdExit_Click
End Sub
