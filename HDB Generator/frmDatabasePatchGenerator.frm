VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmDatabasePatchGenerator 
   Caption         =   "Database Patch Generator"
   ClientHeight    =   6885
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   14010
   LinkTopic       =   "Form2"
   ScaleHeight     =   6885
   ScaleWidth      =   14010
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame framePatchControls 
      Height          =   1215
      Left            =   11280
      TabIndex        =   35
      Top             =   4920
      Width           =   2535
      Begin VB.CommandButton cmdGeneratePatch 
         Caption         =   "Generate .hdb File"
         Height          =   495
         Left            =   120
         TabIndex        =   37
         Top             =   600
         Width           =   2295
      End
      Begin VB.CheckBox checkFileNames 
         Caption         =   "Include File Names?"
         Height          =   255
         Left            =   360
         TabIndex        =   36
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame frameImageControls 
      Height          =   855
      Left            =   8280
      TabIndex        =   32
      Top             =   5040
      Width           =   2295
      Begin VB.CommandButton cmdSwitchImage 
         Caption         =   "Show Back"
         Height          =   495
         Left            =   1320
         TabIndex        =   34
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdShowFront 
         Caption         =   "Show Front"
         Height          =   495
         Left            =   120
         TabIndex        =   33
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame frameDatabaseControls 
      Height          =   1815
      Left            =   120
      TabIndex        =   23
      Top             =   4920
      Width           =   4215
      Begin VB.CommandButton cmdShowSet 
         Caption         =   "Select Set"
         Height          =   375
         Left            =   2760
         TabIndex        =   31
         Top             =   840
         Width           =   1215
      End
      Begin VB.ComboBox cmboSet 
         Height          =   315
         ItemData        =   "frmDatabasePatchGenerator.frx":0000
         Left            =   120
         List            =   "frmDatabasePatchGenerator.frx":0064
         TabIndex        =   30
         Text            =   "Set Names"
         Top             =   840
         Width           =   2415
      End
      Begin VB.CommandButton cmdSortName 
         Caption         =   "Sort By Name"
         Height          =   495
         Left            =   2160
         TabIndex        =   29
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdSortSet 
         Caption         =   "Sort By Set"
         Height          =   495
         Left            =   3120
         TabIndex        =   28
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdMoveBack1Record 
         Caption         =   "Previous Card"
         Height          =   495
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdMoveForward1Record 
         Caption         =   "Next Card"
         Height          =   495
         Left            =   1080
         TabIndex        =   26
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdShowType 
         Caption         =   "Select Type"
         Height          =   375
         Left            =   2760
         TabIndex        =   25
         Top             =   1320
         Width           =   1215
      End
      Begin VB.ComboBox cmboCardType 
         Height          =   315
         ItemData        =   "frmDatabasePatchGenerator.frx":0126
         Left            =   120
         List            =   "frmDatabasePatchGenerator.frx":0169
         TabIndex        =   24
         Text            =   "Card Type"
         Top             =   1320
         Width           =   2415
      End
   End
   Begin VB.CommandButton cmdUpdatePatch 
      Caption         =   "Update && Add To List"
      Height          =   855
      Left            =   5880
      TabIndex        =   11
      Top             =   5160
      Width           =   1335
   End
   Begin VB.ListBox lstCardstoPatch 
      Height          =   4350
      ItemData        =   "frmDatabasePatchGenerator.frx":0293
      Left            =   11280
      List            =   "frmDatabasePatchGenerator.frx":0295
      TabIndex        =   22
      Top             =   360
      Width           =   2655
   End
   Begin MSDataGridLib.DataGrid dataCards 
      Bindings        =   "frmDatabasePatchGenerator.frx":0297
      Height          =   4695
      Left            =   120
      TabIndex        =   18
      Top             =   120
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   8281
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   0   'False
      DefColWidth     =   273
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Card Details"
      Height          =   4815
      Left            =   4080
      TabIndex        =   0
      Top             =   0
      Width           =   3615
      Begin VB.TextBox txtID 
         DataField       =   "ID"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
      Begin MSComDlg.CommonDialog CommonDialog3 
         Left            =   360
         Top             =   2880
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "hlt"
         Filter          =   "*.hlt"
      End
      Begin MSComDlg.CommonDialog CommonDialog2 
         Left            =   120
         Top             =   3960
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "csv"
         Filter          =   "csv"
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   720
         Top             =   4080
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmdSelectFile 
         Caption         =   "Select File"
         Height          =   255
         Left            =   2520
         TabIndex        =   20
         Top             =   2160
         Width           =   975
      End
      Begin VB.TextBox txtCardText 
         DataField       =   "Text"
         DataSource      =   "datCardNames"
         Height          =   2205
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   2520
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         DataField       =   "Other"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   6
         Top             =   1440
         Width           =   615
      End
      Begin VB.TextBox txtType 
         DataField       =   "Type"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   7
         Top             =   1800
         Width           =   2415
      End
      Begin VB.TextBox txtNum 
         DataField       =   "Number"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   2880
         TabIndex        =   13
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox txtCardName 
         DataField       =   "Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   600
         Width           =   2415
      End
      Begin VB.TextBox txtSet 
         DataField       =   "Set"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtFileName 
         DataField       =   "File Name"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   10
         Top             =   2160
         Width           =   1335
      End
      Begin VB.TextBox txtRarity 
         DataField       =   "Rarity"
         DataSource      =   "datCardNames"
         Height          =   285
         Left            =   1080
         TabIndex        =   5
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label lblCardID 
         Caption         =   "ID #:"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblFileName 
         AutoSize        =   -1  'True
         Caption         =   "File Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   2160
         Width           =   750
      End
      Begin VB.Label lblCardText 
         AutoSize        =   -1  'True
         Caption         =   "Card Text:"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblOther 
         AutoSize        =   -1  'True
         Caption         =   "Other:"
         Height          =   195
         Left            =   2400
         TabIndex        =   16
         Top             =   1440
         Width           =   435
      End
      Begin VB.Label lblType 
         AutoSize        =   -1  'True
         Caption         =   "Type:"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   1800
         Width           =   405
      End
      Begin VB.Label lblNumber 
         AutoSize        =   -1  'True
         Caption         =   "Quantity:"
         Height          =   195
         Left            =   2160
         TabIndex        =   14
         Top             =   960
         Width           =   630
      End
      Begin VB.Label lblName 
         AutoSize        =   -1  'True
         Caption         =   "Card Name:"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   840
      End
      Begin VB.Label lblSet 
         AutoSize        =   -1  'True
         Caption         =   "Set:"
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   285
      End
      Begin VB.Label lblRarity 
         AutoSize        =   -1  'True
         Caption         =   "Rarity:"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   1440
         Width           =   450
      End
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   8400
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "ViewCollectionADODC"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCardsCount 
      AutoSize        =   -1  'True
      Caption         =   "Export 0 card(s)."
      Height          =   195
      Left            =   11280
      TabIndex        =   38
      Top             =   120
      Width           =   1155
   End
   Begin VB.Image HLCard 
      Height          =   4695
      Left            =   7800
      Stretch         =   -1  'True
      Top             =   120
      Width           =   3375
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuExit 
         Caption         =   "Exit"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu menuDisplay 
      Caption         =   "Display"
      Begin VB.Menu menuShowAll 
         Caption         =   "Show All"
         Shortcut        =   ^A
      End
      Begin VB.Menu menuFind 
         Caption         =   "Find..."
         Shortcut        =   ^F
      End
   End
End
Attribute VB_Name = "frmDatabasePatchGenerator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'    Project:  Watcher Database HDB Generator
'    Created:  16 December 2003
'    Updated:  24 October 2007
'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Private Sub cmdGeneratePatch_Click()
' This function is responsible for exporting records from the cards.mdb file
'   into a .hdb file.
  CommonDialog2.ShowSave
  If CommonDialog2.filename <> "" Then
  datCardNames.Recordset.Filter = ""
    Open CommonDialog2.filename For Output As #1
      Dim j As Integer
      j = 0
      While (j < Int(lstCardstoPatch.ListCount))
        DisplayCardById (lstCardstoPatch.ItemData(j))
        If checkFileNames.Value = 1 Then ' If file names should be included in patch
          Write #1, txtID.Text, txtCardName.Text, txtSet.Text, txtType.Text, txtRarity.Text, Text1.Text, txtCardText.Text, txtFileName.Text
        Else ' default - don't include file names in patch
          Write #1, txtID.Text, txtCardName.Text, txtSet.Text, txtType.Text, txtRarity.Text, Text1.Text, txtCardText.Text, ""
        End If
        j = j + 1
      Wend
    Close #1
  End If
End Sub

Private Sub cmdMoveBack1Record_Click()
' Move backwards one record in the database.
  If datCardNames.Recordset.BOF = False Then
    If datCardNames.Recordset.EOF = False Then
      datCardNames.Recordset.Update
    End If
    datCardNames.Recordset.MovePrevious
  End If
End Sub

Private Sub cmdMoveForward1Record_Click()
' Move forward one record in the database.
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update
    End If
    datCardNames.Recordset.MoveNext
  End If
End Sub

Private Sub cmdSelectFile_Click()
' Select a new image to do display for the selected record.
  CommonDialog1.ShowOpen
  txtFileName.Text = CommonDialog1.filename
End Sub

Private Sub cmdShowFront_Click()
' Display the image for the front of the selected card.
  Call txtFileName_Change
End Sub

Private Sub cmdShowSet_Click()
' Show all cards in a specific expansion set.
  If (cmboSet.List(cmboSet.ListIndex) = "ALL") Then
    datCardNames.Recordset.Filter = ""
  Else
    datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "'"
  End If
End Sub

Private Sub cmdShowType_Click()
' Only display cards with a specific card type.
    datCardNames.Recordset.Filter = "Type = '" + cmboCardType.List(cmboCardType.ListIndex) + "'"
End Sub

Private Sub cmdSortName_Click()
' Sort all visible cards by card title.
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub cmdSortSet_Click()
' sort all visible cards by card set, then by title.
  datCardNames.Recordset.Sort = "Set, Name"
End Sub

Private Sub cmdSwitchImage_Click()
  On Error Resume Next
  HLCard.Stretch = True
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  filename = getImageLocation(txtFileName.Text, 2)
  If filename <> "" Then
    If Mid$(filename, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(filename)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.Text + "\" + filename)
    End If
  End If
End Sub

Private Sub cmdUpdatePatch_Click()
' Update the currently selected record, then add its ID number to the
'   list of cards to export.
  datCardNames.Recordset.Update
  lstCardstoPatch.AddItem txtCardName.Text
  lstCardstoPatch.ItemData(lstCardstoPatch.ListCount - 1) = txtID.Text
  lblCardsCount.Caption = "Export " & lstCardstoPatch.ListCount & " card(s)."
End Sub

Private Sub Form_Load()
' This function is called when the program is first started.
  On Error Resume Next
  
  ' Connect to the database.
  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh
  
  ' Display the image for the selected card.
  HLCard.Stretch = True
  If txtFileName.Text = "" Then
    HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  Else
    If Mid$(txtFileName.Text, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(txtFileName.Text)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.Text + "/" + txtFileName.Text)
    End If
  End If
  
  ' Hide all fields except card title.
  dataCards.Columns("ID").Visible = False
  dataCards.Columns("Set").Visible = False
  dataCards.Columns("Number").Visible = False
  dataCards.Columns("File Name").Visible = False
  dataCards.Columns("Other").Visible = False
  dataCards.Columns("Text").Visible = False
  dataCards.Columns("Type").Visible = False
  dataCards.Columns("Rarity").Visible = False
  
  ' Sort all cards by card title.
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub Form_Unload(Cancel As Integer)
' Update the database before quiting.
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update
    End If
  End If
End Sub

Private Sub lstCardstoPatch_Click()
' Display the selected card on the list of cards to export.
  Dim j As Integer
  j = 0
  While (j < Int(lstCardstoPatch.ListCount))
    If (lstCardstoPatch.Selected(j) = True) Then
      DisplayCardById (lstCardstoPatch.ItemData(j))
    End If
    j = j + 1
  Wend
End Sub

Public Sub DisplayCardById(idToDisplay)
' Display a specific card in the database who's ID number is idToDisplay.
  datCardNames.Recordset.MoveFirst
  datCardNames.Recordset.Find ("Id = '" & idToDisplay & "'")
End Sub

Private Sub menuExit_Click()
' Save any changes to the database, then quit.
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update
    End If
  End If
  frmViewCollection.Visible = False
  Unload frmViewCollection
End Sub

Private Sub menuFind_Click()
' Search the database by card title.
  On Error Resume Next
  Dim searchString As String
  datCardNames.Recordset.Filter = ""
  
  ' Prompt the user for a title to search for.
  searchString = InputBox("Enter All or part of the title to search for:", "Find Card(s)")
  
  ' Make sure a value was given.
  If searchString <> "" Then
  
    ' replace any occurrences of "'" with "''"
    For i = 1 To Len(searchString)
      If (Mid$(searchString, i, 1) = "'") And (Mid$(searchString, i + 1, 1) <> "'") Then
        searchString = Left$(searchString, i) + "'" + Right$(searchString, Len(searchString) - i)
        i = i + 1
      End If
    Next i
    
    ' Limit results to a specific expansion set, if one is selected.
    If (cmboSet.ListIndex < 1) Then
      datCardNames.Recordset.Filter = "Name LIKE '%" + searchString + "%'"
    Else
      datCardNames.Recordset.Filter = "Set = '" + cmboSet.List(cmboSet.ListIndex) + "' AND " + "Name LIKE '%" + searchString + "%'"
    End If
  End If
End Sub

Private Sub menuShowAll_Click()
' Remove any filters, and display all cards in the database.
  datCardNames.Recordset.Filter = adFilterNone
End Sub

Private Sub txtFileName_Change()
' update the card image when the value of txtFileName is changed.
  On Error Resume Next
  Dim filename As Variant
  HLCard.Stretch = True
  HLCard.Picture = LoadPicture(App.Path + "\empty.jpg")
  filename = getImageLocation(txtFileName.Text, 1)
  If filename <> "" Then
    If Mid$(filename, 2, 1) = ":" Then
      HLCard.Picture = LoadPicture(filename)
    Else
      HLCard.Picture = LoadPicture(App.Path + "\" + txtSet.Text + "\" + filename)
    End If
  End If
End Sub

Private Sub txtSet_Change()
' Update the card image when teh value of txtSet is changed.
  Call txtFileName_Change
End Sub

Public Function getImageLocation(txtFileNames As String, nameToRetrive As Integer)
' Extract the file name for either the front or the back of the card from txtFileNames,
'   based on the value of nameToRetrive.
'   nameToRetrive = 1 - Front, 2 - Back
'   Return "" if there isn't a file name.
  Dim counter As Long
  If (Left$(txtFileNames, 1) = "*") Then
    counter = 1
    While Mid$(txtFileNames, counter, 1) <> "|"
      counter = counter + 1
    Wend
    If nameToRetrive = 1 Then
      getImageLocation = Mid$(txtFileNames, 2, counter - 2)
    Else
      getImageLocation = Mid$(txtFileNames, counter + 1, Len(txtFileNames) - counter)
    End If
  Else
    If nameToRetrive = 1 Then
      getImageLocation = txtFileNames
    Else
      getImageLocation = ""
    End If
  End If
End Function
