# Watcher Database Documentation
![Watcher Database](./screenshots/mainscreen.jpg) 

## Copyright Notice
Copyright 2006 rWatcher.

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is included in the section entitled "GNU Free Documentation License".

This license may also be found online at: http://www.gnu.org/licenses/fdl-1.2.txt


## Installation
**Warning:  If you already have the program installed, running the installer again may erase any file names and card quantities that were entered into the program.  This information is stored in the cards.mdb file, which will be overwritten by the installer.  If you've already installed the program, please see Upgrade Instructions for information about updating the software.**

 - First, download the full Watcher Database Installer from [WatcherDatabase.Tk](http://www.watcherdatabase.tk) (the Highlander.zip file)

![rW-Download Link](./screenshots/tcgfullinstaller.jpg)

 - Extract the Highlander.msi file from the .zip file.  Double click on the .msi file to start the installer.  If you double click on the .msi file from within a file decompression program, some programs such as WinZip and WinRar will attempt to open the .msi file as something other then an installer.

![Installer Icon](./screenshots/installericon.jpg)

 - When the installer launches, you will be presented with the following screen.  Press Next to continue.

![Welcome Screen](./screenshots/Welcome.jpg)

 - You will then be presented with a screen asking you to select the location you would like the program to be installed into.  Either use the default location, type in a directory path into the folder field, or use the "Browse..." button to select a location to install the program into.  After you've chosen a location, press Next.
 
![Select Install Folder](./screenshots/SelectInstallFolder.jpg)

 - You will then be presented with the following screen.  Press next to start the installation.
 
![Start Install](./screenshots/StartInstall.jpg)

 - The installer will now install and configure the program.  When it finishes, you will see the following screen.

![Install Complete](./screenshots/InstallComplete.jpg)

 - Once the installer finishes, a shortcut will be placed on your desktop and start menu.  You may start up the program by clicking on either one of them.
 
![Desktop Icon](./screenshots/WatcherDBDesktopIcon.jpg) ![Start Menu Icon](./screenshots/WatcherDBStartMenu.jpg)

 - If you see the main screen, the the program has installed successfully.

![Main Screen](./screenshots/mainscreen.jpg)


## Upgrade Instructions
### How to upgrade Highlander.exe

 - To determine if your version is out of date, click on the About button on the main screen.

![Main Screen](./screenshots/mainscreen.jpg)

 - On the about screen, it will list a version number.

![About Screen](./screenshots/About.jpg)

 - Then, go to [WatcherDatabase.Tk](http://www.watcherdatabase.tk).
 - Next to the download link for the file Highlanderexe.zip, a version number will be listed.
 
![Upgrade Version](./screenshots/tcgupgrade.jpg)

 - If your version number is lower then the one listed on the site, you can upgrade.  Download the Highlanderexe.zip file.
 - Extract the Highlander.exe file from the .zip file and place it into the directory the program was installed into (by default the program is installed into C:\Program Files\Highlander or C:\Program Files\Watcher Database).  Overwrite the older highlander.exe file with this newer one.

### How to use a DB Patch to add in additional cards
**It is recommended that you make a backup of your cards.mdb file before using any DB Patches.**

 - There are two different kinds of patches, .exe files and .hdb files.
 - .exe files are rarely used.  To install one, extract the .exe file into the directory the program was installed into (the default location is c:\Program Files\Highlander or C:\Program Files\Watcher Database) and double click on the icon.  Follow the on-screen instructions.
 - For .hdb patches, download the .zip file and extract the .hdb file to any location on your computer.
 - Open the Watcher Database program, and from the main screen, select "Apply DB Patch (.hdb file)".
 
![Main Screen](./screenshots/mainscreen.jpg)

 - From this screen, select the patch database button.  Then select the .hdb file in the window that opens up.  The program will now import the information from the .hdb file.
 
![Patch Database Screen](./screenshots/patchdatabase.jpg)

 - When it finishes, a message will be displayed.  You may now exit out of this screen and delete the .hdb file


## Troubleshooting
 - First, make sure you are using the current version of the Highlander.exe file.  See Upgrade Instructions for more information.
 - If you have any DLL errors visit [Microsoft's Web Page](http://www.microsoft.com/downloads/release.asp?ReleaseID=12704) to download the Microsoft Visual Basic 6.0 Runtime files necessary to run this program.
 - If you get any of the following errors:
 
   [ADODC]: Unknown error. [ADO]:
   
   ActiveX Component can't create object.
   
   You will need to download the Microsoft Data Access Components.  These are available as a free download from [Microsoft](http://www.microsoft.com/data/download_25SP1.htm).  Make sure you download a version that is compatible with your operating system.
 - If the program starts up, but card titles are not displayed:  You will need to download the Microsoft Data Access Components.  These are available as a free download from [Microsoft](http://www.microsoft.com/data/download_25SP1.htm).  Make sure you download a version that is compatible with your operating system.
 
![Deck Editor Screen](./screenshots/DeckEditorNoCards.jpg) 

 - If your computer does not support .msi files, you can download an update from microsoft.com.
   - [Windows NT 4.0 and 2000 Users Click here](http://www.microsoft.com/downloads/release.asp?releaseid=32832&NewList=1)
   - [Windows 95, 98, and Me Users Click here](http://www.microsoft.com/downloads/release.asp?ReleaseID=32831)
 - If you get a "Could not find file 'cards.mdb'." message, then the file "cards.mdb" is missing from your computer.  This usually occurs when someone downloads the upgrade, and not the full installer.  Please see Installation for more information on installing the software.
 
![Missing cards.mdb file](./screenshots/missingcardsfile.jpg) 

 - If you receive "Run-time error '91'", this usually occurs when the program has not been properly installed.  Please see Installation for more information on installing the software.

![Error 91](./screenshots/Error91.jpg) 

 - Blank Card titles on a list:  This is caused when you attempt to open a file that references cards that are not in your database (usually when the file was created on a different computer).  Please see Upgrade Instructions to install the latest database patches in order to import the missing cards.

![Error 91](./screenshots/MissingCardError.jpg) 


## Screens

### View Collection
![View Collection](./screenshots/ViewCollection.jpg) 

#### Summary
 - The first third of the window contains a list of card names.
 - The Middle Contains detailed information about the current selected card.
 - The Last Third of the window contains a picture (If Available) of the card.

#### Image File Names
There are several ways to assign a image file name to a card.  First select the card, and then either:
 - Manually enter the file name (and path, if required) for the file into the "File Name" field.
 - Press the "Select File" button, and locate the image of the card.
 - Drag and drop the file over the right area of the window where card images are displayed ( [Example](http://youtube.com/watch?v=Pa8cS-SJ-LE) ).

*Note:*  If a file name (such as ConnorPersonaFront.jpg) is entered, without a file path (such as c:\highlander\) before the file name, the software will look in a default directory for the file.  This is a folder in the application directory, named after the set.  
Example:

If the program is installed into "C:\Program Files\Highlander", the default location for Series Edition cards is "C:\Program Files\Highlander\se".

*Associating Two Images with one card*

The syntax for assigning two images (a card front and back) to one card is: *Front.jpg|Back.jpg

Either absolute file paths or default expansion set paths may be used.

Example 1:  *c:\highlandercards\ConnorPersonaFront.jpg|c:\highlandercards\ConnorPersonaBack.jpg

Example 2:  *ConnorPersonaFront.jpg|ConnorPersonaBack.jpg

#### Menu Options
**The File Menu:**

![View Collection File Menu](./screenshots/ViewCollectionFile.jpg) 

All Save Options will only draw card names from those currently being displayed by the program.  If you want to generate a "Wants List" that contains cards that are missing from a specific set, select that set, then press the save button.
 - **Save Wants List**:  Generates a CSV file of cards that are displayed in the list that have a quantity of 0.
 - **Save Haves list**:  Generates a CSV file of cards that are displayed in the list that have a quantity of 2 or more.
 - **Save Collection List**:  Generates a CSV file of cards that are displayed in teh list that have a quantity of 1 or more.
 - **Create Trade List**:  Generates a .hlt file of all cards currently being displayed.
 - **Exit**:  Exits out of the View Collection Screen.  Does not quit the program.

**The Display Menu**:

![View Collection Display Menu](./screenshots/ViewCollectionDisplay.jpg) 

 - **Show Haves**:  Applies a filter to the database for cards that have a quantity of 2 or more.
 - **Show Collection**:  Applies a filter to the database for cards that have a quantity of 1 or more.
 - **Show Wants**:  Applies a filter to the database for cards that have a quantity of 0.
 - **Show Haves and Wants**:  Applies a filter to the database for cards that do not have a quantity of 1.
 - **Show All**:  Displays all cards in the database.  Removes any filters.
 - **Find...**:  Allows you to filter the database for card titles that contain specific words.  if "Duncan" is typed in, every card with the word duncan will be displayed.

**Other Options:**

The Buttons along the bottom of the window:
 - **Previous Card**:  Moves Up one card
 - **Next Card**:  Moves Down one card
 - **Sort By Name**:  Sorts the cards by their title
 - **Sort By Set**:  Sorts the cards by Set, then by Name
 - **Set Names/Select Set**:  Set Names contains a list of abbreviations for the various sets.  By pressing the Select Set Button, only cards in the selected set will be displayed.
 - **Show Front**:  Displays the image for the front of the card.
 - **Show Back**:  Displays the image for the back of the card.


### View Trade list
![Trade List](./screenshots/TradeList.jpg) 

#### Summary:
 - The list on the left side of the screen is the entire trade list file.  The numbers listed before the card title are the number of cards that were listed in the trade list file.
 - The Middle list is a list of cards that were on the list, that are in your collection.  The numbers listed before the card names are the number of cards that you have.
 - The list on the right side of the window is cards that are on the trade list that you are missing.  The number before the card name is the number from the trade list.
 - Details about a card, such as text and a picture, are displayed below the right two lists when a card title is selected.

#### Menu Options:
**File Menu**:

![Trade List Menu](./screenshots/TradeListMenu.jpg) 

 - **Open Trade List (.hlt)**:  This option will load a trade list file (.hlt).  These files are created from within the view collection area of the program.
 - **Exit**:  This will exit out of the Display Trade area of the program.  It will not completely exit out of the program.


### Print MLE Cards
![Print MLE Cards Screen](./screenshots/PrintMLECardsScreen.jpg) 

#### Summary:
 - This screen is used for printing MLE cards.  Up to 8 cards can fit on a page.
 - The Left side of the screen contains a list of every card.
 - The list of cards to be printed is located in the middle of the screen, along with buttons to add and remove cards from the list, and a Print button.  A counter is located above the card list, telling you how many cards are to be printed and how many pages this will take.
 - The Right side of the screen displays a picture of the currently selected card.

#### Menu Options:
**File Menu**:

![Print MLE Cards File Menu](./screenshots/PrintMLECardsFileMenu.jpg) 

 - **Find...**:  Allows you to search for card titles that contain specific words.
 - **Exit**:  Quits out of the Print MLE Cards screen.
 
#### Other Options:

 - **Only show cards with images**:  Limit the list of cards to those with images associated with them.
 - **Previous Card**:  Move to the previous card on the list.
 - **Next Card**:  Move to the next card on the list.
 - **Sort By Name**:  Sort the list of all available cards by card title.
 - **Sort By Set**:  Sort the list of all available cards by card set.
 - **Add Front >>**:  Add the Front of the currently selected card to the list of cards to print.
 - **Add Back >>**:  Add the back of the currently selected card to the list of cards to print.
 - **<< Delete**:  Removes a card from the list of cards to print.  Select that card on the middle list before pressing.
 - **Print**:  Prints all cards on the middle list.
 - **Clear List**:  Clear all cards from the middle list.
 - **Select Set**:  Limit the list of all available cards to a specific set.
 - **Show Front**:  Display the front of the currently selected card.
 - **Show Back**:  Display the back of the currently selected card.


### Deck Editor
![Deck Editor](./screenshots/DeckEditor.jpg) 

#### Summary:
 - This screen is used for creating and managing Highlander decks.
 - The left side of the screen contains a list of all available cards along with options to filter that list to locate specific cards.  Double click on a card on this list to add it to the deck.
 - The center of the screen contains details of the currently selected card, as well as a picture of it (if available).
 - The right side of the screen contains lists of cards in the deck and pre-game.

#### Menu Options:
**File Menu**:

![Deck Editor File Menu](./screenshots/DeckEditorFileMenu.jpg) 

 - **Open Deck**:  Open a saved deck (.hld) file.
 - **Save Deck As...**:  Save the current deck as a new file (.hld).
 - **Save Deck**:  Save the current deck (either to the current file, or to a new file if the deck has never been saved before).
 - **New Deck**:  Create a new deck (make sure to save the current deck, or changes will be lost).
 - **Export Deck**:  Export the current deck to a .csv file (can be opened in most spreadsheet programs).
 - **Print Deck**:  Print the current deck.
 - **Load Sealed Deck**:  Load a Sealed Deck (.hsd) to use to make a playable deck.
 - **Exit Deck Manager**:  Quit out of the Deck Editor (make sure to save your current deck first, or changes will be lost).

**Options Menu**:

![Deck Editor Options Menu](./screenshots/DeckEditorOptionsMenu.jpg) 

 - **Show Deck Stats**:  Generate statistics for the deck (more information below).
 - **Sort Deck By Name**:  Sort the deck list by card title.
 - **Sort Deck By Type**:  Sort the deck list by card type.
 - **Sort By Type and Name**:  Sort the deck list by card type, sort within each type by card title.
 - **Switch to Sealed Deck Generator**:  Make a new Sealed Deck (.hsd) file (more details below).

#### Other Options:
 - **Add To Deck**:  Add one copy of the currently selected card to the deck.
 - **Add To Pre-Game**:  Add one copy of the currently selected card to the Pre-Game list.
 - **Show All**:  Clear out any search / filter in order to display every possible card on the "All Cards" list.
 - **Sort By Set**:  Sort the "All Cards" list by Set.
 - **Sort By Name**:  Sort the "All Cards" list by card title.
 - **Set Names / Select Set**:  Only display cards from the selected set.
 - **Card Type / Select Type**:  Only display cards of the selected card type.
 - **Card Title / Find**:  Only display cards who's title contain specific words.
 - **Text Search / Find**:  Only display cards who's text contains specific words.
 - **Classification / Display**:  Only display cards with a specific classification (*Note:  Most cards don't have classifications associated with them, so this has limited usefulness*).
 - **Show Front**:  Display an image of the front of the currently selected card (if there is one).
 - **Show Back**:  Display an image of the back of the currently selected card (if there is one).
 - **Delete 1 Card**:  Delete one copy of the selected card from the list.
 - **Add Another Card**:  Add another copy of the selected card to the list.

#### Deck Statistics:

![Deck Editor Deck Statistics Screen](./screenshots/DeckEditorDeckStats.jpg) 

This screen is accessed via the "Options" menu, or by pressing "Control-I".  It displays information about the deck, such as how many attack and blocks there are, or what percentage of the deck is made up of MLE cards.

**Available Buttons**:
 - **Hide Deck Stats**:  Hides the Deck Stats Panel.
 - **Save Stats to Text File**:  Saves the information on the Deck Stats panel to a text file.
 - **Print Deck Stats**:  Prints the information on the Deck Stats panel.

#### Sealed Decks

**Generating a Sealed Deck**:

![Deck Editor Sealed Deck Generator](./screenshots/DeckEditorSealedDeckGeneration.jpg) 

The sealed deck generator can be accessed from the Options menu.  This screen will allow one to create a sealed deck (.hsd) file, which can then be used on the main deck editor screen to build a regular deck (.hld) file from.  
 - The left side of the screen allows you to specify how many of each rarity to include in the deck.
 - The middle of the screen contains information on the currently selected card.
 - The right side of the screen contains a list of all the cards currently in the sealed deck.

**Create Sealed Deck Panel**:
 - The fields along the top of the panel allow you to specify how many of each card to include in the sealed deck.
 - The "Create Booster Pack" and "Create Starter Deck" will set specific values for how many of each rarity to include.
 - The sets drop-down box will allow you to specify which set to use when created the starter deck / booster pack.
 - The "Add Pack / Deck to List" button will randomly select the listed number of cards from each rarity and add them to the Sealed Deck list.  More then one Starter Deck / Booster Pack may be added to the list.
 - The "Exit Sealed Deck Mode" will exit out of the Sealed Deck Generator screen.  make sure to save your sealed deck before exiting, or it will be lost.

**Card Details**:

This is the standard "Card Details" panel found on various screens throughout the program.  It displays information on the currently selected card, and has buttons on the bottom of the panel to display images of the front or back of the card.

**Sealed Deck**:

The right third of the screen displays all the cards currently in the sealed deck.
 - The top of this panel tells you the number of cards.
 - The "Save Sealed Deck" button at the bottom of the panel will save the above list as a .hsd file for use on the main deck editor screen.

#### Using a Sealed Deck File (.hsd)

![Deck Editor Sealed Deck](./screenshots/DeckEditorUsingSealedDeck.jpg) 

This screen will allow you to create a deck (.hld) file from a sealed deck (.hsd) file.  To access this screen, open a sealed deck (.hsd file) by selected File -> Load Sealed Deck, or by pressing "Control-D".

This screen works just like the other deck editor screen.  Double click on a card to add it to the deck or pre-game slot (Persona and Pre-Game cards will automatically be added to Pre-Game, everything else will automatically be added to the deck) or use the Add To Deck / Add To Pre-Game buttons.

The Delete 1 Card and Add Another Card options are available for adding and removing cards from the Deck and Pre-Game list.

**Note:**  You will only be able to add cards up to the available quantity.

When you're done here, save the deck and open it on either of the play game screens to start playing a game with your new deck.


### Play Game (Email)
![Play Game Email](./screenshots/PlayGameEmail.jpg) 

#### Summary
This area of the program can be used to keep track of a game being played through email, instant messaging, chat rooms, etc.
 - The Right side of the screen contains information on the last card that was selected.
 - The Left side of the screen contains lists for all the cards being used.  The top half represents your opponents cards, the bottom half your cards.  Each list has a few buttons below it, which represent actions that can be performed on the cards in that list (such as discarding a card from your hand).
 
**Selecting Multiple Cards:**  

More then one card may be selected before pressing a button (shift-click on two cards to select those and everything in between, control-click on two or more cards to just select those, left click on the first card and drag down to select multiple cards in a row).

**Moving and Resizing Card Lists:** 

The frames surrounding the lists and their buttons may be moved and resized ( [Video Example](http://www.youtube.com/watch?v=bnZELxB0tD8) ).  Enlarge the window to fill your screen, then Left click on a frame and drag it to an empty spot to move one (the mouse needs to be over an empty spot on the window, the rest of the frame can overlap with other item though).  Frames may also be resized by shift-clicking on the lower right corner and dragging the frame to resize.  New sizes and locations will automatically be saved when you exit from this screen.

**Double Clicking on a Card:**

Some card lists will allow you to double click to perform the most common action.  For example:  Double click on a card in your hand to play that card, or double click on a card in play to discard it.  If a list doesn't have a default action associated with it, nothing will happen when double clicking a card.

#### Menu Options

**File Menu:**

![Play Game Email File Menu](./screenshots/PlayGameEmailFile.jpg) 

 - **New Game:**  Select a deck (.hld file) to start a new game with.
 - **Open Saved Game:**  Open a saved game (.hlg).
 - **Save Game As...**:  Save the current game in a new file.
 - **Save Game**:  Save the current game in the existing file (or specify a new file name if this is the first time the game has been saved).
 - **Exit**:  Quit from this screen.  Make sure to save the game and (optionally) the turn log beforehand or changes will be lost.
 
**Show/Hide Menu:**

![Play Game Email Show Hide Menu](./screenshots/PlayGameEmailShowHide.jpg) 

This menu contains options to either display or remove specific information from your screen (such as your deck or pre-game cards).  These additional frames may be moved and resized.  Their status as either Visible or Hidden will be automatically saved when you exit from this screen.

**Sort Hand:**

![Play Game Email Sort Hand Menu](./screenshots/PlayGameEmailSortHand.jpg) 

 - **By Name:**  Sort the cards in your hand by Title.
 - **By Type:**  Sort the cards in your hand by Type.
 - **By Type and Name:**  Sort the cards in your hand by Type, then by Title.

#### Other Options

 - **Show/Hide Sort/Filter Options:**  Shows or hides the below frame, which can be used to limit the cards displayed in the "All Cards" list by searching for cards in a specific set or with a specific title.  Can also sort the list by Set or Title.
 
![Play Game Email Filters](./screenshots/PlayGameEmailFilterOptions.jpg) 


### Play Game (Online)
![Play Game Online](./screenshots/PlayGameOnline.jpg) 

#### Summary
This area of the program can be used to play a game over a local network or the Internet.
 - The Right side of the screen contains information on the last card that was selected.
 - The Left side of the screen contains lists for all the cards being used.  The top half represents your opponents cards, the bottom half your cards.  Each of your lists has a few buttons below it, which represent actions that can be performed on the cards in that list (such as discarding a card from your hand).
 
**Selecting Multiple Cards:**  

More then one card may be selected before pressing a button (shift-click on two cards to select those and everything in between, control-click on two or more cards to just select those, left click on the first card and drag down to select multiple cards in a row).

**Double Clicking on a Card:**

Some card lists will allow you to double click to perform the most common action.  For example:  Double click on a card in your hand to play that card, or double click on a card in play to discard it.  If a list doesn't have a default action associated with it, nothing will happen when double clicking a card.

#### Menu Options

**File Menu:**

![Play Game Online File Menu](./screenshots/PlayGameOnlineFileMenu.jpg) 

 - **New Game:**  Select a Deck (.hld) to use to start a new game.
 - **Open Saved Game:**  Open a saved game (.hlg file) from the Play Game (Email / Chat Room) screen.
 - **Close Connection:**  Close an open Internet / Network connection.  Will end the current game.
 - **Exit:**  Exit from this screen.  Will end the current game.

**Show/Hide Menu:**

![Play Game Online Show Hide Menu](./screenshots/PlayGameOnlineShowHideMenu.jpg) 

This menu contains options to either display or remove specific information from your screen (such as your deck or pre-game cards).

**Show Cards To Opponent Menu:**

![Play Game Online Show Cards To Opponent Menu](./screenshots/PlayGameOnlineShowCardsOpponent.jpg) 

This menu contains options to show cards in your hand or deck to your opponent.

**Sort Hand Menu:**

![Play Game Online Sort Hand Menu](./screenshots/PlayGameOnlineSortHandMenu.jpg) 

 - **By Name:**  Sort the cards in your hand by Title.
 - **By Type:**  Sort the cards in your hand by Type.
 - **By Type and Name:**  Sort the cards in your hand by Type, then by Title.

#### Other Options
 - **Turn Number:**  Located at the Lower Right corner of the screen, this box allows you to keep track of your current turn number.  Whatever is entered in the box will automatically show up in the corresponding box at the top of your opponents screen.
 - **Ability:**  Located at the Lower Right corner of the screen, this box allows you to keep track of your ability.  Whatever is entered in the box will automatically show up in the corresponding box at the top of your opponents screen.
 - **Message To Send / Send Button:**  Allows you to send text messages to your opponent.
 - **Chat Box:**  Located at the bottom of the screen, this box displays any text messages your opponent sends you.  It will also display messages to inform you of your opponents other activities, such as dice rolls, or when they draw cards.

#### Setting Up and Playing a Game

When you first select the "Play Game (Online)" option, you will be presented with an empty window.  Either select New Game (Control-N) or Open Saved Game (Control-O) to begin playing an online game.  After the file (either a deck/.hld or saved game/.hlg) is loaded, you will see the following screen.

![Play Game Online Connect](./screenshots/PlayGameOnlineConnect.jpg) 

At this point, one player must first enter their name and press the "Listen" button.  Once the Listen button is pressed, the program will attempt to determine that players IP address and display it in the "IP Address" field.  If you are on a network, this will usually be your address on the network and not on the Internet.  In this case you will need to find an alternate way to determine you Internet IP address if you wish to play a game with someone who is not on your network.

Once the Listen button is pressed, the buttons will be grayed out and the program will wait for the other player to press "Connect".  The second player must then enter the first players IP address into their IP Address field (either their address on a local network, if both players are on the same network, or the first players IP address on the internet), and the second players name into the name field.  They can then press "Connect" which will cause they program to establish a connection with the first players computer.

If one player is behind a firewall, it is recommended that the player not behind a firewall press the "Listen" button.  If both players are behind firewalls, you may need to customize the settings to get the program to connect properly.  This program uses Port 1234 to establish a connection, so make sure you are able to make and receive connections over that port on your firewalls configuration screen.

![Player 1](./screenshots/PlayGameOnlineWaitingForConnection.jpg) ![Player 2](./screenshots/PlayGameOnlineMakingConnection.jpg) 

If the connection was established properly, you should now see the main play game screen, and should be able to draw and play cards as normal.  When one player closes the window, presses the close connection button, or otherwise exits from this screen, the other player will see a connection closed message.

![Play Game Online Connection Closed](./screenshots/PlayGameOnlineConnectionClosed.jpg) 


### Apply Database Patch
![Apply Database Patch](./screenshots/Patchdb.jpg) 

 - The left side of the screen contains a list of cards currently in the database.
 - The top right side contains some basic instructions for applying a database patch.
 - The middle right side of the screen contains buttons to apply a database patch and to exit out of the patch database area.

 When the "Patch Database" Button is pressed, A window will open up asking you to select a DB patch to use.  Select the .hdb file you wish to import into the database, and press open.  The program will then begin importing the data, displaying a list of what it is doing in the box in the lower right side of the window.  When it finishes, another box will open up telling you so.  The program may appear to stop responding while it patching the database.


## File Types
 - **.hdb**:  Database Patch, contains database entries to be imported into the database using the "Apply DB Patch" Button.
 - **.hlt**:  Trade list, contains a haves/wants list. Created using the "View Collection" Button, Viewed using the "View Trade List" button.
 - **.hlg**:  Saved Game, contains a saved game used by either the "Play Game by Email/Chat Room" button or the "Play Game Online" button.
 - **.hld**:  Deck List, contains a list of cards that are in a saved deck. Created with the "Create/Edit Deck" button, used to start new games.
 - **.csv**:  Comma Delimited, a list of cards that can be imported into most spreadsheet programs.
 - **.txt**:  Text File, used for logs.
 - **.hsd**:  Sealed Deck File.  Created by the Deck Editor, can be used in the deck editor to create a playable deck (.hld) file.


## GNU Free Documentation License
GNU Free Documentation License
Version 1.2, November 2002


 Copyright (C) 2000,2001,2002  Free Software Foundation, Inc.
     51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


0. PREAMBLE

The purpose of this License is to make a manual, textbook, or other
functional and useful document "free" in the sense of freedom: to
assure everyone the effective freedom to copy and redistribute it,
with or without modifying it, either commercially or noncommercially.
Secondarily, this License preserves for the author and publisher a way
to get credit for their work, while not being considered responsible
for modifications made by others.

This License is a kind of "copyleft", which means that derivative
works of the document must themselves be free in the same sense.  It
complements the GNU General Public License, which is a copyleft
license designed for free software.

We have designed this License in order to use it for manuals for free
software, because free software needs free documentation: a free
program should come with manuals providing the same freedoms that the
software does.  But this License is not limited to software manuals;
it can be used for any textual work, regardless of subject matter or
whether it is published as a printed book.  We recommend this License
principally for works whose purpose is instruction or reference.


1. APPLICABILITY AND DEFINITIONS

This License applies to any manual or other work, in any medium, that
contains a notice placed by the copyright holder saying it can be
distributed under the terms of this License.  Such a notice grants a
world-wide, royalty-free license, unlimited in duration, to use that
work under the conditions stated herein.  The "Document", below,
refers to any such manual or work.  Any member of the public is a
licensee, and is addressed as "you".  You accept the license if you
copy, modify or distribute the work in a way requiring permission
under copyright law.

A "Modified Version" of the Document means any work containing the
Document or a portion of it, either copied verbatim, or with
modifications and/or translated into another language.

A "Secondary Section" is a named appendix or a front-matter section of
the Document that deals exclusively with the relationship of the
publishers or authors of the Document to the Document's overall subject
(or to related matters) and contains nothing that could fall directly
within that overall subject.  (Thus, if the Document is in part a
textbook of mathematics, a Secondary Section may not explain any
mathematics.)  The relationship could be a matter of historical
connection with the subject or with related matters, or of legal,
commercial, philosophical, ethical or political position regarding
them.

The "Invariant Sections" are certain Secondary Sections whose titles
are designated, as being those of Invariant Sections, in the notice
that says that the Document is released under this License.  If a
section does not fit the above definition of Secondary then it is not
allowed to be designated as Invariant.  The Document may contain zero
Invariant Sections.  If the Document does not identify any Invariant
Sections then there are none.

The "Cover Texts" are certain short passages of text that are listed,
as Front-Cover Texts or Back-Cover Texts, in the notice that says that
the Document is released under this License.  A Front-Cover Text may
be at most 5 words, and a Back-Cover Text may be at most 25 words.

A "Transparent" copy of the Document means a machine-readable copy,
represented in a format whose specification is available to the
general public, that is suitable for revising the document
straightforwardly with generic text editors or (for images composed of
pixels) generic paint programs or (for drawings) some widely available
drawing editor, and that is suitable for input to text formatters or
for automatic translation to a variety of formats suitable for input
to text formatters.  A copy made in an otherwise Transparent file
format whose markup, or absence of markup, has been arranged to thwart
or discourage subsequent modification by readers is not Transparent.
An image format is not Transparent if used for any substantial amount
of text.  A copy that is not "Transparent" is called "Opaque".

Examples of suitable formats for Transparent copies include plain
ASCII without markup, Texinfo input format, LaTeX input format, SGML
or XML using a publicly available DTD, and standard-conforming simple
HTML, PostScript or PDF designed for human modification.  Examples of
transparent image formats include PNG, XCF and JPG.  Opaque formats
include proprietary formats that can be read and edited only by
proprietary word processors, SGML or XML for which the DTD and/or
processing tools are not generally available, and the
machine-generated HTML, PostScript or PDF produced by some word
processors for output purposes only.

The "Title Page" means, for a printed book, the title page itself,
plus such following pages as are needed to hold, legibly, the material
this License requires to appear in the title page.  For works in
formats which do not have any title page as such, "Title Page" means
the text near the most prominent appearance of the work's title,
preceding the beginning of the body of the text.

A section "Entitled XYZ" means a named subunit of the Document whose
title either is precisely XYZ or contains XYZ in parentheses following
text that translates XYZ in another language.  (Here XYZ stands for a
specific section name mentioned below, such as "Acknowledgements",
"Dedications", "Endorsements", or "History".)  To "Preserve the Title"
of such a section when you modify the Document means that it remains a
section "Entitled XYZ" according to this definition.

The Document may include Warranty Disclaimers next to the notice which
states that this License applies to the Document.  These Warranty
Disclaimers are considered to be included by reference in this
License, but only as regards disclaiming warranties: any other
implication that these Warranty Disclaimers may have is void and has
no effect on the meaning of this License.


2. VERBATIM COPYING

You may copy and distribute the Document in any medium, either
commercially or noncommercially, provided that this License, the
copyright notices, and the license notice saying this License applies
to the Document are reproduced in all copies, and that you add no other
conditions whatsoever to those of this License.  You may not use
technical measures to obstruct or control the reading or further
copying of the copies you make or distribute.  However, you may accept
compensation in exchange for copies.  If you distribute a large enough
number of copies you must also follow the conditions in section 3.

You may also lend copies, under the same conditions stated above, and
you may publicly display copies.


3. COPYING IN QUANTITY

If you publish printed copies (or copies in media that commonly have
printed covers) of the Document, numbering more than 100, and the
Document's license notice requires Cover Texts, you must enclose the
copies in covers that carry, clearly and legibly, all these Cover
Texts: Front-Cover Texts on the front cover, and Back-Cover Texts on
the back cover.  Both covers must also clearly and legibly identify
you as the publisher of these copies.  The front cover must present
the full title with all words of the title equally prominent and
visible.  You may add other material on the covers in addition.
Copying with changes limited to the covers, as long as they preserve
the title of the Document and satisfy these conditions, can be treated
as verbatim copying in other respects.

If the required texts for either cover are too voluminous to fit
legibly, you should put the first ones listed (as many as fit
reasonably) on the actual cover, and continue the rest onto adjacent
pages.

If you publish or distribute Opaque copies of the Document numbering
more than 100, you must either include a machine-readable Transparent
copy along with each Opaque copy, or state in or with each Opaque copy
a computer-network location from which the general network-using
public has access to download using public-standard network protocols
a complete Transparent copy of the Document, free of added material.
If you use the latter option, you must take reasonably prudent steps,
when you begin distribution of Opaque copies in quantity, to ensure
that this Transparent copy will remain thus accessible at the stated
location until at least one year after the last time you distribute an
Opaque copy (directly or through your agents or retailers) of that
edition to the public.

It is requested, but not required, that you contact the authors of the
Document well before redistributing any large number of copies, to give
them a chance to provide you with an updated version of the Document.


4. MODIFICATIONS

You may copy and distribute a Modified Version of the Document under
the conditions of sections 2 and 3 above, provided that you release
the Modified Version under precisely this License, with the Modified
Version filling the role of the Document, thus licensing distribution
and modification of the Modified Version to whoever possesses a copy
of it.  In addition, you must do these things in the Modified Version:

A. Use in the Title Page (and on the covers, if any) a title distinct
   from that of the Document, and from those of previous versions
   (which should, if there were any, be listed in the History section
   of the Document).  You may use the same title as a previous version
   if the original publisher of that version gives permission.
B. List on the Title Page, as authors, one or more persons or entities
   responsible for authorship of the modifications in the Modified
   Version, together with at least five of the principal authors of the
   Document (all of its principal authors, if it has fewer than five),
   unless they release you from this requirement.
C. State on the Title page the name of the publisher of the
   Modified Version, as the publisher.
D. Preserve all the copyright notices of the Document.
E. Add an appropriate copyright notice for your modifications
   adjacent to the other copyright notices.
F. Include, immediately after the copyright notices, a license notice
   giving the public permission to use the Modified Version under the
   terms of this License, in the form shown in the Addendum below.
G. Preserve in that license notice the full lists of Invariant Sections
   and required Cover Texts given in the Document's license notice.
H. Include an unaltered copy of this License.
I. Preserve the section Entitled "History", Preserve its Title, and add
   to it an item stating at least the title, year, new authors, and
   publisher of the Modified Version as given on the Title Page.  If
   there is no section Entitled "History" in the Document, create one
   stating the title, year, authors, and publisher of the Document as
   given on its Title Page, then add an item describing the Modified
   Version as stated in the previous sentence.
J. Preserve the network location, if any, given in the Document for
   public access to a Transparent copy of the Document, and likewise
   the network locations given in the Document for previous versions
   it was based on.  These may be placed in the "History" section.
   You may omit a network location for a work that was published at
   least four years before the Document itself, or if the original
   publisher of the version it refers to gives permission.
K. For any section Entitled "Acknowledgements" or "Dedications",
   Preserve the Title of the section, and preserve in the section all
   the substance and tone of each of the contributor acknowledgements
   and/or dedications given therein.
L. Preserve all the Invariant Sections of the Document,
   unaltered in their text and in their titles.  Section numbers
   or the equivalent are not considered part of the section titles.
M. Delete any section Entitled "Endorsements".  Such a section
   may not be included in the Modified Version.
N. Do not retitle any existing section to be Entitled "Endorsements"
   or to conflict in title with any Invariant Section.
O. Preserve any Warranty Disclaimers.

If the Modified Version includes new front-matter sections or
appendices that qualify as Secondary Sections and contain no material
copied from the Document, you may at your option designate some or all
of these sections as invariant.  To do this, add their titles to the
list of Invariant Sections in the Modified Version's license notice.
These titles must be distinct from any other section titles.

You may add a section Entitled "Endorsements", provided it contains
nothing but endorsements of your Modified Version by various
parties--for example, statements of peer review or that the text has
been approved by an organization as the authoritative definition of a
standard.

You may add a passage of up to five words as a Front-Cover Text, and a
passage of up to 25 words as a Back-Cover Text, to the end of the list
of Cover Texts in the Modified Version.  Only one passage of
Front-Cover Text and one of Back-Cover Text may be added by (or
through arrangements made by) any one entity.  If the Document already
includes a cover text for the same cover, previously added by you or
by arrangement made by the same entity you are acting on behalf of,
you may not add another; but you may replace the old one, on explicit
permission from the previous publisher that added the old one.

The author(s) and publisher(s) of the Document do not by this License
give permission to use their names for publicity for or to assert or
imply endorsement of any Modified Version.


5. COMBINING DOCUMENTS

You may combine the Document with other documents released under this
License, under the terms defined in section 4 above for modified
versions, provided that you include in the combination all of the
Invariant Sections of all of the original documents, unmodified, and
list them all as Invariant Sections of your combined work in its
license notice, and that you preserve all their Warranty Disclaimers.

The combined work need only contain one copy of this License, and
multiple identical Invariant Sections may be replaced with a single
copy.  If there are multiple Invariant Sections with the same name but
different contents, make the title of each such section unique by
adding at the end of it, in parentheses, the name of the original
author or publisher of that section if known, or else a unique number.
Make the same adjustment to the section titles in the list of
Invariant Sections in the license notice of the combined work.

In the combination, you must combine any sections Entitled "History"
in the various original documents, forming one section Entitled
"History"; likewise combine any sections Entitled "Acknowledgements",
and any sections Entitled "Dedications".  You must delete all sections
Entitled "Endorsements".


6. COLLECTIONS OF DOCUMENTS

You may make a collection consisting of the Document and other documents
released under this License, and replace the individual copies of this
License in the various documents with a single copy that is included in
the collection, provided that you follow the rules of this License for
verbatim copying of each of the documents in all other respects.

You may extract a single document from such a collection, and distribute
it individually under this License, provided you insert a copy of this
License into the extracted document, and follow this License in all
other respects regarding verbatim copying of that document.


7. AGGREGATION WITH INDEPENDENT WORKS

A compilation of the Document or its derivatives with other separate
and independent documents or works, in or on a volume of a storage or
distribution medium, is called an "aggregate" if the copyright
resulting from the compilation is not used to limit the legal rights
of the compilation's users beyond what the individual works permit.
When the Document is included in an aggregate, this License does not
apply to the other works in the aggregate which are not themselves
derivative works of the Document.

If the Cover Text requirement of section 3 is applicable to these
copies of the Document, then if the Document is less than one half of
the entire aggregate, the Document's Cover Texts may be placed on
covers that bracket the Document within the aggregate, or the
electronic equivalent of covers if the Document is in electronic form.
Otherwise they must appear on printed covers that bracket the whole
aggregate.


8. TRANSLATION

Translation is considered a kind of modification, so you may
distribute translations of the Document under the terms of section 4.
Replacing Invariant Sections with translations requires special
permission from their copyright holders, but you may include
translations of some or all Invariant Sections in addition to the
original versions of these Invariant Sections.  You may include a
translation of this License, and all the license notices in the
Document, and any Warranty Disclaimers, provided that you also include
the original English version of this License and the original versions
of those notices and disclaimers.  In case of a disagreement between
the translation and the original version of this License or a notice
or disclaimer, the original version will prevail.

If a section in the Document is Entitled "Acknowledgements",
"Dedications", or "History", the requirement (section 4) to Preserve
its Title (section 1) will typically require changing the actual
title.


9. TERMINATION

You may not copy, modify, sublicense, or distribute the Document except
as expressly provided for under this License.  Any other attempt to
copy, modify, sublicense or distribute the Document is void, and will
automatically terminate your rights under this License.  However,
parties who have received copies, or rights, from you under this
License will not have their licenses terminated so long as such
parties remain in full compliance.


10. FUTURE REVISIONS OF THIS LICENSE

The Free Software Foundation may publish new, revised versions
of the GNU Free Documentation License from time to time.  Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.  See
http://www.gnu.org/copyleft/.

Each version of the License is given a distinguishing version number.
If the Document specifies that a particular numbered version of this
License "or any later version" applies to it, you have the option of
following the terms and conditions either of that specified version or
of any later version that has been published (not as a draft) by the
Free Software Foundation.  If the Document does not specify a version
number of this License, you may choose any version ever published (not
as a draft) by the Free Software Foundation.


ADDENDUM: How to use this License for your documents

To use this License in a document you have written, include a copy of
the License in the document and put the following copyright and
license notices just after the title page:

    Copyright (c)  YEAR  YOUR NAME.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.2
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

If you have Invariant Sections, Front-Cover Texts and Back-Cover Texts,
replace the "with...Texts." line with this:

    with the Invariant Sections being LIST THEIR TITLES, with the
    Front-Cover Texts being LIST, and with the Back-Cover Texts being LIST.

If you have Invariant Sections without Cover Texts, or some other
combination of the three, merge those two alternatives to suit the
situation.

If your document contains nontrivial examples of program code, we
recommend releasing these examples in parallel under your choice of
free software license, such as the GNU General Public License,
to permit their use in free software.
