VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "msadodc.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "msdatgrd.ocx"
Begin VB.Form frmDeleteRecords 
   Caption         =   "Patch Database"
   ClientHeight    =   4995
   ClientLeft      =   60
   ClientTop       =   465
   ClientWidth     =   7410
   LinkTopic       =   "Form2"
   ScaleHeight     =   4995
   ScaleWidth      =   7410
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstLog 
      Height          =   2010
      Left            =   4080
      TabIndex        =   2
      Top             =   2280
      Width           =   3135
   End
   Begin VB.CommandButton cmdDelSelected 
      Caption         =   "Patch Database"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   4440
      Width           =   1935
   End
   Begin MSDataGridLib.DataGrid dataCards 
      Bindings        =   "frmDeleteRecords.frx":0000
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   8281
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   0   'False
      DefColWidth     =   273
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc datCardNames 
      Height          =   735
      Left            =   1200
      Top             =   2280
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "ViewCollectionADODC"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblInstructions 
      AutoSize        =   -1  'True
      Caption         =   "Instructions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   5040
      TabIndex        =   5
      Top             =   240
      Width           =   1440
   End
   Begin VB.Label lblInfo2 
      AutoSize        =   -1  'True
      Caption         =   "To begin, press ""Patch Database""."
      Height          =   195
      Left            =   4440
      TabIndex        =   4
      Top             =   1920
      Width           =   2490
   End
   Begin VB.Label lblInfo 
      Caption         =   $"frmDeleteRecords.frx":001B
      Height          =   855
      Left            =   4200
      TabIndex        =   3
      Top             =   720
      Width           =   3015
   End
End
Attribute VB_Name = "frmDeleteRecords"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'    Project:  Watcher Database Record Deletion Patch
'    Created:  31 October 2007
'    Updated:  31 October 2007
'    Copyright 2007 Eric Cavaliere

'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.

'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.

'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Private Sub cmdDelSelected_Click()
'  This function will delete the record who's ID # is
'      specified in the next line from the database.
    datCardNames.Recordset.Filter = "ID = 3960"
    lstLog.AddItem ("Locating Record # 3960...")
    If datCardNames.Recordset.EOF = False Then
        lstLog.AddItem (" - Record Found.")
        datCardNames.Recordset.Delete
        datCardNames.Recordset.Update
        lstLog.AddItem (" - Record Deleted.")
    Else
        lstLog.AddItem (" - Record Not Found.")
    End If
End Sub

Private Sub Form_Load()
'  This code is called when the program first starts.
  On Error Resume Next
  
  ' connect to the cards.mdb file.
  datCardNames.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\cards.mdb;Persist Security Info=False"
  datCardNames.RecordSource = "cards"
  datCardNames.Refresh
    
  ' Hide every column other then the card title.
  dataCards.Columns("ID").Visible = False
  dataCards.Columns("Set").Visible = False
  dataCards.Columns("Number").Visible = False
  dataCards.Columns("File Name").Visible = False
  dataCards.Columns("Other").Visible = False
  dataCards.Columns("Text").Visible = False
  dataCards.Columns("Type").Visible = False
  dataCards.Columns("Rarity").Visible = False
  
  ' Set the default sort (by card title).
  datCardNames.Recordset.Sort = "Name"
End Sub

Private Sub Form_Unload(Cancel As Integer)
' Update the database before quiting.
  If datCardNames.Recordset.EOF = False Then
    If datCardNames.Recordset.BOF = False Then
      datCardNames.Recordset.Update
    End If
  End If
End Sub
